﻿
using UnityEngine;

public class ClientHandler : NetConnection
{
    public bool isConnect_ = false;

    public void handleClose()
    {
        isConnect_ = false;
        Closed();
    }

    public void shutdown()
    {

    }
    
    public void Update()
    {
        if (!isConnect_)
            return;

        if (clientSocket == null || !clientSocket.Connected)
        {
            Debug.Log("Update Client Closed.");
            Closed();
        }
        isConnect_ = false;
    }
}