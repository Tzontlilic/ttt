﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

public class ClientAcceptor
{
    //public List<ClientHandler> clientPool_ = new List<ClientHandler>;

    public Dictionary<string, ClientHandler> clientDict_ = new Dictionary<string, ClientHandler>();//存放套接字

    public int make_svc_handler(Socket socket, string ipstr)
    {
        if(clientDict_.ContainsKey(ipstr))
        {
            return 1;
        }

        ClientHandler ch = new ClientHandler();
        ch.handleConnection(socket, ipstr);
        ch.isConnect_ = true;

        clientDict_.Add(ipstr, ch);
        return 0;
    }

    public void Closed()
    {
        foreach (KeyValuePair<string, ClientHandler> item in clientDict_)
        {
            item.Value.Closed();
        }
        clientDict_.Clear();
    }
}
