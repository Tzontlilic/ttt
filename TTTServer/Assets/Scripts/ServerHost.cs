﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class ServerHost : MonoBehaviour
{

    protected static ServerHost instance_;

    public static ServerHost Inst
    {
        get
        {
            return instance_;
        }
    }

    Thread threadWatch = null; // 负责监听客户端连接请求的 线程；
    Socket socketWatch = null;    

    public Text IPText = null;
    public int ServerPort = 8000;

    //连接管理
    ClientAcceptor acceptor_;

    bool Closehost = false;
    bool needClose = false;

    void Start()
    {
        acceptor_ = new ClientAcceptor();
        InitServer();
    }

    public void Close()
    {
        if (Closehost)
            return;
        if(acceptor_!=null)
        {
            acceptor_.Closed();
        }
        //创建TCP连接类型的Socket 退出自己的现成
        Socket quitSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        IPAddress ipAdress = IPAddress.Parse(IPText.text);              //服务器IP
        IPEndPoint ipEndpoint = new IPEndPoint(ipAdress, ServerPort); //服务器端口
                                            
        IAsyncResult result = quitSocket.BeginConnect(ipEndpoint, null, quitSocket);
        //超时检测
        bool success = result.AsyncWaitHandle.WaitOne(5000, true);
        if (success)
        {
            Closehost = true;
            Debug.Log("Close Host!");
        }
    }

    public void InitServer()
    {
        //获取本地的IP地址
        string AddressIP = string.Empty;
        foreach (IPAddress _IPAddress in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
        {
            if (_IPAddress.AddressFamily.ToString() == "InterNetwork")
            {
                AddressIP = _IPAddress.ToString();
            }
        }
        //给IP控件赋值
        IPText.text = AddressIP;

        // 创建负责监听的套接字，注意其中的参数；
        socketWatch = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        // 获得文本框中的IP对象；
        IPAddress address = IPAddress.Parse(AddressIP);
        // 创建包含ip和端口号的网络节点对象；
        IPEndPoint endPoint = new IPEndPoint(address, ServerPort);
        try
        {
            // 将负责监听的套接字绑定到唯一的ip和端口上；
            socketWatch.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            socketWatch.Bind(endPoint);
        }
        catch (SocketException se)
        {
            Debug.LogError("异常：" + se.Message);
            return;
        }
        // 设置监听队列的长度；
        socketWatch.Listen(10000);
        // 创建负责监听的线程；
        threadWatch = new Thread(WatchConnecting);
        threadWatch.IsBackground = true;
        threadWatch.Start();
        ShowMsg("服务器启动监听成功！");
    }

    /// <summary>
    /// 监听客户端请求
    /// </summary>
    void WatchConnecting()
    {
        while (!Closehost)  // 持续不断的监听客户端的连接请求；
        {
            ShowMsg("Waiting For Client...");
            // 开始监听客户端连接请求，Accept方法会阻断当前的线程；
            Socket sokConnection = socketWatch.Accept(); // 一旦监听到一个客户端的请求，就返回一个与该客户端通信的 套接字；
            if(Closehost)
            {
                ShowMsg("needClose For WatchConnecting...");
                needClose = true;
                return;
            }
            var ssss = sokConnection.RemoteEndPoint.ToString().Split(':');
            ShowMsg("Client[" + ssss[0] + "] Connect...");
            // 将与客户端连接的 套接字 对象添加到集合中；
            acceptor_.make_svc_handler(sokConnection, ssss[0]);
        }
    }    

    public void DestroyClient(ClientHandler ch)
    {
        if(acceptor_.clientDict_.ContainsKey(ch.clientIP))
        {
            acceptor_.clientDict_.Remove(ch.clientIP);
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach(KeyValuePair<string, ClientHandler> item in acceptor_.clientDict_)
        {
            item.Value.Update();
        }
        if (needClose)
        {
            needClose = false;
            ShowMsg("Application.Quit()");
            Application.Quit();
        }
    }


    void ShowMsg(string str)
    {
        //if (!BPS_Help.ChangeByte(txtMsg.Text, 2000))
        //{
        //    txtMsg.Text = "";
        //    txtMsg.AppendText(str + "\r\n");
        //}
        //else
        //{
        //    txtMsg.AppendText(str + "\r\n");
        //}
        Debug.Log("[ServerHost]: " + str);
    }
}
