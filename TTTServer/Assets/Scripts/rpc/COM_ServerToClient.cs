using UnityEngine;
using System;
using System.Collections.Generic;
namespace ComRpc
{
	public class COM_ServerToClientStub
	{ 
		public const string  strFingerprint="5cdd5a110434f7ec7cef31bf55587b83";
		public enum InterfaceID{
			pong_Id,
			syncTimer_Id,
			loginOk_Id,
			startGame_Id,
			initRoleData_Id,
			recvChat_Id,
		}
		public IProtocol __P__ ;
		//construction
		public COM_ServerToClientStub(IProtocol p)
		{ 
			__P__=p; 
		} 
		//stub 
		public void pong()
		{
			Debug.Log("pong");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)0);
			__P__.writeMsgEnd();
		}
		public void syncTimer(uint t)
		{
			Debug.Log("syncTimer");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)1);
			__P__.writeUInt32(t);

			__P__.writeMsgEnd();
		}
		public void loginOk()
		{
			Debug.Log("loginOk");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)2);
			__P__.writeMsgEnd();
		}
		public void startGame(bool isCreate)
		{
			Debug.Log("startGame");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)3);
			__P__.writeBool(isCreate);

			__P__.writeMsgEnd();
		}
		public void initRoleData(COM_InitPlayerData data)
		{
			Debug.Log("initRoleData");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)4);
			data.serialize(__P__);

			__P__.writeMsgEnd();
		}
		public void recvChat(COM_ChatData chatData)
		{
			Debug.Log("recvChat");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)5);
			chatData.serialize(__P__);

			__P__.writeMsgEnd();
		}
	}//class
	public class COM_ServerToClientProxy
	{ 
		public const string strFingerprint="5cdd5a110434f7ec7cef31bf55587b83";
		public enum InterfaceID{
			pong_Id,
			syncTimer_Id,
			loginOk_Id,
			startGame_Id,
			initRoleData_Id,
			recvChat_Id,
		}
		public ICOM_ServerToClientProxy  __I__;
		//construction
		public COM_ServerToClientProxy(ICOM_ServerToClientProxy I)
		{ 
			__I__=I; 
		} 

		public bool dispatch(IProtocol __P__)
		{
			int id=__P__.readUInt16();
			switch (id)
			{
				case 0 :
				{
					return recv_pong(__P__);
				}
				case 1 :
				{
					return recv_syncTimer(__P__);
				}
				case 2 :
				{
					return recv_loginOk(__P__);
				}
				case 3 :
				{
					return recv_startGame(__P__);
				}
				case 4 :
				{
					return recv_initRoleData(__P__);
				}
				case 5 :
				{
					return recv_recvChat(__P__);
				}
				default:
				{
					return false;
				}
			}//switch
		}//dispatch

		//proxy 
		private bool recv_pong(IProtocol __P__)
		{
			Debug.Log("pong");
			return __I__.pong();
		}
		private bool recv_syncTimer(IProtocol __P__)
		{
			Debug.Log("syncTimer");
			uint t=0;
			t=__P__.readUInt32();

			return __I__.syncTimer(t);
		}
		private bool recv_loginOk(IProtocol __P__)
		{
			Debug.Log("loginOk");
			return __I__.loginOk();
		}
		private bool recv_startGame(IProtocol __P__)
		{
			Debug.Log("startGame");
			bool isCreate=false;
			isCreate=__P__.readBool() ;

			return __I__.startGame(isCreate);
		}
		private bool recv_initRoleData(IProtocol __P__)
		{
			Debug.Log("initRoleData");
			COM_InitPlayerData data= new COM_InitPlayerData();
			data.deSerialize(__P__);

			return __I__.initRoleData(data);
		}
		private bool recv_recvChat(IProtocol __P__)
		{
			Debug.Log("recvChat");
			COM_ChatData chatData= new COM_ChatData();
			chatData.deSerialize(__P__);

			return __I__.recvChat(chatData);
		}
	} //class
}//namespace
