using System;
using System.Collections.Generic;
namespace ComRpc
{
	public interface ICOM_ClientToServerProxy
	{ 
		//interface 
		bool ping();
		bool login(COM_LoginData loginData);
		bool logout();
		bool playerEnterGame();
		bool sendChat(ChatChannelType type,string content,long playerId,bool isDiamond);
		bool checkPlayerOnline(long playerId);
		bool addBlack(long playerId);
		bool delBlack(long playerId);
	} //class
} //namespace
