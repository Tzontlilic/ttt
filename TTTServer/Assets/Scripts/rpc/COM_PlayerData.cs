using UnityEngine;
using System;
using System.Collections.Generic;
namespace ComRpc
{
	public class COM_CharacterCProp
	{ 
		public const string strFingerprint="3176ad0dfcbc8ea8ea65adedd563efb1";
		public float base_ = 0.0f;
		public float basePer_ = 0.0f;
		public float baseDelta_ = 0.0f;
		public float per_ = 0.0f;
		public float delta_ = 0.0f;

		//serialize
		public bool  serialize(IProtocol __P__) 
		{ 
			__P__.writeFloat(base_);

			__P__.writeFloat(basePer_);

			__P__.writeFloat(baseDelta_);

			__P__.writeFloat(per_);

			__P__.writeFloat(delta_);

			return true;
		}//serialize 

		//deSerialize
		public bool deSerialize(IProtocol __P__)
		{ 
			base_=__P__.readFloat();

			basePer_=__P__.readFloat();

			baseDelta_=__P__.readFloat();

			per_=__P__.readFloat();

			delta_=__P__.readFloat();

			return true;
		}// deSerialize
	}//class

	public class COM_CharacterIProp
	{ 
		public const string strFingerprint="4be9c54383adfb98f8e6ea3d588730e6";
		public int curValue_ = 0;

		//serialize
		public bool  serialize(IProtocol __P__) 
		{ 
			__P__.writeInt32(curValue_);

			return true;
		}//serialize 

		//deSerialize
		public bool deSerialize(IProtocol __P__)
		{ 
			curValue_=__P__.readInt32();

			return true;
		}// deSerialize
	}//class

	public class COM_SyncCharacterIProp
	{ 
		public const string strFingerprint="f0b721299b24131f3bfe1f9c0ee03cf0";
		public byte id_ = 0;
		public IPropType type_ = 0;
		public COM_CharacterIProp prop_ =  new COM_CharacterIProp();

		//serialize
		public bool  serialize(IProtocol __P__) 
		{ 
			__P__.writeUInt8(id_);

			__P__.writeInt16((short)type_);

			prop_.serialize(__P__);

			return true;
		}//serialize 

		//deSerialize
		public bool deSerialize(IProtocol __P__)
		{ 
			id_=__P__.readUInt8();

			type_=(IPropType)__P__.readInt16();

			prop_.deSerialize(__P__);

			return true;
		}// deSerialize
	}//class

	public class COM_LoginData
	{ 
		public const string strFingerprint="4483ecf2799df395ae3852fcb7d30f1e";
		public string userOpenId_ = "";
		public string clientIP_ = "";
		public string deviceModel_ = "";
		public short serverId_ = 0;
		public string pf_ = "";
		public string via_ = "";
		public string country_ = "";
		public string province_ = "";
		public string city_ = "";
		public int time_ = 0;
		public int verMajor_ = 0;
		public int verMinor_ = 0;
		public int verPatch_ = 0;
		public string token_ = "";
		public string rpcClientToServer_ = "";
		public string rpcServerToClient_ = "";
		public string creative = "";

		//serialize
		public bool  serialize(IProtocol __P__) 
		{ 
			__P__.writeString(userOpenId_);

			__P__.writeString(clientIP_);

			__P__.writeString(deviceModel_);

			__P__.writeInt16(serverId_);

			__P__.writeString(pf_);

			__P__.writeString(via_);

			__P__.writeString(country_);

			__P__.writeString(province_);

			__P__.writeString(city_);

			__P__.writeInt32(time_);

			__P__.writeInt32(verMajor_);

			__P__.writeInt32(verMinor_);

			__P__.writeInt32(verPatch_);

			__P__.writeString(token_);

			__P__.writeString(rpcClientToServer_);

			__P__.writeString(rpcServerToClient_);

			__P__.writeString(creative);

			return true;
		}//serialize 

		//deSerialize
		public bool deSerialize(IProtocol __P__)
		{ 
			userOpenId_=__P__.readString();

			clientIP_=__P__.readString();

			deviceModel_=__P__.readString();

			serverId_=__P__.readInt16();

			pf_=__P__.readString();

			via_=__P__.readString();

			country_=__P__.readString();

			province_=__P__.readString();

			city_=__P__.readString();

			time_=__P__.readInt32();

			verMajor_=__P__.readInt32();

			verMinor_=__P__.readInt32();

			verPatch_=__P__.readInt32();

			token_=__P__.readString();

			rpcClientToServer_=__P__.readString();

			rpcServerToClient_=__P__.readString();

			creative=__P__.readString();

			return true;
		}// deSerialize
	}//class

	public class COM_InitPlayerData
	{ 
		public const string strFingerprint="3af3f0c0515e372648c08537d5580281";
		public long roleId_ = 0;
		public string roleName_ = "";
		public string accountName_ = "";
		public uint iconId_ = 0;
		public uint iconFrame_ = 0;
		public bool isSign = false;
		public bool canLuxurySign = false;
		public long offlineTime_ = 0;
		public long loginTime_ = 0;
		public long createTime_ = 0;

		//serialize
		public bool  serialize(IProtocol __P__) 
		{ 
			__P__.writeInt64(roleId_);

			__P__.writeString(roleName_);

			__P__.writeString(accountName_);

			__P__.writeUInt32(iconId_);

			__P__.writeUInt32(iconFrame_);

			__P__.writeBool(isSign);

			__P__.writeBool(canLuxurySign);

			__P__.writeInt64(offlineTime_);

			__P__.writeInt64(loginTime_);

			__P__.writeInt64(createTime_);

			return true;
		}//serialize 

		//deSerialize
		public bool deSerialize(IProtocol __P__)
		{ 
			roleId_=__P__.readInt64();

			roleName_=__P__.readString();

			accountName_=__P__.readString();

			iconId_=__P__.readUInt32();

			iconFrame_=__P__.readUInt32();

			isSign=__P__.readBool() ;

			canLuxurySign=__P__.readBool() ;

			offlineTime_=__P__.readInt64();

			loginTime_=__P__.readInt64();

			createTime_=__P__.readInt64();

			return true;
		}// deSerialize
	}//class

	public class COM_ChatData
	{ 
		public const string strFingerprint="f43c8b3ba3e5a09885c78558754f24a9";
		public ChatChannelType type_ = 0;
		public long playerId_ = 0;
		public string playerName_ = "";
		public short level_ = 0;
		public uint iconId_ = 0;
		public uint iconFrame_ = 0;
		public string content_ = "";
		public string srcGuild_ = "";
		public long targetId_ = 0;
		public string targetName_ = "";
		public long time_ = 0;

		//serialize
		public bool  serialize(IProtocol __P__) 
		{ 
			__P__.writeInt16((short)type_);

			__P__.writeInt64(playerId_);

			__P__.writeString(playerName_);

			__P__.writeInt16(level_);

			__P__.writeUInt32(iconId_);

			__P__.writeUInt32(iconFrame_);

			__P__.writeString(content_);

			__P__.writeString(srcGuild_);

			__P__.writeInt64(targetId_);

			__P__.writeString(targetName_);

			__P__.writeInt64(time_);

			return true;
		}//serialize 

		//deSerialize
		public bool deSerialize(IProtocol __P__)
		{ 
			type_=(ChatChannelType)__P__.readInt16();

			playerId_=__P__.readInt64();

			playerName_=__P__.readString();

			level_=__P__.readInt16();

			iconId_=__P__.readUInt32();

			iconFrame_=__P__.readUInt32();

			content_=__P__.readString();

			srcGuild_=__P__.readString();

			targetId_=__P__.readInt64();

			targetName_=__P__.readString();

			time_=__P__.readInt64();

			return true;
		}// deSerialize
	}//class

}//namespace
