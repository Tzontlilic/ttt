using UnityEngine;
using System;
using System.Collections.Generic;
namespace ComRpc
{
	public enum PropOPType
	{
		PT_B,
		PT_BP,
		PT_BD,
		PT_P,
		PT_D,
	}

	public enum CPropType
	{
		CPT_NONE,
		CPT_STR,
		CPT_DEX,
		CPT_INT,
		CPT_MAXHP,
		CPT_MAXXP,
		CPT_GENXP,
		CPT_PATK,
		CPT_MATK,
		CPT_PDEF,
		CPT_MDEF,
		CPT_PCRL,
		CPT_MCRL,
		CPT_HPHF,
		CPT_GENHF,
		CPT_AVDL,
		CPT_HITL,
		CPT_RPDEF,
		CPT_RMDEF,
		CPT_ATK2HPR,
		CPT_AHL,
		CPT_RSIL,
		CPT_RGEN,
		CPT_SPEED,
		CPT_RSPEED,
		CPT_FAR,
		CPT_RFAR,
		CPT_FCR,
		CPT_RFCR,
		CPT_FHR,
		CPT_RFHR,
		CPT_DBPR,
		CPT_DAM2HPR,
		CPT_DBMR,
		CPT_MAX,
	}

	public enum IPropType
	{
		IPT_None,
		IPT_Gold,
		IPT_Diamond,
		IPT_Money,
		IPT_VipLevel,
		IPT_VipExp,
		IPT_GuildId,
		IPT_MAX,
	}

	public enum NoticeMsg
	{
		NM_None,
		NM_OK,
		NM_NofindPlayer,
		NM_YourAreInBlack,
		NM_AlreadyInBlack,
		NM_NotOnline,
		NM_MAX,
	}

	public enum ChatChannelType
	{
		CCT_NONE,
		CCT_World,
		CCT_Guild,
		CCT_Private,
		CCT_PrivateHistory,
		CCT_System,
		CCT_GM,
		CCT_Max,
	}

}//namespace
