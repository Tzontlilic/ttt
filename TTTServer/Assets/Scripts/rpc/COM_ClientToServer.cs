using UnityEngine;
using System;
using System.Collections.Generic;
namespace ComRpc
{
	public class COM_ClientToServerStub
	{ 
		public const string  strFingerprint="11924ad46a6da32f200f73de83fcb447";
		public enum InterfaceID{
			ping_Id,
			login_Id,
			logout_Id,
			playerEnterGame_Id,
			sendChat_Id,
			checkPlayerOnline_Id,
			addBlack_Id,
			delBlack_Id,
		}
		public IProtocol __P__ ;
		//construction
		public COM_ClientToServerStub(IProtocol p)
		{ 
			__P__=p; 
		} 
		//stub 
		public void ping()
		{
			Debug.Log("ping");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)0);
			__P__.writeMsgEnd();
		}
		public void login(COM_LoginData loginData)
		{
			Debug.Log("login");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)1);
			loginData.serialize(__P__);

			__P__.writeMsgEnd();
		}
		public void logout()
		{
			Debug.Log("logout");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)2);
			__P__.writeMsgEnd();
		}
		public void playerEnterGame()
		{
			Debug.Log("playerEnterGame");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)3);
			__P__.writeMsgEnd();
		}
		public void sendChat(ChatChannelType type,string content,long playerId,bool isDiamond)
		{
			Debug.Log("sendChat");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)4);
			__P__.writeInt16((short)type);

			__P__.writeString(content);

			__P__.writeInt64(playerId);

			__P__.writeBool(isDiamond);

			__P__.writeMsgEnd();
		}
		public void checkPlayerOnline(long playerId)
		{
			Debug.Log("checkPlayerOnline");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)5);
			__P__.writeInt64(playerId);

			__P__.writeMsgEnd();
		}
		public void addBlack(long playerId)
		{
			Debug.Log("addBlack");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)6);
			__P__.writeInt64(playerId);

			__P__.writeMsgEnd();
		}
		public void delBlack(long playerId)
		{
			Debug.Log("delBlack");
			__P__.writeMsgBegin();
			__P__.writeUInt16((ushort)7);
			__P__.writeInt64(playerId);

			__P__.writeMsgEnd();
		}
	}//class
	public class COM_ClientToServerProxy
	{ 
		public const string strFingerprint="11924ad46a6da32f200f73de83fcb447";
		public enum InterfaceID{
			ping_Id,
			login_Id,
			logout_Id,
			playerEnterGame_Id,
			sendChat_Id,
			checkPlayerOnline_Id,
			addBlack_Id,
			delBlack_Id,
		}
		public ICOM_ClientToServerProxy  __I__;
		//construction
		public COM_ClientToServerProxy(ICOM_ClientToServerProxy I)
		{ 
			__I__=I; 
		} 

		public bool dispatch(IProtocol __P__)
		{
			int id=__P__.readUInt16();
			switch (id)
			{
				case 0 :
				{
					return recv_ping(__P__);
				}
				case 1 :
				{
					return recv_login(__P__);
				}
				case 2 :
				{
					return recv_logout(__P__);
				}
				case 3 :
				{
					return recv_playerEnterGame(__P__);
				}
				case 4 :
				{
					return recv_sendChat(__P__);
				}
				case 5 :
				{
					return recv_checkPlayerOnline(__P__);
				}
				case 6 :
				{
					return recv_addBlack(__P__);
				}
				case 7 :
				{
					return recv_delBlack(__P__);
				}
				default:
				{
					return false;
				}
			}//switch
		}//dispatch

		//proxy 
		private bool recv_ping(IProtocol __P__)
		{
			Debug.Log("ping");
			return __I__.ping();
		}
		private bool recv_login(IProtocol __P__)
		{
			Debug.Log("login");
			COM_LoginData loginData= new COM_LoginData();
			loginData.deSerialize(__P__);

			return __I__.login(loginData);
		}
		private bool recv_logout(IProtocol __P__)
		{
			Debug.Log("logout");
			return __I__.logout();
		}
		private bool recv_playerEnterGame(IProtocol __P__)
		{
			Debug.Log("playerEnterGame");
			return __I__.playerEnterGame();
		}
		private bool recv_sendChat(IProtocol __P__)
		{
			Debug.Log("sendChat");
			ChatChannelType type=0;
			type=(ChatChannelType)__P__.readInt16();

			string content="";
			content=__P__.readString();

			long playerId=0;
			playerId=__P__.readInt64();

			bool isDiamond=false;
			isDiamond=__P__.readBool() ;

			return __I__.sendChat(type,content,playerId,isDiamond);
		}
		private bool recv_checkPlayerOnline(IProtocol __P__)
		{
			Debug.Log("checkPlayerOnline");
			long playerId=0;
			playerId=__P__.readInt64();

			return __I__.checkPlayerOnline(playerId);
		}
		private bool recv_addBlack(IProtocol __P__)
		{
			Debug.Log("addBlack");
			long playerId=0;
			playerId=__P__.readInt64();

			return __I__.addBlack(playerId);
		}
		private bool recv_delBlack(IProtocol __P__)
		{
			Debug.Log("delBlack");
			long playerId=0;
			playerId=__P__.readInt64();

			return __I__.delBlack(playerId);
		}
	} //class
}//namespace
