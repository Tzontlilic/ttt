using System;
using System.Collections.Generic;
namespace ComRpc
{
	public interface ICOM_ServerToClientProxy
	{ 
		//interface 
		bool pong();
		bool syncTimer(uint t);
		bool loginOk();
		bool startGame(bool isCreate);
		bool initRoleData(COM_InitPlayerData data);
		bool recvChat(COM_ChatData chatData);
	} //class
} //namespace
