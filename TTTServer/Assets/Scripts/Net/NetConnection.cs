﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using UnityEngine;
using ComRpc;
using System.Threading;

public class NetConnection : Protocol, ICOM_ClientToServerProxy
{
    public enum connState : uint
    {
        waitConnect,
        connectSuccesss,
        connectFail,
        connected,
        disconnected
    }

    public connState curState = connState.disconnected;
    public COM_ClientToServerProxy mProxy;
    public COM_ServerToClientStub mStub;

    //--------------------------------------- 私有变量 --------------------------------------
    protected Socket clientSocket;
    public string clientIP;
    Thread sokThread = null;

    private List<byte[]> sendBufferList;

    private int realSendPos = 0;
    private int lastPos = 0;
    private bool insertPre = false;

    bool needClose = false;

    public void handleConnection(Socket socket, string ip)
    {
        recvBuffer = new byte[64 * 1024];
        sendBuffer = new byte[64 * 1024];
        sendBufferList = new List<byte[]>();

        clientSocket = socket;
        clientIP = ip;
        sokThread = new Thread(Receive);
        sokThread.IsBackground = true;
        sokThread.Start();
        mStub = new COM_ServerToClientStub(this);
        mProxy = new COM_ClientToServerProxy(this);
        Debug.Log("handleConnection");
        curState = connState.connectSuccesss;
    }

    //--------------------------------------- 接收数据包 --------------------------------------
    #region Receive package
    public void Receive()
    {
        while(!needClose)
        {
            if (!clientSocket.Connected)
            {
                Debug.Log("Receive Thread Closed.");
                Closed();
                return;
            }
            try
            {
                Debug.Log("Client Socket(" + clientIP + ") begin Receive...");
                //SocketError errorCode = new SocketError();
                //int recvSize = clientSocket.Available > 0 ? clientSocket.Available : 1;
                //int bytesAvailable = clientSocket.Receive(recvBuffer, lastPos, recvSize, SocketFlags.None, out errorCode);
                //if (errorCode == SocketError.WouldBlock)
                //    continue;
                int bytesAvailable = clientSocket.Receive(recvBuffer);
                if (bytesAvailable == 0)
                {
                    Debug.Log("Receive ============ 0.");
                    Closed();
                    return;
                }
                Debug.Log("Receive ============ " + bytesAvailable.ToString() );
                if (bytesAvailable > 0)
                {
                    lastPos += bytesAvailable;
                    while (lastPos >= 2)
                    {
                        short msgLen = (short)readInt16();
                        Debug.Log("bytesAvailable: " + bytesAvailable + " , msgLen: " + msgLen);
                        //消息是否完整，完整则处理，不完整，将buffer数据靠前紧缩，等候下次update
                        if (lastPos - recvPos >= msgLen)
                        {
                            //收到客户端的消息
                            mProxy.dispatch(this);  //拆包
                            if (lastPos > recvPos)
                            {
                                lastPos = Crunch(recvBuffer, recvPos, lastPos - recvPos);
                                recvPos = 0;
                            }
                            else
                            {
                                lastPos = 0;
                                recvPos = 0;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    recvPos = 0;
                }
            }
            catch (SocketException e)
            {
                curState = connState.disconnected;
                Debug.Log("Socket 接收错误：" + e);
            }
            catch (ObjectDisposedException e)
            {
                curState = connState.disconnected;
                Debug.Log("Socket 已关闭" + e);
            }
        }
    }
    #endregion
    //--------------------------------------- 接收数据包 --------------------------------------

    //--------------------------------------- 发送数据包 --------------------------------------
    #region send package
    public void Send()
    {
        Debug.Log("Send");
        if (sendBufferList.Count == 0)
            return;
        if (clientSocket == null || !clientSocket.Connected)
        {
            if(clientSocket.Connected == false)
                Debug.Log("Send error.");
            Closed();
            return;
        }
        try
        {
            if (sendBufferList[0] == null) return;
            Debug.Log("------begin send------ " + "");
            SocketError errorCode = new SocketError();
            int finishSend = 0;
            finishSend = clientSocket.Send(sendBufferList[0], realSendPos, sendBufferList[0].Length - realSendPos, SocketFlags.None, out errorCode);
            Debug.Log("errorCode = " + errorCode.ToString());
            //消息没发完，将buffer紧缩至开头，等候下次uptate再发     
            realSendPos += finishSend;
            Debug.Log("realSendPos : " + realSendPos + "/" + sendBufferList[0].Length);
            if (realSendPos >= sendBufferList[0].Length)
            {
                realSendPos = 0;
                sendBufferList.RemoveAt(0);
                //NetHelper.Singleton.receiveFin = false;
            }
        }
        catch (SocketException e)
        {
            Closed();
            Debug.Log("Socket 发送错误：" + e);
        }
        catch (ObjectDisposedException e)
        {
            Closed();
            Debug.Log("Socket 已关闭" + e);
        }
        catch (ArgumentNullException e)
        {
            Closed();
            Debug.Log("网络异常" + e);
        }
    }
    public void resetRealSendPos()
    {
        realSendPos = 0;
    }
    public void setInsertPre(bool value)
    {
        insertPre = value;
    }
    public int getSendListSize()
    {
        return sendBufferList.Count;
    }
    public int getFirstMsgId()
    {
        if (sendBufferList.Count > 0 && (sendBufferList[0] != null))
        {
            short msgId = BitConverter.ToInt16(sendBufferList[0], 2);
            return (int)msgId;
        }
        return -1;
    }
    public bool hasValidMsg()
    {
        //foreach (byte[] msg in sendBufferList)
        //{
        //    if (msg != null)
        //    {
        //        short msgId = BitConverter.ToInt16(msg, 2);
        //        if ((msgId != (short)COM_ClientToServerStub.InterfaceID.ping_Id) &&
        //            (msgId != (short)COM_ClientToServerStub.InterfaceID.syncCD_Id) &&
        //            (msgId != (short)COM_ClientToServerStub.InterfaceID.clientLog_Id))
        //            return true;
        //    }
        //}
        return true;
    }
    public void clearLoginMsg()
    {
        //int i = 0;
        //while (i < sendBufferList.Count)
        //{
        //    if (sendBufferList[i] == null)
        //        sendBufferList.RemoveAt(i);
        //    else
        //    {
        //        short msgId = BitConverter.ToInt16(sendBufferList[0], 2);
        //        if (msgId == (short)COM_ClientToServerStub.InterfaceID.login_Id)
        //            sendBufferList.RemoveAt(i);
        //        else
        //            i++;
        //    }
        //}
    }
    //--------------------------------------- 向服务器发送数据包 --------------------------------------
    #endregion



    //--------------------------------------- 断开连接 --------------------------------------
    #region Close socket
    public void Closed()
    {
        needClose = true;
        if (clientSocket != null && clientSocket.Connected)
        {
            clientSocket.Close();
        }
        clientSocket = null;
        curState = connState.disconnected;
        if(ServerHost.Inst != null)
            ServerHost.Inst.DestroyClient(this as ClientHandler);
    }
    #endregion
    //--------------------------------------- 断开连接 --------------------------------------


    //--------------------------------------- 写缓冲的接口实现 --------------------------------------
    #region Iprotocol implimention

    public override void writeMsgBegin()
    {
        Debug.Log("writeMsgBegin");
        if (curState == connState.disconnected)
        {
            Closed();
        }
        sendPos = 0;
    }

    public override void writeMsgEnd()
    {
        //把消息长度写进去
        byte[] msg = new byte[sendPos + 2];
        byte[] lenByte = BitConverter.GetBytes((short)sendPos);
        Array.Copy(lenByte, 0, msg, 0, 2);
        Array.Copy(sendBuffer, 0, msg, 2, sendPos);
        if (insertPre)
        {
            sendBufferList.Insert(0, null);
            sendBufferList.Insert(0, msg);
        }
        else
        {
            sendBufferList.Add(msg);
        }
        Debug.Log("writeMsgEnd");
    }
    #endregion
    //--------------------------------------- 写缓冲的接口实现--------------------------------------


    //--------------------------------------- 工具杂项--------------------------------------
    #region Miscellaneous
    //向前紧缩数组
    //把从@position开始,@length长度的数据移到数组的起始位置
    public int Crunch(byte[] mByte, int position, int length)
    {
        if (position == 0) return 0;
        Array.Clear(mByte, 0, position);
        int i = 0, j = position;
        while (length > 0)
        {
            mByte[i] = mByte[j];
            mByte[j] = 0;
            ++i; ++j;
            --length;
        }
        return i;
    }
    private void clearSendNull()
    {
        int i = 0;
        while (i < sendBufferList.Count)
        {
            if (sendBufferList[i] == null)
                sendBufferList.RemoveAt(i);
            else
                i++;
        }
    }

    #endregion
    //--------------------------------------- 工具杂项--------------------------------------


    bool ICOM_ClientToServerProxy.ping()
    {
        mStub.pong();
        Send();
        return true;
    }

    public bool login(COM_LoginData loginData)
    {
        mStub.loginOk();
        Send();
        return true;
    }

    public bool logout()
    {
        return true;
    }

    public bool playerEnterGame()
    {
        return true;
    }

    public bool sendChat(ChatChannelType type, string content, long playerId, bool isDiamond)
    {
        return true;
    }

    public bool checkPlayerOnline(long playerId)
    {
        return true;
    }

    public bool addBlack(long playerId)
    {
        return true;
    }

    public bool delBlack(long playerId)
    {
        return true;
    }
}
