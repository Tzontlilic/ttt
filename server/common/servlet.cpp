#include "servlet.h"
#include "network.h"

void Servlet::send(ISerial* servlet, evpp::TCPConn* conn) {
	the_network->send(servlet, conn);
}

void Servlet::send(ISerial* servlet, uint32_t seq, evpp::TCPConn* conn) {
	the_network->send(servlet, seq, conn);
}

void Servlet::call(std::shared_ptr<Servlet> servlet) {
	servlet->conn = this->conn;
	servlet->seq_ = this->seq_;
	the_network->call(servlet);
}

std::unordered_map<uint16_t, Servlet*> ServletMgr::servletmap_;
ServletMgr::~ServletMgr() {
	for (auto it = servletmap_.begin(); it != servletmap_.end(); ++it) {
		delete it->second;
	}
}

int ServletMgr::execute(const evpp::TCPConnPtr& conn, uint16_t msgid, uint32_t seq, evpp::Buffer* msg) {
	auto it = servletmap_.find(msgid);
	if (it == servletmap_.end()) {
		LOG_ERROR << "message[" << msgid << "] no register!";
		return -1;
	}
	auto sv = it->second->clone();//多线程，不能直接使用全局对象
	sv->conn = conn;

	sv->unserial_request(msg);
	sv->seq(seq);
	int r = sv->execute();
	if (r < 0) { return -1; }
	if (sv->get_response() && !sv->need_rpc) {
		evpp::Buffer sb;
		sb.AppendInt32(seq);
		sb.AppendInt16(msgid);
		sv->serial_response(&sb);
		if (sb.length() > 51200) {
			LOG_ERROR << "too long message!";
			return -1;
		}
		else {
			sb.PrependUint16((uint16_t)sb.length() + 2);
			conn->Send(&sb);
		}
	}
	return 0;
}