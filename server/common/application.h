#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include "network.h"
#include "script.h"

class Application
{
public:
	Application(int sid);
	~Application();
	evpp::EventLoop* loop() const {
		return mainloop_.get();
	}
	bool init();
	void run();
	int id() const {
		return id_;
	}
	int ap() const {
		return ap_;
	}
	void ap(int x) {
		ap_ = x;
	}

private:
    std::shared_ptr<Script> script_;
    std::shared_ptr<evpp::EventLoop> mainloop_;
	int id_;
	int ap_;
};

extern std::unique_ptr<Application> the_app;

#endif