#ifndef __GX_SCRIPT_H__
#define __GX_SCRIPT_H__

#include <stack>
#include <vector>
#include <map>
#include <list>
#include <cinttypes>
#include <memory>
#include <assert.h>
#include "lua/lua.hpp"
#include "path.h"
#include "data.h"
#include "tuple_apply.h"

#if(LUA_VERSION_NUM < 502)
lua_Integer lua_tointegerx(lua_State *L, int index, int *isnum);
lua_Number lua_tonumberx(lua_State *L, int index, int *isnum);
#endif

enum class ScriptFileType {
    LUA,
    CSV,
    UNKNOWN,
};

class ScriptTable;
class ScriptVariant;
class ScriptResult;
class Script;

/* ScriptValue */
template <typename _T>
struct ScriptValue;

template <>
struct ScriptValue<int8_t> {
    typedef int8_t type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushinteger(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tointegerx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<uint8_t> {
    typedef uint8_t type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushinteger(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tointegerx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<int16_t> {
    typedef int16_t type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushinteger(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tointegerx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<uint16_t> {
    typedef uint16_t type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushinteger(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tointegerx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<int32_t> {
    typedef int32_t type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushinteger(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tointegerx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<uint32_t> {
    typedef uint32_t type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushinteger(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tointegerx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<int64_t> {
    typedef int64_t type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushinteger(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tointegerx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<uint64_t> {
    typedef uint64_t type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushinteger(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tointegerx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<float> {
    typedef float type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushnumber(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tonumberx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<double> {
    typedef double type;
    static const type default_value() {
        return type();
    }
    static int push(lua_State *L, type value) {
        lua_pushnumber(L, value);
        return 1;
    }
    static type pop(lua_State *L, int index) {
        int isnum;
        type value = (type)lua_tonumberx(L, index, &isnum);
        return isnum ? value : default_value();
    }
};

template <>
struct ScriptValue<std::string> {
    static const char *default_value() {
        return "";
    }
    static int push(lua_State *L, const char *value) {
        lua_pushstring(L, value);
        return 1;
    }
    static const char *pop(lua_State *L, int index) {
        const char *value = lua_tostring(L, index);
        return value ? value : default_value();
    }
};

template <>
struct ScriptValue<const char*> {
    static const char *default_value() {
        return "";
    }
    static int push(lua_State *L, const char *value) {
        if (value) {
            lua_pushstring(L, value);
        }
        else {
            lua_pushnil(L);
        }
        return 1;
    }
    static const char *pop(lua_State *L, int index) {
        const char *value = lua_tostring(L, index);
        return value ? value : default_value();
    }
};

template <>
struct ScriptValue<char*> : public ScriptValue<const char*> { };

template <>
struct ScriptValue<void> {
    static int push(lua_State *L) {
        return 0;
    }
    static void pop(lua_State *L, int index) {
    }
};

template <typename ..._Args>
struct ScriptValue<std::tuple<_Args...>> {
    typedef std::tuple<_Args...> type;
    static const char *default_value() {
        return type();
    }
    static void push(lua_State *L, const type &value) {
        push_tuple<0>(L, value);
        return sizeof...(_Args);
    }
    static type pop(lua_State *L, int index) {
        return type();
    }
private:
    template <unsigned __index>
    static void push_tuple(lua_State *L, const type &t) {
        if (__index >= sizeof...(_Args)) {
            return;
        }
        ScriptValue<typename std::tuple_element<__index, type>::type>::push(L, std::get<__index>(t));
        push_tuple<__index>(L, t);
    }
};

/* ScriptFunction */
template<typename _T, typename _Signature>
class ScriptFunction;

template<typename _T, typename _R, typename... _Args>
class ScriptFunction<_T, _R(_Args...)> {
private:
    typedef std::tuple<_Args...> tuple_type;

    template <unsigned __index>
    static typename std::enable_if<
        __index >= sizeof...(_Args),
        void>::type
    pop(lua_State *L, int top, tuple_type &params) {
    }

    template <unsigned __index>
    static typename std::enable_if<
        __index < sizeof...(_Args),
        void>::type
    pop(lua_State *L, int top, tuple_type &params) {
        if (__index > (unsigned)top) {
            std::get<__index>(params) = ScriptValue<typename std::tuple_element<__index, tuple_type>::type>::default_value();
        }
        else {
            std::get<__index>(params) = ScriptValue<typename std::tuple_element<__index, tuple_type>::type>::pop(L, __index + 1);
        }
        pop<__index + 1>(L, top, params);
    }

public:
    virtual _R operator()(_Args...args) = 0;

    template <typename _Res>
    static typename std::enable_if<
        !std::is_void<_Res>::value,
        int>::type
    execute(lua_State *L, ScriptFunction *instance, _Args...args) {
        return ScriptValue<_R>::push(L, (*instance)(std::forward<_Args>(args)...));
    }
    template <typename _Res>
    static typename std::enable_if<
        std::is_void<_Res>::value,
        int>::type
    execute(lua_State *L, ScriptFunction *instance, _Args...args) {
        (*instance)(std::forward<_Args>(args)...);
        return 0;
    }

    static int wrapper(lua_State *L) {
        static _T instance;
        int top = lua_gettop(L);
        tuple_type params;
        pop<0>(L, top, params);
        return tuple_apply<true>(execute<_R>, params, L, &instance);
    }
};

/* ScriptFunctionManager */
class ScriptFunctionManager {
public:
	static ScriptFunctionManager *instance() {
		static ScriptFunctionManager *obj = nullptr;
		if (!obj) {
			obj = new ScriptFunctionManager();
		}
		return obj;
	}

    bool registerFunction(const char *name, int(*func)(lua_State*)) {
        return _map.emplace(name, func).second;
    }

    void upload(Script *script) ;
private:
	ScriptFunctionManager(){}
    std::map<std::string, int(*)(lua_State*)> _map;
};

/* ScriptFunctionRegister */
template <typename _T>
struct ScriptFunctionRegister {
    ScriptFunctionRegister(const char *name) {
        ScriptFunctionManager::instance()->registerFunction(name, _T::wrapper);
    }
};
#define GX_SCRIPT_REG_FUNC(name, T) static ScriptFunctionRegister<T> __script_func_##T(name)

/* ScriptException */
class ScriptException {
public:
    ScriptException(const char *name, const char *msg) 
    : _name(name), _msg(msg) { }

    const char *name() const {
        return _name.c_str();
    }
    const char *message() const {
        return _msg.c_str();
    }
private:
    std::string _name;
    std::string _msg;
};
/* ScriptInvoker */
template<typename _Signature>
struct ScriptInvoker;

template<typename _R, typename... _Args>
struct ScriptInvoker<_R(_Args...)> {
    static int push(lua_State *L) {
        return 0;
    }

    template <typename _T, typename ..._Params>
    static int push(lua_State *L, _T &&param, _Params&&...params) {
        return ScriptValue<_T>::push(L, std::forward<_T>(param)) + push(L, std::forward<_Params>(params)...);
    }


    template <typename _T>
    static typename std::enable_if<
        !std::is_void<_T>::value,
        _T>::type
    invoke_(lua_State *L, const char *name, _Args...args) {
        int base = lua_gettop(L);
        lua_getglobal(L, name);
        int argc = push(L, std::forward<_Args>(args)...);
        if (lua_pcall(L, argc, LUA_MULTRET, 0)) {
            const char *error = lua_tostring(L, -1);
            lua_pop(L, 1);
            throw ScriptException(name, error);
        }
        int top = lua_gettop(L);
        lua_pop(L, top - base);
        if (top > base) {
            _T r = ScriptValue<_R>::pop(L, base);
            lua_pop(L, top - base);
            return r;
        }
        else {
            _T r = ScriptValue<_R>::default_value();
            lua_pop(L, top - base);
            return r;
        }
    }

    template <typename _T>
    static typename std::enable_if<
        std::is_void<_T>::value,
        _T>::type
    invoke_(lua_State *L, const char *name, _Args...args) {
        int base = lua_gettop(L);
        lua_getglobal(L, name);
        int argc = push(L, std::forward<_Args>(args)...);
        if (lua_pcall(L, argc, LUA_MULTRET, 0)) {
            const char *error = lua_tostring(L, -1);
            lua_pop(L, 1);
            throw ScriptException(name, error);
        }
        int top = lua_gettop(L);
        lua_pop(L, top - base);
    }

    static _R invoke(lua_State *L, const char *name, _Args...args) {
        return invoke_<_R>(L, name, std::forward<_Args>(args)...);
    }
};

/* Script */
class Script : public std::enable_shared_from_this<Script> {
public:
    Script() ;
    ~Script() ;

    operator lua_State*() {
        return _lua;
    }

    int load(const Path &filename, ScriptFileType type = ScriptFileType::UNKNOWN) ;

    int64_t read_integer(const char *name, int64_t default_value = 0) ;
    double read_number(const char *name, double default_value = 0) ;
    std::string read_string(const char *name, const char *default_value = nullptr) ;
    std::shared_ptr<ScriptTable> read_table(const char *name) ;

    ScriptResult call(const char *func, const std::vector<ScriptVariant> &params) ;

    void read_variant(int index, ScriptVariant &var) ;
    std::shared_ptr<ScriptVariant> read_variant(int index) ;

    template <typename _R, typename ..._Args>
    _R invoke(const char *func, _Args...args) {
        ScriptInvoker<_R(_Args...)>::invoke(*this, func, std::forward<_Args>(args)...);
    }

    static Script *get_script(lua_State *L) ;
private:
    static std::map<intptr_t, Script*> &get_map() {
        static std::map<intptr_t, Script*> map;
        return map;
    }
    void reload(Path path, const Path&);
    int load2(const Path &path, ScriptFileType type) ;
    int sys_load(const Path &filename, ScriptFileType type) ;
    int load_lua(const Path &path, const Data *data) ;
    int load_csv(const Path &path, const Data *data) ;
protected:
    lua_State *_lua;
    std::list<Path> _stack;
    Path _root;
};

class ScriptTableIterator;

class ScriptTable : public std::enable_shared_from_this<ScriptTable> {
    friend class Script;
    friend class ScriptVariant;
    friend class ScriptTableFetcher;
public:
    typedef ScriptTableIterator iterator;
public:
    ScriptTable() ;
    ScriptTable(std::shared_ptr<Script> script, int index) ;
    ~ScriptTable() ;

    int64_t read_integer(const char *name, int64_t default_value = 0) ;
    int64_t read_integer(unsigned index, int64_t default_value = 0) ;
    double read_number(const char *name, double default_value = 0) ;
    double read_number(unsigned index, double default_value = 0) ;
    std::string read_string(const char *name, const char *default_value = nullptr) ;
    std::string read_string(unsigned index, const char *default_value = nullptr) ;
    std::shared_ptr<ScriptTable> read_table(const char *name) ;
    std::shared_ptr<ScriptTable> read_table(unsigned index) ;
    std::shared_ptr<ScriptVariant> read(const char *name) ;
    std::shared_ptr<ScriptVariant> read(unsigned index) ;

    bool is_nil() const {
        return _script == nullptr;
    }
    std::shared_ptr<Script> script() const {
        return _script;
    }
    size_t size() const ;
    iterator begin();
    iterator end();

private:
    void push_stack() const ;
protected:
    std::shared_ptr<Script> _script;
};

enum class ScriptVariableType {
    INTEGER,
    NUMBER,
    STRING,
    TABLE,
    NIL,
};

class ScriptVariant {
    friend class Script;
public:
    ScriptVariant() 
    : _type(ScriptVariableType::NIL), _vint(0)
    { }
    ScriptVariant(const ScriptVariant &x) : _type(ScriptVariableType::NIL) {
        assign(x);
    }
    ScriptVariant(int64_t x) 
    : _type(ScriptVariableType::INTEGER),
      _vint(x)
    { }
    explicit ScriptVariant(double x) 
    : _type(ScriptVariableType::NUMBER),
      _vnum(x)
    { }
    explicit ScriptVariant(const char *x) 
    : _type(ScriptVariableType::STRING),
      _vstr(x ? x : "")
    { }
    ScriptVariant(const std::string &x) 
    : _type(ScriptVariableType::STRING),
      _vstr(x)
    { }
    ScriptVariant(const std::string &&x) 
    : _type(ScriptVariableType::STRING),
      _vstr(std::move(x))
    { }
    ScriptVariant(std::shared_ptr<ScriptTable> x) 
    : _type(ScriptVariableType::TABLE),
      _vtab(std::move(x))
    { }
    ScriptVariableType type() const {
        return _type;
    }
    bool is_arithmetic() const {
        return _type == ScriptVariableType::INTEGER || _type == ScriptVariableType::INTEGER;
    }
    bool is_string() const {
        return _type == ScriptVariableType::STRING;
    }
    bool is_table() const {
        return _type == ScriptVariableType::TABLE;
    }
    bool is_nil() const {
        return _type == ScriptVariableType::NIL;
    }
    void clear() {
        switch (_type) {
        case ScriptVariableType::INTEGER:
        case ScriptVariableType::NUMBER:
            _vint = 0;
            break;
        case ScriptVariableType::STRING:
            _vstr.clear();
            break;
        case ScriptVariableType::TABLE:
            _vtab = nullptr;
            break;
        default:
            return;
        }
        _type = ScriptVariableType::NIL;
    }
    int64_t integer() const {
        switch (_type) {
        case ScriptVariableType::INTEGER:
            return _vint;
        case ScriptVariableType::NUMBER:
            return (int64_t)_vnum;
        case ScriptVariableType::STRING:
            return std::strtol(_vstr.c_str(), nullptr, 10);
        case ScriptVariableType::TABLE:
            assert(0);
            break;
        default:
            return 0;
        }
    }
    void assign(int64_t value) {
        switch (_type) {
        case ScriptVariableType::STRING:
            _vstr.clear();
            break;
        case ScriptVariableType::TABLE:
            _vtab = nullptr;
            break;
        default:
            break;
        }
        _type = ScriptVariableType::INTEGER;
        _vint = value;
    }
    double number() const {
        switch (_type) {
        case ScriptVariableType::INTEGER:
            return (double)_vint;
        case ScriptVariableType::NUMBER:
            return _vnum;
        case ScriptVariableType::STRING:
            return std::strtod(_vstr.c_str(), nullptr);
        case ScriptVariableType::TABLE:
            assert(0);
        default:
            return 0;
        }
    }
    void assign(double value) {
        switch (_type) {
        case ScriptVariableType::STRING:
            _vstr.clear();
            break;
        case ScriptVariableType::TABLE:
            _vtab = nullptr;
            break;
        default:
            break;
        }
        _type = ScriptVariableType::NUMBER;
        _vnum = value;
    }
    std::string string() const {
        char buf[128];
        switch (_type) {
        case ScriptVariableType::INTEGER:
#ifdef WIN32
            sprintf_s(buf, "%" PRId64, _vint);
#else
            sprintf(buf, "%" PRId64, _vint);
#endif

            return buf;
        case ScriptVariableType::NUMBER:
#ifdef WIN32
            sprintf_s(buf, "%f", _vnum);
#else
            sprintf(buf, "%f", _vnum);
#endif
            return buf;
        case ScriptVariableType::STRING:
            return _vstr;
        case ScriptVariableType::TABLE:
            assert(0);
        default:
            return "";
        }
    }
    void assign(const std::string &value) {
        switch (_type) {
        case ScriptVariableType::TABLE:
            _vtab = nullptr;
            break;
        default:
            break;
        }
        _type = ScriptVariableType::STRING;
        _vstr = value;
    }
    void assign(const char *value) {
        switch (_type) {
        case ScriptVariableType::TABLE:
            _vtab = nullptr;
            break;
        default:
            break;
        }
        _type = ScriptVariableType::STRING;
        _vstr = value ? value : "";
    }
    void assign(std::string &&value) {
        switch (_type) {
        case ScriptVariableType::TABLE:
            _vtab = nullptr;
            break;
        default:
            break;
        }
        _type = ScriptVariableType::STRING;
        _vstr = std::move(value);
    }
    std::shared_ptr<ScriptTable> table() const {
        switch (_type) {
        case ScriptVariableType::INTEGER:
        case ScriptVariableType::NUMBER:
        case ScriptVariableType::STRING:
            assert(0);
        case ScriptVariableType::NIL:
            return nullptr;
        case ScriptVariableType::TABLE:
            return _vtab;
        default:
            return nullptr;
        }
    }
    void assign(std::shared_ptr<ScriptTable> value) {
        switch (_type) {
        case ScriptVariableType::STRING:
            _vstr.clear();
            break;
        default:
            break;
        }
        _type = ScriptVariableType::TABLE;
        _vtab = std::move(value);
    }
    void assign(const ScriptVariant &x) {
        clear();
        _type = x._type;
        switch (_type) {
        case ScriptVariableType::INTEGER:
            _vint = x._vint;
            break;
        case ScriptVariableType::NUMBER:
            _vnum = x._vnum;
            break;
        case ScriptVariableType::STRING:
            _vstr = x._vstr;
            break;
        case ScriptVariableType::TABLE:
            _vtab = x._vtab;
            break;
        default:
            break;
        }
    }
    void assign(nullptr_t) {
        if (_type != ScriptVariableType::NIL) {
            clear();
        }
    }
    ScriptVariant &operator=(int64_t value) {
        assign(value);
        return *this;
    }
    ScriptVariant &operator=(double value) {
        assign(value);
        return *this;
    }
    ScriptVariant &operator=(const std::string &value) {
        assign(value);
        return *this;
    }
    ScriptVariant &operator=(std::string &&value) {
        assign(std::move(value));
        return *this;
    }
    ScriptVariant &operator=(const char *value) {
        assign(value);
        return *this;
    }
    ScriptVariant &operator=(std::shared_ptr<ScriptTable> value) {
        assign(value);
        return *this;
    }
    ScriptVariant &operator=(const ScriptVariant &x) {
        assign(x);
        return *this;
    }
private:
    void push_stack(Script *script) const ;

private:
    ScriptVariableType _type;
    union {
        int64_t _vint;
        double  _vnum;
    };
    std::string _vstr;
    std::shared_ptr<ScriptTable> _vtab;
};

struct ScriptVariable {
    ScriptVariable() { }
    ScriptVariable(std::shared_ptr<ScriptVariant> n, std::shared_ptr<ScriptVariant> v) 
    : name(n), value(v)
    { }
    ScriptVariable(const ScriptVariable &x) 
    : name(x.name), value(x.value)
    { }
    ScriptVariable(ScriptVariable &&x) 
    : name(), value() {
        name.swap(x.name);
        value.swap(x.value);
    }

    std::shared_ptr<ScriptVariant> name;
    std::shared_ptr<ScriptVariant> value;
};

class ScriptResult {
    friend class Script;
    friend class ScriptTable;
public:
    ScriptResult(bool result) : _result(result) { }
    ScriptResult(const ScriptResult &x) 
    : _result(x._result),
      _values(x._values)
    { }
    ScriptResult(ScriptResult &&x) 
    : _result(x._result),
      _values(std::move(x._values))
    { }

    operator bool() const {
        return _result;
    }
    const std::vector<ScriptVariant> &values() const {
        return _values;
    }
private:
    bool _result;
    std::vector<ScriptVariant> _values;
};

class ScriptTableFetcher {
public:
    ScriptTableFetcher(std::shared_ptr<ScriptTable> table) ;
    ~ScriptTableFetcher() ;
    bool fetch(ScriptVariable &var) ;
private:
    std::shared_ptr<ScriptTable> _table;
};

class ScriptTableIterator {
public:
    ScriptTableIterator(std::shared_ptr<ScriptTableFetcher> fetcher) : _fetcher(fetcher) { }
    ScriptTableIterator(const ScriptTableIterator &x) 
    : _fetcher(x._fetcher),
      _var(x._var)
    { }

    const ScriptVariable &operator*() const {
        return _var;
    }
    ScriptTableIterator& operator++() {
        if (_fetcher) {
            _fetcher->fetch(_var);
        }
        return *this;
    }
    ScriptTableIterator operator++(int) {
        ScriptTableIterator tmp(*this);
        if (_fetcher) {
            _fetcher->fetch(_var);
        }
        return tmp;
    }
    bool operator==(const ScriptTableIterator &x) const {
        return _var.name == x._var.name;
    }
    bool operator!=(const ScriptTableIterator &x) const {
        return _var.name != x._var.name;
    }
private:
    std::shared_ptr<ScriptTableFetcher> _fetcher;
    ScriptVariable _var;
};

#endif