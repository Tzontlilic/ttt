#ifndef __GX_FILELOADER_H__
#define __GX_FILELOADER_H__

#include <memory>
#include "data.h"
#include "path.h"


struct FileLoader {
	static std::shared_ptr<Data> load(const Path &path) ;
	static std::shared_ptr<Data> load(const char *path)  {
		return load(Path(path));
	}
};

#endif