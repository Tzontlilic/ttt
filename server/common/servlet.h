#ifndef	__SERVLET_H__
#define __SERVLET_H__

#include <unordered_map>
#include <stdint.h>
#include "evpp/buffer.h"
#include "evpp/tcp_conn.h"
#include "serial.h"

class Servlet
{
public:
	friend class ServletMgr;
	Servlet(bool b): need_rpc(b), seq_(0), conn() {}
	virtual ~Servlet(){}
public:
	virtual ISerial* get_request() = 0;
	virtual ISerial* get_response() = 0;
	virtual void serial_request(evpp::Buffer* buffer) = 0;
	virtual void unserial_request(evpp::Buffer* buffer) = 0;
	virtual void serial_response(evpp::Buffer* buffer) = 0;
	virtual void unserial_response(evpp::Buffer* buffer) = 0;
	virtual std::shared_ptr<Servlet> clone() = 0;
	virtual uint16_t msgid() const = 0;
	virtual std::string msgname() const = 0;
	virtual int execute() { return 0; };
	void seq(uint32_t seq) { seq_ = seq; };
	uint32_t seq() { return seq_;};
	void send(ISerial* servlet, evpp::TCPConn* conn);
	void send(ISerial* servlet, uint32_t seq, evpp::TCPConn* conn);
	void call(std::shared_ptr<Servlet> servlet);

protected:
	bool need_rpc;
	evpp::TCPConnPtr conn;
private:
	uint32_t seq_;
};

class ServletMgr
{
public:
	~ServletMgr();
	static ServletMgr *instance() {
		static ServletMgr *o;
		if (o == nullptr) {
			o = new ServletMgr();
		}
		return o;
	}
	int execute(const evpp::TCPConnPtr&, uint16_t, uint32_t, evpp::Buffer*);
	static void register_servlet(Servlet* servlet) {
		servletmap_.emplace(servlet->msgid(), servlet);
	}
	Servlet* get_servlet(uint16_t id) {
		auto it = servletmap_.find(id);
		if (it != servletmap_.end()) {
			return it->second;
		}
		else {
			return nullptr;
		}
	}
private:
	ServletMgr(){}
	static std::unordered_map<uint16_t, Servlet*> servletmap_;
};

#endif