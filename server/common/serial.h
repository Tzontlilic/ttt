#ifndef __ISERIAL_H__
#define __ISERIAL_H__

#include "evpp/buffer.h"
#include <stdint.h>
#include <string>

class ISerial
{
public:
	virtual void serial(evpp::Buffer*) const = 0;
	virtual void unserial(evpp::Buffer*) = 0;
	virtual uint16_t msgid() const { return 0; };
};

#endif