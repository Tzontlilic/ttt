#ifndef __NETWORK_H__
#define __NETWORK_H__

#include <list>
#include <mutex>
#include <map>
#include <atomic>
#include <array>
#include "evpp/tcp_conn.h"
#include "netid.h"
#include "servlet.h"

class evpp::InvokeTimer;
class evpp::TCPConn;
class ISerial;
class Servlet;
class Script;

class Network {
public:
	struct Addr {
		int id;
		std::string name;
		std::string addr;
		std::string ip;
		uint16_t port;
	};
	Network();
	~Network();
	bool init(std::shared_ptr<Script>, evpp::EventLoop*);
	void send(ISerial*, uint32_t, evpp::TCPConn* conn) const;
	void send(ISerial* servlet, evpp::TCPConn* conn) {
		uint32_t seq = ++seq_;
		send(servlet, seq, conn);
	}
	void call(std::shared_ptr<Servlet>);
	Addr& addr(int type) {
		return addrs_.at(type);
	}
private:
	void on_connect(const evpp::TCPConnPtr&);
	void on_connect_server(const evpp::TCPConnPtr&, int);
	void on_message(const evpp::TCPConnPtr&, evpp::Buffer*);
	int on_request(const evpp::TCPConnPtr&, uint16_t, uint32_t , evpp::Buffer*);
	int on_response(const evpp::TCPConnPtr&, uint16_t, uint32_t , evpp::Buffer*);
	void rpc_timeout(int16_t, uint32_t);

private:
	typedef std::array<std::vector<Addr>, SERVLET_UNKNOWN> AddrList;
	typedef std::array<std::vector<evpp::TCPConnPtr>, SERVLET_UNKNOWN> Tokens;
	Tokens tokens_;
	std::array<Addr, SERVLET_UNKNOWN> addrs_;
	std::list<evpp::TCPConnPtr> acceptlist_;
	std::shared_ptr<evpp::InvokeTimer> timer_;
	std::mutex mutex_;
	std::map<uint32_t, std::shared_ptr<Servlet>> callbackmap_;
    std::atomic<uint32_t> seq_;
    evpp::EventLoop* mainloop_;  // the listening loop
	std::vector<std::shared_ptr<evpp::TCPServer>> servers_;
	std::vector<std::shared_ptr<evpp::TCPClient>> clients_;
};

extern std::unique_ptr<Network> the_network;

#endif