#ifndef __GX_CSVLOADER_H__
#define __GX_CSVLOADER_H__

#include <vector>
#include <string>
#include <map>
#include <memory>
#include "data.h"

class CsvColInfo {
    friend class CsvRow;
public:
    CsvColInfo(int col, const char *name) : _col(col), _name(name) {}
    unsigned column() const  {
        return _col;
    }
    const char *name() const  {
        return _name.c_str();
    }
private:
    unsigned _col;
    std::string _name;
};

class CsvInfo {
    friend class CsvRow;
    friend class CsvLoader;
public:
    const CsvColInfo *operator[](const char *name) const ;
    const CsvColInfo *operator[](unsigned col) const ;
    const std::vector<std::shared_ptr<CsvColInfo>> &columns() const  {
        return _cols;
    }
private:
    bool add_col(const char *name, unsigned col);

private:
    std::map<std::string, std::shared_ptr<CsvColInfo>> _name_map;
    std::map<unsigned, std::shared_ptr<CsvColInfo>> _col_map;
    std::vector<std::shared_ptr<CsvColInfo>> _cols;
};

class CsvRow {
    friend class CsvLoader;
public:
    size_t size() const  {
        return _cols.size();
    }
    const char *operator[](unsigned i) const ;
    const char *operator[](const char *name) const ;

    CsvInfo *info() const  {
        return _info.get();
    }
    
    uint64_t getu(unsigned i, unsigned defaultValue = 0) const ;
    uint64_t getu(const char *name, unsigned defaultValue = 0) const ;

    int64_t geti(unsigned i, int defaultValue = 0) const ;
    int64_t geti(const char *name, int defaultValue = 0) const ;

    const char *gets(unsigned i, const char *defaultValue) const ;
    const char *gets(const char *name, const char *defaultValue) const ;
    bool is_blank() const ;
protected:
    std::shared_ptr<CsvInfo> _info;
    std::vector<std::string> _cols;
};

class CsvLoader {
public:
    int load(const Data &data);
    const std::vector<std::shared_ptr<CsvRow>> &rows() const  {
        return _rows;
    }
public:
    std::vector<std::shared_ptr<CsvRow>> _rows;
};

#endif