#include "network.h"
#include "evpp/buffer.h"
#include "evpp/tcp_server.h"
#include "evpp/tcp_client.h"
#include "evpp/duration.h"
#include "script.h"
#include "application.h"
#include "utils.h"

Network::Network() : seq_(0)
{
}

Network::~Network()
{
}

bool Network::init(std::shared_ptr<Script> script, evpp::EventLoop* loop) {
	auto tab = script->read_table("the_network");
	if (tab->is_nil()) {
		return false;
	}
	addrs_[SERVLET_CLIENT].id = SERVLET_CLIENT;
	addrs_[SERVLET_CLIENT].name = "agentclient";
	addrs_[SERVLET_CLIENT].addr = tab->read_string("agentclient");
	auto strarr = split(addrs_[SERVLET_CLIENT].addr, ':');
	assert(strarr.size() == 2);
	addrs_[SERVLET_CLIENT].ip = strarr[0];
	addrs_[SERVLET_CLIENT].port = atoi(strarr[1].c_str());

	addrs_[SERVLET_LOGIN].id = SERVLET_LOGIN;
	addrs_[SERVLET_LOGIN].name = "loginsvr";
	addrs_[SERVLET_LOGIN].addr = tab->read_string("login");

	addrs_[SERVLET_DB].id = SERVLET_DB;
	addrs_[SERVLET_DB].name = "dbsvr";
	addrs_[SERVLET_DB].addr = tab->read_string("db");

	addrs_[SERVLET_AGENT].id = SERVLET_AGENT;
	addrs_[SERVLET_AGENT].name = "agentsvr";
	addrs_[SERVLET_AGENT].addr = tab->read_string("agent");

	addrs_[SERVLET_MAP].id = SERVLET_MAP;
	addrs_[SERVLET_MAP].name = "mapsvr";
	addrs_[SERVLET_MAP].addr = tab->read_string("map");

	addrs_[SERVLET_MAP_CLIENT].id = SERVLET_MAP_CLIENT;
	addrs_[SERVLET_MAP_CLIENT].name = "mapclient";
	addrs_[SERVLET_MAP_CLIENT].addr = tab->read_string("mapclient");

	addrs_[SERVLET_WORLD].id = SERVLET_WORLD;
	addrs_[SERVLET_WORLD].name = "worldsvr";
	addrs_[SERVLET_WORLD].addr = tab->read_string("world");

	auto agenttab = tab->read_table("agentarr");
	if (agenttab->is_nil()) {
		return false;
	}

	std::vector<Addr> agentaddrs;
	for (int line = 1;; ++line) {
		Addr tmp;
		tmp.addr = agenttab->read_string(line);
		if (tmp.addr.empty()) {
			break;
		}
		tmp.id = SERVLET_AGENT;
		tmp.name = "agentsvr";
		agentaddrs.emplace_back(tmp);
	}

	auto maptab = tab->read_table("maparr");
	if (maptab->is_nil()) {
		return false;
	}
	std::vector<Addr> mapaddrs;
	for (int line = 1;; ++line) {
		Addr tmp;
		tmp.addr = maptab->read_string(line);
		if (tmp.addr.empty()) {
			break;
		}
		tmp.id = SERVLET_MAP;
		tmp.name = "mapsvr";
		mapaddrs.emplace_back(tmp);
	}

	AddrList listenlist;
	listenlist[SERVLET_LOGIN].emplace_back(addrs_[SERVLET_LOGIN]);
	listenlist[SERVLET_AGENT].emplace_back(addrs_[SERVLET_CLIENT]);
	listenlist[SERVLET_AGENT].emplace_back(addrs_[SERVLET_AGENT]);
	listenlist[SERVLET_MAP].emplace_back(addrs_[SERVLET_MAP_CLIENT]);
	listenlist[SERVLET_MAP].emplace_back(addrs_[SERVLET_MAP]);
	listenlist[SERVLET_WORLD].emplace_back(addrs_[SERVLET_WORLD]);
	listenlist[SERVLET_DB].emplace_back(addrs_[SERVLET_DB]);

	AddrList connectlist;
	connectlist[SERVLET_LOGIN] = agentaddrs;
	connectlist[SERVLET_LOGIN].emplace_back(addrs_[SERVLET_DB]);
	connectlist[SERVLET_LOGIN].emplace_back(addrs_[SERVLET_WORLD]);
	connectlist[SERVLET_AGENT] = mapaddrs;
	connectlist[SERVLET_AGENT].emplace_back(addrs_[SERVLET_DB]);
	connectlist[SERVLET_AGENT].emplace_back(addrs_[SERVLET_WORLD]);
	connectlist[SERVLET_MAP].emplace_back(addrs_[SERVLET_WORLD]);
	connectlist[SERVLET_CLIENT].emplace_back(addrs_[SERVLET_LOGIN]);
	connectlist[SERVLET_CLIENT].emplace_back(addrs_[SERVLET_CLIENT]);

	auto mylisten = listenlist[the_app->id()];
	for (auto &addr : mylisten) {
		auto server = std::make_shared<evpp::TCPServer>(loop, addr.addr, addr.name, 0);
		server->SetMessageCallback(std::bind(&Network::on_message, this, std::placeholders::_1, std::placeholders::_2));
		server->SetConnectionCallback(std::bind(&Network::on_connect,this, std::placeholders::_1));
		server->Init();
		server->Start();
		servers_.emplace_back(server);
	}
	auto myconnect = connectlist[the_app->id()];
	for (auto addr : myconnect) {
		if (addr.id == SERVLET_WORLD) continue;
		if (addr.id == SERVLET_MAP) continue;
		auto client = std::make_shared<evpp::TCPClient>(loop, addr.addr, addr.name);
		client->SetMessageCallback(std::bind(&Network::on_message, this, std::placeholders::_1, std::placeholders::_2));
		client->SetConnectionCallback(std::bind(&Network::on_connect_server, this, std::placeholders::_1, addr.id));
		client->Connect();
		clients_.emplace_back(client);
	}
	return true;
}

void Network::on_connect(const evpp::TCPConnPtr& conn) {
	if (conn->IsConnected()) {
		LOG_INFO << "A new connection " << conn->remote_addr();
		acceptlist_.push_back(conn);
	} else if (conn->IsDisconnected() == true){
		if (conn->fd() == -1) {
			LOG_INFO << "Failed to establish a connection to" << conn->remote_addr();
		} else {
			LOG_INFO << "An exist connection Disconnected from " << conn->remote_addr();
		}
	}
	else {
		LOG_INFO << "########### Disconnected from " << conn->remote_addr();
	}
}
void Network::on_connect_server(const evpp::TCPConnPtr& conn, int servid) {
	if (conn->IsConnected()) {
		LOG_INFO << "A new connection " << conn->remote_addr();
		tokens_[servid].emplace_back(conn);
	} else if (conn->IsDisconnected() == true){
		if (conn->fd() == -1) {
			LOG_INFO << "Failed to establish a connection to" << conn->remote_addr();
		} else {
			LOG_INFO << "An exist connection Disconnected from " << conn->remote_addr();
		}
	}
	else {
		LOG_INFO << "########### Disconnected from " << conn->remote_addr();
	}
}
//len(2 byte) | seq(4 byte) | msgId(2 byte) | body
void Network::on_message(const evpp::TCPConnPtr& conn, evpp::Buffer* msg) {
	size_t len = 0;
	int r = 0;
	while (msg->length() >= 8) {
		len = msg->PeekInt16();
		if (len > 8192 || len < 4) {
			conn->Close();
			return;
		}
		if (len  > msg->length()) {
			break;
		}
		msg->Skip(2);
		uint32_t seq = msg->ReadUint32();
		uint16_t msgid = msg->ReadUint16();
		int servletid = msgid >> 12;
		if (the_app->id() == SERVLET_CLIENT && msgid < 0x800) {
			//for test
			r = on_response(conn, msgid, seq, msg);
		}
		else {
			if (servletid == the_app->id() || servletid == the_app->ap()) {
				r = on_request(conn, msgid, seq, msg);
			} else {
				r = on_response(conn, msgid, seq, msg);
			}
		}
		if (r < 0) {
			conn->Close();
			return;
		}
	}

	if (msg->length() > 0) {
		auto butt = msg->NextAll();
		msg->Append(butt);
	}
}

int Network::on_request(const evpp::TCPConnPtr& conn, uint16_t msgid, uint32_t seq, evpp::Buffer* msg) {
	return ServletMgr::instance()->execute(conn, msgid, seq, msg);
}
int Network::on_response(const evpp::TCPConnPtr& conn, uint16_t msgid, uint32_t seq, evpp::Buffer* msg) {
	std::shared_ptr<Servlet> sv; 
	{
		std::lock_guard<std::mutex> lock(mutex_);
		auto it = callbackmap_.find(seq);
		if (it != callbackmap_.end()) {
			sv = it->second;
		}
	}
	if (sv) {
		sv->unserial_response(msg);
		sv->execute();
		LOG_INFO << "callback ok, " << msgid;
		{
			std::lock_guard<std::mutex> lock(mutex_);
			callbackmap_.erase(seq);
		}
	} else {
		msg->Reset();
		LOG_ERROR << "callback function don't exist,msgid=" << msgid;
	}

	return 0;
}

void Network::send(ISerial* req, uint32_t seq, evpp::TCPConn* conn) const {
	evpp::Buffer buf(8192);
	buf.AppendUint32(seq);
	buf.AppendUint16(req->msgid());
	req->serial(&buf);
	if (buf.length() > 51200) {
		LOG_ERROR << "too long message!";
		return;
	}
	else {
		buf.PrependUint16((uint16_t)buf.length() + 2);
		conn->Send(&buf);
	}
}
void Network::call(std::shared_ptr<Servlet> servlet) {
	uint32_t seq = ++seq_;
	{
		std::lock_guard<std::mutex> lock(mutex_);
		callbackmap_.emplace(seq, servlet);
	}

	int servletid = servlet->msgid() >> 12;
	auto conn = tokens_.at(servletid).back();
	send(servlet->get_request(), seq, conn.get());
	//mainloop_->RunAfter(evpp::Duration(5.0), std::bind(&Network::rpc_timeout, this, servlet->msgid(), seq));
}

void Network::rpc_timeout(int16_t msgid, uint32_t seq) {
	std::lock_guard<std::mutex> lock(mutex_);
	auto it = callbackmap_.find(seq);
	if (it != callbackmap_.end()) {
		callbackmap_.erase(it);
		LOG_WARN << "rpc timeout, msgId="<<msgid<<", seq=" << seq;
	}
}