#include "application.h"
#include "evpp/event_loop.h"

std::unique_ptr<Application> the_app;
std::unique_ptr<Network> the_network;
Application::Application(int sid) : id_(sid), ap_(-1){
	script_.reset(new Script());
	the_network.reset(new Network());
	mainloop_.reset(new evpp::EventLoop());
}

Application::~Application()
{
}

bool Application::init() {
	ScriptFunctionManager::instance()->upload(script_.get());
	if (script_->load(Path("/network.lua")) < 0) {
		return false;
	}
	if (!the_network->init(script_, mainloop_.get())) {
		LOG_ERROR << "init network failed";
		return false;
	}
	return true;
}

void Application::run() {
	mainloop_->Run();
}