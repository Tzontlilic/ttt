#pragma once
#include "serial.h"
#include "servlet.h"


#include "message.h"
struct DB_AccountQuery;
struct DB_AccountQueryReq : ISerial {
    std::string user;
    
    DB_AccountQueryReq()
    : ISerial(),
      user()
    { }
    
    uint16_t msgid() const override {
        return DB_ACCOUNT_QUERY;
    }
    std::string msgname() const {
        return "DB_AccountQueryReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendString(this->user);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->user = buf->ReadString();
    }
    
};
struct DB_AccountQueryRsp : ISerial {
    int32_t rc;
    uint32_t uid;
    
    DB_AccountQueryRsp()
    : ISerial(),
      rc(),
      uid()
    { }
    
    uint16_t msgid() const override {
        return DB_ACCOUNT_QUERY;
    }
    std::string msgname() const {
        return "DB_AccountQueryRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendInt32(this->rc);
        buf->AppendUint32(this->uid);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->rc = buf->ReadInt32();
        this->uid = buf->ReadUint32();
    }
    
};
struct DB_AccountQuery : Servlet {
    DB_AccountQueryReq req;
    DB_AccountQueryRsp rsp;
    DB_AccountQuery(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return DB_ACCOUNT_QUERY;
    }
    std::string msgname() const override {
        return "DB_ACCOUNT_QUERY";
    }
    DB_AccountQueryReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    DB_AccountQueryRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<DB_AccountQuery>(need_rpc);
    }
    int execute() override;
};
struct DB_AccountRegister;
struct DB_AccountRegisterReq : ISerial {
    std::string user;
    std::string nickname;
    uint32_t platform;
    
    DB_AccountRegisterReq()
    : ISerial(),
      user(),
      nickname(),
      platform()
    { }
    
    uint16_t msgid() const override {
        return DB_ACCOUNT_REGISTER;
    }
    std::string msgname() const {
        return "DB_AccountRegisterReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendString(this->user);
        buf->AppendString(this->nickname);
        buf->AppendUint32(this->platform);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->user = buf->ReadString();
        this->nickname = buf->ReadString();
        this->platform = buf->ReadUint32();
    }
    
};
struct DB_AccountRegisterRsp : ISerial {
    int32_t rc;
    
    DB_AccountRegisterRsp()
    : ISerial(),
      rc()
    { }
    
    uint16_t msgid() const override {
        return DB_ACCOUNT_REGISTER;
    }
    std::string msgname() const {
        return "DB_AccountRegisterRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendInt32(this->rc);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->rc = buf->ReadInt32();
    }
    
};
struct DB_AccountRegister : Servlet {
    DB_AccountRegisterReq req;
    DB_AccountRegisterRsp rsp;
    DB_AccountRegister(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return DB_ACCOUNT_REGISTER;
    }
    std::string msgname() const override {
        return "DB_ACCOUNT_REGISTER";
    }
    DB_AccountRegisterReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    DB_AccountRegisterRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<DB_AccountRegister>(need_rpc);
    }
    int execute() override;
};

