#pragma once
#include "serial.h"
#include "servlet.h"
#include "libgame/g_bag.h"
#include "libgame/g_value.h"


#include "message.h"
struct DB_Load;
struct DB_LoadReq : ISerial {
    uint32_t id;
    
    DB_LoadReq()
    : ISerial(),
      id()
    { }
    
    uint16_t msgid() const override {
        return DB_LOAD;
    }
    std::string msgname() const {
        return "DB_LoadReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->id);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->id = buf->ReadUint32();
    }
    
};
struct DB_LoadRsp : ISerial {
    int32_t rc;
    std::string name;
    uint64_t login_time;
    uint64_t logout_time;
    std::vector<G_BagItemOpt> bag;
    std::vector<G_ValueOpt> values;
    
    DB_LoadRsp()
    : ISerial(),
      rc(),
      name(),
      login_time(),
      logout_time(),
      bag(),
      values()
    { }
    
    uint16_t msgid() const override {
        return DB_LOAD;
    }
    std::string msgname() const {
        return "DB_LoadRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendInt32(this->rc);
        buf->AppendString(this->name);
        buf->AppendUint64(this->login_time);
        buf->AppendUint64(this->logout_time);
        buf->AppendUint32(this->bag.size());
        for (auto &sscc_i : this->bag) {
            sscc_i.serial(buf);
        }
        buf->AppendUint32(this->values.size());
        for (auto &sscc_i : this->values) {
            sscc_i.serial(buf);
        }
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->rc = buf->ReadInt32();
        this->name = buf->ReadString();
        this->login_time = buf->ReadUint64();
        this->logout_time = buf->ReadUint64();
        do {
            uint32_t sscc_size = buf->ReadUint32();
            for (size_t sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                this->bag.emplace_back();
                this->bag.back().unserial(buf);
            }
        } while (0);
        do {
            uint32_t sscc_size = buf->ReadUint32();
            for (size_t sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                this->values.emplace_back();
                this->values.back().unserial(buf);
            }
        } while (0);
    }
    
};
struct DB_Load : Servlet {
    DB_LoadReq req;
    DB_LoadRsp rsp;
    DB_Load(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return DB_LOAD;
    }
    std::string msgname() const override {
        return "DB_LOAD";
    }
    DB_LoadReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    DB_LoadRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<DB_Load>(need_rpc);
    }
    int execute() override;
};
struct DB_Login;
struct DB_LoginReq : ISerial {
    uint32_t id;
    uint64_t time;
    
    DB_LoginReq()
    : ISerial(),
      id(),
      time()
    { }
    
    uint16_t msgid() const override {
        return DB_LOGIN;
    }
    std::string msgname() const {
        return "DB_LoginReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->id);
        buf->AppendUint64(this->time);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->id = buf->ReadUint32();
        this->time = buf->ReadUint64();
    }
    
};
struct DB_LoginRsp : ISerial {
    int32_t rc;
    
    DB_LoginRsp()
    : ISerial(),
      rc()
    { }
    
    uint16_t msgid() const override {
        return DB_LOGIN;
    }
    std::string msgname() const {
        return "DB_LoginRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendInt32(this->rc);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->rc = buf->ReadInt32();
    }
    
};
struct DB_Login : Servlet {
    DB_LoginReq req;
    DB_LoginRsp rsp;
    DB_Login(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return DB_LOGIN;
    }
    std::string msgname() const override {
        return "DB_LOGIN";
    }
    DB_LoginReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    DB_LoginRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<DB_Login>(need_rpc);
    }
    int execute() override;
};

