#pragma once
#include "serial.h"


struct G_BagItemOpt : ISerial {
    uint32_t id;
    uint32_t count;
    
    G_BagItemOpt()
    : ISerial(),
      id(),
      count()
    { }
    
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->id);
        buf->AppendUint32(this->count);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->id = buf->ReadUint32();
        this->count = buf->ReadUint32();
    }
    
};

