#pragma once
#include "serial.h"


struct G_ValueOpt : ISerial {
    uint8_t id;
    uint32_t value;
    
    G_ValueOpt()
    : ISerial(),
      id(),
      value()
    { }
    
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint8(this->id);
        buf->AppendUint32(this->value);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->id = buf->ReadUint8();
        this->value = buf->ReadUint32();
    }
    
};

