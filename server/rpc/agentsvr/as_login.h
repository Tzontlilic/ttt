#pragma once
#include "serial.h"
#include "servlet.h"
#include "libgame/g_value.h"
#include "libgame/g_bag.h"


#include "message.h"
struct AS_Login;
struct AS_LoginReq : ISerial {
    uint32_t id;
    
    AS_LoginReq()
    : ISerial(),
      id()
    { }
    
    uint16_t msgid() const override {
        return AS_LOGIN;
    }
    std::string msgname() const {
        return "AS_LoginReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->id);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->id = buf->ReadUint32();
    }
    
};
struct AS_LoginRsp : ISerial {
    int32_t rc;
    uint64_t key;
    std::string host;
    uint16_t port;
    
    AS_LoginRsp()
    : ISerial(),
      rc(),
      key(),
      host(),
      port()
    { }
    
    uint16_t msgid() const override {
        return AS_LOGIN;
    }
    std::string msgname() const {
        return "AS_LoginRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendInt32(this->rc);
        buf->AppendUint64(this->key);
        buf->AppendString(this->host);
        buf->AppendUint16(this->port);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->rc = buf->ReadInt32();
        this->key = buf->ReadUint64();
        this->host = buf->ReadString();
        this->port = buf->ReadUint16();
    }
    
};
struct AS_Login : Servlet {
    AS_LoginReq req;
    AS_LoginRsp rsp;
    AS_Login(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return AS_LOGIN;
    }
    std::string msgname() const override {
        return "AS_LOGIN";
    }
    AS_LoginReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    AS_LoginRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<AS_Login>(need_rpc);
    }
    int execute() override;
};

