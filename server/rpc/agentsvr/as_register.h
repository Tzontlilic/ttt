#pragma once
#include "serial.h"
#include "servlet.h"

#include "message.h"
struct AS_Register;
struct AS_RegisterReq : ISerial {
    
    AS_RegisterReq()
    : ISerial()
    { }
    
    uint16_t msgid() const override {
        return AS_REGISTER;
    }
    std::string msgname() const {
        return "AS_RegisterReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
    }
    
    void unserial(evpp::Buffer* buf) override {
    }
    
};
struct AS_RegisterRsp : ISerial {
    
    AS_RegisterRsp()
    : ISerial()
    { }
    
    uint16_t msgid() const override {
        return AS_REGISTER;
    }
    std::string msgname() const {
        return "AS_RegisterRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
    }
    
    void unserial(evpp::Buffer* buf) override {
    }
    
};
struct AS_Register : Servlet {
    AS_RegisterReq req;
    AS_RegisterRsp rsp;
    AS_Register(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return AS_REGISTER;
    }
    std::string msgname() const override {
        return "AS_REGISTER";
    }
    AS_RegisterReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    AS_RegisterRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<AS_Register>(need_rpc);
    }
    int execute() override;
};

