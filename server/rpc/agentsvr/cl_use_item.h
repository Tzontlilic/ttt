#pragma once
#include "serial.h"
#include "servlet.h"

#include "message.h"
struct CL_UseItem;
struct CL_UseItemReq : ISerial {
    uint32_t item_id;
    uint32_t count;
    
    CL_UseItemReq()
    : ISerial(),
      item_id(),
      count()
    { }
    
    uint16_t msgid() const override {
        return CL_USE_ITEM;
    }
    std::string msgname() const {
        return "CL_UseItemReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->item_id);
        buf->AppendUint32(this->count);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->item_id = buf->ReadUint32();
        this->count = buf->ReadUint32();
    }
    
};
struct CL_UseItemRsp : ISerial {
    
    CL_UseItemRsp()
    : ISerial()
    { }
    
    uint16_t msgid() const override {
        return CL_USE_ITEM;
    }
    std::string msgname() const {
        return "CL_UseItemRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
    }
    
    void unserial(evpp::Buffer* buf) override {
    }
    
};
struct CL_UseItem : Servlet {
    CL_UseItemReq req;
    CL_UseItemRsp rsp;
    CL_UseItem(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return CL_USE_ITEM;
    }
    std::string msgname() const override {
        return "CL_USE_ITEM";
    }
    CL_UseItemReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    CL_UseItemRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<CL_UseItem>(need_rpc);
    }
    int execute() override;
};

