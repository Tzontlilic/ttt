#pragma once
#include "serial.h"
#include "servlet.h"
#include "libgame/g_value.h"
#include "libgame/g_bag.h"

#include "message.h"
struct CL_NotifyItems;
struct CL_NotifyItemsReq : ISerial {
    std::vector<G_BagItemOpt> items;
    
    CL_NotifyItemsReq()
    : ISerial(),
      items()
    { }
    
    uint16_t msgid() const override {
        return CL_NOTIFY_ITEMS;
    }
    std::string msgname() const {
        return "CL_NotifyItemsReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->items.size());
        for (auto &sscc_i : this->items) {
            sscc_i.serial(buf);
        }
    }
    
    void unserial(evpp::Buffer* buf) override {
        do {
            uint32_t sscc_size = buf->ReadUint32();
            for (size_t sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                this->items.emplace_back();
                this->items.back().unserial(buf);
            }
        } while (0);
    }
    
};
struct CL_NotifyItems : Servlet {
    CL_NotifyItemsReq req;
    CL_NotifyItems(bool b) : Servlet(b), req() {};
    uint16_t msgid() const override {
        return CL_NOTIFY_ITEMS;
    }
    std::string msgname() const override {
        return "CL_NOTIFY_ITEMS";
    }
    CL_NotifyItemsReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    ISerial* get_response() override {
        return nullptr;
    }
    void serial_response(evpp::Buffer* buf) override {
    }
    void unserial_response(evpp::Buffer* buf) override {
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<CL_NotifyItems>(need_rpc);
    }
    int execute() {return 0;};
};
struct CL_NotifyValues;
struct CL_NotifyValuesReq : ISerial {
    std::vector<G_ValueOpt> values;
    
    CL_NotifyValuesReq()
    : ISerial(),
      values()
    { }
    
    uint16_t msgid() const override {
        return CL_NOTIFY_VALUES;
    }
    std::string msgname() const {
        return "CL_NotifyValuesReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->values.size());
        for (auto &sscc_i : this->values) {
            sscc_i.serial(buf);
        }
    }
    
    void unserial(evpp::Buffer* buf) override {
        do {
            uint32_t sscc_size = buf->ReadUint32();
            for (size_t sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                this->values.emplace_back();
                this->values.back().unserial(buf);
            }
        } while (0);
    }
    
};
struct CL_NotifyValues : Servlet {
    CL_NotifyValuesReq req;
    CL_NotifyValues(bool b) : Servlet(b), req() {};
    uint16_t msgid() const override {
        return CL_NOTIFY_VALUES;
    }
    std::string msgname() const override {
        return "CL_NOTIFY_VALUES";
    }
    CL_NotifyValuesReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    ISerial* get_response() override {
        return nullptr;
    }
    void serial_response(evpp::Buffer* buf) override {
    }
    void unserial_response(evpp::Buffer* buf) override {
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<CL_NotifyValues>(need_rpc);
    }
    int execute() {return 0;};
};

