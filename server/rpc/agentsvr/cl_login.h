#pragma once
#include "serial.h"
#include "servlet.h"

#include "message.h"
struct CL_Login;
struct CL_LoginReq : ISerial {
    uint32_t uid;
    uint64_t key;
    
    CL_LoginReq()
    : ISerial(),
      uid(),
      key()
    { }
    
    uint16_t msgid() const override {
        return CL_LOGIN;
    }
    std::string msgname() const {
        return "CL_LoginReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->uid);
        buf->AppendUint64(this->key);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->uid = buf->ReadUint32();
        this->key = buf->ReadUint64();
    }
    
};
struct CL_LoginRsp : ISerial {
    std::string name;
    
    CL_LoginRsp()
    : ISerial(),
      name()
    { }
    
    uint16_t msgid() const override {
        return CL_LOGIN;
    }
    std::string msgname() const {
        return "CL_LoginRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendString(this->name);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->name = buf->ReadString();
    }
    
};
struct CL_Login : Servlet {
    CL_LoginReq req;
    CL_LoginRsp rsp;
    CL_Login(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return CL_LOGIN;
    }
    std::string msgname() const override {
        return "CL_LOGIN";
    }
    CL_LoginReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    CL_LoginRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<CL_Login>(need_rpc);
    }
    int execute() override;
};

