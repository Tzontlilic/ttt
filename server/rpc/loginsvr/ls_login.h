#pragma once
#include "serial.h"
#include "servlet.h"

#include "message.h"
struct LS_LoginAccount;
struct LS_LoginAccountReq : ISerial {
    std::string user;
    std::string authstr;
    
    LS_LoginAccountReq()
    : ISerial(),
      user(),
      authstr()
    { }
    
    uint16_t msgid() const override {
        return LS_LOGIN_ACCOUNT;
    }
    std::string msgname() const {
        return "LS_LoginAccountReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendString(this->user);
        buf->AppendString(this->authstr);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->user = buf->ReadString();
        this->authstr = buf->ReadString();
    }
    
};
struct LS_LoginAccountRsp : ISerial {
    uint32_t uid;
    uint64_t key;
    std::string host;
    uint16_t port;
    
    LS_LoginAccountRsp()
    : ISerial(),
      uid(),
      key(),
      host(),
      port()
    { }
    
    uint16_t msgid() const override {
        return LS_LOGIN_ACCOUNT;
    }
    std::string msgname() const {
        return "LS_LoginAccountRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->uid);
        buf->AppendUint64(this->key);
        buf->AppendString(this->host);
        buf->AppendUint16(this->port);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->uid = buf->ReadUint32();
        this->key = buf->ReadUint64();
        this->host = buf->ReadString();
        this->port = buf->ReadUint16();
    }
    
};
struct LS_LoginAccount : Servlet {
    LS_LoginAccountReq req;
    LS_LoginAccountRsp rsp;
    LS_LoginAccount(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return LS_LOGIN_ACCOUNT;
    }
    std::string msgname() const override {
        return "LS_LOGIN_ACCOUNT";
    }
    LS_LoginAccountReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    LS_LoginAccountRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<LS_LoginAccount>(need_rpc);
    }
    int execute() override;
};

