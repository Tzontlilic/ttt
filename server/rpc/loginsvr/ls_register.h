#pragma once
#include "serial.h"
#include "servlet.h"

#include "message.h"
struct LS_Register;
struct LS_RegisterReq : ISerial {
    std::string user;
    std::string passwd;
    uint32_t platform;
    std::string nickname;
    uint8_t side;
    
    LS_RegisterReq()
    : ISerial(),
      user(),
      passwd(),
      platform(),
      nickname(),
      side()
    { }
    
    uint16_t msgid() const override {
        return LS_REGISTER;
    }
    std::string msgname() const {
        return "LS_RegisterReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendString(this->user);
        buf->AppendString(this->passwd);
        buf->AppendUint32(this->platform);
        buf->AppendString(this->nickname);
        buf->AppendUint8(this->side);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->user = buf->ReadString();
        this->passwd = buf->ReadString();
        this->platform = buf->ReadUint32();
        this->nickname = buf->ReadString();
        this->side = buf->ReadUint8();
    }
    
};
struct LS_RegisterRsp : ISerial {
    
    LS_RegisterRsp()
    : ISerial()
    { }
    
    uint16_t msgid() const override {
        return LS_REGISTER;
    }
    std::string msgname() const {
        return "LS_RegisterRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
    }
    
    void unserial(evpp::Buffer* buf) override {
    }
    
};
struct LS_Register : Servlet {
    LS_RegisterReq req;
    LS_RegisterRsp rsp;
    LS_Register(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return LS_REGISTER;
    }
    std::string msgname() const override {
        return "LS_REGISTER";
    }
    LS_RegisterReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    LS_RegisterRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<LS_Register>(need_rpc);
    }
    int execute() override;
};

