#pragma once
#include "serial.h"
#include "servlet.h"

#include "message.h"
struct LS_Auth;
struct LS_AuthReq : ISerial {
    uint32_t seq;
    uint8_t ok;
    
    LS_AuthReq()
    : ISerial(),
      seq(),
      ok()
    { }
    
    uint16_t msgid() const override {
        return LS_AUTH;
    }
    std::string msgname() const {
        return "LS_AuthReq";
    }
    
    void serial(evpp::Buffer* buf) const override {
        buf->AppendUint32(this->seq);
        buf->AppendUint8(this->ok);
    }
    
    void unserial(evpp::Buffer* buf) override {
        this->seq = buf->ReadUint32();
        this->ok = buf->ReadUint8();
    }
    
};
struct LS_AuthRsp : ISerial {
    
    LS_AuthRsp()
    : ISerial()
    { }
    
    uint16_t msgid() const override {
        return LS_AUTH;
    }
    std::string msgname() const {
        return "LS_AuthRsp";
    }
    
    void serial(evpp::Buffer* buf) const override {
    }
    
    void unserial(evpp::Buffer* buf) override {
    }
    
};
struct LS_Auth : Servlet {
    LS_AuthReq req;
    LS_AuthRsp rsp;
    LS_Auth(bool b) : Servlet(b), req(), rsp() {};
    uint16_t msgid() const override {
        return LS_AUTH;
    }
    std::string msgname() const override {
        return "LS_AUTH";
    }
    LS_AuthReq* get_request() override {
        return &req;
    }
    void serial_request(evpp::Buffer* buf) override {
        req.serial(buf);
    }
    void unserial_request(evpp::Buffer* buf) override {
        req.unserial(buf);
    }
    LS_AuthRsp* get_response() override {
        return &rsp;
    }
    void serial_response(evpp::Buffer* buf) override {
        rsp.serial(buf);
    }
    void unserial_response(evpp::Buffer* buf) override {
        rsp.unserial(buf);
    }
    std::shared_ptr<Servlet> clone() override {
        return std::make_shared<LS_Auth>(need_rpc);
    }
    int execute() override;
};

