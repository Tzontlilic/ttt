#pragma once

#include "netid.h"
#define LS_BEGIN 0x1000
#define LS_LOGIN_ACCOUNT 0x1000
#define LS_LOGIN_SESSION 0x1001
#define LS_REGISTER 0x1002
#define LS_AUTH 0x1003
#define CL_BEGIN 0x0
#define CL_LOGIN 0x1
#define CL_USE_ITEM 0x2
#define CL_NOTIFY_BEGIN 0x800
#define CL_NOTIFY_VALUES 0x801
#define CL_NOTIFY_ITEMS 0x802
#define AS_BEGIN 0x3000
#define AS_LOGIN 0x3001
#define AS_REGISTER 0x3002
#define AS_AUTH 0x3003
#define DB_BEGIN 0x2000
#define DB_ACCOUNT_QUERY 0x2001
#define DB_ACCOUNT_REGISTER 0x2002
#define DB_LOAD 0x2003
#define DB_LOGIN 0x2004
#define MS_BEGIN 0x4000
#define MS_LOGIN 0x4001
#define MC_BEGIN 0x5000
#define MC_LOGIN 0x5001
#define WS_BEGIN 0x6000
#define WS_LOGIN 0x6001
#define E_FAIL 0x100
#define E_TIMEOUT 0x101
#define E_DUP 0x102
#define E_EXIST 0x103
#define E_NOTEXIST 0x104
#define E_READY 0x105
#define E_NOTREADY 0x106
#define E_CLOSED 0x107

