#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "platform.h"
#include "application.h"
#include "hash.h"

GX_NS_USING

class Player
{
public:
	Player();
	~Player();

private:
	uint32_t uid_;
	std::string name_;
	uint64_t    session_key_;
};

class PlayerMgr
{

public:
	static PlayerMgr* instance() {
		static PlayerMgr* obj = nullptr;
		if (obj == nullptr) {
			obj = new PlayerMgr();
		}
		return obj;
	}
	~PlayerMgr();
	uint64_t make_session_key() noexcept;
private:
	PlayerMgr();
};

#endif