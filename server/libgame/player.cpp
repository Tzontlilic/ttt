#include "player.h"
#include "evpp/timestamp.h"
#include "hash.h"

Player::Player()
{
}

Player::~Player()
{
}

PlayerMgr::PlayerMgr()
{
}

PlayerMgr::~PlayerMgr()
{
}

uint64_t PlayerMgr::make_session_key() noexcept {
	uint64_t t = evpp::Timestamp::Now().UnixMicro();
	long r = std::rand();
	return hash_iterative(&t, sizeof(t), hash_iterative(&r, sizeof(r)));
}
