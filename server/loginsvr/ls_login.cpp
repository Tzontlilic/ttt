#include "loginsvr.h"
#include "loginsvr/ls_login.h"
#include "agentsvr/as_login.h"
#include "dbsvr/db_account.h"

int LS_LoginAccount::execute() {
	std::shared_ptr<DB_AccountQuery> dbquery = std::make_shared<DB_AccountQuery>(true);
	dbquery->req.user = req.user;
	call(dbquery);
	return 0;
}

int DB_AccountQuery::execute() {
	if (rsp.rc == 0) {
		std::shared_ptr<AS_Login> aslogin = std::make_shared<AS_Login>(true);
		aslogin->req.id = rsp.uid;
		call(aslogin);
	}
	else {
		conn->Close();
	}
	return 0;
}

int AS_Login::execute() {
	if (rsp.rc == 0) {
		LS_LoginAccountRsp login_rsp;
		login_rsp.uid  = req.id;
		login_rsp.host = rsp.host;
		login_rsp.port = rsp.port;
		login_rsp.key  = rsp.key;
		auto client_conn = conn;
		send(&login_rsp, seq(), conn.get());
	}
	else {
		conn->Close();
	}
	return 0;
}