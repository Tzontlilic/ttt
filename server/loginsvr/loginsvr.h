#ifndef __LOGINSVR_H__
#define __LOGINSVR_H__

#include "loginsvr/ls_login.h"
#include "loginsvr/ls_register.h"

#define REGISTER_PROTOCOL() {   \
	auto mgr = ServletMgr::instance();  \
	mgr->register_servlet(new LS_LoginAccount(true));  \
	mgr->register_servlet(new LS_Register(true));  \
}

#endif  