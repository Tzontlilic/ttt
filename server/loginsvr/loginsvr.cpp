#include <thread>
#include "application.h"
#include "loginsvr.h"
#include "evpp/event_loop.h"

int main() {
	//google::InitGoogleLogging("game");
	//google::SetLogFilenameExtension(".text");
	//FLAGS_log_dir = "./";
	REGISTER_PROTOCOL();
	the_app = std::make_unique<Application>(SERVLET_LOGIN);
	if (!the_app->init()) {
		the_app.reset(nullptr);
		std::chrono::milliseconds dura(1000);
		std::this_thread::sleep_for(dura);
		return -1;
	}

	the_app->run();
	return 0;
}

#ifdef _WIN32
#include "winmain-inl.h"
#endif