#include "loginsvr.h"
#include "loginsvr/ls_register.h"
#include "dbsvr/db_account.h"

int LS_Register::execute() {
	std::shared_ptr<DB_AccountRegister> dbreg= std::make_shared<DB_AccountRegister>(true);
	dbreg->req.user		= req.user;
	dbreg->req.nickname = req.nickname;
	dbreg->req.platform = req.platform;
	call(dbreg);
	return 0;
}

int DB_AccountRegister::execute() {
	req;
	if (rsp.rc == 0) {
		LOG_INFO << "register success";
	}
	else {
		LOG_INFO << "register failed";
	}
	return 0;
}