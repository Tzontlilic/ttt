local outerip = "127.0.0.1"
local innerip = "127.0.0.1"
the_network = {
    login       = outerip .. ":3100",
    agentclient = outerip .. ":3200",
    mapclient   = outerip .. ":3300",
    agent       = innerip .. ":3400",
    map         = innerip .. ":3500",
    world       = innerip .. ":3600",
    db          = innerip .. ":3700",
    agentarr = {
        [1] = innerip .. ":3400",
        -- [2] = "192.168.254.1:3400",
        -- [3] = "192.168.254.2:3400",
        -- ...
    },
    maparr = {
        [1] = innerip .. ":3500",
        -- [2] = "192.168.254.1:3500",
        -- [3] = "192.168.254.2:3500",
        -- ...
    }
}

the_listen = {
    login = {0, "login"},
    agent = {0, "agent", "agentclient"},
    map   = {0, "map", "mapclient"},
    world = {0, "world"},
    db    = {0, "db"}
}

the_connect = {
    login = {"agentarr", "world", "db"},
    agent = {"maparr", "world", "db"},
    map   = {"world"}
}
