#ifndef __AGENTSVR_H__
#define __AGENTSVR_H__

#include "agentsvr/as_login.h"
#include "agentsvr/cl_login.h"

#define REGISTER_PROTOCOL() {   \
	auto mgr = ServletMgr::instance();  \
	mgr->register_servlet(new AS_Login(false));  \
	mgr->register_servlet(new CL_Login(true));  \
}

#endif