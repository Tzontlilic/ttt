#include "agentsvr/cl_login.h"
#include "dbsvr/db_login.h"
#include "agentsvr/cl_notify.h"

int CL_Login::execute() {
	//check key

	auto dbload = std::make_shared<DB_Load>(true);
	dbload->req.id = req.uid;
	call(dbload);
	return 0;
}

int DB_Load::execute() {
	req;
	rsp;

	CL_NotifyItemsReq itemreq;
	itemreq.items = std::move(rsp.bag);
	send(&itemreq, conn.get());

	CL_NotifyValuesReq valuereq;
	valuereq.values = std::move(rsp.values);
	send(&valuereq, conn.get());

	CL_LoginRsp cl_rsp;
	cl_rsp.name = rsp.name;
	send(&cl_rsp, seq(), conn.get());
	return 0;
}