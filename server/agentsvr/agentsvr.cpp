#include <thread>
#include "agentsvr.h"
#include "application.h"

int main() {
	//google::InitGoogleLogging("game");
	//google::SetLogFilenameExtension(".log");
	//FLAGS_log_dir = "./";

	REGISTER_PROTOCOL();

	the_app = std::make_unique<Application>(SERVLET_AGENT);
	the_app->ap(SERVLET_CLIENT);
	if (!the_app->init()) {
		the_app.reset(nullptr);
		std::chrono::milliseconds dura(1000);
		std::this_thread::sleep_for(dura);
		return -1;
	}
	the_app->run();
	return 0;
}

#ifdef _WIN32
#include "winmain-inl.h"
#endif
