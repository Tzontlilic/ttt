#include "agentsvr/as_login.h"
#include "player.h"

int AS_Login::execute() {
	auto addr = the_network->addr(SERVLET_CLIENT);
	rsp.host = addr.ip;
	rsp.port = addr.port;
	rsp.key = PlayerMgr::instance()->make_session_key();

	LOG_INFO << "AS_Login coming " << ";";
	return 0;
}