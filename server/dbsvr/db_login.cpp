#include "dbsvr.h"
#include "dbsvr/db_login.h"

int DB_Load::execute() {
	auto res = the_sqls._queryPlayer.query(req.id);
	if (!res->fetch()) {
		rsp.rc = E_NOTEXIST;
		return rsp.rc;
	}
	rsp.name << res;
	rsp.login_time << res;
	rsp.logout_time << res;

	auto value_res = the_sqls._queryValue.query(req.id);
	while (value_res->fetch()) {
		rsp.values.emplace_back();
		auto& item = rsp.values.back();
		item.id << value_res;
		item.value << value_res;
	}
	auto bag_res = the_sqls._queryBag.query(req.id);
	while (bag_res->fetch()) {
		rsp.bag.emplace_back();
		auto& item = rsp.bag.back();
		item.id << bag_res;
		item.count << bag_res;
	}
	return 0;
}

int DB_Login::execute() {
	return 0;
}
