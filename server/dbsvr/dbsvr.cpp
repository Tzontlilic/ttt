#include "platform.h"
#include "evpp/log_config.h"
#include "dbsvr.h"
#include "application.h"

#include "mysql.h"


DB_ServletSQL the_sqls;
std::atomic<uint32_t> the_max_uid;

int main() {

	REGISTER_PROTOCOL();

	std::shared_ptr<MySQL> mysql_game = std::make_shared<MySQL>(
		"127.0.0.1",
		3306,
		"root",
		"123456",
		"game");

	if (!mysql_game->connect()) {
		LOG_ERROR << "connect to game database false, " << mysql_game->errorMsg();
		return 1;
	}
	else {
		LOG_ERROR << "connect to game database ok. ";
	}

	if (!StatementContainer::instance()->prepare(mysql_game)) {
		LOG_ERROR << "mysql game prepare statement failed.";
		return 1;
	}
	the_sqls._setUTF8.exec();

	auto res = the_sqls._queryMaxid.query();
	if (res->fetch()) {
		uint32_t id;
		id << res;
		the_max_uid = id;
	}
	else {
		the_max_uid = 1024;
	}

	the_app = std::make_unique<Application>(SERVLET_DB);
	if (!the_app->init()) {
		the_app.reset(nullptr);
		std::chrono::milliseconds dura(1000);
		std::this_thread::sleep_for(dura);
		return -1;
	}
	the_app->run();

	return 0;
}

#ifdef _WIN32
#include "winmain-inl.h"
#endif
