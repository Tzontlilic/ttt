#include "dbsvr.h"
#include "dbsvr/db_account.h"
#include "libgame/g_defines.h"
#include "evpp/timestamp.h"

int DB_AccountQuery::execute() {
	auto res = the_sqls._queryAccount.query(req.user.c_str());
	if (res->fetch()) {
		rsp.uid << res;
		return 0;
	}
	else {
		rsp.rc = E_FAIL;
		return rsp.rc;
	}

}

int DB_AccountRegister::execute() {
	auto res = the_sqls._queryAccount.query(req.user.c_str());
	if (res->fetch()) {
		rsp.rc = E_FAIL;
		return rsp.rc;
	}

	the_sqls._registerAccount.exec(req.user.c_str(),  evpp::Timestamp::Now().Unix(), req.platform);
	unsigned id = 0;
	auto id_rs = the_sqls._queryId.query();
	id_rs->fetch();
	id << id_rs;

	unsigned uid = ++the_max_uid;
	the_sqls._insertPlayer.exec(id, uid, req.nickname.c_str());
	the_sqls._insertValue.exec(uid, G_VALUE_LEVEL, 1);
	the_sqls._insertBag.exec(uid, 1001, 1);
	return 0;
}