#ifndef __DBSVR_H__
#define __DBSVR_H__

#include <atomic>
#include "dbsvr/db_account.h"
#include "dbsvr/db_login.h"
#include "mysql.h"
GX_NS_USING;

#define REGISTER_PROTOCOL() {   \
	auto mgr = ServletMgr::instance();  \
	mgr->register_servlet(new DB_AccountQuery(false));  \
	mgr->register_servlet(new DB_AccountRegister(false));  \
	mgr->register_servlet(new DB_Load(false));  \
	mgr->register_servlet(new DB_Login(false));  \
}

struct DB_ServletSQL {
	//game sql
	GX_STMT(_queryAccount,
		"select uid from player where account=(select id from account where user=?);",
		const char*);
	GX_STMT(_registerAccount,
		"insert into account(user, create_time, platform) values(?,?,?);",
		const char*, uint64_t, unsigned);
	GX_STMT(_insertPlayer,
		"insert into player(account, uid, name) values(?,?,?);",
		unsigned, unsigned, const char*);
	GX_STMT(_insertValue,
		"insert into value (uid, id, value) values (?, ?, ?);",
		unsigned, unsigned, unsigned);
	GX_STMT(_insertBag, "insert into bag (uid, id, count) values (?, ?, ?);",
		unsigned, unsigned, unsigned);
	GX_STMT(_queryMaxid, "select ifnull(max(uid), 1024) as maxx from player;"); 
	GX_STMT(_queryPlayer, "select name,login_time,logout_time from player where uid=?;",
		unsigned); 
	GX_STMT(_queryValue, "select id,value from value where uid=?;",
		unsigned); 
	GX_STMT(_queryBag, "select id,count from bag where uid=?;",
		unsigned); 

	//common sql
	GX_STMT(_queryId, "select last_insert_id();");
	GX_STMT(_setUTF8, "set names utf8mb4;");
	GX_STMT(_setUniqueChecks0, "SET unique_checks=0;");
	GX_STMT(_setUniqueChecks1, "SET unique_checks=1;");
	GX_STMT(_ping, "select 1;");
};

extern DB_ServletSQL the_sqls;
extern std::atomic<uint32_t> the_max_uid;

#endif