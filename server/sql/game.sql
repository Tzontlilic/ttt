--
-- Table structure for table `account`
--
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
 `id`            int(10)      unsigned   NOT NULL   AUTO_INCREMENT,
 `user`          varchar(64)             NOT NULL,
 `create_time`   bigint(20)   unsigned   NOT NULL,
 `platform`      int(10)      unsigned   NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Table structure for table `player`
--
DROP TABLE IF EXISTS `player`;
CREATE TABLE `player` (
 `account`       int(10)       unsigned   NOT NULL,
 `server`        int(10)       unsigned   NOT NULL,
 `uid`           int(10)       unsigned   NOT NULL,
 `name`          varchar(32)              NOT NULL,
 `login_time`    bigint(20)    unsigned   DEFAULT '0',
 `logout_time`   bigint(20)    unsigned   DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `account_id` (`account`),
  UNIQUE KEY `player_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `value`
--
DROP TABLE IF EXISTS `value`;
CREATE TABLE `value` (
 `uid`           int(10)       unsigned   NOT NULL,
 `id`            tinyint(4)    unsigned   NOT NULL,
 `value`         int(20)       unsigned   NOT NULL,
  PRIMARY KEY (`uid`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `bag`
--
DROP TABLE IF EXISTS `bag`;
CREATE TABLE `bag` (
 `uid`           int(10)       unsigned   NOT NULL,
 `id`            int(10)       unsigned   NOT NULL,
 `count`         int(10)       unsigned   NOT NULL,
  PRIMARY KEY (`uid`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
