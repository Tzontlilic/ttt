@echo off
cd /d %~dp0

if not exist ..\rpc (mkdir ..\rpc)

setlocal enabledelayedexpansion

for /r %%i in (*.sdl) do (
 set v=%%~dpi
 call :loop
 set parent=!%~dp0!
 set parent=!parent:~0,-4!
 set son=!v:%~dp0=!
 if not exist !parent!rpc\!son! (mkdir !parent!rpc\!son!)
)

for /r %%i in (*.sdl) do (
 set v=%%i
 call :loop
 set in=!v:%~dp0=!
 set out=..\rpc\!in!
 set out=!out:.sdl=.h!
 echo compile !in!
 sscc -i !in! -o !out! -l cpp
)

setlocal disabledelayedexpansion
pause

:loop
if "!v:~-1!"==" " set "v=!v:~0,-1!" & goto loop
