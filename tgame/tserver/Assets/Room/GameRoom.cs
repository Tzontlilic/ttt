﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tnet;
using UnityEngine;



public class GameRoom
{
    public enum RoomState {
        prepare,
        running,
        pause,
        stop,
    }
    public RoomState state;
    public int roomId;
    public GameRoom(int _roomId)
    {
        roomId = _roomId;
        state = RoomState.prepare;
        curGameActions[0] = new List<G_StepOpt>();
    }

    List<Player> roomPlayers = new List<Player>();
    public void AddPlayer(Player _player)
    {
        if (roomPlayers.Count == 0)
            roomId = _player.getID();
        if(!roomPlayers.Contains(_player))
            roomPlayers.Add(_player);
        Log.Debug("AddPlayer Room Player Size = " + roomPlayers.Count);
        SyncRoomPlayers();
    }

    public bool RemovePlayer(Player _player)
    {
        if (roomPlayers.Contains(_player))
        {
            roomPlayers.Remove(_player);
            SyncRoomPlayers();
            Log.Debug("RemovePlayer Room Player Size = " + roomPlayers.Count);
            if (roomPlayers.Count == 0)
            {
                state = RoomState.prepare;
            }
            return true;
        }
        return false;
    }


    public void SyncRoomPlayers()
    {
        CL_NotifySyncRoomInfoReq req = new CL_NotifySyncRoomInfoReq();
        req.players = new List<G_RoomPlayerOpt>();
        req.roomId = (int)roomId;
        for ( int i =0; i < roomPlayers.Count; i++)
        {
            var opt = new G_RoomPlayerOpt();
            opt.id = roomPlayers[i].getID();
            opt.name = (roomPlayers[i] as Player).name;
            req.players.Add(opt);
        }
        byte[] bytestream = TConn.Pack(req);
        for (int j = 0; j < roomPlayers.Count; ++j)
        {
            roomPlayers[j].RawSend(bytestream);
        }
    }

    int curStepCount = 0;
    /// <summary>
    /// int is stepcount
    /// list is players operate
    /// </summary>
    public Dictionary<int, List<G_StepOpt>> curGameActions = new Dictionary<int, List<G_StepOpt>>();

    public void RcvPlayerAction(int playerId, int actionId, float[] fp)
    {
        List<G_StepOpt> stepActions = curGameActions[curStepCount];
        G_StepOpt opt;
        opt.playerId = playerId;
        opt.actionId = actionId;
        opt.fp = new List<float>();
        opt.fp.AddRange(fp);
        stepActions.Add(opt);
    }

    public void UpdateStepCount()
    {
        List<G_StepOpt> stepActions = curGameActions[curStepCount];
        for (int i = 0; i < stepActions.Count; i++)
        {
            DoStepAction(stepActions[i]);
        }
        curStepCount++;
        curGameActions[curStepCount] = new List<G_StepOpt>();
    }
    public void DoStepAction(G_StepOpt opt)
    {
        //Debug.Log(opt.playerId + " Do Action " + opt.actionId);
    }
    public void BroadcastStep()
    {
        List<G_StepOpt> stepActions = curGameActions[curStepCount];
        CL_NotifySyncStepReq req = new CL_NotifySyncStepReq();
        req.stepId = curStepCount + 1;
        req.actions = new List<G_StepOpt>();
        req.actions.AddRange(stepActions);
        byte[] bytestream = TConn.Pack(req);
        for (int j = 0; j < roomPlayers.Count; ++j)
        {
            roomPlayers[j].RawSend(bytestream);
        }
        curGameActions.Remove(curStepCount - 1);
    }
    public bool checkAllPlayerReady()
    {

        for (int j = 0; j < roomPlayers.Count; ++j)
        {
            if (!roomPlayers[j].ready)
            {
                return false;
            }
        }
        return true;
    }

    public void Broadcast(ISerial req)
    {
        byte[] bytestream = TConn.Pack(req);
        for (int j = 0; j < roomPlayers.Count; ++j)
        {
            roomPlayers[j].RawSend(bytestream);
        }
    }

    public void BeginGame()
    {
        main.gm.state = RoomState.running;
        curStepCount = 0;
        curGameActions.Clear();
        curGameActions[0] = new List<G_StepOpt>();
    }
}
