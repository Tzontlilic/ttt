﻿using System.Collections.Generic;
using System;

namespace Tnet
{
    public class MC_LoginServlet : MC_Login {
        public override int Execute(TConn conn)
        {
            Log.Debug(" MC_LoginServlet, user = " + req.user);
            if (conn.player != null)
            {
                rsp.uid = conn.player.getID();
                rsp.name = req.user;
                rsp.bag = new List<G_BagItemOpt>();
                rsp.values = new List<G_ValueOpt>();
                return 0;
            }
            int playerId = DBAction.Instance.GetPlayer(req.user);
            if (playerId < 0)
            {
                playerId = DBAction.Instance.RegisterAccount(req.user, req.deviceId, req.deviceModel);
            }
            if (playerId < 0)
            {
                return -1;
            }
            Player p = new Player(playerId);
            p.name = req.user;
            p.setConn(conn);
            conn.player = p;
            rsp.uid = playerId;
            rsp.name = p.name;

            rsp.bag = new List<G_BagItemOpt>();
            rsp.values = new List<G_ValueOpt>();
            G_BagItemOpt item;
            item.id = 2;
            item.count = 3;
            rsp.bag.Add(item);
            G_ValueOpt value;
            value.id = 5;
            value.value = 6;
            rsp.values.Add(value);
            return 0;
        }
    }
}