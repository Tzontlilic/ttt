﻿using System.Collections.Generic;

namespace Tnet
{
    public class MC_EnterSceneServlet : MC_EnterScene {
        public override int Execute(TConn conn) {
            CL_NotifyEnterSceneReq req;
            req.roomId = (int)main.gm.roomId;
            main.gm.Broadcast(req);
            return 0;
        }
    }
}
