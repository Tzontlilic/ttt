﻿using System.Collections.Generic;

namespace Tnet
{
    public class MC_ReadyLockStepServlet : MC_ReadyLockStep
    {
        public override int Execute(TConn conn)
        {
            Player p = conn.player;
            p.ready = true;
            Log.Debug(" MC_ReadyLockStepServlet, user = " + p.name);
            if (main.gm.checkAllPlayerReady())
            {
                Log.Debug("All PlayerReady, GameRoom running ");
                CL_NotifyStartGameReq req;
                req.roomId = (int)main.gm.roomId;
                main.gm.Broadcast(req);
                main.gm.BeginGame();
            }
            return 0;
        }
    }
}
