﻿using System.Collections.Generic;

namespace Tnet
{
    public class MC_LeaveRoomServlet : MC_LeaveRoom
    {
        public override int Execute(TConn conn)
        {
            rsp.roomid = req.roomid;
            rsp.uid = req.uid;
            if (main.gm != null && req.roomid == main.gm.roomId )
            {
                if( main.gm.RemovePlayer(conn.player) )
                    rsp._isOK = 1;
                else
                    rsp._isOK = 0;
            }
            return 0;
        }
    }
}
