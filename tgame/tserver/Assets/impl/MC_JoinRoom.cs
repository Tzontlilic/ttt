﻿using System.Collections.Generic;

namespace Tnet
{
    public class MC_JoinRoomServlet : MC_JoinRoom {
        public override int Execute(TConn conn) {
            if (main.gm == null)
            {
                Log.Debug("Join create new room ok, roomId = " + req.uid);
                main.gm = new GameRoom(req.uid);
            }
            if(main.gm.state == GameRoom.RoomState.running)
            {
                rsp.state = 0;
            }
            else
            {
                rsp.state = 1;
                main.gm.AddPlayer(conn.player);
            }
            rsp.roomid = main.gm.roomId;
            return 0;
        }
    }
}
