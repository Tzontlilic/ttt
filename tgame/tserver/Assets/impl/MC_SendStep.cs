﻿using System.Collections.Generic;

namespace Tnet
{
    public class MC_SendStepServlet : MC_SendStep {
        public override int Execute(TConn conn) {
            //Log.Debug(" recv send step ok, actionId = "+req.actionId);
            main.gm.RcvPlayerAction(conn.player.getID(), req.actionId, req.fp.ToArray());
            return 0;
        }
    }
}
