﻿using System.Collections.Generic;

namespace Tnet
{
    public class MC_CreateRoomServlet : MC_CreateRoom {
        public override int Execute(TConn conn)
        {
            if (main.gm == null)
            {
                main.gm = new GameRoom(req.uid);
            }
            main.gm.AddPlayer(conn.player);
            Log.Debug(" create room ok , id = "+req.uid);
            return 0;
        }
    }
}