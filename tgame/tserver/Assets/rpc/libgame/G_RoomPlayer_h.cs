
namespace Tnet {
    public struct G_RoomPlayerOpt : ISerial {
        public int id;
        public string name;
        public int teamId;
        
        public ushort MsgId() {
            return 0;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.id);
            buf.WriteString(this.name);
            buf.WriteInt(this.teamId);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.id = buf.ReadInt();
            this.name = buf.ReadString();
            this.teamId = buf.ReadInt();
        }
        
    }
}

