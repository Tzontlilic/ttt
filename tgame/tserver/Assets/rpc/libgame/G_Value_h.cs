
namespace Tnet {
    public struct G_ValueOpt : ISerial {
        public byte id;
        public int value;
        
        public ushort MsgId() {
            return 0;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteByte(this.id);
            buf.WriteInt(this.value);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.id = buf.ReadByte();
            this.value = buf.ReadInt();
        }
        
    }
}

