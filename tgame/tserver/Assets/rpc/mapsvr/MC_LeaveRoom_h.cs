using System;
using System.Collections.Generic;


namespace Tnet {
    public struct MC_LeaveRoomReq : ISerial {
        public int uid;
        public int roomid;
        
        public ushort MsgId() {
            return Message.MC_LEAVE_ROOM;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.uid);
            buf.WriteInt(this.roomid);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.uid = buf.ReadInt();
            this.roomid = buf.ReadInt();
        }
        
    }
    public struct MC_LeaveRoomRsp : ISerial {
        public int uid;
        public int roomid;
        public int _isOK;
        
        public ushort MsgId() {
            return Message.MC_LEAVE_ROOM;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.uid);
            buf.WriteInt(this.roomid);
            buf.WriteInt(this._isOK);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.uid = buf.ReadInt();
            this.roomid = buf.ReadInt();
            this._isOK = buf.ReadInt();
        }
        
    }
    public abstract class MC_LeaveRoom : Servlet {
        public MC_LeaveRoomReq req;
        public MC_LeaveRoomRsp rsp;
        private Action<MC_LeaveRoom> cb;
        public MC_LeaveRoom(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.MC_LEAVE_ROOM;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return rsp;
        }
        public override void SerialResponse(ByteBuffer buf) {
            rsp.Serial(buf);
        }
        public override void UnserialResponse(ByteBuffer buf) {
            rsp.Unserial(buf);
        }
        public override int OnBack() {
            if (cb != null) {
                cb(this);
            }
            return 0;
        }
        public Action<MC_LeaveRoom> Cb {
            set {
                cb = value;
            }
        }
    }
}

