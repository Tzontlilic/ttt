using System;
using System.Collections.Generic;


namespace Tnet {
    public struct MC_StartRoomReq : ISerial {
        public int roomid;
        
        public ushort MsgId() {
            return Message.MC_START_ROOM;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.roomid);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.roomid = buf.ReadInt();
        }
        
    }
    public abstract class MC_StartRoom : Servlet {
        public MC_StartRoomReq req;
        public MC_StartRoom(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.MC_START_ROOM;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
}

