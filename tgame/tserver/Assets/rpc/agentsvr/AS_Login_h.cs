using System;


namespace Tnet {
    public struct CL_LoginReq : ISerial {
        public uint uid;
        public ulong key;
        
        public ushort MsgId() {
            return Message.AS_LOGIN;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteUint(this.uid);
            buf.WriteUlong(this.key);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.uid = buf.ReadUint();
            this.key = buf.ReadUlong();
        }
        
    }
    public struct CL_LoginRsp : ISerial {
        public string name;
        
        public ushort MsgId() {
            return Message.AS_LOGIN;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteString(this.name);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.name = buf.ReadString();
        }
        
    }
    public abstract class CL_Login : Servlet {
        public CL_LoginReq req;
        public CL_LoginRsp rsp;
        private Action<CL_Login> cb;
        public CL_Login(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.AS_LOGIN;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return rsp;
        }
        public override void SerialResponse(ByteBuffer buf) {
            rsp.Serial(buf);
        }
        public override void UnserialResponse(ByteBuffer buf) {
            rsp.Unserial(buf);
        }
        public override int OnBack() {
            if (cb != null) {
                cb(this);
            }
            return 0;
        }
        public Action<CL_Login> Cb {
            set {
                cb = value;
            }
        }
    }
}

