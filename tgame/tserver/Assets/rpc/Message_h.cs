
namespace Tnet {
    public class Message {
        public const int SERVLET_CLIENT = 0x0;
        public const int SERVLET_LOGIN = 0x1;
        public const int SERVLET_DB = 0x2;
        public const int SERVLET_AGENT = 0x3;
        public const int SERVLET_MAP = 0x4;
        public const int SERVLET_MAP_CLIENT = 0x5;
        public const int SERVLET_WORLD = 0x6;
        public const int SERVLET_UNKNOWN = 0x7;
        public const int LS_BEGIN = 0x1000;
        public const int LS_LOGIN_ACCOUNT = 0x1000;
        public const int LS_LOGIN_SESSION = 0x1001;
        public const int LS_REGISTER = 0x1002;
        public const int LS_AUTH = 0x1003;
        public const int CL_BEGIN = 0x0;
        public const int CL_NOTIFY_BEGIN = 0x800;
        public const int CL_NOTIFY_VALUES = 0x801;
        public const int CL_NOTIFY_ITEMS = 0x802;
        public const int CL_NOTIFY_SYNCROOMINFO = 0x803;
        public const int CL_NOTIFY_SYNCSTEP = 0x804;
        public const int CL_NOTIFY_ENTERSCENE = 0x805;
        public const int CL_NOTIFY_STARTGAME = 0x806;
        public const int AS_BEGIN = 0x3000;
        public const int AS_LOGIN = 0x3001;
        public const int AS_REGISTER = 0x3002;
        public const int AS_AUTH = 0x3003;
        public const int DB_BEGIN = 0x2000;
        public const int DB_ACCOUNT_QUERY = 0x2001;
        public const int DB_ACCOUNT_REGISTER = 0x2002;
        public const int DB_LOAD = 0x2003;
        public const int DB_LOGIN = 0x2004;
        public const int MS_BEGIN = 0x4000;
        public const int MS_LOGIN = 0x4001;
        public const int MC_BEGIN = 0x5000;
        public const int MC_LOGIN = 0x5001;
        public const int MC_CREATE_ROOM = 0x5002;
        public const int MC_ENTER_SCENE = 0x5003;
        public const int MC_READY_LOCKSTEP = 0x5004;
        public const int MC_LOCKSTEP_BEGIN = 0x5800;
        public const int MC_SEND_STEP = 0x5801;
        public const int MC_JOIN_ROOM = 0x5802;
        public const int MC_LEAVE_ROOM = 0x5803;
        public const int MC_START_ROOM = 0x5804;
        public const int MC_LOCKSTEP_END = 0x5805;
        public const int WS_BEGIN = 0x6000;
        public const int WS_LOGIN = 0x6001;
    }
}

