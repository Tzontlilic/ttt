﻿namespace Tnet
{
    public interface ISerial {
        void Serial(ByteBuffer buf);
        void Unserial(ByteBuffer buf);
        ushort MsgId();
    }
}