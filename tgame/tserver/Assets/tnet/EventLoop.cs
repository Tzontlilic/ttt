﻿using System.Collections.Generic;
using System.Threading;

namespace Tnet
{
    public enum EventType : int {
        connectOk,
        connectFail,
        disconnect,
        receiveData
    }
    public class EventData {
        public EventData(int type, TConn target, ByteBuffer buf = null) {
            this.type = type;
            this.target = target;
            this.buf = buf;
        }
        public int type;
        public TConn target;
        public ByteBuffer buf;
    }
    public class EventLoop {
        private AutoResetEvent signal = new AutoResetEvent(false);
        private readonly object mutex = new object();
        private Queue<EventData> msgQueue = new Queue<EventData>();
        private bool newThread = true;

        Thread thread;
        public EventLoop(bool b = true) {
            newThread = b;
            if (newThread) {
                thread = new Thread(OnMessage);
                thread.IsBackground = true;
                thread.Start();
            }
        }
        public void Notify() {
            signal.Set();
        }
        public void PushMsg(EventData x) {
            lock (mutex) {
                msgQueue.Enqueue(x);
            }
            if (newThread) {
                signal.Set();
            }
        }

        private void OnMessage() {
            while (true) {
                signal.WaitOne();
                Update();
            }
        }
        public void Update() {
            while (msgQueue.Count > 0) {
                EventData msg = null;
                lock (mutex) {
                    if (msgQueue.Count > 0) {
                        msg = msgQueue.Dequeue();
                    }
                }
                try {
                    if (msg != null) {
                        if (msg.type == (int)EventType.connectOk) {
                            msg.target.OnConnectOk();
                        }
                        else if(msg.type == (int)EventType.connectFail) {
                            msg.target.OnConnectFail();
                        }
                        else if(msg.type == (int)EventType.disconnect) {
                            msg.target.OnDisconnect();
                            msg.target.OnRemove();
                            if (newThread) {
                                TServer.DelConn(msg.target);
                            }
                        }
                        else if (msg.type > (int)EventType.receiveData) {
                            msg.target.ProcMsg(msg.type, msg.buf);
                        }
                        else {
                            Log.Debug("unknow message type:"+msg.type);
                        }
                    }
                }
                catch (System.Exception ex) {
                    Log.Error(ex.ToString());
                }
            }
        }
    }
}