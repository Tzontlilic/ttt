﻿using System;
using System.Collections.Generic;
using Tnet;

public class Player : IStepUnit
{
    int playerId;
    private TConn conn = null;
    public string name = null;
    public bool ready = false;
    public Player(int _id)
    {
        playerId = _id;
    }
    public void setConn(TConn _conn)
    {
        conn = _conn;
        conn.disconnect = disconnect;
    }
    private void disconnect()
    {
        conn = null;
        if (main.gm != null)
        {
            main.gm.RemovePlayer(this);
        }
    }
    public void Send(ISerial req)
    {
        if (conn != null)
        {
            conn.Send(req);
        }
    }
    public void RawSend(byte[] bytes)
    {
        if (conn != null)
        {
            conn.RawSend(bytes);
        }
    }
    public void Init()
    {
    }

    public int getID()
    {
        return playerId; 
    }

    public void StepUpdate()
    {
        
    }

    public void Release()
    {
       
    }

    public void AddPendingAction(string _act)
    {
        
    }
}
