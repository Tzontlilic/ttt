﻿using UnityEngine;
using Mono.Data.Sqlite;
using System;
using System.IO;
using Tnet;

class SqliteDB
{
    private SqliteConnection dbConnection;
    private string dbpath = "game.sqlite";
    private SqliteDB() { }
    private static SqliteDB instance;
    public static SqliteDB Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new SqliteDB();
            }
            return instance;
        }
    }
    public void OpenDB()
    {
        try
        {
            bool exists = File.Exists(dbpath);
            if (!exists)
            {
                SqliteConnection.CreateFile(dbpath);
            }
            dbConnection = new SqliteConnection("Data Source=" + dbpath);
            dbConnection.Open();
            Log.Info("Connected to db");
            if (!exists)
            {
                CreateTable();
            }
        }
        catch (Exception e)
        {
            Log.Error(e.ToString());
        }
    }
    public SqliteConnection DBConn()
    {
        return dbConnection;
    }
    /// <summary>
    /// 关闭连接
    /// </summary>
    public void Close()
    {
        if (dbConnection != null)
        {
            dbConnection.Close();
        }
        dbConnection = null;
        Debug.Log("Disconnected from db.");
    }
    public void ExecuteQuery(string sql)
    {
        SqliteCommand dbCommand = new SqliteCommand(sql, dbConnection);
        try
        {
            dbCommand.ExecuteNonQuery();
            Log.Info("create table");
        }
        catch (Exception ex)
        {
            Log.Error(ex.ToString());
        }
    }

    // 创建表
    public void CreateTable()
    {
        /** account table **/
        string sql = @"CREATE TABLE account(
                id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                user        VARCHAR(60) NOT NULL UNIQUE,
                deviceid    VARCHAR(128) NOT NULL,
                devicemodel VARCHAR(128) NOT NULL,
                createtime  BIGING      NOT NULL);";
        ExecuteQuery(sql);

        /** player table **/
        sql = @"CREATE TABLE player(
                id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                account     INT NOT NULL,
                name        VARCHAR(60) NOT NULL UNIQUE,
                logintime   BIGINT NOT NULL);";
        ExecuteQuery(sql);

        /** bag table **/
        sql = @"CREATE TABLE bag(
                uid         INT NOT NULL,  
                id          INT NOT NULL,
                count       INT NOT NULL,
                constraint pk primary key(uid, id));";
        ExecuteQuery(sql);

        /** value table **/
        sql = @"CREATE TABLE value(
                uid         INT NOT NULL,  
                id          INT NOT NULL,
                value       INT NOT NULL,
                constraint pk primary key(uid, id));";
        ExecuteQuery(sql);
    }
}