﻿using System;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using Tnet;
public class DBAction
{
    private SqliteCommand insertAccount = new SqliteCommand("insert into account(user, deviceid, devicemodel, createtime) values(@COL1,@COL2,@COL3,@COL4);", SqliteDB.Instance.DBConn());
    private SqliteCommand insertPlayer = new SqliteCommand("insert into player(account, name, logintime) values(@COL1,@COL2,@COL3);", SqliteDB.Instance.DBConn());
    private SqliteCommand queryAccount = new SqliteCommand("select id, user, createtime from account where user=@COL1;", SqliteDB.Instance.DBConn());
    private SqliteCommand queryPlayer = new SqliteCommand("select id, name, logintime from player where account=@COL1;", SqliteDB.Instance.DBConn());
    private SqliteCommand queryLastAccountId = new SqliteCommand("select max(id) as maxid from account;", SqliteDB.Instance.DBConn());
    private SqliteCommand queryLastPlayerId = new SqliteCommand("select max(id) as maxid from player;", SqliteDB.Instance.DBConn());
    private SqliteCommand updateItem = new SqliteCommand("replace into bag(uid, id, count) values (?, ?, ?)", SqliteDB.Instance.DBConn());
    private SqliteCommand updateValue = new SqliteCommand("replace into value(uid, id, value) values (?, ?, ?)", SqliteDB.Instance.DBConn());
    public void Prepare()
    {
        insertAccount.Prepare();
        insertPlayer.Prepare();
        queryAccount.Prepare();
        queryPlayer.Prepare();
        queryLastAccountId.Prepare();
        queryLastPlayerId.Prepare();
        updateItem.Prepare();
        updateValue.Prepare();
    }
    private DBAction() { }
    private static DBAction instance;
    public static DBAction Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new DBAction();
            }
            return instance;
        }
    }
    public int GetPlayer(string name)
    {
        queryAccount.Parameters.AddWithValue("@COL1", name);
        int pid = -1;
        try
        {
            SqliteDataReader res = queryAccount.ExecuteReader();
            if (res.Read())
            {
                int accountId = res.GetInt32(0);
                queryPlayer.Parameters.AddWithValue("@COL1", accountId);
                res.Close();
                res = queryPlayer.ExecuteReader();
                if (res.Read()) {
                    pid = res.GetInt32(0);
                }
            }
            res.Close();
        }
        catch (Exception ex)
        {
            Log.Error(ex.ToString());
            throw new SqliteException(ex.Message);
        }
        return pid;
    }

    public int RegisterAccount(string user, string deviceId, string deviceModel)
    {
        DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
        long timestamp = (long)(DateTime.Now - startTime).TotalSeconds;
        insertAccount.Parameters.AddWithValue("@COL1", user);
        insertAccount.Parameters.AddWithValue("@COL2", deviceId);
        insertAccount.Parameters.AddWithValue("@COL3", deviceModel);
        insertAccount.Parameters.AddWithValue("@COL4", timestamp);

        string nickname = user;
        insertPlayer.Parameters.AddWithValue("@COL2", nickname);
        insertPlayer.Parameters.AddWithValue("@COL3", (long)timestamp);
        int pid = -1;
        try
        {
            insertAccount.ExecuteNonQuery();
            SqliteDataReader res = queryLastAccountId.ExecuteReader();
            if (res.Read())
            {
                int lastid = res.GetInt32(0);
                insertPlayer.Parameters.AddWithValue("@COL1", lastid);
                insertPlayer.ExecuteNonQuery();
                res.Close();
                res = queryLastPlayerId.ExecuteReader();
                if (res.Read()) {
                    pid = res.GetInt32(0);
                    //TEST 添加默认道具，初始属性值
                    UpdateOneItem(pid, 1001, 1);
                    UpdateOneItem(pid, 1002, 1);
                    UpdateOneValue(pid, G_Defines.G_VALUE_LEVEL, 1);
                    UpdateOneValue(pid, G_Defines.G_VALUE_EXP, 0);
                    UpdateOneValue(pid, G_Defines.G_VALUE_GOLD, 1000);
                }
            }
            res.Close();
        }
        catch (Exception ex)
        {
            Log.Error(ex.ToString());
        }
        return pid;
    }
    public void UpdateOneItem(int uid, int id, int count)
    {
        updateItem.Parameters.AddWithValue("@COL1", uid);
        updateItem.Parameters.AddWithValue("@COL2", id);
        updateItem.Parameters.AddWithValue("@COL3", count);
        try
        {
            updateItem.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            Log.Error(ex.ToString());
        }
    }
    public void UpdateOneValue(int uid, int id, int value)
    {
        updateValue.Parameters.AddWithValue("@COL1", uid);
        updateValue.Parameters.AddWithValue("@COL2", id);
        updateValue.Parameters.AddWithValue("@COL3", value);
        try
        {
            updateValue.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            Log.Error(ex.ToString());
        }
    }
    public void UpdateItems(int uid, G_BagItemOpt[] items)
    {
        foreach (G_BagItemOpt opt in items)
        {
            UpdateOneItem(uid, opt.id, opt.count);
        }
    }
    public void UpdateValues(int uid, G_ValueOpt[] values)
    {
        foreach (G_ValueOpt opt in values)
        {
            UpdateOneValue(uid, opt.id, opt.value);
        }
    }
}