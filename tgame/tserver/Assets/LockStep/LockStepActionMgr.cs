﻿using System.Collections.Generic;
using UnityEngine;

public class LockStepActionMgr
{
    public LockStepActionInfo m_currStepActionInfo;

    public LockStepActionMgr()
    {

    }

    public void start()
    {
        m_currStepActionInfo = new LockStepActionInfo(0);
    }

    public void GameFrameTurn()
    {
        m_currStepActionInfo.ProcessActions();
    }

}

public class PendingActionInfo
{
    public List<string> m_pendingActions = new List<string>();

    public PendingActionInfo()
    {
    }

    public void addPendingAction(string _actionStr)
    {
        m_pendingActions.Add(_actionStr);
    }
}

public class LockStepActionInfo
{
    /// <summary>
    /// int used for playerID in network
    /// string used for action 
    /// </summary>
    public List<LockActionInfo> m_lockStepActions = new List<LockActionInfo>();


    public LockStepActionInfo(int _stepID)
    {
    }

    public bool ReadyForNextStep()
    {
            return true;
    }


    public void ProcessActions()
    {
    }
}


public struct LockActionInfo
{
    public uint playerID;
    public string actionStr;

    public LockActionInfo(uint _playerID, string _actionStr)
    {
        playerID = _playerID;
        actionStr = _actionStr;
    }

    public void DoAction()
    {
        Debug.Log(playerID.ToString() + " Do Action " + actionStr);
    }
}
