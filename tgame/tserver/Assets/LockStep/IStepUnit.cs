﻿
using Tnet;
public interface IStepUnit
{
    void Init();

    int getID();

    void StepUpdate();

    void Release();

    void AddPendingAction(string _act);
    void Send(ISerial req);
    void RawSend(byte[] bytes);

    string ToString();
}
