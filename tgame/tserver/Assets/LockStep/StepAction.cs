﻿
using System;
using System.Collections.Generic;

public class StepAction
{
    static bool m_bInit = false;
    public string m_sName = string.Empty;
    public static List<StepAction> m_Actions = new List<StepAction>();

    public StepAction()
    {

    }
    public StepAction(string name) { m_sName = name; }

    public virtual bool parseParam(string str) { return true; }


    public static void addLockObj(StepAction sa)
    {
        if (findAction(sa.m_sName) == null)
        {
            m_Actions.Add(sa);
        }
    }

    public static StepAction findAction(string name)
    {
        for (int i = 0; i < m_Actions.Count; i++)
        {
            if (m_Actions[i].m_sName == name)
            {
                return m_Actions[i];
            }
        }
        return null;
    }

    public virtual void Action(IStepUnit _stepUnit)
    {

    }

    public static void LoadActions()
    {

    }
}

public class DirMove : StepAction
{
    public struct Param
    {
        public float x_;
        public float y_;
        public float z_;
    };

    public Param p;
    public override bool parseParam(string str)
    {
        string[] perPar = str.Split(' ');
        p = new Param();
        p.x_ = Convert.ToSingle(perPar[0]);
        p.y_ = Convert.ToSingle(perPar[1]);
        p.z_ = Convert.ToSingle(perPar[2]);
        return true;
    }

    public override void Action(IStepUnit _stepUnit)
    {
    }
}

public class StopMove : StepAction
{    

    public override void Action(IStepUnit _stepUnit)
    {
    }
}