﻿
using System.Collections.Generic;

public class LockStepBase
{
    static bool m_bInit = false;
    public string m_sName = string.Empty;
    public static List<LockStepBase> m_LockStepObjs = new List<LockStepBase>();

    public LockStepBase()
    {

    }
    public LockStepBase(string name) { m_sName = name; }


    public static void addLockObj(LockStepBase lb)
    {
        if (findLockObj(lb.m_sName) == null)
        {
            m_LockStepObjs.Add(lb);
        }
    }

    public static LockStepBase findLockObj(string name)
    {
        for (int i = 0; i < m_LockStepObjs.Count; i++)
        {
            if (m_LockStepObjs[i].m_sName == name)
            {
                return m_LockStepObjs[i];
            }
        }
        return null;
    }

    public virtual void UpdateStep()
    {

    }
}