﻿using UnityEngine;
using Tnet;
using System.Collections.Generic;

public class main : MonoBehaviour {
    [SerializeField]
    private float initCd = 0.2f;
    private float stepCD;

    public int ActionCount = 0;

    public static GameRoom gm = null;

    EventLoop loop = new EventLoop(false);
    TServer server;
	// Use this for initialization
	void Start () {
        stepCD = initCd;
        App.Init(Message.SERVLET_MAP_CLIENT);
        RegisterServlet();
        SqliteDB.Instance.OpenDB();
        DBAction.Instance.Prepare();

        server = new TServer(loop);
        server.InitServer(4, 9527);
    }

    void RegisterServlet() {
        Servlet.Add(new MC_LoginServlet());
        Servlet.Add(new MC_CreateRoomServlet());
        Servlet.Add(new MC_JoinRoomServlet());
        Servlet.Add(new MC_LeaveRoomServlet());
        Servlet.Add(new MC_EnterSceneServlet());
        Servlet.Add(new MC_StartRoomServlet());
        Servlet.Add(new MC_ReadyLockStepServlet());
        Servlet.Add(new MC_SendStepServlet());
    }

    // Update is called once per frame
    void Update()
    {
        loop.Update();
        if (gm != null && gm.state == GameRoom.RoomState.running)
        {
            stepCD -= Time.deltaTime;
            if (stepCD < 0)
            {
                stepCD += initCd;
                gm.BroadcastStep();
                gm.UpdateStepCount();
            }
        }
        if( gm != null )
            ActionCount = gm.curGameActions.Count;
    }
    static int idx = 1;
    public void addAccount()
    {
        idx++;
        DBAction.Instance.UpdateOneItem(1, 1001 + idx, 8);
        DBAction.Instance.UpdateOneValue(1, 1001 + idx, 88);
    }
    void OnDestroy() {
        //server.quit = true;
        //server.allDone.Set();
        SqliteDB.Instance.Close();
    }
}