﻿using UnityEngine;

namespace Tnet
{
    public class CL_NotifyItemsServlet : CL_NotifyItems {
        public override int Execute(TConn conn) {
            foreach(G_BagItemOpt x in req.items) {
                Debug.Log(string.Format("id={0},count={1}", x.id, x.count));
            }
            return 0;
        }
    }
    public class CL_NotifyValuesServlet : CL_NotifyValues {
        public override int Execute(TConn conn) {
            foreach(G_ValueOpt x in req.values) {
                Debug.Log(string.Format("id={0},value={1}", x.id, x.value));
            }
            return 0;
        }
    }

    public class CL_NotifySnycStepServlet : CL_NotifySyncStep {
        public override int Execute(TConn conn) {
            foreach (G_StepOpt x in req.actions) {
                Debug.Log(string.Format("step={0}, id={1}", req.stepId, x.actionId));
            }
            return 0;
        }
    }
}