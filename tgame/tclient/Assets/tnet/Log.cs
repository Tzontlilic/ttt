﻿using System;
using System.Threading;
using System.IO;

namespace Tnet
{
    public class Log {
        private static StreamWriter w;
        public static void Init(string path) {
            w = File.AppendText(path);
        }
        public static void Debug(string str) {
            w.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss "));
            w.Write("{0:D4}", Thread.CurrentThread.ManagedThreadId);
            w.Write(" D: ");
            w.Write(str + Environment.NewLine);
            w.Flush();
        }
        public static void Info(string str) {
            w.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss "));
            w.Write("{0:D4}", Thread.CurrentThread.ManagedThreadId);
            w.Write(" I: ");
            w.Write(str + Environment.NewLine);
            w.Flush();
        }
        public static void Error(string str) {
            w.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss "));
            w.Write("{0:D4}", Thread.CurrentThread.ManagedThreadId);
            w.Write(" E: ");
            w.Write(str + Environment.NewLine);
            w.Flush();
        }
    }
}