﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace Tnet
{
    public class TServer {
        Socket listener = null;
        Thread threadWatch = null;
        int workLoopNum = 0;
        int lastLoop = 0;
        EventLoop[] loops;
        EventLoop mainLoop;
        static HashSet<TConn> connSet = new HashSet<TConn>();
        static private readonly object connMutex = new object();

        public AutoResetEvent allDone = new AutoResetEvent(false);
        public TServer(EventLoop mainLoop) {
            this.mainLoop = mainLoop;
        }
        public void InitServer(int threadNum,int port) {
            try {
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddress = null;
                foreach (IPAddress ip in ipHostInfo.AddressList) {
                    if (ip.AddressFamily.ToString() == "InterNetwork") {
                        ipAddress = ip;
                        break;
                    }
                }
                if (ipAddress == null) {
                    Log.Error("no InterNetwork");
                    App.Quit();
                    return;
                }
                IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(localEndPoint);
                listener.Listen(10000);
                Log.Info("server listen on " + ipAddress.ToString() + ":" + port);
            }
            catch (Exception e) {
                Log.Debug(e.ToString());
                App.Quit();
                return;
            }

            threadWatch = new Thread(WatchConnecting);
            threadWatch.IsBackground = true;
            threadWatch.Start();

            workLoopNum = threadNum;
            loops = new EventLoop[workLoopNum];
            for (int i = 0; i < workLoopNum; ++i) {
                loops[i] = new EventLoop(true);
            }
        }
        private EventLoop GetNextLoop() {
            return loops[lastLoop++ % workLoopNum];
        }
        private void WatchConnecting() {
            try {
                while (true) {
                    // Start an asynchronous socket to listen for connections.
                    Log.Debug("Waiting for a connection...");
                    listener.BeginAccept( new AsyncCallback(AcceptCallback), listener);
                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }
            }
            catch (Exception e) {
                Log.Debug(e.ToString());
            }
        }

        private void AcceptCallback(IAsyncResult ar) {
            // Signal the listen thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            TConn conn = new TConn(handler, GetNextLoop(), mainLoop);
            conn.Handle();
            AddConn(conn);
        }
        public static void AddConn(TConn conn) {
            lock(connMutex) {
                connSet.Add(conn);
                Log.Debug("AddConn");
            }
        }
        public static void DelConn(TConn conn) {
            lock(connMutex) {
                connSet.Remove(conn);
                Log.Debug("DelConn");
            }
        }
        public void Broadcast(ISerial msg) {
            foreach (TConn conn in connSet) {
                conn.Send(msg);
            }
        }
    }
}