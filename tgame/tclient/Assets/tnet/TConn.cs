﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Tnet
{

    public enum connState : int {
        waitConnect,
        connectSuccesss,
        connectFail,
        connected,
        disconnected
    }

    //客户端连接socket代理与服务器接收的socket代理共用
    public class TConn {
        private Socket client = null;
        private EventLoop loop = null;
        private EventLoop mainLoop = null;
        private MemoryStream memStream = null;
        private BinaryReader reader;

        private const int MAX_READ = 8192;
        private ushort _seq = 1;
        private byte[] byteBuffer = new byte[MAX_READ];
        public connState curState = connState.disconnected;
        private  Dictionary<ushort, Servlet> callbackMap = new Dictionary<ushort, Servlet>();

        private Action _connectOk;
        private Action _connectFail;
        private Action _disconnect;
        // Use this for initialization
        public TConn(EventLoop mainLoop) {
            this.loop = mainLoop;
            memStream = new MemoryStream(1024);
            reader = new BinaryReader(memStream);
        }
        
        public TConn(Socket client, EventLoop loop, EventLoop mainLoop) {
            this.client = client;
            this.loop = loop;
            this.mainLoop = mainLoop;
            memStream = new MemoryStream(1024);
            reader = new BinaryReader(memStream);
            InitSocketOpt();
        }

        public void OnRemove() {
            this.Close();
            reader.Close();
            memStream.Close();
        }

        void InitSocketOpt() {
            client.SendTimeout = 1000;
            client.ReceiveTimeout = 1000;
            client.NoDelay = true;
            client.Blocking = false;
        }

        public void Handle() {
            try {
                client.BeginReceive(byteBuffer, 0, MAX_READ, 0, new AsyncCallback(ReceiveCallback), client);
            }
            catch (ArgumentException e) {
                curState = connState.connectFail;
                EventData ed = new EventData((int)EventType.connectFail, this);
                loop.PushMsg(ed);
                Log.Debug(e.ToString());
            }
            catch (InvalidOperationException e) {
                curState = connState.connectFail;
                EventData ed = new EventData((int)EventType.connectFail, this);
                loop.PushMsg(ed);
                Log.Debug(e.ToString());
            }
            catch (SocketException e) {
                curState = connState.connectFail;
                EventData ed = new EventData((int)EventType.connectFail, this);
                loop.PushMsg(ed);
                Log.Debug("Socket连接异常代号：" + e.ErrorCode + " : " + e);
            }
        }

        /// <summary>
        /// 连接服务器
        /// </summary>
        public void Connect(string host, int port) {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            InitSocketOpt();
            IPAddress ipAdress = IPAddress.Parse(host);
            IPEndPoint ipEndpoint = new IPEndPoint(ipAdress, port);
            curState = connState.waitConnect;
            IAsyncResult result = client.BeginConnect(ipEndpoint, new AsyncCallback(ConnectCallback), client);
            //超时检测
            bool success = result.AsyncWaitHandle.WaitOne(3000, true);
            if (!success) {
                curState = connState.connectFail;
                Log.Error("connect Time Out");
            }
        }

        private void ConnectCallback(IAsyncResult ar) {
            try {
                Socket so = (Socket)ar.AsyncState;
                so.EndConnect(ar);
                so.BeginReceive(byteBuffer, 0, MAX_READ, 0, new AsyncCallback(ReceiveCallback), so);
                EventData ed = new EventData((int)EventType.connectOk, this);
                loop.PushMsg(ed);
                curState = connState.connectSuccesss;
            }
            catch (Exception e) {
                //连接失败
                curState = connState.connectFail;
                EventData ed = new EventData((int)EventType.connectFail, this);
                loop.PushMsg(ed);
                Log.Debug(e.ToString());
            }
        }
        public Action connectOk {
            set {
                _connectOk = value;
            }
        }
        public Action connectFail {
            set {
                _connectFail = value;
            }
        }
        public Action disconnect {
            set {
                _disconnect = value;
            }
        }

        public void OnConnectOk() {
            if (_connectOk != null) {
                _connectOk();
            }
        }
        public void OnConnectFail() {
            if (_connectFail != null) {
                _connectFail();
            }
        }
        public void OnDisconnect() {
            if (_disconnect != null) {
                _disconnect();
            }
        }

        /// <summary>
        /// 读取消息
        /// </summary>
        void ReceiveCallback(IAsyncResult ar) {
            Socket so = (Socket)ar.AsyncState;
            int bytesRead = 0;
            try {
                bytesRead = so.EndReceive(ar);
                if (bytesRead < 1) {
                    //远程主机断开了连接
                    EventData ed = new EventData((int)EventType.disconnect, this);
                    loop.PushMsg(ed);
                    Log.Error("[ReceiveCallback] remote host closed this socket, bytesRead<1.");
                    return;
                }
                OnReceive(byteBuffer, bytesRead);   //分析数据包内容，抛给逻辑层
                //分析完，再次监听服务器发过来的新消息
                so.BeginReceive(byteBuffer, 0, MAX_READ, 0, new AsyncCallback(ReceiveCallback), so);
            }
            catch (Exception ex) {
                EventData ed = new EventData((int)EventType.disconnect, this);
                loop.PushMsg(ed);
                Log.Error("[ReceiveCallback]" + ex.Message);
            }
        }

        void OnReceive(byte[] bytes, int length) {
            memStream.Seek(0, SeekOrigin.End);
            memStream.Write(bytes, 0, length);
            //Reset to beginning
            memStream.Seek(0, SeekOrigin.Begin);
            while (RemainingBytes() > 2) {
                ushort messageLen = reader.ReadUInt16();
                if (RemainingBytes() >= messageLen) {
                    //copy to ByteBuffer to be unserialize
                    byte[] message = reader.ReadBytes(messageLen);
                    ByteBuffer buffer = new ByteBuffer(message);
                    int mainId = buffer.ReadShort();
                    EventData ev = new EventData(mainId, this, buffer);
                    if (mainLoop != null && mainId >=Message.MC_LOCKSTEP_BEGIN && mainId < Message.MC_LOCKSTEP_END) {
                        //当服务器收到帧同步消息放在Unity的主Update 中处理
                        mainLoop.PushMsg(ev);
                    }
                    else {
                        loop.PushMsg(ev);
                    }
                }
                else {
                    //Back up the position two bytes
                    memStream.Position = memStream.Position - 2;
                    break;
                }
            }
            //Create a new stream with any leftover bytes
            byte[] leftover = reader.ReadBytes((int)RemainingBytes());
            memStream.SetLength(0);     //Clear
            memStream.Write(leftover, 0, leftover.Length);
        }

        //|length 2byte|msgId 2byte|seq 2byte|body|
        public void ProcMsg(int msgId, ByteBuffer buf) {
            int sid = msgId >> 12;
            ushort seq = buf.ReadUshort();
            if (sid == App.Id) {
                Servlet sv = Servlet.FindAndClone(msgId);
                if (sv != null) {
                    sv.UnserialRequest(buf);
                    if (sv.Execute(this) == 0 ) {
                        if (sv.Rsp() != null) {
                            Send(sv.Rsp(), seq);
                        }
                    }
                    else {
                        EventData ed = new EventData((int)EventType.disconnect, this);
                        loop.PushMsg(ed);
                    }
                }
            }
            else {
                Servlet sv = null;
                callbackMap.TryGetValue(seq, out sv);
                if(sv != null) {
                    sv.UnserialResponse(buf);
                    sv.OnBack();
                    callbackMap.Remove(seq);
                }
                else {
                    Log.Debug("no callback find. msgId=" + msgId);
                }
            }
        }

        /// <summary>
        /// 剩余的字节
        /// </summary>
        private long RemainingBytes() {
            return memStream.Length - memStream.Position;
        }


        /// <summary>
        /// 写数据
        /// </summary>
        void WriteMessage(byte[] message) {
            MemoryStream ms = null;
            using (ms = new MemoryStream()) {
                ms.Position = 0;
                BinaryWriter writer = new BinaryWriter(ms);
                ushort msglen = (ushort)message.Length;
                writer.Write(msglen);
                writer.Write(message);
                writer.Flush();
                if (client != null && client.Connected) {
                    byte[] payload = ms.ToArray();
                    client.BeginSend(payload, 0, payload.Length, 0, new AsyncCallback(OnWrite), client);
                }
                else {
                    Log.Debug("client.connected----->>false");
                }
            }
        }
        void OnWrite(IAsyncResult ar) {
            try {
                Socket so = (Socket)ar.AsyncState;
                client.EndSend(ar);
            }
            catch (Exception ex) {
                Log.Debug("OnWrite--->>>" + ex.Message);
            }
        }


        // 打印字节
        void PrintBytes() {
            string returnStr = string.Empty;
            for (int i = 0; i < byteBuffer.Length; i++) {
                returnStr += byteBuffer[i].ToString("X2");
            }
            Log.Debug(returnStr);
        }

        /// <summary>
        /// 关闭链接
        /// </summary>
        public void Close() {
            if (client != null && client.Connected) {
                client.Close();
                client = null;
            }
            curState = connState.disconnected;
        }
        public static byte[] Pack(ISerial req) {
            ushort seq = 1;
            ByteBuffer writeBuf = new ByteBuffer();
            writeBuf.WriteUshort(0);  //给长度预留位置
            writeBuf.WriteUshort(req.MsgId());
            writeBuf.WriteUshort(seq);
            req.Serial(writeBuf);
            MemoryStream ms = writeBuf.Stream();
            ms.Seek(0, SeekOrigin.Begin);
            writeBuf.WriteUshort((ushort)(ms.Length - 2));
            writeBuf.Flush();
            return ms.ToArray();
        }

        public void Send(ISerial req, ushort seq) {
            if (client == null && client.Connected == false) {
                Log.Debug("client.connected----->>false");
                return;
            }
            ByteBuffer writeBuf = new ByteBuffer();
            writeBuf.WriteUshort(0);  //给长度预留位置
            writeBuf.WriteUshort(req.MsgId());
            writeBuf.WriteUshort(seq);
            req.Serial(writeBuf);
            MemoryStream ms = writeBuf.Stream();
            ms.Seek(0, SeekOrigin.Begin);
            writeBuf.WriteUshort((ushort)(ms.Length - 2));
            writeBuf.Flush();
            RawSend(ms.ToArray());
        }
        public void RawSend(byte[] bytes) {
            try {
                client.BeginSend(bytes, 0, bytes.Length, 0, new AsyncCallback(OnWrite), client);
            }
            catch (Exception ex) {
                Log.Error("[Send Exception]" + ex.Message);
                throw ex;
            }
        }
        public void Send(ISerial req) {
            Send(req, ++_seq);
        }
        public void Call(Servlet sv) {
            ushort seq = ++_seq;
            callbackMap.Add(seq, sv);
            Send(sv.Req(), seq);
        }
    }
}