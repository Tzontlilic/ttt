﻿using System.Collections.Generic;

namespace Tnet
{
    public abstract class Servlet {
        public static Dictionary<int, Servlet> servletMap = new Dictionary<int, Servlet>();
        public static Servlet FindAndClone(int msgId) {
            Servlet res = null;
            servletMap.TryGetValue(msgId, out res);
            if (res != null) {
                return (Servlet )res.MemberwiseClone();
            }
            else {
                return null;
            }
        }
        public bool needRpc = true;
        public static void Add(Servlet s) {
            servletMap.Add(s.MsgId(), s);
        }
        public abstract ushort MsgId();
        public abstract ISerial Req();
        public abstract ISerial Rsp();
        public abstract void SerialRequest(ByteBuffer buf);
        public abstract void UnserialRequest(ByteBuffer buf);
        public abstract void SerialResponse(ByteBuffer buf);
        public abstract void UnserialResponse(ByteBuffer buf);
        public abstract int Execute(TConn conn);
        public abstract int OnBack();
    }
}