﻿namespace Tnet
{
    public class App {
        private static int id;
        public static void Init(int id) {
            App.id = id;
            Log.Init("log.txt");
        }
        public static int Id {
            get {
                return id;
            }
        }
        public static void Quit() {

        }
    }
}