﻿using UnityEngine;
using Tnet;
using System.Collections.Generic;

public class main : MonoBehaviour {

    EventLoop loop = new EventLoop(false);
    TConn conn;
	// Use this for initialization
	void Start () {
        App.Init(Message.SERVLET_CLIENT);
        RegisterServlet();
        
        conn = new TConn(loop);
        conn.Connect("192.168.0.101", 9527);
        conn.connectOk = () =>
        {
            Debug.Log("connect ok");
        };
        conn.connectFail = () =>
        {
            Debug.Log("connect fail");
        };
        conn.disconnect = () =>
        {
            Debug.Log("remote server disconnect!");
        };
    }
    void RegisterServlet() { 
        Servlet.Add(new CL_NotifyItemsServlet());
        Servlet.Add(new CL_NotifyValuesServlet());
        Servlet.Add(new CL_NotifySnycStepServlet());
    }
	
	// Update is called once per frame
	void Update () {
        loop.Update();
        UpdatKeyboard();
	}

    public void Login()
    {
        MC_LoginServlet mclogin = new MC_LoginServlet();
        mclogin.req.user = "tanlei_9";
        mclogin.req.deviceId = SystemInfo.deviceUniqueIdentifier;
        conn.Call(mclogin);
        mclogin.Cb = (self) => {
            Debug.Log("mclogin ok,uid="+ self.rsp.uid);
        };
    }

    public void CeateRoom()
    {
        MC_CreateRoomServlet mc_creatroom = new MC_CreateRoomServlet();
        mc_creatroom.req.uid = 1002;
        conn.Call(mc_creatroom);
        mc_creatroom.Cb = (self) => {
            Debug.Log("create room ok,uid=!"+ self.rsp.roomid);
        };
    }

    public void JoinRoom()
    {
        MC_JoinRoomServlet joinroom = new MC_JoinRoomServlet();
        joinroom.req.uid = 1002;
        conn.Call(joinroom);
        joinroom.Cb = (self) => {
            Debug.Log("join room ok,uid=!"+ self.rsp.roomid);
        };
    }
    public void SendStep()
    {
        MC_SendStepReq req;
        req.actionId = 1;
        req.fp = new List<float>();
        conn.Send(req);
    }
    public void StartRoom()
    {
        MC_StartRoomReq req;
        req.roomid = 1001;
        conn.Send(req);
    }

    [SerializeField]
    KeyCode testKeyCode;
    public void UpdatKeyboard()
    {
        if ( Input.GetKeyUp(testKeyCode))
        {
            MC_SendStepReq req;
            req.actionId = testKeyCode.GetHashCode();
            req.fp = new List<float>();
            conn.Send(req);
            Debug.Log("send Step Input Up = " + testKeyCode.ToString());
        }
    }
}