using System;
using System.Collections.Generic;


namespace Tnet {
    public struct MC_EnterSceneReq : ISerial {
        public int roomid;
        
        public ushort MsgId() {
            return Message.MC_ENTER_SCENE;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.roomid);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.roomid = buf.ReadInt();
        }
        
    }
    public abstract class MC_EnterScene : Servlet {
        public MC_EnterSceneReq req;
        public MC_EnterScene(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.MC_ENTER_SCENE;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
}

