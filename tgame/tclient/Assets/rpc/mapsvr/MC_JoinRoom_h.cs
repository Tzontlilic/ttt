using System;
using System.Collections.Generic;


namespace Tnet {
    public struct MC_JoinRoomReq : ISerial {
        public int uid;
        
        public ushort MsgId() {
            return Message.MC_JOIN_ROOM;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.uid);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.uid = buf.ReadInt();
        }
        
    }
    public struct MC_JoinRoomRsp : ISerial {
        public sbyte state;
        public int roomid;
        
        public ushort MsgId() {
            return Message.MC_JOIN_ROOM;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteSbyte(this.state);
            buf.WriteInt(this.roomid);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.state = buf.ReadSbyte();
            this.roomid = buf.ReadInt();
        }
        
    }
    public abstract class MC_JoinRoom : Servlet {
        public MC_JoinRoomReq req;
        public MC_JoinRoomRsp rsp;
        private Action<MC_JoinRoom> cb;
        public MC_JoinRoom(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.MC_JOIN_ROOM;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return rsp;
        }
        public override void SerialResponse(ByteBuffer buf) {
            rsp.Serial(buf);
        }
        public override void UnserialResponse(ByteBuffer buf) {
            rsp.Unserial(buf);
        }
        public override int OnBack() {
            if (cb != null) {
                cb(this);
            }
            return 0;
        }
        public Action<MC_JoinRoom> Cb {
            set {
                cb = value;
            }
        }
    }
}

