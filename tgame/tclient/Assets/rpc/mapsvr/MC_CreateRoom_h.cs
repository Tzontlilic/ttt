using System;
using System.Collections.Generic;


namespace Tnet {
    public struct MC_CreateRoomReq : ISerial {
        public int uid;
        
        public ushort MsgId() {
            return Message.MC_CREATE_ROOM;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.uid);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.uid = buf.ReadInt();
        }
        
    }
    public struct MC_CreateRoomRsp : ISerial {
        public int roomid;
        
        public ushort MsgId() {
            return Message.MC_CREATE_ROOM;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.roomid);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.roomid = buf.ReadInt();
        }
        
    }
    public abstract class MC_CreateRoom : Servlet {
        public MC_CreateRoomReq req;
        public MC_CreateRoomRsp rsp;
        private Action<MC_CreateRoom> cb;
        public MC_CreateRoom(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.MC_CREATE_ROOM;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return rsp;
        }
        public override void SerialResponse(ByteBuffer buf) {
            rsp.Serial(buf);
        }
        public override void UnserialResponse(ByteBuffer buf) {
            rsp.Unserial(buf);
        }
        public override int OnBack() {
            if (cb != null) {
                cb(this);
            }
            return 0;
        }
        public Action<MC_CreateRoom> Cb {
            set {
                cb = value;
            }
        }
    }
}

