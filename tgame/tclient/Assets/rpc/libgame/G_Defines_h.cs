
namespace Tnet {
    public class G_Defines {
        public const int G_VALUE_MONEY = 0x0;
        public const int G_VALUE_COIN = 0x1;
        public const int G_VALUE_GOLD = 0x2;
        public const int G_VALUE_LEVEL = 0x3;
        public const int G_VALUE_EXP = 0x4;
        public const int G_VALUE_VIP = 0x5;
        public const int G_VALUE_UNKNOWN = 0x6;
        public const int G_CHAT_CHANNEL_WORLD = 0x0;
        public const int G_CHAT_CHANNEL_ROOM = 0x1;
        public const int G_CHAT_CHANNEL_PERSION = 0x2;
        public const int G_CHAT_CHANNEL_SYSTEM = 0x3;
        public const int G_CHAT_CHANNEL_UNKNOWN = 0x4;
        public const int G_RAND_MAX = 0x2710;
        public const int G_NICKNAME_LIMIT = 0x30;
        public const int G_USERNAME_LIMIT = 0x40;
    }
}

