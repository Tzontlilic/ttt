@echo off
cd /d %~dp0

SET client_dir=\tclient\Assets\rpc\
SET server_dir=\tserver\Assets\rpc\
SET tclient_dir=\..\TTTGame\Assets\Script\NetComponent\rpc\

setlocal enabledelayedexpansion

for /r %%i in (*.sdl) do (
 set v=%%~dpi
 call :loop
 set parent=!%~dp0!
 set parent=!parent:~0,-4!
 set son=!v:%~dp0=!
 if not exist !parent!%client_dir%!son! (mkdir !parent!%client_dir%!son!)
 if not exist !parent!%server_dir%!son! (mkdir !parent!%server_dir%!son!)
 if not exist !parent!%tclient_dir%!son! (mkdir !parent!%tclient_dir%!son!)
)

for /r %%i in (*.sdl) do (
 set v=%%i
 call :loop
 set in=!v:%~dp0=!
 set client_out=..%client_dir%!in!
 set client_out=!client_out:.sdl=_h.cs!
 set server_out=..%server_dir%!in!
 set server_out=!server_out:.sdl=_h.cs!
 set tclient_out=..%tclient_dir%!in!
 set tclient_out=!tclient_out:.sdl=_h.cs!
 echo compile !in!
 sscc -i !in! -o !client_out! -l csharp
 sscc -i !in! -o !server_out! -l csharp
 sscc -i !in! -o !tclient_out! -l csharp
)

setlocal disabledelayedexpansion
pause

:loop
if "!v:~-1!"==" " set "v=!v:~0,-1!" & goto loop
