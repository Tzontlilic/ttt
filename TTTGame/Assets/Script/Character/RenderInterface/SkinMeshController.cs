﻿using System;
using System.Collections.Generic;
using Tnet;
using UnityEngine;

public enum EEquipType
{
    EET_Head,
    EET_Chest,
    EET_Hand,
    EET_Feet,
    EET_Max
}

public enum EAnimTrigerType
{
    EATT_Triger,
    EATT_Float,
    EATT_Int,
    EATT_Bool,
    EATT_Max
}

public enum EAnimType
{
    EAT_Fire,
    EAT_Fight,
    EAT_Reload,
    EAT_MoveSpeed,
    EAT_BodyStatus,
    EAT_Max
}

public class SkinMeshController
{

    /// <summary>
    /// SkinMesh Instance of Unity GameObject
    /// </summary>
    public GameObject Instance = null;

    /// <summary>
    /// Equipment informations
    /// </summary>
    string[] equipments = new string[(int)EEquipType.EET_Max];

    /// <summary>
    /// Animator controller for SkinMesh
    /// </summary>
    Animator animator = null;

    /// <summary>
    /// EAT_Fire,    EAT_Fight,    EAT_Reload,    EAT_MoveSpeed,    EAT_BodyStatus,
    /// </summary>
    string[] animStrs = new string[(int)EAnimType.EAT_Max]
    {
         "fire",    "fight",    "reload",   "Speed_f",  "body_status",
    };

    public void SpawnGeom(string res)
    {
        UnityEngine.Object obj = ResourcesMgr.Load(res);
        Instance = GameObject.Instantiate(obj) as GameObject;
        Instance.transform.SetParent(Main.inst.battleScene.transform, false);
        animator = Instance.GetComponentInChildren<Animator>();

        //int a = animator.get();
        //Debug.Log("----------------------------------动画的个数一共是：" + a);
        //int i = 0;
        //string[] animName = new string[animator.GetClipCount()];
        ////Debug.Log("---这些动画的名字是：" + animName);
        //foreach (AnimationState state in animator)
        //{
        //    animName[i++] = state.name;
        //    Debug.Log(animName[i - 1]);
        //}
    }

    public void SetGeomSection(EEquipType et, string res)
    {
        equipments[(int)et] = res;
    }

    public void CombineGeom()
    {
        UnityEngine.Object res = null;
        // Create and collect other parts SkinnedMeshRednerer
        List<SkinnedMeshRenderer> meshes = new List<SkinnedMeshRenderer>();
        GameObject[] objects = new GameObject[4];
        for (int i = 0; i < equipments.Length; i++)
        {
            if (equipments[i] == string.Empty)
                continue;
            res = ResourcesMgr.Load("Prefab/" + equipments[i]);
            objects[i] = GameObject.Instantiate(res) as GameObject;
            SkinnedMeshRenderer mesh = objects[i].GetComponentInChildren<SkinnedMeshRenderer>();
            meshes.Add(mesh);
        }

        // Combine meshes
        if (meshes.Count > 0)
            UCharacterMgr.instance.CombineSkinnedMgr.CombineObject(Instance, meshes.ToArray(), true);

        // Delete temporal resources
        for (int i = 0; i < objects.Length; i++)
        {
            GameObject.DestroyImmediate(objects[i].gameObject);
        }
    }

    bool isInLockStep = false;
    public void BeginLockStep()
    {
        isInLockStep = true;
        animator.enabled = true;
        animator.speed = 0;
        animator.playableGraph.SetTimeUpdateMode(UnityEngine.Playables.DirectorUpdateMode.Manual);
    }

    public void LeaveLockStep()
    {
        isInLockStep = false;
        animator.enabled = true;
        animator.speed = 1;
        animator.playableGraph.SetTimeUpdateMode(UnityEngine.Playables.DirectorUpdateMode.UnscaledGameTime);
    }

    public void UpdateLockAnim(float _delatTime)
    {
        animator.speed = Main.inst.animSpeed;
        animator.playableGraph.Evaluate(_delatTime);
        //animator.speed = 0;
    }

    public void PlayAnimWithValue(EAnimTrigerType tt, EAnimType at, object value)
    {
        if(animator == null)
        {
            FpDebug.Log("SkinMesh AnimController Null!");
            return;
        }

        string AnimStr = animStrs[(int)at];
        
        switch (tt)
        {
            case EAnimTrigerType.EATT_Triger:
                animator.SetTrigger(AnimStr);
                break;
            case EAnimTrigerType.EATT_Bool:
                animator.SetBool(AnimStr, Convert.ToBoolean(value));
                break;
            case EAnimTrigerType.EATT_Float:
                animator.SetFloat(AnimStr, Convert.ToSingle(value));
                break;
            case EAnimTrigerType.EATT_Int:
                animator.SetInteger(AnimStr, Convert.ToInt32(value));
                break;
        }
    }

    public int GetStandStatuesValue()
    {
        if (animator == null)
        {
            FpDebug.Log("SkinMesh AnimController Null!");
            return 0;
        }
        string AnimStr = animStrs[(int)EAnimType.EAT_BodyStatus];
        return animator.GetInteger(AnimStr);
    }

    public void PlayStandAnim()
    {
        PlayAnimWithValue(EAnimTrigerType.EATT_Float, EAnimType.EAT_MoveSpeed, 0);
        PlayAnimWithValue(EAnimTrigerType.EATT_Int, EAnimType.EAT_BodyStatus, 1);
        //animator.speed = 1;
    }

    public void PlayCrouchAnim()
    {
        PlayAnimWithValue(EAnimTrigerType.EATT_Int, EAnimType.EAT_BodyStatus, 2);
    }

    public void SetMoveAnimSpeed(float rate)
    {
        PlayAnimWithValue(EAnimTrigerType.EATT_Float, EAnimType.EAT_MoveSpeed, rate);
    }

    public void PlayAttackAnim()
    {
        PlayAnimWithValue(EAnimTrigerType.EATT_Triger, EAnimType.EAT_Fire, 0);
    }

    public void PlayReloadAnim()
    {
        PlayAnimWithValue(EAnimTrigerType.EATT_Triger, EAnimType.EAT_Reload, 0);
    }

    public void PlayFightAnim()
    {
        PlayAnimWithValue(EAnimTrigerType.EATT_Triger, EAnimType.EAT_Fight, 0);
    }

    public void DestroyThis()
    {
        if( Instance != null )
            GameObject.DestroyImmediate(Instance);
        Instance = null;
    }
}
