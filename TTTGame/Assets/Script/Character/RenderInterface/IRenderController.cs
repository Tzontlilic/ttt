﻿
public interface IRenderController
{
    void SpawnGeom(string res);

    void SetGeomSection(EEquipType et, string res);

    void CombineGeom();

    void DestroyThis();
}
