﻿using UnityEngine;

public enum EWeaponType
{
    None = 0,
    Pistol,
    EmptyReload1,
    EmptyReload2,
    Shotgun,
    AssultRifle02,
    AssultRifle01,
    SubMachineGun,
    RPG,            //火箭炮
    Minigun,
}

public class GunType : MonoBehaviour
{
    public EWeaponType type = EWeaponType.None;
    public Transform firePos = null;
    public GameObject bulletObj = null;
    public float bulletSpeed = 10;
    public float bulletRange = 10;
    public GameObject FireFlashObj = null;
    public GameObject HitEffect = null;
    public float bulletWidth = 1;

    public float bForce = 100f;
    public float bForceRange = 10f;

    public float sectorAngle = 45;

    public float bulletDmg = 100;
}