﻿using UnityEngine;

public class EnemyController : UCharacterController
{
    private CharacterController _characterController;

    public EnemyController(int profId) : base(profId)
    {
        charType = ECharType.ECT_Enemy;
        _characterController = Instance.GetComponent<CharacterController>();
    }

    public override void Update()
    {
        if (Instance.activeInHierarchy == false)
            return;

        Vector3 movementVector = Vector3.zero;
        if (_characterController.enabled)
        {
            movementVector += Physics.gravity;
            _characterController.Move(movementVector * Time.deltaTime * 5);
        }
    }
}