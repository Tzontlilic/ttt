﻿using UnityEngine;
using CnControls;
using UnityEngine.Networking;

// This is merely an example, it's for an example purpose only
// Your game WILL require a custom controller scripts, there's just no generic character control systems, 
// they at least depend on the animations

[RequireComponent(typeof(CharacterController))]
public class ThidPersonExampleController : MonoBehaviour
{
    public float MovementSpeed = 10f;
    public bool canMove = true;
    public bool inMove = true;

    private Transform _mainCameraTransform;
    private Transform _transform;
    private CharacterController _characterController;
    
    private void OnEnable()
    {
        _characterController = GetComponent<CharacterController>();
        _transform = GetComponent<Transform>();
    }

    public void Update()
    {
    }

    public bool isCCEnable()
    {
        return _characterController != null && _characterController.enabled;
    }

    public CharacterController GetCC()
    {
        return _characterController;
    }

    public Vector3 getCurMovement()
    {
        if(_mainCameraTransform == null)
            _mainCameraTransform = MapCamera.instance.myCamera.transform;

        // Just use CnInputManager. instead of Input. and you're good to go
        var inputVector = new Vector3(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));

        // If we have some input
        Vector3 movementVector = _mainCameraTransform.TransformDirection(inputVector);
        movementVector.y = 0f;
        movementVector.Normalize();

        return movementVector;
    }    

    public Vector3 getCurRotation()
    {
        if (_mainCameraTransform == null)
            _mainCameraTransform = MapCamera.instance.myCamera.transform;

        // Just use CnInputManager. instead of Input. and you're good to go
        var inputVector = new Vector3(CnInputManager.GetAxis("HorizontalRot"), CnInputManager.GetAxis("VerticalRot"));

        // If we have some input
        Vector3 rotationVector = _mainCameraTransform.TransformDirection(inputVector);
        rotationVector.y = 0f;
        rotationVector.Normalize();

        return rotationVector;
    }


}
