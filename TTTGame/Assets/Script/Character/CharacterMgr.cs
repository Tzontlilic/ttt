﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UCharacterMgr  {

    private static volatile UCharacterMgr _instance = null;
    public static UCharacterMgr instance
    {
        get
        {
            if (_instance == null)
                _instance = new UCharacterMgr();
            return _instance;
        }
    }

    private readonly string[] index = new string[] { "004", "006", "008" };

    private UCombineSkinnedMgr skinnedMgr = null;
	public UCombineSkinnedMgr CombineSkinnedMgr { get{ return skinnedMgr; } }

	private int characterIndex = 0;

    private Dictionary<int, PlayerController> playerChars = new Dictionary<int, PlayerController>();

	private Dictionary<int,UCharacterController> characterDic = new Dictionary<int, UCharacterController>();

    private List<UCharacterController> trapList = new List<UCharacterController>();

    public List<UCharacterController> enemyList = new List<UCharacterController>();

    public UCharacterMgr () {

		skinnedMgr = new UCombineSkinnedMgr ();
	}

    /// <summary>
    /// Create Player for Controller
    /// </summary>
    /// <returns></returns>
    public PlayerController GeneratePlayer(int _playerId, string _name)
    {
        //create Player first
        if(playerChars.ContainsKey(_playerId))
        {
            return playerChars[_playerId];
        }
        else
        {
            PlayerController playerChar = new PlayerController(4);
            playerChar.playerId = _playerId;
            playerChar.position = new Vector3(0, 0, 0);
            playerChar.Instance.name = _name + "_" + _playerId.ToString();
            playerChars[_playerId] = (playerChar);
            return playerChar;
        }
    }

    public void RemovePlayer(int _playerId)
    {
        if (playerChars.ContainsKey(_playerId))
        {
            playerChars[_playerId].DestroyCharacter();
            playerChars.Remove(_playerId);
        }
    }

    public TrapController GenerateTrap()
    {
        TrapController instance = new TrapController(3);
        trapList.Add(instance);
        return instance;
    }

    public EnemyController GenerateEnemy(int profId, Vector3 pos)
    {
        EnemyController instance = new EnemyController(profId);
        instance.position = pos;
        instance.Instance.name = "Enemy_" + enemyList.Count.ToString();
        enemyList.Add(instance);
        return instance;
    }

    /// <summary>
    /// Create A Character With Geom Section
    /// </summary>
    /// <param name="skeleton"></param>
    /// <param name="weapon"></param>
    /// <param name="head"></param>
    /// <param name="chest"></param>
    /// <param name="hand"></param>
    /// <param name="feet"></param>
    /// <param name="combine"></param>
    /// <returns></returns>
    protected UCharacterController Generatecharacter (int profId, Vector3 pos)
	{
		UCharacterController cha = new UCharacterController (profId);
        cha.Instance.transform.position = pos;
        characterDic.Add(characterIndex, cha);
		characterIndex ++;
		return cha;
	}

	public void Removecharacter (CharacterController character)
	{

	}

    public int getObjPlayerId(GameObject _obj)
    {
        foreach (PlayerController player in playerChars.Values)
        {
            if(player.Instance == _obj)
            {
                if (!player.IsDead())
                    return player.playerId;
                else
                    return -1;
            }
        }
        return -1;
    }

    public void Update () {

        foreach (PlayerController player in playerChars.Values)
        {
            player.Update();
        }        

        foreach (UCharacterController character in characterDic.Values)
		{
			character.Update();
        }

        foreach (UCharacterController character in enemyList)
        {
            character.Update();
        }

        for (int i = 0; i < trapList.Count; i++) 
        {
            trapList[i].Update();
        }
	}
}
