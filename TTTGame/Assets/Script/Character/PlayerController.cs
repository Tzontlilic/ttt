﻿using CnControls;
using System.Collections.Generic;
using Tnet;
using UnityEngine;
using VGame;

public enum EAnimStateType
{
    EAS_PreAttack,
    EAS_Attack,
    EAS_End,
    EAS_None
}

public class PlayerController : UCharacterController, IStepUnit
{
    EAnimStateType curAnimState = EAnimStateType.EAS_None;    

    int curStandState = 1;
        
    bool isInShooting = false;

    ThidPersonExampleController thidController = null;

    public PlayerController(int profId):base(profId)
    {
        charType = ECharType.ECT_Player;
        InitPlayer();
        if (m_profession.weaponId != -1)
            SwitchWeapon(m_profession.weaponId);
    }

    public void InitPlayer()
    {
        thidController = Instance.AddComponent<ThidPersonExampleController>();
        thidController.MovementSpeed = 3;
    }

    public override void PlayStand()
    {
        base.PlayStand();
        if(thidController != null)
            thidController.MovementSpeed = 0;
    }

    public override void PlayRun()
    {
        base.PlayRun();
    }

    public void OnAttackEnd()
    {
        if( hasReload == false)
        {
            PlayReload();
            return;
        }

        if (isInLockShoot)
            PlayAttack();
    }

    //跑步状态无法射击
    public override bool CanAttack()
    {
        if (m_moveState == ECharMoveState.ECMS_Run && isInMove())
            return false;
        return base.CanAttack();
    }

    public override void PlayCrouchAim()
    {
        if( !isInEdit)
        {
            if( CanLockTarget())
            {
                m_SkinMeshController.PlayCrouchAnim();
                BeginLockTarget();
            }
        }
        else
        {
            m_SkinMeshController.PlayCrouchAnim();
        }
    }        

    public void SetAttackState(EAnimStateType eas)
    {
        switch (eas)
        {
            case EAnimStateType.EAS_PreAttack:
                PlayCrouchAim();
                SetMoveDirection(Vector3.zero);
                break;
            case EAnimStateType.EAS_End:
                thidController.canMove = true;
                lockedTarget = null;
                showAimEffect(false);
                break;
        }
        curAnimState = eas;
    }
    

    public override bool isInMove()
    {
        if (m_moveDir.magnitude > 0.001f)
        {
            return true;
        }
        return false;
    }

    Vector3 bornPos = Vector3.zero;
    Quaternion bornRot = Quaternion.Euler(0, 180, 0);
    EAnimStateType cacheAnimState = EAnimStateType.EAS_None;
    int roomStageIndex = -1;
    public void EnterEditPlayer(int _stageIndex = 0)
    {
        Instance.transform.SetParent( Main.inst.stageTrans[_stageIndex], false);
        Instance.transform.localPosition = new Vector3(0, 48, 0);
        Instance.transform.localRotation = Quaternion.Euler(0, 180, 0);
        Instance.transform.localScale = new Vector3(200, 200, 200);
        isInEdit = true;
        if (thidController != null)
        {
            thidController.inMove = false;
            thidController.canMove = false;
        }
        CharacterController ccc = Instance.gameObject.GetComponent<CharacterController>();
        ccc.enabled = false;
        SetLayerRecursively(Instance, GNS.DUILayer);
        cacheAnimState = curAnimState;
        SetAttackState(EAnimStateType.EAS_End);
        PlayStand();
        roomStageIndex = _stageIndex;
        RoomUIManager.instance.SetPlayerName(_stageIndex, GameRoom.instance.getPlayerName(playerId));
    }

    public void LeaveRoom()
    {
        RoomUIManager.instance.SetPlayerName(roomStageIndex, "Waiting Player");
        roomStageIndex = -1;
    }

    public void InitBornPos(int _bornIndex)
    {
        Transform bornTrans = GameSceneMgr.instance.GetBornTrans(_bornIndex);
        bornPos = bornTrans.position;
        bornRot = bornTrans.rotation;
    }
    
    public void EnterBattle()
    {
        AddStateBar();
        isInEdit = false;
        Instance.transform.SetParent(Main.inst.battleScene.transform, false);
        Instance.transform.position = bornPos;
        Instance.transform.localRotation = bornRot;
        Instance.transform.localScale = new Vector3(2, 2, 2);
        if (thidController != null)
        {
            thidController.inMove = false;
            thidController.canMove = true;
        }
        CharacterController ccc = Instance.gameObject.GetComponent<CharacterController>();
        ccc.enabled = true;
        SetAttackState(EAnimStateType.EAS_End);
        if (this == GameRoom.instance.Player)
        {
            UpdateFOVMesh(m_gunInfo.bulletRange, m_gunInfo.sectorAngle);
            SetLayerRecursively(Instance, GNS.PlayerLayer);
        }
        else
            SetLayerRecursively(Instance, GNS.EnemyFogLayer);
        if (m_SkinMeshController != null)
            m_SkinMeshController.BeginLockStep();
    }

    public override void Update()
    {
        if(isInEdit)
        {
            if (thidController != null)
            {
                thidController.MovementSpeed = 0;
            }
            return;
        }
        base.Update();
    }

    GameObject DeathEffObj = null;
    public void Die()
    {
        if (DeathEffObj == null)
        {
            DeathEffObj = GameObject.Instantiate(Main.inst.DeathEffPrefab) as GameObject;
            DeathEffObj.transform.SetParent(Main.inst.battleScene.transform, false);
            DeathEffObj.transform.position = m_cbp.spine.position;
        }
        if (visualisationObj != null)
            visualisationObj.SetActive(false);
        Instance.SetActive(false);
    }

    public void Live()
    {
        if (DeathEffObj != null)
        {
            GameObject.Destroy(DeathEffObj);
            DeathEffObj = null;
        }
        Instance.transform.position = bornPos;
        Instance.transform.localRotation = bornRot;
        if (visualisationObj != null)
            visualisationObj.SetActive(true);
        Instance.SetActive(true);
    }

    #region Step Logic
    bool m_bStepFinished = true;
    int m_iCurStepCount = 0;
    List<string> pendingActions = new List<string>();
    List<string> currActions = new List<string>();
    void IStepUnit.Init()
    {
        m_bStepFinished = true;
    }

    int IStepUnit.getID()
    {
        return playerId;
    }

    void IStepUnit.StepUpdate(int _deltaTime)
    {
        UpdateSyncStep(_deltaTime);
        UpdateSelfStep(_deltaTime);
    }

    void IStepUnit.Release()
    {
        LockStepMgr.RemoveStepUnit(this);
    }

    string IStepUnit.ToString()
    {
        string unitDesc = "";
        if (Instance != null)
            unitDesc = Instance.name;
        if (playerId != -1)
            unitDesc += "(" + playerId.ToString() + ")";
        return unitDesc;
    }

    void IStepUnit.AddPendingAction(string _act)
    {
        pendingActions.Add(_act);
    }

    bool IStepUnit.ReadyForNextStep()
    {
        return currActions.Count == 0;
    }

    public void SetMoveDirection(Vector3 _dir)
    {
        m_moveDir = _dir;
    }

    bool isInLockShoot = false;
    public void SetShootDir(Vector3 _dir)
    {
        if (_dir == Vector3.zero)
            isInLockShoot = false;
        else
            isInLockShoot = true;
        m_rotDir = _dir;
    }

    public void UpdateSyncStep(int _deltaTime)
    {
        if (IsDead())
        {
            UpdateDeathTime(_deltaTime);
            return;
        }

        float dt = _deltaTime / 1000.0f;
        if (thidController != null && thidController.isCCEnable() && curAnimState != EAnimStateType.EAS_PreAttack && !IsDead())
        {
            //curStep Move
            float curSpeed = 0;
            if (m_moveState == ECharMoveState.ECMS_Run)
                curSpeed = getCurCProp(CPropType.CPT_MAXSPEED) * 2;
            else if (m_moveState == ECharMoveState.ECMS_Walk)
                curSpeed = getCurCProp(CPropType.CPT_MAXSPEED);
            Vector3 moveDelta = (m_moveDir + Physics.gravity) * dt * curSpeed / 2.0f;
            thidController.GetCC().Move(moveDelta);
            if(isInLockShoot)
            {
                if (m_rotDir.magnitude > 0.0001f)
                {
                    ThisTrans.forward = Vector3.Lerp(ThisTrans.forward, m_rotDir, _deltaTime / 10.0f);// m_rotDir;
                }
                PlayAttack();
            }
            else
            {
                if (m_moveDir.magnitude > 0.0001f)
                {
                    ThisTrans.forward = Vector3.Lerp(ThisTrans.forward, m_moveDir, _deltaTime / 10.0f);// m_rotDir;
                }
            }
        }

        if (ClientGame.isCurState(EGameState.VGS_GamePlay))
        {
            UpdateVisionTarget();
            if (curAnimState == EAnimStateType.EAS_PreAttack)
            {
                if (!isLockTargetValid())
                {
                    LockAim(false);
                }
                else
                {
                    Instance.transform.LookAt(lockedTarget.Instance.transform, Vector3.up);
                }
                m_moveDir = Vector3.zero;
            }
            else
            {
                if (isInMove())
                    PlayRun();
                else
                    PlayStand();
            }
            if (m_SkinMeshController != null)
                m_SkinMeshController.UpdateLockAnim(dt);

            UpdateFadeOutTime(_deltaTime);
        }
    }

    public void UpdateSelfStep(int _deltaTime)
    {
        if (!GameRoom.instance.IsControlPlayer(this as PlayerController))
            return;

        float dt = _deltaTime / 1000.0f;

        MapCamera.instance.BindPlayer(Instance.transform.position, dt);

        //next step Move
        if (LockStepMgr.g_bStart)
        {

            // If we have some input
            Vector3 curMove = thidController.getCurMovement();
            if( curAnimState == EAnimStateType.EAS_PreAttack )
                curMove = Vector3.zero;
            if ( curMove != m_moveDir )
            {
                SendSyncLockMove(curMove.x, curMove.z);
            }

            Vector3 curRot = thidController.getCurRotation();
            if (curAnimState == EAnimStateType.EAS_PreAttack)
                curRot = Vector3.zero;
            if (curRot != m_rotDir && !isInLock)
            {
                SendSyncLockRot(curRot.x, curRot.z);
            }

            if (CnInputManager.GetButtonDown("WalkRun"))
            {
                SendWalkRun(m_moveState == ECharMoveState.ECMS_Walk);
            }
            if (CnInputManager.GetButtonUp("AttackJoyStick"))
            {
                SendSyncAttack();
            }
            //if (CnInputManager.GetButtonDown("LockAim"))
            //{
            //    SendLockTarget();
            //}
        }
    }    

    public void UpdateDeathTime(int deltaTime)
    {
        if( IsDead() )
        {
            DeathTime -= deltaTime;
            if( DeathTime <= 0 )
            {
                Alive();
            }
        }
    }

    public void SendSyncLockMove(float _x, float _z)
    {
        MC_SendStepReq req;
        req.actionId = 0;
        req.fp = new List<float>();
        req.fp.Add(_x);
        req.fp.Add(_z);
        ClientGame.instance.NetSend(req);
    }

    public void SendSyncLockRot(float _x, float _z)
    {
        MC_SendStepReq req;
        req.actionId = 4;
        req.fp = new List<float>();
        req.fp.Add(_x);
        req.fp.Add(_z);
        ClientGame.instance.NetSend(req);
    }

    public void SendSyncAttack()
	{
		if (!CanAttack())
			return;

		MC_SendStepReq req;
		req.actionId = 1;
		req.fp = new List<float>();
		ClientGame.instance.NetSend(req);
	}

    //跑步状态无法瞄准
	public void SendLockTarget()
    {
        if (m_moveState == ECharMoveState.ECMS_Run && isInMove() )
            return;
        if (!CanLockTarget())
            return;
        MC_SendStepReq req;
		req.actionId = 2;
		req.fp = new List<float>();
		if (isInLock)
			req.fp.Add (0);
		else
			req.fp.Add (2);
		ClientGame.instance.NetSend(req);
    }

    public void SendWalkRun(bool _inRun)
    {
        MC_SendStepReq req;
        req.actionId = 3;
        req.fp = new List<float>();
        if (!_inRun)
            req.fp.Add(0);
        else
            req.fp.Add(2);
        ClientGame.instance.NetSend(req);
    }

    #endregion


    #region Received Acion Deal
    int lockTargetIndex = 0;
    bool isInLock = false;
    public void LockAim(bool lck)
    {
        if (lck)
        {
            //UpdateVisionTarget();
            SetAttackState(EAnimStateType.EAS_PreAttack);
        }
        else
        {
            SetAttackState(EAnimStateType.EAS_End);
            lockedTarget = null;
        }
        isInLock = lck;
    }

    public void SetRunState(bool _inRun)
    {
        m_moveState = _inRun ? ECharMoveState.ECMS_Run : ECharMoveState.ECMS_Walk;
    }
    #endregion
}
