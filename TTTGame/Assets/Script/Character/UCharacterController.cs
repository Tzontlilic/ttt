﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Tnet;
using VGame;

public enum ECharAnimState
{
    ECAS_Stand,
    ECAS_Attack,
    ECAS_Die,
    ECAS_Max
}
public enum ECharMoveState
{
    ECMS_Walk,
    ECMS_Run,
}


public enum ECharType
{
    ECT_Player,
    ECT_Trap,
    ECT_Enemy,
    ECT_Max
}

public class UCharacterController : CPropData {

    public SkinMeshController m_SkinMeshController = null;

    public int playerId = 0;
    
    /// <summary>
    /// GameObject reference
    /// </summary>
	public GameObject Instance
    {
        get
        {
            if(m_SkinMeshController != null)
            {
                return m_SkinMeshController.Instance;
            }
            return null;
        }
    }

    //Character GameObject Transform Ref
    Transform m_Trans = null;
    public Transform ThisTrans
    {
        get
        {
            if (m_Trans == null && m_SkinMeshController != null)
            {
                m_Trans = m_SkinMeshController.Instance.transform;
            }
            return m_Trans;
        }
    }

    //Character Prof Info
    public UnitConfig m_profession = null;

    //Character Weapon Info
    public WeaponConfig m_gunInfo = null;
    public GameObject m_weaponGO = null;
    protected Transform weaponBindTrans = null;
    protected SkinnedMeshRenderer weaponSkinMesh = null;
    public Transform firePos = null;

    public Vector3 position
    {
        get
        {
            if (ThisTrans != null)
                return ThisTrans.position;
            return Vector3.zero;
        }
        set
        {
            if (ThisTrans != null)
                ThisTrans.position = value;
        }
    }

    public Vector3 forward
    {
        get
        {
            if (ThisTrans != null)
                return ThisTrans.forward;
            return new Vector3(1, 0, 0);
        }
    }    

    /// <summary>
    /// The Charactor Type Of Geom
    /// </summary>
    protected ECharType charType = ECharType.ECT_Max;

    //define for mini Geom
    protected CustomBindPos m_cbp = null;

    protected UIStateBar stateBar = null;

    protected ECharMoveState m_moveState = ECharMoveState.ECMS_Walk;

    protected float m_fMoveSpeed = 0;

    protected Vector3 m_moveDir = Vector3.zero;

    protected Vector3 m_rotDir = Vector3.zero;

    public UCharacterController(int profId) : base()
    {
        m_profession = UnitProfession.getProfession(profId);
        SpawnGeom();
        InitChar();
        GameRules.calcCharacterProps(this, 1);
    }

    #region Equipment
    public virtual void SpawnGeom()
    {
        if (m_profession == null)
            return;

        m_SkinMeshController = new SkinMeshController();
        m_SkinMeshController.SpawnGeom(m_profession.res);
        m_SkinMeshController.SetGeomSection(EEquipType.EET_Chest, m_profession.chest);
        m_SkinMeshController.SetGeomSection(EEquipType.EET_Head, m_profession.head);
        m_SkinMeshController.SetGeomSection(EEquipType.EET_Hand, m_profession.hand);
        m_SkinMeshController.SetGeomSection(EEquipType.EET_Feet, m_profession.feet);
        m_SkinMeshController.CombineGeom();

        PlayStand();
    }

    public void InitChar()
    {
        m_cbp = Instance.GetComponentInChildren<CustomBindPos>();
        if (m_cbp != null)
        {
            weaponBindTrans = m_cbp.weapon;
            if (weaponBindTrans != null)
            {
                weaponSkinMesh = weaponBindTrans.GetComponent<SkinnedMeshRenderer>();
                m_weaponGO = weaponBindTrans.gameObject;
            }
            m_cbp.onAnimShoot = attrShoot;
            m_cbp.onAnimReload = attrReload;
            firePos = m_cbp.firePos;
        }
    }
	
	public void ChangeWeapon (string weapon)
	{
        UnityEngine.Object res = ResourcesMgr.Load (GSTR.WeaponResPath + weapon);
		GameObject oldWeapon = m_weaponGO;
		m_weaponGO = GameObject.Instantiate (res) as GameObject;
		m_weaponGO.transform.parent = oldWeapon.transform.parent;
		m_weaponGO.transform.localPosition = Vector3.zero;
		m_weaponGO.transform.localScale = Vector3.one;
		m_weaponGO.transform.localRotation = Quaternion.identity;
		
		GameObject.Destroy(oldWeapon);
	}
	
	public void ChangeEquipment (EEquipType equipType, string equipment, bool combine = false)
    {
        m_SkinMeshController.SetGeomSection(equipType, equipment);
        m_SkinMeshController.CombineGeom();
    }

    public bool CanUseWeapon(WeaponConfig weapon)
    {
        if (m_profession.weaponTypes.Contains(weapon.type))
            return true;
        return false;
    }

    protected bool isInEdit = false;
    protected int curWeaponType = -1;
    public void SwitchWeapon(int id)
    {
        if (weaponSkinMesh == null)
            return;

        WeaponConfig weaponInfo = UnityWeapon.getWeapon(id);
        if (weaponInfo != null)
        {
            GameObject gunRes = ResourcesMgr.Load(GSTR.WeaponResPath + weaponInfo.res) as GameObject;
            if (gunRes != null)
            {
                MeshFilter mf = gunRes.GetComponent<MeshFilter>();
                if (mf != null)
                {
                    weaponSkinMesh.sharedMesh = mf.sharedMesh;
                    MeshRenderer mr = gunRes.GetComponent<MeshRenderer>();
                    weaponSkinMesh.sharedMaterial = mr.sharedMaterials[weaponInfo.matid];
                }
                if (isInEdit)
                    SetLayerRecursively(Instance, GNS.DUILayer);
                else
                    SetLayerRecursively(Instance, 2);
            }

            curWeaponType = (int)weaponInfo.type;
            m_gunInfo = weaponInfo;
        }
        Main.inst.gunDesc.text = m_gunInfo.name;
        Main.inst.bulletUI.SetActive(m_gunInfo.canLockAim);
    }

    public Vector3 getFirePos()
    {
        if (firePos != null)
            return firePos.position;
        return position;
    }

    public Vector3 getHitPos()
    {
        if (m_cbp != null)
            return m_cbp.spine.position;
        return position;
    }
    #endregion

    #region Anim Control
    protected UCharacterController lockedTarget = null;
    public virtual void PlayStand ()
    {
        m_SkinMeshController.PlayStandAnim();
        lockedTarget = null;
    }

    public virtual void PlayRun()
    {
        //if > 1 become Run Anim
        if (m_moveState == ECharMoveState.ECMS_Walk)
            m_SkinMeshController.SetMoveAnimSpeed(1);
        else if (m_moveState == ECharMoveState.ECMS_Run)
            m_SkinMeshController.SetMoveAnimSpeed(2);
    }

    public virtual bool CanAttack()
    {
        if (hasReload == false)
            return false;
        return true;
    }

    protected bool hasReload = true;
    protected LaserEffect aimEffect = null;
    public virtual void PlayCrouchAim()
    {
        m_SkinMeshController.PlayCrouchAnim();
    }

    public void PrepareAttack()
    {
        m_cbp.onAnimShoot = attrShoot;
    }

    public void PlayAttack (bool _needReload = false) {
        if (!CanAttack())
            return;

        //2 is In CouchAim Cant shoot in continuous
        if (m_SkinMeshController.GetStandStatuesValue() == 2 && !hasReload)
            return;

        if(_needReload)
            hasReload = false;

        m_SkinMeshController.PlayAttackAnim();
    }

    public void PlayReload()
    {
        if(m_SkinMeshController.GetStandStatuesValue() == 2)
        {
            if (aimEffect != null)
                aimEffect.gameObject.SetActive(false);
        }
        hasReload = false;
        m_SkinMeshController.PlayReloadAnim();
		FpDebug.Log (playerId.ToString () + "play Reload at Step = " + LockStepMgr.g_iStepCount.ToString ());
    }
    #endregion

    #region Visualisation 
    protected GameObject visualisationObj = null;
    protected FieldOfView FOVMesh = new FieldOfView();
    protected MeshFilter FOV_MeshFilter = null;
    public void UpdateFOVMesh(float sectorRadius, float sectorAngle)
    {
        if (visualisationObj == null)
        {
            visualisationObj = new GameObject(Instance.name + "Vision");
            if( m_cbp != null)
                visualisationObj.transform.parent = m_cbp.custom02;
            else
                visualisationObj.transform.parent = Instance.transform;
            visualisationObj.transform.localPosition = new Vector3(0, 0.1f, 0);
            visualisationObj.transform.localScale = Vector3.one;
            visualisationObj.transform.localRotation = Quaternion.Euler(0, 0, 0);

            FOV_MeshFilter = visualisationObj.AddComponent<MeshFilter>();
            FOV_MeshFilter.mesh = FOVMesh.CreateMesh(sectorRadius, sectorAngle, 9, visualisationObj.transform);
            visualisationObj.AddComponent<MeshRenderer>().sharedMaterial = Main.inst.visionMat;
            //FOV_MeshFilter.mesh.MarkDynamic();
        }
        else
        {
            FOV_MeshFilter.mesh.Clear();
            FOV_MeshFilter.mesh = FOVMesh.CreateMesh(sectorRadius, sectorAngle, 9, visualisationObj.transform);
        }

        if (isInEdit)
            visualisationObj.SetActive(false);
        else
            visualisationObj.SetActive(true);
    }

    public List<UCharacterController> visionTargets = new List<UCharacterController>();
    public bool UpdateVisionTarget()
    {
        if (GameRoom.instance.GameStarted == false)
            return false;

        for (int i = 0; i < visionTargets.Count; i++)
        {
            visionTargets[i].ShowAimImg(false);
        }
        if (!m_gunInfo.canLockAim)
            return false;
        TargetFilter targetFilter = TargetFilter.findFilter("selVisionEnemy");
        visionTargets = targetFilter.selectTarget(this);
        if (visionTargets.Count > 0)
        {
            for (int i = 0; i < visionTargets.Count; i++)
            {
                visionTargets[i].ShowAimImg(true);
            }
        }
        else
        {
            lockedTarget = null;
            showAimEffect(false);
        }
        return false;
    }

    public void showAimEffect(bool show)
    {
        if (show == false && aimEffect != null)
        {
            MonoBehaviour.Destroy(aimEffect.gameObject);
            aimEffect = null;
            return;
        }

        if (show && lockedTarget != null && m_gunInfo != null)
        {
            if (aimEffect == null)
            {
                GameObject go = GameObject.Instantiate(Main.inst.aimLaser) as GameObject;
                aimEffect = go.GetComponent<LaserEffect>();
            }
            aimEffect.setLaserObject(firePos.gameObject, lockedTarget.Instance);
            aimEffect.target_bind = BodyPos.Spine;
        }
    }

    public void BeginLockTarget()
    {
        if (visionTargets.Count > 0)
        {
            if (lockedTarget == null)
            {
                lockedTarget = visionTargets[0];
                showAimEffect(true);
            }
            else
            {
                int index = visionTargets.IndexOf(lockedTarget);
                if (index == -1)
                {
                    lockedTarget = visionTargets[0];
                    showAimEffect(true);
                }
                else
                {
                    if (index - 1 >= 0)
                    {
                        lockedTarget = visionTargets[index - 1];
                        showAimEffect(true);
                    }
                    else
                    {
                        lockedTarget = visionTargets[visionTargets.Count - 1];
                        showAimEffect(true);
                    }
                }
            }
        }
        else
        {
            lockedTarget = null;
            showAimEffect(false);
        }
    }


    public bool CanLockTarget()
    {
        if (lockedTarget == null && visionTargets.Count == 0)
            return false;
        return true;
    }

    public bool isLockTargetValid()
    {
        if (lockedTarget != null)
        {
            return CanAimTarget(lockedTarget, m_gunInfo.bulletRange);
        }
        return false;
    }

    public bool CanAimTarget(UCharacterController _target, float _visionDistance)
    {
        Vector3 firePos = getFirePos();
        Vector3 hitPos = _target.getHitPos();
        Vector3 dirToEnemy = (hitPos - firePos).normalized;
        float distance = Vector3.Distance(hitPos, firePos);
        if (distance > m_gunInfo.bulletRange)
            return false;

        float angles = Vector3.Angle(forward, dirToEnemy);
        if (Mathf.Abs(angles) > m_gunInfo.sectorAngle / 2.0f && distance >= Main.inst.visionDistance)
            return false;

        if (Physics.Raycast(firePos, dirToEnemy, distance, Main.inst.FOV_CastLayer))
            return false;

        return true;
    }

    #endregion

    #region Anim Event Call
    public void attrReload()
    {
        Debug.Log("===============>attrReload");
        if (!hasReload)
        {
            hasReload = true;
            if (lockedTarget != null)
            {
                if (aimEffect != null)
                    aimEffect.gameObject.SetActive(true);
            }
        }
    }

    public Action EndAttackAction = null;
    public void attrShoot()
    {
        if (isInEdit)
        {
            if (m_SkinMeshController.GetStandStatuesValue() == 2)
                PlayReload();
            return;
        }
        if (m_weaponGO != null && m_gunInfo != null)
        {
            BulletController bc = null;
            if (lockedTarget != null)
                bc = App.Game.bulletMgr.GenerateBullet(this, m_gunInfo.speBulletId, firePos.position, firePos.rotation);
            else
                bc = App.Game.bulletMgr.GenerateBullet(this, m_gunInfo.defBulletId, firePos.position, firePos.rotation);


            if (lockedTarget != null)
            {
                bc.SetLockedTarget(lockedTarget);
            }
            else
            {
                bc.InitTarget(GameRoom.instance.targetPlayers);
            }


            if (m_gunInfo.fireEff != string.Empty)
            {
                UnityEngine.Object fireEff = ResourcesMgr.Load(m_gunInfo.fireEff);
                if (fireEff != null)
                {
                    GameObject flashObj = GameObject.Instantiate(fireEff) as GameObject;
                    flashObj.transform.parent = firePos;
                    flashObj.transform.localPosition = Vector3.zero;
                    flashObj.transform.localRotation = Quaternion.identity;
                    flashObj.AddComponent<SelfCleaner>().fLife = 0.2f;
                }
            }
        }
    }
    #endregion
    
    #region view Anim
    public void PlayViewAttact()
    {
        m_SkinMeshController.PlayAttackAnim();
    }
    public void PlayFight()
    {
        m_SkinMeshController.PlayFightAnim();
    }
    #endregion

    public void PlayExplosive()
    {
        if (Main.inst.BlockObjRoot != null)
        {
            Rigidbody[] allRB = Main.inst.BlockObjRoot.GetComponentsInChildren<Rigidbody>();
            for (int i = 0; i < allRB.Length; i++)
            {
                if (allRB[i].isKinematic)
                {
                    allRB[i].isKinematic = false;
                }
                //allRB[i].AddExplosionForce(Main.inst.expForce, Main.inst.expObj.transform.position, Main.inst.expRadiu, 3.0F);
                //allRB[i].AddExplosionForce(Main.inst.expForce, WeaponInstance.transform.position + Main.inst.expOffset, Main.inst.expRadiu, 3.0F);
            }
        }
        //if(animationState == ECharAnimState.ECAS_Die)
        //{
        //    Rigidbody[] allRB = Instance.GetComponentsInChildren<Rigidbody>();
        //    for (int i = 0; i < allRB.Length; i++)
        //    {
        //        if (allRB[i].isKinematic)
        //        {
        //            allRB[i].isKinematic = false;
        //        }
        //        //allRB[i].AddExplosionForce(Main.inst.expForce, Main.inst.expObj.transform.position, Main.inst.expRadiu, 3.0F);
        //        //allRB[i].AddExplosionForce(Main.inst.expForce, WeaponInstance.transform.position + Main.inst.expOffset, Main.inst.expRadiu, 3.0F);
        //    }
        //}
    }

    public void PlayDie()
    {
        //if (animationState == ECharAnimState.ECAS_Die)
        //    return;

        //MeshCollider mc = m_weaponGO.GetComponent<MeshCollider>();
        //if (mc != null && mc.enabled == false)
        //    mc.enabled = true;

        //animationState = ECharAnimState.ECAS_Die;
        //if(animationController.isActiveAndEnabled == true)
        //{
        //    animationController.enabled = false;
        //}
        //Rigidbody[] allRB = Instance.GetComponentsInChildren<Rigidbody>();
        //for (int i = 0; i < allRB.Length; i++)
        //{
        //    if (allRB[i].isKinematic)
        //    {
        //        allRB[i].isKinematic = false;
        //    }
        //    //allRB[i].AddExplosionForce(Main.inst.expForce, Main.inst.expObj.transform.position, Main.inst.expRadiu, 3.0F);
        //    //allRB[i].AddExplosionForce(Main.inst.expForce, WeaponInstance.transform.position + Main.inst.expOffset, Main.inst.expRadiu, 3.0F);
        //}
        //if (thidController !=null)
        //    thidController.canMove = false;

    }

    public void PlayLive()
    {
        //if (animationState != ECharAnimState.ECAS_Die)
        //    return;
        //Rigidbody[] allRB = Instance.GetComponentsInChildren<Rigidbody>();
        //for (int i = 0; i < allRB.Length; i++)
        //{
        //    if (allRB[i].isKinematic == false)
        //    {
        //        allRB[i].isKinematic = true;
        //    }
        //}
        //m_weaponGO.transform.localPosition = Vector3.zero;
        //m_weaponGO.transform.localScale = Vector3.one;
        //m_weaponGO.transform.localRotation = Quaternion.identity;
        //if (animationController.isActiveAndEnabled == false)
        //{
        //    animationController.enabled = true;
        //}
        //MeshCollider mc = m_weaponGO.GetComponent<MeshCollider>();
        //if (mc != null && mc.enabled == true)
        //    mc.enabled = false;
        //PlayStand();
    }    

    public virtual bool isInMove()
    {
        return false;
    }
	
	// Update is called once per frame
	public virtual void Update () {

        if (!ClientGame.isCurState(EGameState.VGS_GamePlay))
            return;

        if (visualisationObj != null)
        {
            FOVMesh.UpdateMesh();
        }
        if (HPUIShowTime > 0)
        {
            HPUIShowTime -= Time.deltaTime;
            if (HPUIShowTime < 0)
            {
                HPUIShowTime = 0;
                m_showHPUI = false;
            }
        }
    }

    public Transform getStateUITrans()
    {
        if (m_cbp != null)
            return m_cbp.custom01;
        return null;
    }
    public Transform getAimTrans()
    {
        if (m_cbp != null)
            return m_cbp.spine;
        return null;
    }

    public void AddStateBar()
    {
        if(stateBar == null)
        {
            UnityEngine.Object res = ResourcesMgr.Load("Prefab/UI/UIStateBar");
            GameObject ui = GameObject.Instantiate(res) as GameObject;
            ui.transform.localPosition = Vector3.zero;
            ui.transform.localRotation = Quaternion.identity;
            ui.transform.SetParent(Main.inst.UIRoot.transform, false);
            stateBar = ui.GetComponent<UIStateBar>();
            if (stateBar != null)
            {
                stateBar.Bind(this);
            }
        }
    }

    public void ShowAimImg(bool show)
    {
        if (GameRoom.instance.IsControlPlayer(this as PlayerController))
            return;

        if (show)
            FadeOutFog();

        if (aimEffect != null)
            SetLayerRecursively(aimEffect.gameObject, GNS.PlayerLayer);
    }


    float FadeOutTime = 5000;
    public void FadeOutFog()
    {
        SetLayerRecursively(Instance, GNS.PlayerLayer);
        FadeOutTime = 5000;
        if (stateBar != null)
        {
            stateBar.gameObject.SetActive(true);
        }
    }

    public void FadeInFog()
    {
        SetLayerRecursively(Instance, GNS.EnemyFogLayer);
        if (stateBar != null)
        {
            stateBar.gameObject.SetActive(false);
        }
    }

    public void UpdateFadeOutTime(int _deltaTime)
    {
        if (GameRoom.instance.IsControlPlayer(this as PlayerController))
            return;
        if ( FadeOutTime > 0 )
        {
            FadeOutTime -= _deltaTime;
            if (FadeOutTime <= 0)
                FadeInFog();
        }
    }
    

    //
    public void fightNotice(BattleNotice txtType)
    {

    }

    public void fightNotice(string pStr)
    {
        UnityEngine.Object res = ResourcesMgr.Load("Prefab/UI/DmgHUD");
        GameObject ui = GameObject.Instantiate(res) as GameObject;
        ui.transform.localPosition = Vector3.zero;
        ui.transform.localRotation = Quaternion.identity;
        ui.transform.SetParent(Main.inst.UIRoot.transform, false);
        DmgHUD dmghud = ui.GetComponent<DmgHUD>();
        dmghud.Bind(this, pStr);
    }

    public bool m_showHPUI = false;
    float HPUIShowTime = 5;
    public void OnDamage(Damage damage)
    {
        //Debug.Log(this);
        int damageValue = damage.sum_;
                
        if (damage.sum_ != 0)
        {
            //GloblaValue.trig_FE_Event(GloblaValue.FE_HPUPDATE, null, this, damage.sum_);
            if (damage.type_ != DamageType.DT_Heal && damage.type_ != DamageType.DT_HOT && damage.type_ != DamageType.DT_Xp && damage.type_ != DamageType.DT_DeathXp)
            {
                damageValue = Utils.Limit(damage.sum_, 0, (int)damage.victimHp_);
            }
        }

        if (damageValue > 0)
        {
            fightNotice("+" + damageValue.ToString());
            m_showHPUI = true;
            HPUIShowTime = 5;
        }
        else if( damageValue == 0)
            fightNotice("miss");
        //如果已经死亡不再死亡
        DecHp(damage);
    }


    // call back buffmanager and process attrevent
    private void DecHp(Damage dmg)
    {
        // fire event die 
        if (IsDead())
        {
            //animator.SetBool("Death_b", true);
            //animator.SetInteger("DeathType_int", 1);
        }
    }

    //time record in millisecond
    protected int DeathTime = 5000;
    public void OnDie()
    {
        if( GameRoom.instance.IsControlPlayer(this as PlayerController) )
        {
            RenderSetting.instance.CameraDeathEffect();
        }
        (this as PlayerController).Die();
        DeathTime = 5000;
    }

    public void Alive()
    {
        GameRules.calcCharacterProps(this, 1);

        if (GameRoom.instance.IsControlPlayer(this as PlayerController))
        {
            RenderSetting.instance.CameraLiveEffect();
        }
        (this as PlayerController).Live();
    }

    public void DestroyGeom()
    {
        if( stateBar != null)
            MonoBehaviour.Destroy(stateBar.gameObject);
        stateBar = null;
        m_SkinMeshController.DestroyThis();
        m_cbp = null;
    }

    public void DestroyCharacter()
    {
        DestroyGeom();
        (this as IStepUnit).Release();
    }

    public void SetLayerRecursively(GameObject go, int layerNumber)
    {
        foreach (Transform trans in go.GetComponentsInChildren<Transform>(includeInactive: true))
        {
            trans.gameObject.layer = layerNumber;
        }
    }
}
