﻿using UnityEngine;
using System.Collections.Generic;
using Tnet;

public class netMain : MonoBehaviour {

    EventLoop loop = new EventLoop(false);
    TConn conn;
	// Use this for initialization
	void Start () {
        NetApp.Init(Message.SERVLET_CLIENT);
        RegisterServlet();
        Log.Init("log.txt");
        
        conn = new TConn(loop);
        conn.Connect("192.168.3.54", 9527);
        conn.connectOk = () => {
            Debug.Log("connect ok");
        };

    }
    void RegisterServlet() {
        Servlet.Add(new CL_NotifyItemsServlet());
        Servlet.Add(new CL_NotifyValuesServlet());
        Servlet.Add(new CL_NotifySnycStepServlet());
    }
	
	// Update is called once per frame
	void Update () {
        loop.Update();
        UpdatKeyboard();
	}

    public void Login()
    {
        MC_LoginServlet mclogin = new MC_LoginServlet();
        mclogin.req.user = "tanlei";
        conn.Call(mclogin);
        mclogin.Cb = (self) => {
            Debug.Log("mclogin ok,uid=!"+ self.rsp.uid);
        };
    }

    public void CeateRoom()
    {
        MC_CreateRoomServlet mc_creatroom = new MC_CreateRoomServlet();
        mc_creatroom.req.uid = 1002;
        conn.Call(mc_creatroom);
        mc_creatroom.Cb = (self) => {
            Debug.Log("create room ok,uid=!"+ self.rsp.roomid);
        };
    }

    public void JoinRoom()
    {
        MC_JoinRoomServlet joinroom = new MC_JoinRoomServlet();
        joinroom.req.uid = 1002;
        conn.Call(joinroom);
        joinroom.Cb = (self) => {
            Debug.Log("join room ok,uid=!"+ self.rsp.roomid);
        };
    }
    public void StartRoom()
    {
        MC_StartRoomReq req;
        req.roomid = 1001;
        conn.Send(req);
    }

    [SerializeField]
    KeyCode testKeyCode;
    public void UpdatKeyboard()
    {
        if ( Input.GetKeyUp(testKeyCode))
        {
            MC_SendStepReq req;
            req.actionId = 1;
            req.fp = new List<float>();
            conn.Send(req);
            Debug.Log("send Step Input Up = " + testKeyCode.ToString());
        }
    }
}