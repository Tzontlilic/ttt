﻿using UnityEngine;

namespace Tnet
{
    public class CL_NotifyItemsServlet : CL_NotifyItems {
        public override int Execute(TConn conn) {
            foreach(G_BagItemOpt x in req.items) {
                Debug.Log(string.Format("id={0},count={1}", x.id, x.count));
            }
            return 0;
        }
    }
    public class CL_NotifyValuesServlet : CL_NotifyValues {
        public override int Execute(TConn conn) {
            foreach(G_ValueOpt x in req.values) {
                Debug.Log(string.Format("id={0},value={1}", x.id, x.value));
            }
            return 0;
        }
    }

    public class CL_NotifySyncRoomInfoServlet : CL_NotifySyncRoomInfo
    {
        public override int Execute(TConn conn)
        {
            Debug.Log("CL_NotifySyncRoomInfoServlet");
            GameRoom.instance.SyncRoomPlayers(req);
            return 0;
        }
    }

    public class CL_NotifyEnterSceneServlet : CL_NotifyEnterScene {
        public override int Execute(TConn conn) {
            Debug.Log("CL_NotifyEnterSceneServlet ");
            GameRoom.instance.EnterGame();
            return 0;
        }
    }

    public class CL_NotifyStartGameServlet : CL_NotifyStartGame {
        public override int Execute(TConn conn) {
            GameRoom.instance.StartGame();
            return 0;
        }
    }

    public class CL_NotifySnycStepServlet : CL_NotifySyncStep
    {
        public override int Execute(TConn conn)
        {
            LockStepMgr.instance.ReceiveAction(req.stepId, req.actions.ToArray());
            return 0;
        }
    }
}