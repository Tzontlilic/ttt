using System.Collections.Generic;


namespace Tnet {
    public struct CL_NotifyItemsReq : ISerial {
        public List<G_BagItemOpt> items;
        
        public ushort MsgId() {
            return Message.CL_NOTIFY_ITEMS;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteUint((uint)this.items.Count);
            foreach (G_BagItemOpt sscc_i in this.items) {
                sscc_i.Serial(buf);
            }
        }
        
        public void Unserial(ByteBuffer buf) {
            do {
                this.items = new List<G_BagItemOpt>();
                uint sscc_size = buf.ReadUint();
                for (uint sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                    G_BagItemOpt item = new G_BagItemOpt();
                    item.Unserial(buf);
                    this.items.Add(item);
                }
            } while (false);
        }
        
    }
    public abstract class CL_NotifyItems : Servlet {
        public CL_NotifyItemsReq req;
        public CL_NotifyItems(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.CL_NOTIFY_ITEMS;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
    public struct CL_NotifyValuesReq : ISerial {
        public List<G_ValueOpt> values;
        
        public ushort MsgId() {
            return Message.CL_NOTIFY_VALUES;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteUint((uint)this.values.Count);
            foreach (G_ValueOpt sscc_i in this.values) {
                sscc_i.Serial(buf);
            }
        }
        
        public void Unserial(ByteBuffer buf) {
            do {
                this.values = new List<G_ValueOpt>();
                uint sscc_size = buf.ReadUint();
                for (uint sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                    G_ValueOpt item = new G_ValueOpt();
                    item.Unserial(buf);
                    this.values.Add(item);
                }
            } while (false);
        }
        
    }
    public abstract class CL_NotifyValues : Servlet {
        public CL_NotifyValuesReq req;
        public CL_NotifyValues(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.CL_NOTIFY_VALUES;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
    public struct CL_NotifySyncRoomInfoReq : ISerial {
        public int roomId;
        public List<G_RoomPlayerOpt> players;
        
        public ushort MsgId() {
            return Message.CL_NOTIFY_SYNCROOMINFO;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.roomId);
            buf.WriteUint((uint)this.players.Count);
            foreach (G_RoomPlayerOpt sscc_i in this.players) {
                sscc_i.Serial(buf);
            }
        }
        
        public void Unserial(ByteBuffer buf) {
            this.roomId = buf.ReadInt();
            do {
                this.players = new List<G_RoomPlayerOpt>();
                uint sscc_size = buf.ReadUint();
                for (uint sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                    G_RoomPlayerOpt item = new G_RoomPlayerOpt();
                    item.Unserial(buf);
                    this.players.Add(item);
                }
            } while (false);
        }
        
    }
    public abstract class CL_NotifySyncRoomInfo : Servlet {
        public CL_NotifySyncRoomInfoReq req;
        public CL_NotifySyncRoomInfo(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.CL_NOTIFY_SYNCROOMINFO;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
    public struct CL_NotifySyncStepReq : ISerial {
        public int stepId;
        public List<G_StepOpt> actions;
        
        public ushort MsgId() {
            return Message.CL_NOTIFY_SYNCSTEP;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.stepId);
            buf.WriteUint((uint)this.actions.Count);
            foreach (G_StepOpt sscc_i in this.actions) {
                sscc_i.Serial(buf);
            }
        }
        
        public void Unserial(ByteBuffer buf) {
            this.stepId = buf.ReadInt();
            do {
                this.actions = new List<G_StepOpt>();
                uint sscc_size = buf.ReadUint();
                for (uint sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                    G_StepOpt item = new G_StepOpt();
                    item.Unserial(buf);
                    this.actions.Add(item);
                }
            } while (false);
        }
        
    }
    public abstract class CL_NotifySyncStep : Servlet {
        public CL_NotifySyncStepReq req;
        public CL_NotifySyncStep(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.CL_NOTIFY_SYNCSTEP;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
    public struct CL_NotifyEnterSceneReq : ISerial {
        public int roomId;
        
        public ushort MsgId() {
            return Message.CL_NOTIFY_ENTERSCENE;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.roomId);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.roomId = buf.ReadInt();
        }
        
    }
    public abstract class CL_NotifyEnterScene : Servlet {
        public CL_NotifyEnterSceneReq req;
        public CL_NotifyEnterScene(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.CL_NOTIFY_ENTERSCENE;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
    public struct CL_NotifyStartGameReq : ISerial {
        public int roomId;
        
        public ushort MsgId() {
            return Message.CL_NOTIFY_STARTGAME;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.roomId);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.roomId = buf.ReadInt();
        }
        
    }
    public abstract class CL_NotifyStartGame : Servlet {
        public CL_NotifyStartGameReq req;
        public CL_NotifyStartGame(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.CL_NOTIFY_STARTGAME;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
}

