using System;
using System.Collections.Generic;


namespace Tnet {
    public struct MC_LoginReq : ISerial {
        public string user;
        public string deviceModel;
        public string deviceId;
        
        public ushort MsgId() {
            return Message.MC_LOGIN;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteString(this.user);
            buf.WriteString(this.deviceModel);
            buf.WriteString(this.deviceId);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.user = buf.ReadString();
            this.deviceModel = buf.ReadString();
            this.deviceId = buf.ReadString();
        }
        
    }
    public struct MC_LoginRsp : ISerial {
        public int uid;
        public string name;
        public List<G_BagItemOpt> bag;
        public List<G_ValueOpt> values;
        
        public ushort MsgId() {
            return Message.MC_LOGIN;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.uid);
            buf.WriteString(this.name);
            buf.WriteUint((uint)this.bag.Count);
            foreach (G_BagItemOpt sscc_i in this.bag) {
                sscc_i.Serial(buf);
            }
            buf.WriteUint((uint)this.values.Count);
            foreach (G_ValueOpt sscc_i in this.values) {
                sscc_i.Serial(buf);
            }
        }
        
        public void Unserial(ByteBuffer buf) {
            this.uid = buf.ReadInt();
            this.name = buf.ReadString();
            do {
                this.bag = new List<G_BagItemOpt>();
                uint sscc_size = buf.ReadUint();
                for (uint sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                    G_BagItemOpt item = new G_BagItemOpt();
                    item.Unserial(buf);
                    this.bag.Add(item);
                }
            } while (false);
            do {
                this.values = new List<G_ValueOpt>();
                uint sscc_size = buf.ReadUint();
                for (uint sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                    G_ValueOpt item = new G_ValueOpt();
                    item.Unserial(buf);
                    this.values.Add(item);
                }
            } while (false);
        }
        
    }
    public abstract class MC_Login : Servlet {
        public MC_LoginReq req;
        public MC_LoginRsp rsp;
        private Action<MC_Login> cb;
        public MC_Login(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.MC_LOGIN;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return rsp;
        }
        public override void SerialResponse(ByteBuffer buf) {
            rsp.Serial(buf);
        }
        public override void UnserialResponse(ByteBuffer buf) {
            rsp.Unserial(buf);
        }
        public override int OnBack() {
            if (cb != null) {
                cb(this);
            }
            return 0;
        }
        public Action<MC_Login> Cb {
            set {
                cb = value;
            }
        }
    }
}

