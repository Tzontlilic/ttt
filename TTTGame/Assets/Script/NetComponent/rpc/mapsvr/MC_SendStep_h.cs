using System;
using System.Collections.Generic;


namespace Tnet {
    public struct MC_SendStepReq : ISerial {
        public int actionId;
        public List<float> fp;
        
        public ushort MsgId() {
            return Message.MC_SEND_STEP;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.actionId);
            buf.WriteUint((uint)this.fp.Count);
            foreach (float sscc_i in this.fp) {
                buf.WriteFloat(sscc_i);
            }
        }
        
        public void Unserial(ByteBuffer buf) {
            this.actionId = buf.ReadInt();
            do {
                this.fp = new List<float>();
                uint sscc_size = buf.ReadUint();
                for (uint sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                    float item = new float();
                    item = buf.ReadFloat();
                    this.fp.Add(item);
                }
            } while (false);
        }
        
    }
    public abstract class MC_SendStep : Servlet {
        public MC_SendStepReq req;
        public MC_SendStep(bool needRpc = false) {
            this.needRpc = needRpc;
        }
        public override ushort MsgId() {
            return Message.MC_SEND_STEP;
        }
        public override ISerial Req() {
            return req;
        }
        public override void SerialRequest(ByteBuffer buf) {
            req.Serial(buf);
        }
        public override void UnserialRequest(ByteBuffer buf) {
            req.Unserial(buf);
        }
        public override ISerial Rsp() {
            return null;
        }
        public override void SerialResponse(ByteBuffer buf) {
        }
        public override void UnserialResponse(ByteBuffer buf) {
        }
        public override int OnBack() {
            return 0;
        }
    }
}

