using System.Collections.Generic;


namespace Tnet {
    public struct G_StepOpt : ISerial {
        public int playerId;
        public int actionId;
        public List<float> fp;
        
        public ushort MsgId() {
            return 0;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.playerId);
            buf.WriteInt(this.actionId);
            buf.WriteUint((uint)this.fp.Count);
            foreach (float sscc_i in this.fp) {
                buf.WriteFloat(sscc_i);
            }
        }
        
        public void Unserial(ByteBuffer buf) {
            this.playerId = buf.ReadInt();
            this.actionId = buf.ReadInt();
            do {
                this.fp = new List<float>();
                uint sscc_size = buf.ReadUint();
                for (uint sscc_i = 0 ; sscc_i < sscc_size; ++sscc_i) {
                    float item = new float();
                    item = buf.ReadFloat();
                    this.fp.Add(item);
                }
            } while (false);
        }
        
    }
}

