
namespace Tnet {
    public struct G_BagItemOpt : ISerial {
        public int id;
        public int count;
        
        public ushort MsgId() {
            return 0;
        }
        
        public void Serial(ByteBuffer buf) {
            buf.WriteInt(this.id);
            buf.WriteInt(this.count);
        }
        
        public void Unserial(ByteBuffer buf) {
            this.id = buf.ReadInt();
            this.count = buf.ReadInt();
        }
        
    }
}

