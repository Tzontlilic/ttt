﻿/*

    It is a Unity project that display how to build the avatar equipment system in Unity.
    Equipment system is very important in the Game, specially in MMO Game.

    Normally, equipment system contains tow important parts. 
    Since the appearance of equipments are different(the mesh are different), so to merge these meshes together is necessary. 
    Second, after merge meshes, the new mesh contains many materials(in this project, it has 4 material), that means it has at least 4 drawcalls(depends in the shader).
    So to merge materials together will reduce drawcalls and improve game performance.

*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using VGame;

/// <summary>
/// A simple framework of the game.
/// </summary>
public class App
{
	private static App app = new App();

	public static App Game { get { return app; } }    

    public BulletMgr bulletMgr = new BulletMgr();

    public void Update ()
	{
        UCharacterMgr.instance.Update ();	
        //bulletMgr.Update();
    }

    public void InitGameCharactor(int _playerId)
    {
    }

    bool hasInitEnemy = false;
    public void InitGameEnemy(Vector3[] pos)
    {
        if (hasInitEnemy)
            return;
        for( int i =0; i < pos.Length; i++)
        {
            UCharacterMgr.instance.GenerateEnemy(4, pos[i]);
        }
        //CharacterMgr.GenerateTrap();
        hasInitEnemy = true;
    }

    public UCharacterController[] getAllEnemy()
    {
        return UCharacterMgr.instance.enemyList.ToArray();
    }
}

public class Main : MonoBehaviour {    
    
    /// <summary>
    /// The avatar in the scene.
    /// </summary>
    public static Main inst = null;

    public Material visionMat = null;

    public GameObject UIRoot = null;

    public GameObject BlockObjRoot = null;
    Rigidbody[] allRB = null;

    public GameObject bulletUI = null;

    public GameObject dmgHUDUI = null;

    public GameObject effPrefab = null;
    public GameObject DeathEffPrefab = null;

    public GameObject aimLaser = null;

    public GameObject[] AllGuns = null;

    public GameObject[] AllBody = null;

    public Vector3[] EnemyBirthPos = null;

    public Text gunDesc = null;

    private int gunID = 0;

    public UIGunListPanel UIGunList = null;

    public Camera ViewCamera = null;

    public LayerMask DUILayer;

    public GameObject battleScene = null;

    public GameObject ViewUI = null;
    public GameObject GameUI = null;
    public PanelManager PanelMgr = null;
    public GameObject UIBGObj = null;
    public GameObject UIStage = null;

    public LayerMask FOV_CastLayer;

    public bool animLockUpdate = true;

    [Range(-1, 1)]
    public float animSpeed = 1;

    public float visionDistance = 5;

    public int FOVMeshEdgeAdjust = 3;

    // Use this for initialization
    void Start () {
        inst = this;
    }

    public void InitGunList()
    {
        UIGunList.InitGunList();
    }

    public Transform[] stageTrans = null;
    public void OnEnterBattle()
    {
        if (battleScene != null)
        {
            battleScene.SetActive(true);
            //App.Game.InitGameEnemy(EnemyBirthPos);
            UIBGObj.SetActive(false);
            StartCoroutine(BeginLockStepDelay(2));
        }
    }

    IEnumerator BeginLockStepDelay(float second)
    {
        yield return new WaitForSeconds(second);
        ClientGame.instance.ReadyGame();
    }

    public void BeginBattle()
    {
        PanelMgr.Close(OnEnterBattle);
    }
	
	// Update is called once per frame
	void Update () {	
		//App.Game.Update();
	}

    void LateUpdate()
    {
        App.Game.Update();
    }

    public void PlayViewAttack()
    {
        GameRoom.instance.Player.PlayViewAttact();
    }

    public void PlayViewFight()
    {
        GameRoom.instance.Player.PlayFight();
    }

    public void PlayViewStand()
    {
        GameRoom.instance.Player.PlayStand();
    }

    public void PlayViewCrouch()
    {
        GameRoom.instance.Player.PlayCrouchAim();
    }

    bool farView = false;
    public Text ViewTxt = null;
    public void ViewChg()
    {
        farView = !farView;
        if (farView)
        {
            MapCamera.instance.ShiftUpAim();
            ViewTxt.text = "Near";
        }
        else
        {
            MapCamera.instance.ShiftDownAim();
            ViewTxt.text = "Far";
        }
    }
    public void LockAim()
    {
        GameRoom.instance.Player.SendLockTarget();
    }

    public void SwitchWeapon()
    {
        gunID++;
        if (gunID >= AllGuns.Length)
            gunID = 0;
        GameRoom.instance.Player.SwitchWeapon(gunID);
    }

    public GameObject ScreenTraceObj(Vector2 ScreenPos, LayerMask layer)
    {
        GameObject hitObj = null;
        RaycastHit hit;
        Ray ray = Main.inst.ViewCamera.ScreenPointToRay(ScreenPos);
        if (Physics.Raycast(ray, out hit, float.MaxValue, layer))
        {
            hitObj = hit.collider.gameObject;
        }
        return hitObj;
    }

    public void AddExplosive(Vector3 expPos, float force, float radiu)
    {
        if (BlockObjRoot != null)
        {
            if(allRB == null)
            {
                allRB = BlockObjRoot.GetComponentsInChildren<Rigidbody>();
            }
            for (int i = 0; i < allRB.Length; i++)
            {
                if (allRB[i].isKinematic)
                {
                    allRB[i].isKinematic = false;
                }
                allRB[i].AddExplosionForce(force, expPos, radiu);
            }
        }
    }


    void OnGUI () {
        
    }

}
