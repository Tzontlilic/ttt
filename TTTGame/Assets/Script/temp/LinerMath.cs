﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class polygon
{
}
public class LinerMath
{

	//========================================
	static int g_nAdjArc = 40;
	static float g_nAdjLen = 1.5f;
	static float g_nAdjLenBig = 4.0f;
	
	static float INFINITY=-200000;
	
	public static float dis_PP(float x1,float y1,float x2,float y2)
	{
		return Mathf.Sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
	}
	public static float dis_PP(Vector2 a,Vector2 b)
	{
		return Mathf.Sqrt((float)(a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
	}
	/********************************************/
	/*             判断两点是否共点             */
	/********************************************/
	public static bool equal_PP(Vector2 a,Vector2 b)
	{
		if( a.x==b.x && a.y==b.y )
			return true;
		else
			return false;
	}
	/********************************************/
	/*     方向向量dir逆时针旋转angle角度         	*/
	/*|cos(a), -sin(a)|逆时针旋转矩阵
	/*|sin(a),  cos(a)|
	/*假设向量B是向量A逆时针旋转angle角度所得，则：
	/* B=(A.x*(cos(angle) - sin(angle)), a.y*(sin(angle) + cos(angle))
	/********************************************/
	public static Vector2 rotDir( Vector2 dir, float angle )
	{  
		float a = angle * Mathf.PI / 180;
		Vector2 result = new Vector2(dir.x*Mathf.Cos (a) + dir.y*Mathf.Sin(a), -dir.x*Mathf.Sin(a) + dir.y*Mathf.Cos(a));
		return result;
	}

	public static Vector2 rotDirRadian( Vector2 dir, float ra )
	{  
		Vector2 result = new Vector2(dir.x*Mathf.Cos (ra) + dir.y*Mathf.Sin(ra), -dir.x*Mathf.Sin(ra) + dir.y*Mathf.Cos(ra));
		return result;
	}
	
	/********************************************/
	/* 返回(P1-P0)*(P2-P0)的叉积。              */
	/* 若为正，则<P0,P1>在<P0,P2>的顺时针方向   */
	/* 若为零，则<P0,P1><P0,P2>共线；           */
	/* 若为负，则<P0,P1>在<P0,P2>的在逆时针方向 */
	/* 这个函数可以确定两条线段在交点处的转向   */
	/* 比如确定p0p1和p1p2在p1处是左转还是右转， */
	/* 只要求 (p2-p0)*(p1-p0)，                 */
	/* 若<0则左转，>0则右转，=0则共线           */
	/********************************************/
	public static float multiply( Vector2 p1 , Vector2 p2 , Vector2 p0 )
	{     
		////应用平方根是为了加大误差值
		//float ajk=(p1.x-p0.x)*(p2.y-p0.y);
		//ajk=Mathf.Sqrt(fabs(ajk));
		//float bjk=(p2.x-p0.x)*(p1.y-p0.y);
		//bjk=Mathf.Sqrt(fabs(bjk));
		//int abjk=ajk-bjk;
		return ((p1.x-p0.x)*(p2.y-p0.y)-(p2.x-p0.x)*(p1.y-p0.y));
	}
	/********************************************/
	/*           判断点p是否在线段ab上          */
	/********************************************/
	public static bool online( Vector2 a,Vector2 b, Vector2 p )
	{   
		//if( equal_PP(p,a) || equal_PP(p,b) )
		//	return true;
		//int aj=multiply(b,p,a);
		//bool ak=min(a.x,b.x)<p.x<max(a.x,b.x);
		//bool al=min(a.y,b.y)<p.y<max(a.y,b.y);
		return( (multiply(b,p,a)==0)&&( (Mathf.Min(a.x,b.x)<=p.x&&p.x<=Mathf.Max(a.x,b.x) )&&( Mathf.Min(a.y,b.y)<=p.y&&p.y<=Mathf.Max(a.y,b.y))) );
	}
	/************************************************************************/
	/* 计算点P到直线AB的垂足
/************************************************************************/
	public static Vector2 dis_CZ(Vector2 p,Vector2 a,Vector2 b)
	{
		if(online(a,b,p))
		{
			return p;
		}
		Vector2 d = new Vector2 ();//垂足
		float dx,dy;//垂足
		float ax=a.x;
		float ay=a.y;
		float bx=b.x;
		float by=b.y;
		float px=p.x;
		float py=p.y;
		if(a.x==b.x)
		{
			d.x=a.x;
			d.y=p.y;
			dx=d.x;
			dy=d.y;
		}
		else if(a.y==b.y)
		{
			d.x=p.x;
			d.y=a.y;
			dx=d.x;
			dy=d.y;
		}
		else 
		{
			float k;//斜率
			k=(by-ay)/(bx-ax);
			dx=(k*k*ax+k*(py-ay)+px)/(k*k+1); 
			dy=k*(dx-ax)+ay;
			d.x=dx;
			d.y=dy;
		}
		return d;
	}
	/************************************************************************/
	/* 计算点P到线段AB的距离 
	设线段的两端点为pt1和pt2，斜率为：k = ( pt2.y - pt1. y ) / (pt2.x - pt1.x );
	该直线方程为：y = k* ( x - pt1.x) + pt1.y。
	其垂线的斜率为 - 1 / k，垂线方程为：y = (-1/k) * (x - Vector2.x) + Vector2.y 。 
	　　联立两直线方程解得：
	x = ( k^2 * pt1.x + k * (Vector2.y - pt1.y ) + Vector2.x ) / ( k^2 + 1) ，
	y = k * ( x - pt1.x) + pt1.y;
	然后再判断垂足是否在线段上，如果在线段上则返回P到垂足的距离；
	如果不在则计算两端点到垂足的距离，选择距离较近的返回。
	/************************************************************************/
	public static float dis_PL(Vector2 p,Vector2 a,Vector2 b)
	{
		if(online(a,b,p))
			return 0.0f;
		Vector2 d = new Vector2 ();//垂足
		d=dis_CZ(p,a,b);
		//如果点在线段AB上，应该用ONLINE但精度还是高，所以此处简化，判断D是否在AB所形成的矩形内
		if((Mathf.Min(a.x,b.x)<=d.x&&d.x<=Mathf.Max(a.x,b.x) )||( Mathf.Min(a.y,b.y)<=d.y&&d.y<=Mathf.Max(a.y,b.y)))//online(a,b,d))
		{
			return dis_PP(p.x,p.y,d.x,d.y);
		}
		else
		{
			
			return Mathf.Min(dis_PP(a.x,a.y,d.x,d.y),dis_PP(b.x,b.y,d.x,d.y));
		}
	}
	/************************************************************************/
	/* 计算点P到AB所在直线的距离 
	/************************************************************************/
	public static float dis_PBL(Vector2 p,Vector2 a,Vector2 b)
	{
		if(online(a,b,p))
			return 0.0f;
		Vector2 d = new Vector2 ();//垂足
		d=dis_CZ(p,a,b);
		return dis_PP(p.x,p.y,d.x,d.y);
	}
	/********************************************/
	/*   快速排斥+跨立判断直线段a1a2和b1b2相交     */
	/********************************************/
	public static bool inter_SS( Vector2 a1,Vector2 a2,Vector2 b1,Vector2 b2)
	{ 
		bool flag = ((Mathf.Max (a1.x, a2.x) >= Mathf.Min (b1.x, b2.x)) && (Mathf.Max (b1.x, b2.x) >= Mathf.Min (a1.x, a2.x))
					&& (Mathf.Max (a1.y, a2.y) >= Mathf.Min (b1.y, b2.y)) && (Mathf.Max (b1.y, b2.y) >= Mathf.Min (a1.y, a2.y)));
		if( flag )
		{
			flag = (multiply(b1,a2,a1)*multiply(a2,b2,a1)>=0) && (multiply(a1,b2,b1)*multiply(b2,a2,b1)>=0);
		}
		return flag;
	}
	/********************************************/
	/*              判断点是否在圆内            */
	/********************************************/
	public static bool insideRound( Vector2 o,float r ,Vector2 p )
	{     
		return ( dis_PP(p.x,p.y,o.x,o.y)<=r );
	}
	/************************************************************************/
	/* 判断线段与圆是否相交 
	先计算圆心到线段的最短距离如果大于半径则不线段AB和圆R不相交
	/************************************************************************/
	public static bool inter_SR(Vector2 a, Vector2 b, Vector2 o, float r)
	{
		//a,b都包含在圆O内，则没有交点
		//if(insideRound(o,r,a)&&insideRound(o,r,b))
		//	return false;
		if(insideRound(o,r,a)||insideRound(o,r,b))
			return true;
		float k= dis_PL(o,a,b);
		if(r>=k)
			return true;
		else
			return false;
	}
	/************************************************************************/
	/* 求线段AB在延长线B点以外,从A开始N个长度的点P                          */
	/************************************************************************/
	public static Vector2 LineProlong(Vector2 a,Vector2 b,float fLen)
	{
		Vector2 p = new Vector2 ();
		if(a.x==b.x)//平行于Y轴
		{
			if(a.y>b.y)
			{
				p.y=b.y-fLen;
			}
			else
			{
				p.y=b.y+fLen;
			}
			p.x=a.x;
			return p;
		}
		if(a.y==b.y)
		{
			if(a.x>b.x)
			{
				p.x=b.x-fLen;
			}
			else
			{
				p.x=b.x+fLen;
			}
			p.y=a.y;
			return p;
		}
		float ax=a.x;
		float ay=a.y;
		float bx=b.x;
		float by=b.y;
		float ba=(ay-by)/(ax-bx);
		//求点B在直线BD上左右一个单位的两个点M,N的坐标
		float fGen=Mathf.Sqrt(ba*ba+1);
		float fGen1=fGen*-1;
		float mx,my,nx,ny;
		mx=fLen/fGen+bx;
		my=ba*(mx-bx)+by;
		nx=fLen/fGen1+bx;
		ny=ba*(nx-bx)+by;
		//求远离D点的坐标
		if(dis_PP(mx,my,ax,ay)>dis_PP(nx,ny,ax,ay))
		{
			if( fLen < 0 )
			{
				p.x=nx;
				p.y=ny;
			}
			else
			{
				p.x=mx;
				p.y=my;
			}
			return p;
		}
		else
		{
			if( fLen < 0 )
			{
				p.x=mx;
				p.y=my;
			}
			else
			{
				p.x=nx;
				p.y=ny;
			}
			return p;
		}
	}
	/************************************************************************/
	/* 如果寻路的点同时在圆上和多边形顶点上，则取此顶点的角平分线的反方向一个单位
	传入的为角ABC，B为角所在点
	/************************************************************************/
	public static Vector2 adjust(Vector2 a, Vector2 b,Vector2 c, float adjLen)
	{
		Vector2 Yes = new Vector2 ();
		//求线段ba在延长线a点以外,从b开始200个长度的点P
		Vector2 aa=LineProlong(b,a,200.0f);
		//求线段bc在延长线c点以外,从b开始200个长度的点P
		Vector2 bb=LineProlong(b,c,200.0f);
		//cc 是点aa 和 bb 的中点
		Vector2 cc = new Vector2 ();
		cc.x=(aa.x+bb.x)/2.0f;
		cc.y=(aa.y+bb.y)/2.0f; 
		//当角度小于50度时，调节点与B点距离加大30:103.5  45:153.1 
		//公式是 2*200.0*sin(aa,bb的角度) 这里设为40
		float nLimit=2.0f*200.0f*Mathf.Sin(g_nAdjArc*3.14159265358979323846f/180.0f);
		//aa,bb 两点间距离
		float aabb=dis_PP(aa,bb);
		if(aabb<nLimit)
			Yes=LineProlong(cc,b,adjLen);
		else
			Yes=LineProlong(cc,b,adjLen);//6是调节点，
		return Yes;
	}
	
	/************************************************************************/
	/* 如果寻路的点同时在圆上和多边形顶点上，则取此顶点的角平分线的反方向一个单位
	传入的为角ABC，B为角所在点, 并且判断是否在多边形内，若是，则取反方向的 位置
	/************************************************************************/
	//public static Vector2 adjustInPolygon(Vector2 a, Vector2 b,Vector2 c, polygon poly)
	//{
	//	Vector2 Yes = new Vector2 ();
	//	//求线段ba在延长线a点以外,从b开始200个长度的点P
	//	Vector2 aa=LineProlong(b,a,200.0f);
	//	//求线段bc在延长线c点以外,从b开始200个长度的点P
	//	Vector2 bb=LineProlong(b,c,200.0f);
	//	//cc 是点aa 和 bb 的中点
	//	Vector2 cc = new Vector2 ();
	//	cc.x=(aa.x+bb.x)/2.0f;
	//	cc.y=(aa.y+bb.y)/2.0f; 
	//	//当角度小于50度时，调节点与B点距离加大30:103.5  45:153.1 
	//	//公式是 2*200.0*sin(aa,bb的角度) 这里设为40
	//	float nLimit=2.0f*200.0f*Mathf.Sin(g_nAdjArc*3.14159265358979323846f/180.0f);
	//	//aa,bb 两点间距离
	//	float aabb=dis_PP(aa,bb);
	//	if(aabb<nLimit)
	//		Yes=LineProlong(cc,b,g_nAdjLenBig);
	//	else
	//		Yes=LineProlong(cc,b,g_nAdjLen);//6是调节点
				
	//	if( insidePolygon(poly.pAdjList, Yes))
	//	{
	//		if(aabb<nLimit)
	//			Yes=LineProlong(cc,b,-g_nAdjLenBig);
	//		else
	//			Yes=LineProlong(cc,b,-g_nAdjLen);//6是调节点
	//	}
		
	//	return Yes;
	//}
	/********************************************/
	/*          判断点是否在多边形内            */
	/********************************************/
	public static bool insidePolygon( List<Vector2> ver , Vector2 point )
	//bool insidePolygon( int vcount , Vector2[] ver , Vector2 point )
	{    
		int i,c=0;
		Vector2 lineA,lineB,edgeA,edgeB;
		lineA=lineB=point;
		lineB.x=INFINITY;
		for (i=0; i < ver.Count; i++)
		{
			edgeA=ver[i]; 
			if( i== ver.Count - 1)
	 			edgeB=ver[0];
			else
				edgeB=ver[i+1];
			if( online(edgeA,edgeB,point) )
				return true;
			if( edgeA.y==edgeB.y )
				continue;
			if( (Mathf.Min(edgeA.y,edgeB.y)!=point.y)&&inter_SS(lineA,lineB,edgeA,edgeB))
				c++;
		}
		return (c%2) == 1 ;
	}
    
    /********************************************/
    /*          两个向量的夹角            */
    /********************************************/
    public static double AngleBetween(Vector2 vector1, Vector2 vector2)
    {
        double sin = vector1.x * vector2.y - vector2.x * vector1.y;
        double cos = vector1.x * vector2.x + vector1.y * vector2.y;

        return Math.Atan2(sin, cos) * (180 / Math.PI);
    }

}
public class Mathfx
{
    public static float Hermite(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, value * value * (3.0f - 2.0f * value));
    }

    public static float Sinerp(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, Mathf.Sin(value * Mathf.PI * 0.5f));
    }

    public static float Coserp(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, 1.0f - Mathf.Cos(value * Mathf.PI * 0.5f));
    }

    public static float Berp(float start, float end, float value)
    {
        value = Mathf.Clamp01(value);
        //Change The Pow value higher to lessen the berp, lower to exaggerate it.
        value = (Mathf.Sin(value * Mathf.PI * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + (1.2f * (1f - value)));
        return start + (end - start) * value;
    }

    public static Vector3 Berp(Vector3 from, Vector3 to, float t)
    {
        from.x = Berp(from.x, to.x, t);
        from.y = Berp(from.y, to.y, t);
        from.z = Berp(from.z, to.z, t);
        return from;
    }

    public static float SmoothStep(float x, float min, float max)
    {
        x = Mathf.Clamp(x, min, max);
        float v1 = (x - min) / (max - min);
        float v2 = (x - min) / (max - min);
        return -2 * v1 * v1 * v1 + 3 * v2 * v2;
    }

    public static float Lerp(float start, float end, float value)
    {
        return ((1.0f - value) * start) + (value * end);
    }

    public static Vector3 NearestPoint(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 lineDirection = Vector3.Normalize(lineEnd - lineStart);
        float closestPoint = Vector3.Dot((point - lineStart), lineDirection) / Vector3.Dot(lineDirection, lineDirection);
        return lineStart + (closestPoint * lineDirection);
    }

    public static Vector3 NearestPointStrict(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 fullDirection = lineEnd - lineStart;
        Vector3 lineDirection = Vector3.Normalize(fullDirection);
        float closestPoint = Vector3.Dot((point - lineStart), lineDirection) / Vector3.Dot(lineDirection, lineDirection);
        return lineStart + (Mathf.Clamp(closestPoint, 0.0f, Vector3.Magnitude(fullDirection)) * lineDirection);
    }
    public static float Bounce(float x)
    {
        return Mathf.Abs(Mathf.Sin(6.28f * (x + 1f) * (x + 1f)) * (1f - x));
    }

    // test for value that is near specified float (due to floating point inprecision)
    // all thanks to Opless for this!
    public static bool Approx(float val, float about, float range)
    {
        return ((Mathf.Abs(val - about) < range));
    }

    // test if a Vector3 is close to another Vector3 (due to floating point inprecision)
    // compares the square of the distance to the square of the range as this
    // avoids calculating a square root which is much slower than squaring the range
    public static bool Approx(Vector3 val, Vector3 about, float range)
    {
        return ((val - about).sqrMagnitude < range * range);
    }

    /*
      * CLerp - Circular Lerp - is like lerp but handles the wraparound from 0 to 360.
      * This is useful when interpolating eulerAngles and the object
      * crosses the 0/360 boundary.  The standard Lerp function causes the object
      * to rotate in the wrong direction and looks stupid. Clerp fixes that.
      */
    public static float Clerp(float start, float end, float value)
    {
        float min = 0.0f;
        float max = 360.0f;
        float half = Mathf.Abs((max - min) / 2.0f);//half the distance between min and max
        float retval = 0.0f;
        float diff = 0.0f;

        if ((end - start) < -half)
        {
            diff = ((max - start) + end) * value;
            retval = start + diff;
        }
        else if ((end - start) > half)
        {
            diff = -((max - end) + start) * value;
            retval = start + diff;
        }
        else retval = start + (end - start) * value;

        // Debug.Log("Start: "  + start + "   End: " + end + "  Value: " + value + "  Half: " + half + "  Diff: " + diff + "  Retval: " + retval);
        return retval;
    }
    public static float ScaleFloat(float target, float sourceMin, float sourceMax, float resultMin, float resultMax)
    {
        float result = resultMin + (float)(target - sourceMin) / (sourceMax - sourceMin) * (resultMax - resultMin);
        return result;
    }
    public static Color ScaleColorRGB(float target, float sourceMin, float sourceMax, Color resultMin, Color resultMax)
    {
        Color color = resultMin;
        color.r = Mathfx.ScaleFloat(target, sourceMin, sourceMax, resultMin.r, resultMax.r);
        color.g = Mathfx.ScaleFloat(target, sourceMin, sourceMax, resultMin.g, resultMax.g);
        color.b = Mathfx.ScaleFloat(target, sourceMin, sourceMax, resultMin.b, resultMax.b);
        return color;
    }
    public static Vector3 PolarToCartesian(Vector2 polar)
    {
        //an origin vector, representing lat,lon of 0,0. 
        Vector3 origin = Vector3.forward;
        //build a quaternion using euler angles for lat,lon
        Quaternion rotation = Quaternion.Euler(-polar.x, -polar.y, 0);
        //transform our reference vector by the rotation. Easy-peasy!
        Vector3 point = rotation * origin;

        return point;
    }

    public static Vector3 Rotate90_XZ(Vector3 v)
    {
        return new Vector3(-v.z, 0, v.x);
    }
}
