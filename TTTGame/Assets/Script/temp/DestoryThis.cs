﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestoryThis : MonoBehaviour {
	
	private ParticleSystem[] 	psList;
	//private SpawnPool 			myTestPool;
	private bool[] 				psListTag;
	private int 				skillID_;
	private GameObject			caster_;


	// Use this for initialization
	void Start () {
		psList = this.GetComponentsInChildren<ParticleSystem>();
	}

	void LateUpdate(){

		if (psList == null)
			return;
		for(int i = 0; i < psList.Length; i++)
		{
			if(psList[i].IsAlive()){
				return;
			}
		}
		try
		{
			Destroy(gameObject);
			//myTestPool = PoolManager.Pools["Effects"];
			//myTestPool.Despawn(this.transform);
		}
		catch (KeyNotFoundException)
		{
			//Destroy(gameObject);
		}
	}

	public void Destory()
	{
		if (psList == null)
			return;

		try
		{
            psList = null;
			Destroy(gameObject);
		}
		catch (KeyNotFoundException)
		{
			//Destroy(gameObject);
		}
	}
	
	public GameObject refCaster
	{		
		get {
			return caster_;
		}
		set {
			caster_ = value;
		}
	}

	public int refSkill
	{		
		get {
			return skillID_;
		}
		set {
			skillID_ = value;
		}
	}
}


//根据时间删除gameObjec对象的脚本，时间自由制定
public class DestoryThisByTime : MonoBehaviour
{

    public float destoryTime = 1.0f;
    // Use this for initialization
    void Start()
    {
        //Destroy (gameObject, destoryTime);
    }

    void LateUpdate()
    {
        destoryTime = destoryTime - Time.deltaTime;
        if (destoryTime <= 0f)
        {
            try
            {
                Destroy(gameObject);
                //myTestPool = PoolManager.Pools["Effects"];
                //myTestPool.Despawn(this.transform);
            }
            catch (KeyNotFoundException)
            {
                //Destroy(gameObject);
            }
        }
    }

}