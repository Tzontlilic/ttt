using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;


// Summary:
//     The Resources class allows you to find and access Objects including assets.
public class ResourcesMgr
{
    public static Dictionary<string, UnityEngine.AssetBundle> ResList = new Dictionary<string,UnityEngine.AssetBundle>();
    public static bool UseStreaming = false;
    const string LocalPath = "Assets/Resource/";
    static Dictionary<string, List<string>> TypeResList;

    public static UnityEngine.Object[] FindObjectsOfTypeAll(Type type)
	{
        bool isComponent = type.IsSubclassOf(typeof(UnityEngine.Component));
#if UNITY_EDITOR
        if(TypeResList == null)
        {
            
            TypeResList = new Dictionary<string, List<string>>();
            string[] all = UnityEditor.AssetDatabase.FindAssets(".", new string[] { "Assets/Resource" });
            for (int i = 0; i < all.Length; ++i)
            {
                UnityEditor.EditorUtility.DisplayProgressBar("Loading", "Searching assets, please wait...", (float)i / all.Length);
                string fileName = UnityEditor.AssetDatabase.GUIDToAssetPath(all[i]);
                UnityEngine.Object o = UnityEditor.AssetDatabase.LoadMainAssetAtPath(fileName);
                if (o == null)
                    continue;
                if (!TypeResList.ContainsKey(o.GetType().Name))
                {
                    TypeResList.Add(o.GetType().Name, new List<string>());
                }
                System.Type tp = o.GetType();
                if (tp == type || tp.IsSubclassOf(type) && !TypeResList[o.GetType().Name].Contains(fileName))
                    TypeResList[o.GetType().Name].Add(fileName);

                if (UnityEditor.PrefabUtility.GetPrefabType(o) == UnityEditor.PrefabType.Prefab)
                {
                    UnityEngine.Component[] t = (o as UnityEngine.GameObject).GetComponents<UnityEngine.Component>();
                    foreach (UnityEngine.Component a in t)
                    {
                        if (a == null)
                            continue;
                        if (!TypeResList.ContainsKey(a.GetType().Name))
                        {
                            TypeResList.Add(a.GetType().Name, new List<string>());
                        }
                        if (!TypeResList[a.GetType().Name].Contains(fileName))
                            TypeResList[a.GetType().Name].Add(fileName);
                    }
                }
                
            }
            UnityEditor.EditorUtility.ClearProgressBar();
        }

        List<UnityEngine.Object> res = new List<UnityEngine.Object>();
        if (!TypeResList.ContainsKey(type.Name))
            return res.ToArray();
        
        for (int i = 0; i < TypeResList[type.Name].Count; ++i)
        {
            string fileName = TypeResList[type.Name][i];
            UnityEngine.Object o = UnityEditor.AssetDatabase.LoadMainAssetAtPath(fileName);
            if (o == null || res.Contains(o))
                continue;
            if (!isComponent)
            {
                res.Add(o);
            }
            else if (UnityEditor.PrefabUtility.GetPrefabType(o) == UnityEditor.PrefabType.Prefab)
            {
                UnityEngine.Object t = (o as UnityEngine.GameObject).GetComponent(type);
                if (t != null && !res.Contains(t)) 
                    res.Add(t);
            }
        }
        UnityEngine.Object[] arr = res.ToArray();
        return arr;
#else
        return null;
#endif
	}
	//
	// Summary:
	//     Loads an asset stored at path in a Resources folder.
    public static UnityEngine.Object Load(string path)
	{
        if (!path.Contains("/"))
            return null;
        if (UseStreaming)
        { 
            AssetsData info = AssetManager.Instance.GetAssetInfo(path);
            if(info == null)
            {
                return null;
            }
            if (!ResList.ContainsKey(info.ResName))
            {
                return null;
            }
            else
            {
                if (info.Type == (int)AssetType.Model)
                {
                    string subsetName = path.Substring(path.LastIndexOf('/') + 1, path.Length - path.LastIndexOf('/') - 1);
                    UnityEngine.Transform t = ResList[info.ResName].LoadAsset(subsetName, typeof(UnityEngine.Transform)) as UnityEngine.Transform;

                    return t.gameObject;
                }
                else
                {
                    return ResList[info.ResName].mainAsset;
                }
            }
        }
        return LoadLocal(path);
	}
    
    static Dictionary<string, UnityEngine.Object> _LoadedResDict = new Dictionary<string, UnityEngine.Object>();
    static UnityEngine.Object LoadLocal(string path)
    {
        if(_LoadedResDict.ContainsKey(path))
        {
            return _LoadedResDict[path];
        }
        else
        {
            UnityEngine.Object obj = UnityEngine.Resources.Load(path);
            if(obj != null)
            {
                _LoadedResDict[path] = obj;
                return obj;
            }
            return null;
        }
    }

    static void UnloadResource()
    {
        _LoadedResDict.Clear();
    }
		
	//
	// Summary:
	//     Asynchronously loads an asset stored at path in a Resources folder.
    /*public static UnityEngine.ResourceRequest LoadAsync(string path)
	{
		return UnityEngine.Resources.LoadAsync(path);
	}
    public static UnityEngine.ResourceRequest LoadAsync<T>(string path) where T : UnityEngine.Object
	{
		return UnityEngine.Resources.LoadAsync<T>(path);
	}
	//
	// Summary:
	//     Asynchronously loads an asset stored at path in a Resources folder.
    public static UnityEngine.ResourceRequest LoadAsync(string path, Type type)
	{
		return UnityEngine.Resources.LoadAsync(path, type);
	}*/
	
	//
	// Summary:
	//     Unloads assetToUnload from memory.
    public static void UnloadAsset(string name)
    {
		AssetsData info = AssetManager.Instance.GetAssetInfo(name);
        if (info != null)
        {
            if (ResList.ContainsKey(info.ResName))
            {
                ResList[info.ResName].Unload(false);
                ResList.Remove(info.ResName);
                //Debug.LogWarning(info.ResName + " - unloaded");
            }
        } 
    }

    public static void UnloadAllAsset()
    {
        foreach (UnityEngine.AssetBundle a in ResList.Values)
        {
            a.Unload(false);
        }
        ResList.Clear();
    }

    public static void UnloadUnusedAssets()
	{
        foreach(UnityEngine.AssetBundle a in ResList.Values)
        {
            a.Unload(false);
        }
        ResList.Clear();
	}    
}