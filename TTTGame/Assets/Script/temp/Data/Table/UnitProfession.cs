﻿using System;
using System.Collections.Generic;
using ComRpc;

public class UnitProfession
{
    public static Dictionary<int, UnitConfig> units_ = new Dictionary<int, UnitConfig>();

    private static CSVLoader unitTable;

    public static bool loadUnit()
    {
        unitTable = new CSVLoader("profession", ',', true);
        if (!unitTable.isValid_)
            return false;

        units_.Clear();

        for (int row = 0; row < unitTable.records_.Count; row++)
        {
            UnitConfig newUnit = new UnitConfig();
            newUnit.id = unitTable.get_int(row, "id");
            newUnit.res = unitTable.get_string(row, "res");
            newUnit.name = unitTable.get_string(row, "name");
            newUnit.props = new List<COM_CPropValue>();
            for( int i =0; i < (int)CPropType.CPT_MAX; i++)
            {
                CPropType t = (CPropType)i;
                if( unitTable.checkColunmKey(t.ToString()) != -1 )
                {
                    COM_CPropValue newProp = new COM_CPropValue();
                    newProp.propOpType_ = (int)PropOPType.PT_B;
                    newProp.propType_ = t;
                    newProp.value_ = unitTable.get_float(row, t.ToString());
                    newUnit.props.Add(newProp);
                }
            }

            newUnit.hand = unitTable.get_string(row, "hand");
            newUnit.chest = unitTable.get_string(row, "chest");
            newUnit.head = unitTable.get_string(row, "head");
            newUnit.feet = unitTable.get_string(row, "feet");
            newUnit.weaponId = unitTable.get_int(row, "weaponId");

            newUnit.weaponTypes = new List<EWeaponType>();
            string[] wTypeStr = unitTable.get_string(row, "weaponType").Split(';');
            for( int i =0; i < wTypeStr.Length; i++)
            {
                if (wTypeStr[i] == string.Empty)
                    continue;
                EWeaponType type = (EWeaponType)Enum.Parse(typeof(EWeaponType), wTypeStr[i]);
                newUnit.weaponTypes.Add(type);
            }

            units_.Add(newUnit.id, newUnit);
        }
        FpDebug.Log("All Unit Loaded = " + units_.Count);
        return true;
    }

    public static UnitConfig getProfession(int id)
    {
        if (units_.ContainsKey(id))
        {
            return units_[id];
        }
        return null;
    }
}
