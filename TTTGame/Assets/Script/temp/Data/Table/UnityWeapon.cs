﻿using System;
using System.Collections.Generic;
using ComRpc;
using UnityEngine;

public class UnityWeapon
{
    public static Dictionary<int, WeaponConfig> weapons_;

    private static CSVLoader weaponTable;
    public static bool loadWeapon()
    {
        weaponTable = new CSVLoader("WeaponTable", ',', true);
        if (!weaponTable.isValid_)
            return false;

        weapons_ = new Dictionary<int, WeaponConfig>();

        for (int row = 0; row < weaponTable.records_.Count; row++)
        {
            WeaponConfig newWeapon = new WeaponConfig();
            newWeapon.id = weaponTable.get_int(row, "id");
            newWeapon.res = weaponTable.get_string(row, "res");
            newWeapon.name = weaponTable.get_string(row, "name");
            newWeapon.matid = weaponTable.get_int(row, "matid");

            string typeStr = weaponTable.get_string(row, "type");
            if( typeStr != string.Empty )
            {
                newWeapon.type = (EWeaponType)Enum.Parse(typeof(EWeaponType), typeStr);
            }
            newWeapon.defBulletId = weaponTable.get_int(row, "defBullet");
            newWeapon.speBulletId = weaponTable.get_int(row, "speBullet");
            newWeapon.bulletRange = weaponTable.get_float(row, "bulletRange");
            newWeapon.sectorAngle = weaponTable.get_float(row, "sectorAngle");
            newWeapon.fireEff = weaponTable.get_string(row, "fireEff");
            newWeapon.canLockAim = weaponTable.get_bool(row, "canLockAim");
            weapons_.Add(newWeapon.id, newWeapon);
        }
        FpDebug.Log("All Weapon Loaded = " + weapons_.Count);
        return true;
    }

    public static WeaponConfig getWeapon(int id)
    {
        if(weapons_.ContainsKey(id))
        {
            return weapons_[id];
        }
        return null;
    }
}

public class WeaponBullet
{
    public static Dictionary<int, BulletConfig> bullets_;

    private static CSVLoader bulletTable;
    public static bool loadBullet()
    {
        bulletTable = new CSVLoader("BulletTable", ',', true);
        if (!bulletTable.isValid_)
            return false;

        bullets_ = new Dictionary<int, BulletConfig>();

        for (int row = 0; row < bulletTable.records_.Count; row++)
        {
            BulletConfig newBullet = new BulletConfig();
            newBullet.id = bulletTable.get_int(row, "id");
            newBullet.res = bulletTable.get_string(row, "res");
            newBullet.name = bulletTable.get_string(row, "name");
            newBullet.speed = bulletTable.get_float(row, "speed");
            newBullet.width = bulletTable.get_float(row, "width");
            newBullet.phyForce = bulletTable.get_float(row, "phyForce");
            newBullet.phyForceRange = bulletTable.get_float(row, "phyForceRange");
            newBullet.dmgAction = bulletTable.get_string(row, "dmgAction");
            newBullet.hitEff = bulletTable.get_string(row, "hitEff");
            bullets_[newBullet.id] = newBullet;
        }
        FpDebug.Log("All Bullet Loaded = " + bullets_.Count);
        return true;
    }

    public static BulletConfig getBullet(int id)
    {
        if (bullets_.ContainsKey(id))
        {
            return bullets_[id];
        }
        FpDebug.LogError("Bullet Id(" + id.ToString() + ") not exist!");
        return null;
    }
}