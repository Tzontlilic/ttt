﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// 技能相关定义

// 技能类型，远程，非远程
public enum SkillType{
	NO_REMOTE,
	REMOTE
}

// 技能类型【普通，自动，必杀，被动，光环，战阵】
public enum SkillClass{
	NORMAL,
	AUTO,
	KILLED,
	NO_ACTIVE,
	AURA,
  TROOP
}

public enum BulletType {
  BT_SIGNLE,			//单体
  BT_CROSS,				//穿透
  BT_ALL,					//全体
  BT_CIRCLE,			//回旋
  BT_BOUNCE				//弹射
}

public enum SkillStateType{
	ICE, // 冰冻
	TEND, // 击退
	RIDICULE, // 嘲讽
	BACKBLOOD,  // 吸血
	ADDICT, // 魅惑
	FIXED,// 定身
	FUNK, //恐惧
	SILENCE //沉默
}

public class SkillEffect{
	public GameObject effectPrefab;
	public AudioSource audioEffect;
	public BodyPos pos;
	public string name;
	public float camShift = 0; 
	public float scale = 1.0f;
	public bool canFlow = true;
	public bool bindSelf = true;
}

// 技能定义
public class SkillConfig{

	public int Id;
	public string name;
	public string description;
	public int cdSubType;
	public float initCD;
	public float CD;
	public int action;
	public List<int> effects_id = new List<int>();
	public int hurt_effect_id;
	public int heal_effect_id;
	//技能自带属性
	public List<COM_CPropValue> CPVs_ = new List<COM_CPropValue>();
	//升级效果
	public List<COM_CPropValue> CPVLvs_ = new List<COM_CPropValue>();

	//子弹类型对象
	public string bulletName;
	
	public SkillClass skillClass;
	public SkillType skillType;
	public bool isDirectional;		//是否是指向型技能
	public bool canBroken = true;	//victim
	public bool breakTarget;		//attacker
	public int	woundID;			//被击动画（含胸还是仰头）
    public float fightCapacity;
	//
    //技能效果
	public string regEvent_;		//被动技能用来相应事件
	public bool   useTarget_;		//被动技能使用target作为action效果对象
    public string targetFilter_;    //用来选取目标
    public string targetCondition_; //目标的施法条件判断
    public string casterCondition_; //施法者的施法条件判断
    public string targetAction_;    //目标技能的具体逻辑（伤害，状态，buff）
    public string casterAction_;    //施法者的技能效果
	public Color  hitHLColor_ = Color.gray;		//击中目标后的被击颜色
	public string endFilter_;    	//技能结束时处理用来选取目标
	public string endAction_;    	//技能结束时处理效果

	public string skillIcon;			// 技能图标
	public int activeStep;				// 技能激活品质
	public List<int> groupSkills = new List<int>();	// 同一个技能的其他表现形式，以随机形式出现
	public string effectDesc;

}