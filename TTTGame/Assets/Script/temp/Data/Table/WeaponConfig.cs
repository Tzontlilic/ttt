﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponConfig
{
    public int id;
    public string res;
    public string name;
    public int matid;
    public EWeaponType type;
    public int defBulletId;
    public int speBulletId;
    public float bulletRange;
    public float sectorAngle;
    public bool canLockAim;
    public string fireEff;
}

public class BulletConfig
{
    public int id;
    public string res;
    public string name;
    public float speed;
    public float width;
    public float phyForce;
    public float phyForceRange;
    public string dmgAction;
    public string hitEff;
}