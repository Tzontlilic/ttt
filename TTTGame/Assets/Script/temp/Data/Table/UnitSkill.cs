using System;
using System.Collections.Generic;
using ComRpc;
using UnityEngine;

public class UnitSkill{
	
	public static List<SkillConfig> skills_;

    private static CSVLoader skillTable;
	
	public static bool loadSkill ()
	{
        skillTable = new CSVLoader("skill", ',', true);
		if (!skillTable.isValid_)
			return false;
		
		skills_ = new List<SkillConfig> ();
		for (int row = 0; row < skillTable.records_.Count; row++) 
		{
			SkillConfig newSkill = new SkillConfig();
			//new_state.stateId_      = stateTable.get_int(row, "state_id");
			newSkill.Id = skillTable.get_int(row,"SkillId");
			newSkill.name = skillTable.get_string(row,"SkillName");
			newSkill.description = skillTable.get_string (row,"Description");
			newSkill.description = newSkill.description.Replace("\\n", "\n");
			newSkill.isDirectional = skillTable.get_bool (row,"DirectionalType");
			string groupids = skillTable.get_string(row,"Rand_Group");
			if( groupids.Length > 0 )
			{
				string[] idStrs = groupids.Split(';');
				foreach(string idStr in idStrs )
					newSkill.groupSkills.Add(Convert.ToInt32(idStr));
			}

			newSkill.cdSubType = skillTable.get_int(row,"CDSubType");
			newSkill.initCD = skillTable.get_int(row,"InitCD");
			newSkill.CD = skillTable.get_int(row,"CD");
			newSkill.action = skillTable.get_int(row,"ActionID");

			string effects = skillTable.get_string(row,"Effect");
			if(effects != null && effects.Trim() != ""){
				string[] effectStrs = effects.Split(';');
				foreach(string idStr in effectStrs )
					newSkill.effects_id.Add(Convert.ToInt32(idStr));
			}

			newSkill.hurt_effect_id = skillTable.get_int(row,"HurtEffect");
			newSkill.heal_effect_id = skillTable.get_int(row,"HealEffect");

			newSkill.bulletName = skillTable.get_string(row, "Bullet");

			newSkill.skillClass = (SkillClass)Enum.Parse(typeof(SkillClass), skillTable.get_string(row,"SkillClass"));

			newSkill.skillType = (SkillType)Enum.Parse(typeof(SkillType), skillTable.get_string(row,"SkillType"));
			newSkill.regEvent_ = skillTable.get_string(row,"Event");
			newSkill.useTarget_ = skillTable.get_bool(row, "UseTarget");
			newSkill.canBroken = skillTable.get_bool(row,"CanBroken");
			newSkill.breakTarget = skillTable.get_bool(row,"BreakTarget");
			newSkill.woundID = skillTable.get_int(row,"WoundID");
			newSkill.targetFilter_ = skillTable.get_string(row,"TargetFilter");
			newSkill.targetCondition_ = skillTable.get_string(row,"TargetCondition");
			newSkill.casterCondition_ = skillTable.get_string(row,"CasterCondition");
			newSkill.targetAction_ = skillTable.get_string(row,"TargetAction");
			newSkill.casterAction_ = skillTable.get_string(row,"CasterAction");
			newSkill.endFilter_ = skillTable.get_string(row,"EndFilter");
			newSkill.endAction_ = skillTable.get_string(row,"EndAction");
			string colorStr = skillTable.get_string(row, "HitHColor");
			string[] colorArr = colorStr.Split(';');
			newSkill.effectDesc = skillTable.get_string(row,"EffectDes");

			if(colorStr != "")
			{
				newSkill.hitHLColor_.r = float.Parse(colorArr[0])/255f;
				newSkill.hitHLColor_.g = float.Parse(colorArr[1])/255f;
				newSkill.hitHLColor_.b = float.Parse(colorArr[2])/255f;
				newSkill.hitHLColor_.a = float.Parse(colorArr[3])/255f;
			}

			newSkill.skillIcon = skillTable.get_string(row, "Icon"); 
			newSkill.activeStep = skillTable.get_int(row, "OpenStep");
            newSkill.fightCapacity = skillTable.get_float(row, "FightCapacity");
			//skillPorps
			string attrProp = skillTable.get_string(row, "SkillAttribute");
			if( attrProp.Length > 0 )
			{
				string[] perPropStrs = attrProp.Split(';');
				newSkill.CPVs_.Clear();
				for(int i =0;i < perPropStrs.Length; i++)
				{
					newSkill.CPVs_.Add(parseCProp(perPropStrs[i]));
				}
			}
			string attrLvProp = skillTable.get_string(row, "SkillAttributeLvup"); 
			if( attrLvProp.Length > 0 )
			{
				string[] perLvPropStrs = attrLvProp.Split(';');
				newSkill.CPVLvs_.Clear();
				for(int i =0;i < perLvPropStrs.Length; i++)
				{
					newSkill.CPVLvs_.Add(parseCProp(perLvPropStrs[i]));
				}
			}

			skills_.Add(newSkill);
			check(newSkill);
		}
		return true;
	}
	
	private static COM_CPropValue parseCProp(string propStr)
	{
		string[] perPar = propStr.Split(' ');
		COM_CPropValue prop = new COM_CPropValue ();	
		prop.propType_ = (CPropType)Enum.Parse(typeof(CPropType), perPar[0]);
		prop.propOpType_ = (int)(PropOPType)Enum.Parse(typeof(PropOPType), perPar[1]);
		prop.value_ = Convert.ToSingle (perPar [2]);
		return prop;
	}
	
	public static SkillConfig findSkill(int skillId)
	{
		for (int i = 0; i < skills_.Count; i++) {
			if( skills_[i].Id == skillId){
				return skills_[i];
			}
		}
		return null;
	}

	public static bool checkAction(string actionStr)
	{
		if (actionStr.Length == 0)
			return true;
		string[] casterActionStrs = actionStr.Split(';');
		foreach (string aStr in casterActionStrs ) {
			string[] actionSplitStrs = aStr.Split(' ');
			string name = actionSplitStrs[0];
			SkillAction action = SkillAction.findAction(name);
			if( action == null )
			{
				Debug.LogWarning("checkAction skill action null: " + aStr);
				return false;
			}
			if (actionSplitStrs.Length > 1) {
				string param = aStr.Substring(aStr.IndexOf(' ') + 1, aStr.Length - aStr.IndexOf(' ') - 1);
				if( action.parseParam(param)  == false )
				{
					Debug.LogWarning("checkAction skill action paramError: " + aStr);
					return false;
				}
			}
		}

		return true;
	}
	
	private static bool checkFilter(string filterStr)
	{
		if (filterStr.Length == 0)
			return true;
		string name = filterStr;
		TargetFilter targetFilter = TargetFilter.findFilter(filterStr);
		string param = "";
		if( targetFilter == null )
		{
			name = filterStr.Substring(0, filterStr.IndexOf(' '));
			param = filterStr.Substring(filterStr.IndexOf(' ') + 1, filterStr.Length - filterStr.IndexOf(' ') - 1);
			targetFilter = TargetFilter.findFilter(name);
		}
		if (targetFilter != null) {
			return targetFilter.parseParam (param);
		} else
			return false;
	}

	private static void check(SkillConfig newSkill)
	{
		if( checkAction(newSkill.targetAction_) == false )
			Debug.LogError(newSkill.name +" (" + newSkill.targetAction_ + ") targetAction_ has Error!");
		
		if( checkAction(newSkill.casterAction_) == false )
			Debug.LogError(newSkill.name +" (" + newSkill.casterAction_ + ") casterAction_ has Error!");

		if( checkAction(newSkill.endAction_) == false )
			Debug.LogError(newSkill.name +" (" + newSkill.casterAction_ + ") casterAction_ has Error!");
		
		if( checkFilter(newSkill.targetFilter_) == false )
			Debug.LogError(newSkill.name +" (" + newSkill.targetFilter_ + "targetFilter has Error!");
	}
}