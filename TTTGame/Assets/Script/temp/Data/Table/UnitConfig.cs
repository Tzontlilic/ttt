﻿
using System.Collections.Generic;

public class UnitConfig
{
    public int id;
    public string name;
    public string res;
    public string desc;
    public List<COM_CPropValue> props;
    public string hand;
    public string chest;
    public string head;
    public string feet;
    public int weaponId;
    public List<EWeaponType> weaponTypes;
}
