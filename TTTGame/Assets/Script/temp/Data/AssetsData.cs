﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.IO;
using System.Net;
using System.Text;

using UnityEngine;

public enum AssetType : byte
{
	Unknow = 0,
	Animator = 1,
	Drop = 2,
	Monster = 3,
	Texture = 4,
	UI = 5,
    UIEffect = 6,
	Effect = 7,
	XML = 8,
	Scene = 9,
	BaseObject = 10,
	Necessary = 11,
	Sound = 12,
	Character = 13,
	Material = 14,   
	Map = 15,
	Atlas = 16,
    Font = 17,
	Model = 0x63,
}

public class AssetsData
{
    public string Name;
    public string ResName;
    public int Pos;
    public int Type;
    public int Version;

    public int Size;
    public string VerUrl = "";
    public bool Parse(string data)
    {
        string dataStr = data.Trim();
        string[] t = dataStr.Split('&');
        if(t.Length == 6)
        {
            Name = t[0];
            ResName = t[1];
            Type = Convert.ToInt32(t[2]);
            Pos = Convert.ToInt32(t[3]);
            Version = Convert.ToInt32(t[4]);
            Size = Convert.ToInt32(t[5]);
            VerUrl = Version.ToString() + "/";
            return true;
        }
        return false;
    }

    public override  string ToString()
    {
        StringBuilder s = new StringBuilder(64);
        s.Append(Name + "&");
        s.Append(ResName + "&");
        s.Append(Type.ToString() + "&");
        s.Append(Pos.ToString() + "&");
        s.Append(Version.ToString() + "&");
        s.Append(Size.ToString());
        return s.ToString();
    }
    public void Clone(AssetsData a)
    {
        this.Name = a.Name;
        this.ResName = a.ResName;
        this.Type = a.Type;
        this.Version = a.Version;
        this.Pos = a.Pos;
        this.Size = a.Size;
        this.VerUrl = a.VerUrl;
    }

    public string ResUrl
    {
        get
        {
            return this.VerUrl + this.ResName;
        }
    }

    public static bool operator >(AssetsData opeLeft, AssetsData opeRight)
    {
        return (((opeLeft != null) && (opeRight != null)) && opeLeft.Version > opeRight.Version);
    }

    public static bool operator <(AssetsData opeLeft, AssetsData opeRight)
    {
        return (((opeLeft != null) && (opeRight != null)) && opeLeft.Version < opeRight.Version);
    }
}













