﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.IO;
using System.Net;
using System.Text;

using UnityEngine;
public class AssetManager
{
    
    private static Dictionary<string, AssetsData> AssetsInfo = new Dictionary<string, AssetsData>();
    private static Hashtable Downlist = new Hashtable();
    private static AssetManager _instance;

    public static AssetManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new AssetManager();
                AssetsInfo.Clear();
                Downlist.Clear();
            }
            return _instance;
        }
    }
	public IEnumerator Down(string Name, Action<System.Object> onComplete, System.Object userData = null)
    {
        if(!ResourcesMgr.UseStreaming)
        {
            if(onComplete != null)
                onComplete(userData);
			yield break;
        }
        AssetsData info = AssetManager.Instance.GetAssetInfo(Name);
        if (info == null)
        {
            if (ResourcesMgr.UseStreaming)
                FpDebug.LogError("Not exit bundle - " + Name);
            else
                UnityEngine.Resources.Load(Name);
            if (onComplete != null)
                onComplete(userData);
            yield break;
        }
        if (!info.ResName.Contains("/"))
        {
            if (onComplete != null)
                onComplete(userData);
            yield break;
        }
        bool needLoad = false;
        AssetBundle resBundle = null;
        if (ResourcesMgr.ResList.TryGetValue(info.ResName, out resBundle))
        {
            if(resBundle == null)
            {
                ResourcesMgr.ResList.Remove(info.ResName);
                needLoad = true;
            }
        }
        else
        {
            needLoad = true;
        }
        if (needLoad)
        {
            if (Downlist.ContainsKey(info.ResName))
            {
                int i = 0;
                while (i < 20)
                {
                    if (ResourcesMgr.ResList.ContainsKey(info.ResName) || !Downlist.ContainsKey(info.ResName))
                    {
                        break;
                    }
                    i++;
                    yield return new WaitForSeconds(0.5f);
                }
                if (onComplete != null)
                    onComplete(userData);
                yield break;
			}
			if(Downlist.ContainsKey(info.ResName))
            {
                if (onComplete != null)
                    onComplete(userData);
                yield break;
            }
				
			Downlist.Add(info.ResName, 0);
            string url = GetUrl(info.Name);
            WWW www = new WWW(url);
            yield return www;
            Downlist.Remove(info.ResName);
            if (www.error == null)
            {
                if (!ResourcesMgr.ResList.ContainsKey(info.ResName))
                {
                    ResourcesMgr.ResList[info.ResName] = www.assetBundle;
                }
                //Debug.LogWarning(info.ResName + " - loaded");
                if (onComplete != null)
                    onComplete(userData);

                www = null;
            }
            else
            {
                if (onComplete != null)
                    onComplete(userData);
                FpDebug.LogError("Error: " + www.error + "\nThis url is " + url);
            }
        }
        else
        {
            if (onComplete != null)
                onComplete(userData);
        }
    }
    public bool HasAssetByResNmae(string ResName)
    {
        foreach(AssetsData i in AssetsInfo.Values)
        {
            if (i.ResName == ResName)
                return true;
        }
        return false;
    }
   
    public AssetsData GetAssetInfo(string _name)
    {
        if (AssetsInfo.ContainsKey(_name))
        {
            return AssetsInfo[_name];
        }
        return null;
    }


    private static string GetSDCardPath(string name)
    {
        return ("file://" + Application.streamingAssetsPath + "/" + TargetPath);
    }

   
    public static string GetUrl(string _name)
    {
        return GetUrl(null, _name);
    }

    public static string GetUrl(string root, string _name)
    {
        string sDCardPath = string.Empty;
        _name = _name.Replace(".unity3d", string.Empty);
        AssetsData info = null;
        if (!string.IsNullOrEmpty(root))
        {
            sDCardPath = root + _name;
        }
        else if (AssetsInfo.TryGetValue(_name,out info ))
        {

            switch (info.Pos)
            {
                case 0:
                    sDCardPath = AssetRootPath + info.ResName;
                    break;

                case 1:
                    
                    sDCardPath = PersistentRootPath + info.ResName;
                    break;

                case 2:
                    sDCardPath = GetSDCardPath(info.ResName);
                    break;
                default:
                    sDCardPath = AssetRootPath + info.ResName;
                    FpDebug.LogError(string.Format("Not found this file's [{0}] in pos [{1}]", info.Name, info.Pos));
                    break;
            }
            
        }
        else
        {
            sDCardPath = AssetRootPath + _name;
        }
        return (sDCardPath + ".unity3d");
    }

    public string GetDir(string _name)
    {
        return this.GetDir(null, _name);
    }

    public string GetDir(string root, string _name)
    {
        string sDCardPath = string.Empty;
        _name = _name.Replace(".unity3d", string.Empty);
        AssetsData info = null;
        if (!string.IsNullOrEmpty(root))
        {
            sDCardPath = root + _name;
        }
        else if (AssetsInfo.TryGetValue(_name, out info))
        {
            switch (AssetsInfo[_name].Pos)
            {
                case 0:
                    sDCardPath = Application.streamingAssetsPath + "/" + TargetPath + info.ResUrl;
                    break;

                case 1:
                    sDCardPath = Application.persistentDataPath + "/" + TargetPath + info.ResUrl;
                    break;

                case 2:
                    sDCardPath = Application.streamingAssetsPath + "/" + TargetPath + info.ResUrl;
                    break;
                default:
                    sDCardPath = Application.streamingAssetsPath + "/" + TargetPath + info.ResUrl;
                    FpDebug.LogError(string.Format("Not found this file's [{0}] in pos [{1}]", info.ResUrl, info.Pos));
                    break;
            }

        }
        else
        {
            sDCardPath = Application.streamingAssetsPath + "/" + TargetPath + _name;
        }
        return (sDCardPath + ".unity3d");
    }
    public void SetAssetsInfo(AssetsData data)
    {
        this.SetAssetsInfo(data.Name, data);
    }

    public void SetAssetsInfo(string _name, AssetsData data)
    {
        if (_name.EndsWith(".unity3d"))
        {
            _name = _name.Replace(".unity3d", string.Empty);
        }
        AssetsInfo[_name] = data;
    }

    private void Unload(string asset)
    {
        ResourcesMgr.UnloadAsset(asset);
    }

    public static string AssetRootPath
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android)
                return (Application.streamingAssetsPath + "/" + TargetPath);
            else
                return ("file://" + Application.streamingAssetsPath + "/" + TargetPath);
        }
    }

    public static string PersistentRootPath
    {
        get
        {
            return ("file:///" + Application.persistentDataPath + "/" + TargetPath);
        }
    }

    public static string SaveRootPath
    {
        get
        {
            return (Application.persistentDataPath + "/" + TargetPath);
        }
    }

    public static string TargetPath
    {
        get
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
                return "IOS/";
            else
                return "Android/";
        }
    }
    private class DelAsset
    {
        public string _asset = string.Empty;
        public float _time;

        public DelAsset(string ass, float time)
        {
            this._asset = ass;
            this._time = time;
        }
    }
}











