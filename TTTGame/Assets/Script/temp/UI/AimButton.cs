﻿using System;

using UnityEngine;
using UnityEngine.EventSystems;

public class AimButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    public bool prepareAim = true;

    public bool attack = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        if(attack)
        {
            GameRoom.instance.Player.SendSyncAttack();
            return;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
    }

    void ContinuousShoot()
    {
        GameRoom.instance.Player.PlayAttack();
    }
}