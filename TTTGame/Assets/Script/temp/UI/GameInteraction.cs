﻿
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using VGame;

public class GameInteraction : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IScrollHandler
{
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (ClientGame.isCurState(VGame.EGameState.VGS_Lobby))
            GameRoom.instance.OnBeginDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (ClientGame.isCurState(VGame.EGameState.VGS_Lobby))
            GameRoom.instance.OnPointClick(eventData);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
    }

    public void OnScroll(PointerEventData eventData)
    {
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if( ClientGame.isCurState(VGame.EGameState.VGS_Lobby))
            GameRoom.instance.OnDrag(eventData);
    }
}
