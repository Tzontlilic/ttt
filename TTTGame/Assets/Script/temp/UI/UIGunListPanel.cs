﻿using UnityEngine;

public class UIGunListPanel : MonoBehaviour
{
    public GameObject listRoot = null;
    public GameObject gunItemObj = null;

    void Start()
    {

    }

    public void InitGunList()
    {
        if (listRoot != null)
        {
            for(int i =0; i < listRoot.transform.childCount; i++)
            {
                Destroy(listRoot.transform.GetChild(i).gameObject);
            }
        }

        if (gunItemObj != null)
        {
            int listCount = 0;
            for (int i = 0; i < UnityWeapon.weapons_.Count; i++)
            {
                WeaponConfig weapon = UnityWeapon.weapons_[i];
                if(GameRoom.instance.Player.CanUseWeapon(weapon))
                {
                    GameObject GunListItem = Instantiate(gunItemObj) as GameObject;
                    UIGunListItem UIWeaponItem = GunListItem.GetComponent<UIGunListItem>();
                    if (UIWeaponItem != null)
                    {
                        UIWeaponItem.InitWeapon(weapon);
                    }
                    GunListItem.transform.SetParent(listRoot.transform, false);
                    GunListItem.transform.localPosition = Vector3.zero;
                    GunListItem.transform.localRotation = Quaternion.identity;
                    listCount++;
                }
            }
            RectTransform rt = listRoot.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, listCount * 100);
        }
    }
}