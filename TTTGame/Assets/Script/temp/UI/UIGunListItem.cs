﻿using UnityEngine.UI;
using UnityEngine;

public class UIGunListItem : MonoBehaviour
{
    public Text Desc = null;
    public Transform bindTrans = null;
    WeaponConfig curWeaponInfo = null;
    Animator btnAnim = null;

    public void InitWeapon(WeaponConfig weaponInfo)
    {
        if (weaponInfo.res != string.Empty)
        {
            Object gunRes = ResourcesMgr.Load(GSTR.WeaponResPath + weaponInfo.res);
            if (gunRes != null)
            {
                GameObject weaponObj = Instantiate(gunRes) as GameObject;
                weaponObj.transform.SetParent(bindTrans, false);
                weaponObj.transform.localPosition = new Vector3(0, 0, 0);
                weaponObj.transform.localRotation = Quaternion.Euler(0, 90, 0);
                weaponObj.transform.localScale = new Vector3(200, 200, 200);
                foreach (Transform trans in weaponObj.GetComponentsInChildren<Transform>(includeInactive: true))
                {
                    trans.gameObject.layer = 8;
                }
                Desc.text = weaponInfo.name;
            }
        }
        curWeaponInfo = weaponInfo;
        btnAnim = GetComponent<Animator>();
    }

    public void OnClick()
    {
        if( curWeaponInfo != null )
        {
            if(GameRoom.instance.Player != null)
            {
                GameRoom.instance.Player.SwitchWeapon(curWeaponInfo.id);
                if(btnAnim != null)
                {
                    btnAnim.SetTrigger("Select");
                }
            }
        }
    }
}