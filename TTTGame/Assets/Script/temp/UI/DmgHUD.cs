﻿
using UnityEngine;

public class DmgHUD : MonoBehaviour
{
    UCharacterController charactor = null;
    Transform bindTrans;

    void Start()
    {
        this.gameObject.AddComponent<DestoryThisByTime>().destoryTime = 0.5f;
    }


    public void Bind(UCharacterController cha, string text = "+0")
    {
        charactor = cha;
        bindTrans = charactor.getStateUITrans();
        UnityEngine.UI.Text hudText = GetComponentInChildren<UnityEngine.UI.Text>();
        hudText.text = text;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (charactor != null && bindTrans != null)
        {
            //Vector3 vWorldPos = trans.localToWorldMatrix.MultiplyPoint(Vector3.zero);
            Vector3 screenPos = MapCamera.instance.WorldToScreenPoint(bindTrans.position);
            (transform as RectTransform).anchoredPosition = screenPos;
        }
    }
}