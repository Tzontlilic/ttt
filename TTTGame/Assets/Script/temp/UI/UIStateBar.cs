﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIStateBar : MonoBehaviour {

    UCharacterController charactor = null;
    public Transform aimImg = null;
    public UnityEngine.UI.Text hpText = null;
    public UnityEngine.UI.Text nameText = null;
    Transform trans;
    UnityEngine.UI.Slider hpSlider = null;

    // Use this for initialization
    void Start () {
        hpSlider = GetComponentInChildren<UnityEngine.UI.Slider>();
        hpText.gameObject.SetActive(false);
    }

    public void Bind(UCharacterController cha)
    {
        charactor = cha;
        trans = charactor.getStateUITrans();
        nameText.text = GameRoom.instance.getPlayerName(cha.playerId);
    }
	
	// Update is called once per frame
	void LateUpdate () {

        if (charactor != null && trans != null)
        {
            //Vector3 vWorldPos = trans.localToWorldMatrix.MultiplyPoint(Vector3.zero);
            Vector3 screenPos = MapCamera.instance.WorldToScreenPoint(trans.position);
            Vector2 pos2 = new Vector2(screenPos.x, screenPos.y);
            (transform as RectTransform).anchoredPosition = pos2;
        }

        if (!charactor.m_showHPUI )
        {
            if( hpSlider.gameObject.activeInHierarchy )
            {
                hpSlider.gameObject.SetActive(false);
            }
            return;
        }
        else
        {
            if (!hpSlider.gameObject.activeInHierarchy)
                hpSlider.gameObject.SetActive(true);
            if (hpSlider != null)
                hpSlider.value = charactor.hp / charactor.maxHp;
            //hpText.text = charactor.hp.ToString();
        }
    }
}
