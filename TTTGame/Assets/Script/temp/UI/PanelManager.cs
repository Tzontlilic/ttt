﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;

public class PanelManager : MonoBehaviour {
    
    public Animator[] initiallyOpens;

    private int m_OpenParameterId;
    //current Open Panel
	private Animator m_RightOpen;
    //current Open Panel
    private Animator m_LeftOpen;

    private GameObject m_PreviouslySelected;
    private GameObject m_PreviouslyLSelected;

    const string k_OpenTransitionName = "Open";
	const string k_ClosedStateName = "Closed";

    Action OnPanelClose = null;

	public void Init()
	{
		m_OpenParameterId = Animator.StringToHash (k_OpenTransitionName);

		if (initiallyOpens == null)
			return;

        for( int i =0;i < initiallyOpens.Length; i++)
        {
            OpenPanel(initiallyOpens[i]);
        }
	}

    public void Close( Action OnClose = null )
    {
        CloseCurrent();
        CloseLCurrent();
        OnPanelClose = OnClose;
    }

    public void ClosePanel(Animator anim)
    {
        anim.gameObject.SetActive(true);
        anim.SetBool(m_OpenParameterId, false);
        StartCoroutine(DisablePanelDeleyed(anim));
    }

	public void OpenPanel (Animator anim)
	{
		if (m_RightOpen == anim)
			return;

		anim.gameObject.SetActive(true);
        GameObject newPreviouslySelected = null;
        if(EventSystem.current!= null)
            newPreviouslySelected = EventSystem.current.currentSelectedGameObject;

		anim.transform.SetAsLastSibling();

		CloseCurrent();

		m_PreviouslySelected = newPreviouslySelected;

		m_RightOpen = anim;
		m_RightOpen.SetBool(m_OpenParameterId, true);

		GameObject go = FindFirstEnabledSelectable(anim.gameObject);

		SetSelected(go);
    }

    public void OpenLPanel(Animator anim)
    {
        if (m_LeftOpen == anim)
            return;

        anim.gameObject.SetActive(true);
        var newPreviouslySelected = EventSystem.current.currentSelectedGameObject;

        anim.transform.SetAsLastSibling();

        CloseLCurrent();

        m_PreviouslyLSelected = newPreviouslySelected;

        m_LeftOpen = anim;
        m_LeftOpen.SetBool(m_OpenParameterId, true);

        GameObject go = FindFirstEnabledSelectable(anim.gameObject);

        SetSelected(go);
    }

    static GameObject FindFirstEnabledSelectable (GameObject gameObject)
	{
		GameObject go = null;
		var selectables = gameObject.GetComponentsInChildren<Selectable> (true);
		foreach (var selectable in selectables) {
			if (selectable.IsActive () && selectable.IsInteractable ()) {
				go = selectable.gameObject;
				break;
			}
		}
		return go;
	}

	public void CloseCurrent()
	{
		if (m_RightOpen == null)
			return;

		m_RightOpen.SetBool(m_OpenParameterId, false);
		SetSelected(m_PreviouslySelected);
		StartCoroutine(DisablePanelDeleyed(m_RightOpen));
		m_RightOpen = null;
    }

    public void CloseLCurrent()
    {
        if (m_LeftOpen == null)
            return;

        m_LeftOpen.SetBool(m_OpenParameterId, false);
        SetSelected(m_PreviouslyLSelected);
        StartCoroutine(DisablePanelDeleyed(m_LeftOpen));
        m_LeftOpen = null;
    }

    IEnumerator DisablePanelDeleyed(Animator anim)
	{
		bool closedStateReached = false;
		bool wantToClose = true;
		while (!closedStateReached && wantToClose)
		{
			if (!anim.IsInTransition(0))
				closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(k_ClosedStateName);

			wantToClose = !anim.GetBool(m_OpenParameterId);

			yield return new WaitForEndOfFrame();
		}

		if (wantToClose)
			anim.gameObject.SetActive(false);

        if( m_LeftOpen == null || m_RightOpen == null )
        {
            if(OnPanelClose != null)
            {
                OnPanelClose();
                OnPanelClose = null;
            }
        }
	}

	private void SetSelected(GameObject go)
	{
		//EventSystem.current.SetSelectedGameObject(go);
	}
}
