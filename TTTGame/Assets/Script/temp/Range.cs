using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class Range 
{
	public Range(float min, float max) {
		this.min = min;
		this.max = max;
		this.range = max - min;
	}

	public float min, max;
	public float range;

	public float Evaluate(float val) {
		return range * val + min;
	}

    public float Lerp(float val)
    {
        return range* val;
    }

    public bool checkInclusive( float val )
    {
        return val <= max && val >= min;
    }
}
