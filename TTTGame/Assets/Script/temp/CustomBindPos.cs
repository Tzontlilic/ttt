﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum BodyPos
{
    Head,
    Spine,
    Chest,
    Body,
    LeftHand,
    RightHand,
    Foot,
    Weapon,
    FirePos,
    Pivot,
    Custom01,
    Custom02,
    Custom03,
    Custom04,
    Custom05,
    Custom06,
    Custom07,
    Custom08,
    Custom09,
    Custom10,
    None
}

public class CustomBindPos : MonoBehaviour {

	public Transform head;
	public Transform spine;
	public Transform chest;
	public Transform body;
	public Transform leftHand;
	public Transform rightHand;
	public Transform foot;
	public Transform weapon;
	public Transform custom01;
	public Transform custom02;
	public Transform custom03;
	public Transform custom04;
	public Transform custom05;
	public Transform custom06;
	public Transform custom07;
	public Transform custom08;
	public Transform custom09;
	public Transform custom10;
	public Transform firePos;
	private Transform pivot;

    public Action onAnimShoot;
    public Action onAnimReload;

    public Transform calc_pos (BodyPos bp){
		Transform tempPard;
		switch(bp) {
		case BodyPos.Head :
			tempPard = body;
			break;
		case BodyPos.Spine:
			tempPard = spine;
			break;
		case BodyPos.Chest:
			tempPard = chest;
			break;
		case BodyPos.Body:
			tempPard = body;
			break;
		case BodyPos.LeftHand:
			tempPard = leftHand;
			break;
		case BodyPos.RightHand:
			tempPard = rightHand;
			break;
		case BodyPos.Foot:
			tempPard = foot;
			break;
		case BodyPos.Weapon:
			tempPard = weapon;
			break;
		case BodyPos.FirePos:
			tempPard = firePos;
			break;
		case BodyPos.None:
			tempPard = transform;
			break;
		case BodyPos.Pivot:
			tempPard = transform.Find("Pivot");
			break;
		case BodyPos.Custom01:
			tempPard = custom01;
			break;
		case BodyPos.Custom02:
			tempPard = custom02;
			break;
		case BodyPos.Custom03:
			tempPard = custom03;
			break;
		case BodyPos.Custom04:
			tempPard = custom04;
			break;
		case BodyPos.Custom05:
			tempPard = custom05;
			break;
		case BodyPos.Custom06:
			tempPard = custom06;
			break;
		case BodyPos.Custom07:
			tempPard = custom07;
			break;
		case BodyPos.Custom08:
			tempPard = custom08;
			break;
		case BodyPos.Custom09:
			tempPard = custom09;
			break;
		case BodyPos.Custom10:
			tempPard = custom10;
			break;
		default:
			tempPard = transform;
			break;
		}
		if(tempPard == null){
			tempPard = transform;
		}
		return tempPard;
	}

	void Reset()
	{
		Transform[] trans = transform.GetComponentsInChildren<Transform> ();
		List<Transform> tranList = new List<Transform> ();
		tranList.InsertRange (0,trans);
		//		tranList.ForEach (r=>Debug.Log(r.name));
		head = tranList.Find (r=>r.name.Contains("Head"));
		spine = tranList.Find (r=>r.name.Contains("Spine"));
		chest = tranList.Find (r=>r.name.Contains("Spine1"));
		body = tranList.Find (r=>r.name.Contains("Spine1"));
		leftHand = tranList.Find (r=>r.name.Contains("L Hand"));
		rightHand = tranList.Find (r=>r.name.Contains("R Hand"));
		foot = tranList.Find (r=>r.name.Contains("L Foot"));
		weapon = tranList.Find (r=>r.name.Contains("Bone"));
    }

    public void AttrShoot()
    {
        if( onAnimShoot != null )
        {
            onAnimShoot();
            onAnimShoot = null;
        }
    }

    public void AttrReload()
    {
        FpDebug.Log("AttrReload");
        if (onAnimReload != null)
        {
            onAnimReload();
        }
    }
    void OnDrawGizmos()
    {
        Vector3 forwardVec = gameObject.transform.forward;
        Vector3 orginPos = firePos.position;
        Gizmos.color = Color.green;
        Ray ray = new Ray(firePos.position, forwardVec);
        Gizmos.DrawLine(orginPos, orginPos + 1000*forwardVec);
        RaycastHit hit;
        if (Physics.Raycast(orginPos, forwardVec, out hit, float.MaxValue, GNS.StaticBlockLayerMask))
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(hit.point, 0.6f);
        }
    }
}
