﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneMgr : FpSingleton<GameSceneMgr>
{
    [SerializeField]
    Transform[] bornPos = null;

    void Start()
    {
        _instance = this;
    }

    public Transform GetBornTrans(int _index)
    {
        if (_index < bornPos.Length)
            return bornPos[_index];
        return null;
    }
}
