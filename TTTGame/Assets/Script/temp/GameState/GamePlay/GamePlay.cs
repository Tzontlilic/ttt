﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VGame
{
    class GamePlay : GameState
    {
        private string[] _panelNames = {};

        public override void BeginState()
        {
            base.BeginState();
            Main.inst.OnEnterBattle();
            MapCamera.instance.SetCamEnable(true);
            MapCamera.instance.ShiftUpAim();
        }

        public override void EndState()
        {
            base.EndState();
            MapCamera.instance.SetCamEnable(false);
        }

        public override string GetCurUIRootName()
        {
            return base.GetCurUIRootName() + "GamePlay";
        }

        void OnUILoaded()
        {
            FpDebug.Log("VGamePlay all other ui loaded.................");
        }

        public override void OnSceneReady()
        {
            //
            //ClientGame.instance.ReadyGame();
        }
    }
}
