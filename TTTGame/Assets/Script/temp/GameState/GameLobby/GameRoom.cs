﻿using System;
using System.Collections.Generic;
using Tnet;
using UnityEngine;
using UnityEngine.EventSystems;
using VGame;

public struct RoomPlayer
{
    public uint playerId;
    public string playerName;    
}

public class GameRoom
{
    private static volatile GameRoom _instance = null;
    public static GameRoom instance
    {
        get
        {
            if (_instance == null)
                _instance = new GameRoom();
            return _instance;
        }
    }


    public PlayerController Player = null;

    Dictionary<int, PlayerController> RoomPlayers = new Dictionary<int, PlayerController>();

    Dictionary<int, PlayerController> challengerPlayers = new Dictionary<int, PlayerController>();


    public GameRoom()
    {
        roomState = RoomState.prepare;
        roomId = 0;
        isGameStart = false;
    }

    public enum RoomState
    {
        prepare,
        running,
        pause,
        stop,
    }
    RoomState roomState = RoomState.prepare;

    public int roomId;
    List<G_RoomPlayerOpt> roomInfos = new List<G_RoomPlayerOpt>();

    public void Reset()
    {
        roomInfos.Clear();
        roomId = 0;
        isGameStart = false;
    }

    public void SyncRoomPlayers( CL_NotifySyncRoomInfoReq _roomInfo )
    {
        roomInfos.Clear();
        foreach (G_RoomPlayerOpt x in _roomInfo.players)
        {
            Debug.Log(string.Format("Room player id ={0}, name={1}", x.id, x.name));
        }
        roomInfos.AddRange(_roomInfo.players);
        if( Player != null)
            UpdateTeamPlayer();
    }

    public void InitRoom(int _roomId)
    {
        isGameStart = false;
        int stageCnt = 1;
        roomId = _roomId;
        for( int i =0; i < roomInfos.Count; i++)
        {
            if(roomInfos[i].id == ClientGame.instance.playerId)
                InitPlayer(roomInfos[i].id, roomInfos[i].name, 0);
            else
            {
                InitPlayer(roomInfos[i].id, roomInfos[i].name, stageCnt);
                stageCnt++;
            }
        }
        RoomUIManager.instance.SetStartBtn(Player.playerId == roomId);
    }

    public void InitPlayer(int _playerId, string _name, int _stageIndex)
    {
        if( _playerId == ClientGame.instance.playerId)
        {
            if( Player == null)
            {
                Player = UCharacterMgr.instance.GeneratePlayer(_playerId, _name);
                Player.Instance.transform.position = new Vector3(0, 0, 0);
                Player.Instance.transform.eulerAngles = new Vector3(0, 0, 0);
                LockStepMgr.AddStepUnit(Player);
            }
            Player.EnterEditPlayer(_stageIndex);
        }
        else
        {
            if(!RoomPlayers.ContainsKey(_playerId))
            {
                PlayerController teamPlayer = UCharacterMgr.instance.GeneratePlayer(_playerId, _name);
                teamPlayer.Instance.transform.position = new Vector3(0, 0, 0);
                teamPlayer.Instance.transform.eulerAngles = new Vector3(0, 0, 0);
                RoomPlayers[_playerId] = teamPlayer;
                teamPlayer.EnterEditPlayer(_stageIndex);
                LockStepMgr.AddStepUnit(teamPlayer);
            }
        }
    }

    void UpdateTeamPlayer()
    {
        if( RoomPlayers.Count > 0 )
        {
            foreach (KeyValuePair<int, PlayerController> item in RoomPlayers)
            {
                item.Value.LeaveRoom();
                LockStepMgr.RemoveStepUnit(item.Value);
                UCharacterMgr.instance.RemovePlayer(item.Key);
            }
            RoomPlayers.Clear();
        }
        int stageCnt = 1;
        for (int i = 0; i < roomInfos.Count; i++)
        {
            if (roomInfos[i].id != ClientGame.instance.playerId)
            {
                InitPlayer(roomInfos[i].id, roomInfos[i].name, stageCnt);
                stageCnt++;
            }
        }
    }

    public string getPlayerName(int _playerId)
    {
        for (int i = 0; i < roomInfos.Count; i++)
        {
            if (roomInfos[i].id == _playerId)
            {
                return roomInfos[i].name;
            }
        }
        return "[playerName]";
    }

    public int getRoomIndex(int _playerId)
    {
        for (int i = 0; i < roomInfos.Count; i++)
        {
            if (roomInfos[i].id == _playerId)
            {
                return i;
            }
        }
        return -1;
    }

    public PlayerController getPlayer(int _playerId)
    {
        if( _playerId == Player.playerId)
        {
            return Player;
        }
        if(RoomPlayers.ContainsKey(_playerId))
        {
            return RoomPlayers[_playerId];
        }
        return null;
    }

    #region Player Mouse Operation in LobbyRoom
    bool dragPlayer = false;
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Player == null)
            return;
        GameObject goHit = ScreenTraceObj(eventData.position, Main.inst.DUILayer);
        if (goHit != null)
        {
            if (goHit == Player.Instance)
            {
                dragPlayer = true;
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (dragPlayer)
        {
            Player.Instance.transform.Rotate(0, -eventData.delta.x, 0);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (dragPlayer)
        {
            Player.Instance.transform.Rotate(0, -eventData.delta.x, 0);
            dragPlayer = false;
        }
    }

    public void OnPointClick(PointerEventData eventData)
    {
        if (Player == null)
            return;
        GameObject goHit = ScreenTraceObj(eventData.position, Main.inst.DUILayer);
        if (goHit != null)
        {
            if (goHit == Player.Instance)
            {
                Player.PlayViewAttact();
            }
        }
    }

    public GameObject ScreenTraceObj(Vector2 ScreenPos, LayerMask layer)
    {
        GameObject hitObj = null;
        RaycastHit hit;
        Ray ray = Main.inst.ViewCamera.ScreenPointToRay(ScreenPos);
        if (Physics.Raycast(ray, out hit, float.MaxValue, layer))
        {
            hitObj = hit.collider.gameObject;
        }
        return hitObj;
    }
    #endregion

    #region EnterGame
    public PlayerController[] targetPlayers = null;
    public void EnterGame()
    {
        targetPlayers = new PlayerController[RoomPlayers.Count + 1];
        int n = 0;
        foreach (KeyValuePair<int, PlayerController> item in RoomPlayers)
        {
            int roomIndex = getRoomIndex(item.Value.playerId);
            if( roomIndex != -1)
            {
                item.Value.InitBornPos(roomIndex);
                item.Value.EnterBattle();
            }
            targetPlayers[n]=item.Value;
            n++;
        }
        if( Player != null )
        {
            int roomIndex = getRoomIndex(Player.playerId);
            Player.InitBornPos(roomIndex);
            Player.EnterBattle();
            targetPlayers[n] = Player;
        }
        //begin to enter scene
        ClientGame.ToState(EGameState.VGS_GamePlay);
    }

    public void StartGame()
    {
        LockStepMgr.instance.BeginLockStep();
        isGameStart = true;
    }

    bool isGameStart = false;
    public bool GameStarted
    {
        get
        {
            return isGameStart;
        }
    }

    public bool IsControlPlayer(PlayerController _player)
    {
        return _player == Player;
    }
    #endregion
}
