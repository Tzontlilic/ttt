﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomUIManager : FpSingleton<RoomUIManager>
{
    [SerializeField]
    Text[] UIPlayerTxt = null;

    [SerializeField]
    GameObject StartBtn = null;

    void Start()
    {
        _instance = this;
    }

    public void SetPlayerName(int _index, string _name)
    {
        if(_index < UIPlayerTxt.Length)
            UIPlayerTxt[_index].text = _name;
    }

    public void SetStartBtn(bool _show)
    {
        if (StartBtn != null)
            StartBtn.SetActive(_show);
    }
}
