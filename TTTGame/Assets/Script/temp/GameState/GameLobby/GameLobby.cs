﻿using UnityEngine;

namespace VGame
{
    class GameLobby : GameState
    {
        public GameObject LobbyUI = null;

        public override void BeginState()
        {
            base.BeginState();
            GameRoom.instance.InitRoom(ClientGame.instance.roomId);
        }

        public override void EndState()
        {
            base.EndState();
        }

        public override string GetCurUIRootName()
        {
            return base.GetCurUIRootName() + "GameLobby";
        }
    }
}
