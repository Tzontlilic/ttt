﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Tnet;

namespace VGame
{
    public class ClientGame : MonoBehaviour
    {
        [Header("GameStatemgr")]
        public static GameState[] _allStates;
        public static GameState _curState = new GameState();

        EventLoop loop = new EventLoop(false);
        TConn conn;

        public string account = "client";
        public string serverIP = "10.0.16.211";
        public int serverPort = 6644;        

        public static ClientGame instance = null;

        [SerializeField]
        bool isLogin = false;

        public int playerId = 0;

        public int roomId = 0;

        void Start()
        {
            isLogin = false;
            instance = this;
            DontDestroyOnLoad(this);

            LoadTable();

            InitState();

            NetApp.Init(Message.SERVLET_CLIENT);
            Log.Init("netlog.txt");

            ConnectToServer();

            Application.targetFrameRate = 60;
        }

        void Update()
        {
            loop.Update();
        }

        public void LoadTable()
        {
            UnitProfession.loadUnit();
            UnityWeapon.loadWeapon();
            WeaponBullet.loadBullet();
            //UnitSkill.loadSkill();
            TargetFilter.loadFilter();
            SkillAction.loadActions();
            StepAction.LoadActions();
        }

        public static void InitState()
        {
            int stateCount = (int)EGameState.VGS_Max;
            _allStates = new GameState[stateCount];
            _allStates[(int)EGameState.VGS_Login] = new GameLogin();
            _allStates[(int)EGameState.VGS_Lobby] = new GameLobby();
            _allStates[(int)EGameState.VGS_GamePlay] = new GamePlay();
            _allStates[(int)EGameState.VGS_Loading] = new GameLoading();
            for (int i = 0; i < _allStates.Length; i++)
            {
                _allStates[i].UIOBJ = GameObject.Find(_allStates[i].GetCurUIRootName());
                if (_allStates[i].UIOBJ != null)
                    _allStates[i].UIOBJ.SetActive(false);
                else
                    Debug.Log(((EGameState)i).ToString() + " UIOBJ is null!");
            }
            //Init to Login
            ToState(EGameState.VGS_Login);
        }

        public static void LoadingToState(EGameState EState)
        {
            GameLoading vgl = getState(EGameState.VGS_Loading) as GameLoading;
            vgl._nextState = EState;
            ToState(EGameState.VGS_Loading);
        }

        public static void ToState(EGameState EState)
        {
            if (isCurState(EState))
                return;
            GameState newState = _allStates[(int)EState];
            curState = newState;
        }

        public static bool isCurState(EGameState EState)
        {
            if (_allStates != null && _allStates.Length > 0)
                return _allStates[(int)EState] == curState;
            return false;
        }

        public static GameState getState(EGameState EState)
        {
            if (_allStates != null && _allStates.Length > 0 && (int)EState < _allStates.Length)
                return _allStates[(int)EState];
            return null;
        }

        public static GameState curState
        {
            set
            {
                if (_curState != value)
                {
                    _curState.EndState();
                    _curState = value;
                    _curState.BeginState();
                }
            }
            get { return _curState; }
        }

        void RegisterServlet()
        {
            Servlet.Add(new CL_NotifyItemsServlet());
            Servlet.Add(new CL_NotifyValuesServlet());
            Servlet.Add(new CL_NotifySyncRoomInfoServlet());
            Servlet.Add(new CL_NotifyEnterSceneServlet());
            Servlet.Add(new CL_NotifyStartGameServlet());
            Servlet.Add(new CL_NotifySnycStepServlet());
        }

        public void ConnectToServer()
        {
            Debug.Log("Begin ConnectToServer");
            RegisterServlet();

            conn = new TConn(loop);
            conn.Connect(serverIP, serverPort);
            conn.connectOk = () =>
            {
                _AccountInput.text = LocalPrefs.GetLastAccName();
                if (_AccountInput.text == "")
                    _AccountInput.text = account;
            };
        }

        [SerializeField]
        UnityEngine.UI.InputField _AccountInput = null;
        public void LoginGame(Animator _LoginAnim)
        {
            if (_AccountInput.text == "")
            {
                Debug.Log("Please Input A UserName!");
                return;
            }

            MC_LoginServlet mclogin = new MC_LoginServlet();
            mclogin.req.user = _AccountInput.text;
            mclogin.req.deviceId = SystemInfo.deviceUniqueIdentifier;
            mclogin.req.deviceModel = SystemInfo.deviceModel;
            conn.Call(mclogin);
            mclogin.Cb = (self) =>
            {
                Debug.Log("mclogin ok,uid =" + self.rsp.uid);
                playerId = self.rsp.uid;
                isLogin = true;
                LocalPrefs.SetLastAccName(self.req.user);
                _LoginAnim.SetTrigger("OK");
            };
        }

        public void JoinRoom()
        {
            if (!isLogin)
            {
                Debug.Log("Login error!");
                return;
            }
            MC_JoinRoomServlet joinroom = new MC_JoinRoomServlet();
            joinroom.req.uid = playerId;
            conn.Call(joinroom);
            joinroom.Cb = (self) =>
            {
                Debug.Log("join room ok,uid=!" + self.rsp.roomid);
                roomId = self.rsp.roomid;
                ToState(EGameState.VGS_Lobby);
            };
        }

        public void LeaveRoom()
        {
            if( roomId != 0)
            {
                MC_LeaveRoomServlet leaveRoom = new MC_LeaveRoomServlet();
                leaveRoom.req.uid = playerId;
                leaveRoom.req.roomid = roomId;
                conn.Call(leaveRoom);
                leaveRoom.Cb = (self) =>
                {
                    Debug.Log("leave room ok,uid=!" + self.rsp.roomid);
                    roomId = 0;
                    ToState(EGameState.VGS_Login);
                };
            }
        }

        public void BeginEnterScene()
        {
            MC_EnterSceneReq req;
            req.roomid = 0;
            conn.Send(req);
        }
        public void ReadyGame()
        {
            MC_ReadyLockStepReq req;
            req.roomid = 0;
            conn.Send(req);
        }

        public void NetSend(ISerial req)
        {
            if( conn != null )
            {
                conn.Send(req);
            }
        }
    }
}