﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace VGame
{
    public enum EGameState
    {
        VGS_Login,
        VGS_Lobby,
        VGS_Loading,
        VGS_GamePlay,
        VGS_Max
    }

    public class GameState
    {
        public static string curUI = "3DCanvas/";
        public GameObject UIOBJ = null;
        
        public virtual void BeginState()
        {
            if (UIOBJ != null)
                UIOBJ.SetActive(true);
        }

        public virtual void EndState()
        {
            if (UIOBJ != null)
                UIOBJ.SetActive(false);
        }

        public virtual string GetCurUIRootName()
        {
            return "3DCanvas/";
        }

        public virtual void LoadUIResource()
        {

        }

        public virtual void OnSceneReady()
        {

        }

        public virtual void ReloadUI()
        {
        }

        public static void LoadUI(string uiFile)
        {
        }

        public static void LoadCurUI()
        {
        }

        public static void LoadScene(string name)
        {
        }
    }
}
