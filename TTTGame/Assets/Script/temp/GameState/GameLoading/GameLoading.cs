﻿using UnityEngine;

namespace VGame
{
    class GameLoading : GameState
    {
        public EGameState _nextState = EGameState.VGS_Max;
        GameObject LoadingUIPrefab = null;
        public GameObject LoadingUI = null;

        public override void BeginState()
        {
            base.BeginState();
        }

        public override void EndState()
        {
            base.EndState();
        }

        public override string GetCurUIRootName()
        {
            return base.GetCurUIRootName() + "GameLoading";
        }

        public void EndLoading()
        {
        }

        public void ChgToNextState()
        {
            ClientGame.ToState(_nextState);
        }
    }
}
