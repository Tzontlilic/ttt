﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VGame
{
    class GameLogin : GameState
    {
        GameObject LoginUIPrefab = null;
        public GameObject LoginUI = null;

        public override void BeginState()
        {
            base.BeginState();
            Debug.Log("GameLogin UIOBJ = " + UIOBJ.name);
        }

        public override void EndState()
        {
            base.EndState();
        }

        public override string GetCurUIRootName()
        {
            return base.GetCurUIRootName() + "GameLogin";
        }
    }
}
