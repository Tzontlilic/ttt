﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {
	
	public Transform target;

	public bool _followPosition = false;
	public iTween.EaseType pType;
	public bool _beginMove = false;
	public float _pspeed;

	public bool _followRotation = false;
	bool _needRot = false;
	public float _rspeed;

	// Use this for initialization
	void Start () {
	
	}

	void Update() {
		if( target == null )
			return;
//		Vector3 targetDir = target.position - transform.position;
//		float step = speed * Time.deltaTime;
//		Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 1.0F);
//		Debug.DrawRay(transform.position, newDir, Color.red);
//		transform.rotation = Quaternion.LookRotation(newDir);
		if( !transform.position.Equals(target.position) && _followPosition )
		{
			_beginMove = true;
			iTween.MoveTo(this.gameObject, iTween.Hash(
				"position", target.position, 
				"speed", _pspeed,
				"easeType", pType,
				"oncomplete", "OnComplete",
				"onupdate", "OnUpdate"
				));
		}
		if (Mathf.Abs(transform.eulerAngles.y - target.eulerAngles.y) > 0.0F && _followRotation && _needRot == false ) {
			_needRot = true;
		}
		if (_needRot && _followRotation) {
            if (transform.rotation.Equals(target.rotation))
                _needRot = false;
            else
                transform.rotation = target.rotation;//Quaternion.RotateTowards( transform.rotation, target.rotation, 1.0F);				
		}
		//test
		//transform.RotateAround (transform.position, Vector3.up, RandomManager.Range(-10,10));
	}

	void OnComplete()
	{
		_beginMove = false;
		_followPosition = false;
	}
	
	void OnUpdate()
	{
		
	}
}
