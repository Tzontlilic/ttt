﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaserEffect : MonoBehaviour {
	
	public Transform start_;
	public Transform end_;

	public GameObject caster_;
	public BodyPos caster_bind;
	public GameObject target_;
	public BodyPos target_bind;

	//peformance
	public GameObject startEff_ = null;
	public GameObject endEff_ = null;	
	public float tilingDis = 1.0f;
	public Vector2 uvSpeed;
	public List<LineRenderer> lrs_;
	public bool isInit = false;
	public bool needReturn = false;

	//TEST
	private Transform returnTest;
	private float returnDis = 0.1f;

	// Use this for initialization
	void Start () {
		
//		if( caster_  != null && target_ != null )
//		{
//			start_ = caster_.calc_pos(caster_bind);
//			end_ = target_.calc_pos(target_bind);
//		}
		calcStartEnd ();

		if( start_ == null || end_ == null )
		{
            FpDebug.LogWarning("LaserEffect init miss transform!");
			Destroy (gameObject);
			return;
		}

		if( startEff_ != null )
		{
			startEff_ = MonoBehaviour.Instantiate (startEff_) as GameObject;
            startEff_.transform.parent = this.transform;
			startEff_.SetActive (true);
			Transform[] allChildren = startEff_.GetComponentsInChildren<Transform> ();
			foreach(Transform child in allChildren){
				child.gameObject.layer = start_.gameObject.layer;
			}
		}
		if( endEff_ != null )
		{
			endEff_ = MonoBehaviour.Instantiate (endEff_) as GameObject;
            endEff_.transform.parent = this.transform;
			endEff_.SetActive (true);
			Transform[] allChildren = endEff_.GetComponentsInChildren<Transform> ();
			foreach(Transform child in allChildren){
				child.gameObject.layer = end_.gameObject.layer;
			}
		}

		if( start_  != null && end_ != null )
		{
			foreach (LineRenderer lr in lrs_) {	
				if( needReturn )
					lr.positionCount = 4;
				else
					lr.positionCount = 2;
				lr.SetPosition (0, start_.position);
				lr.SetPosition (1, end_.position);
				if( needReturn )
				{
					lr.SetPosition (2, end_.position +(end_.position - start_.position).normalized*returnDis);
					lr.SetPosition (3, start_.position);
				}
			}
		}
	}
    void OnEnable()
    {
        calcStartEnd();
    }

    public void setFightUnit(GameObject caster, GameObject target)
	{
		if( caster == null )
			caster_ = null;
		else
			caster_ = caster.gameObject;

		if( target == null )
			target_ = null;
		else
			target_ = target.gameObject;
	}
	
	public void setLaserObject(GameObject caster, GameObject target)
	{
		caster_ = caster;
		target_ = target;
	}

	public void setLaserObj(Transform s, Transform e)
	{
		start_ = s;
		end_ = e;
	}

	void calcStartEnd()
	{
		if( caster_  != null && target_ != null )
		{
			//战阵
			CustomBindPos cbp = caster_.GetComponent<CustomBindPos>();
            if (cbp != null)
                start_ = cbp.calc_pos(caster_bind);
            else
                start_ = caster_.transform;

			cbp = target_.GetComponent<CustomBindPos>();
            if (cbp != null)
                end_ = cbp.calc_pos(target_bind);
            else
                end_ = target_.transform;

        }
	}
	void updateEffecPos()
	{
//		if( caster_  != null && target_ != null )
//		{
//			start_ = caster_.calc_pos(caster_bind);
//			end_ = target_.calc_pos(target_bind);
//		}
		calcStartEnd ();
		if( endEff_ != null )
		{
            endEff_.transform.position = end_.position;
		}
		if( startEff_ != null )
		{
			startEff_.transform.position = start_.position;
		}
		if( endEff_ != null && startEff_ != null)
			startEff_.transform.LookAt (end_.position);
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if ( caster_ == null || target_ == null ) {
			MonoBehaviour.Destroy (startEff_);
			MonoBehaviour.Destroy (endEff_);
			Destroy (gameObject);
		} 
		else 
		{
			float dis = Vector3.Distance(start_.position, end_.position);
			float curTiling = dis/tilingDis;
			foreach (LineRenderer lr in lrs_) {	
				if( needReturn )
					lr.positionCount = 4;
				else
					lr.positionCount = 2;
				lr.SetPosition (0, start_.position);
				lr.SetPosition (1, end_.position);
				if( needReturn )
				{
					lr.SetPosition (2, end_.position +(end_.position - start_.position).normalized*returnDis);
					lr.SetPosition (3, start_.position);
				}
				//lr.material.SetTextureScale("particleTexture", new Vector2(curTiling, 1));
				foreach( Material mat in lr.materials )
				{
					mat.mainTextureScale = new Vector2(curTiling, 1);
					mat.mainTextureOffset = mat.mainTextureOffset + uvSpeed*Time.deltaTime;
					float x = Mathf.Clamp(mat.mainTextureOffset.x, -1, 1 );
					x = (Mathf.Abs(x)==1)?0:x;
					float y = Mathf.Clamp(mat.mainTextureOffset.y, -1, 1 );
					y = (Mathf.Abs(y)==1)?0:y;
				}
			}			
			updateEffecPos ();
		}
	}
}
