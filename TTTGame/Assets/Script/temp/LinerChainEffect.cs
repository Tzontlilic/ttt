using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LinerChainEffect : MonoBehaviour {
	
	private Transform start_;
	private Transform end_;
	public GameObject caster_;
    public List<GameObject> targets_;
    public BodyPos target_bind;
    public GameObject chainEff_ = null;
	List<GameObject> chainEffInst_ = new List<GameObject> ();
	public float tilingDis = 1.0f;
	public Vector2 uvSpeed;
	GameObject startBind;
	public LineRenderer lr_;
	public float returnDis = 0.1f;

	public bool needCheck = false;
	public int effectId_;
	bool isInit = false;
	
	// Use this for initialization
	void Start () {	
		spawnChainEffects ();
		updateChain ();
		isInit = true;
	}

	void spawnChainEffects()
	{
		spwanEffect(caster_);
		for (int i = 0; i < targets_.Count; i++) {
			spwanEffect(targets_[i]);
		}
	}

	void spwanEffect(GameObject fu)
	{
		GameObject chainEffobj = MonoBehaviour.Instantiate (chainEff_) as GameObject;
		chainEffobj.SetActive (true);
        chainEffobj.transform.parent = calc_pos(fu, target_bind);
		chainEffobj.transform.localPosition = Vector3.zero;
		chainEffobj.transform.localRotation = Quaternion.Euler(Vector3.zero);
		Transform[] allChildren = chainEffobj.GetComponentsInChildren<Transform> ();
		foreach(Transform child in allChildren){
			child.gameObject.layer = caster_.gameObject.layer;
		}
		chainEffInst_.Add (chainEffobj);
	}

	void updateChain()
	{		
		if( caster_  != null && targets_.Count != 0 )
		{
			/// 第一个人是一个点,但是需要作用2次
			/// 其他每个角色需要2个点，一个是终点一个是起点
			/// 往返两次,除去最后一个target只需要往返一次
			/// (targets_.Count-1)*4 + 2 + 2
			lr_.SetVertexCount (targets_.Count*4);

            GameObject curTar = caster_;
            GameObject nextTar = null;
			float curDis = 0;
			
			//正向chain
			addChainStart();
			int lrCount = 1;
			for(int i = 0; i < targets_.Count; i++)
			{	
				curDis += Vector3.Distance(curTar.transform.position, targets_[i].transform.position);
				addLinerChain(lrCount, curTar, targets_[i]);
				lrCount+=2;
				curTar = targets_[i];
			}
			//反向chain
			for(int i = targets_.Count - 2; i >= 0; i--)
			{
				nextTar = targets_[i];
				addLinerChain(lrCount, curTar, nextTar);
				lrCount+=2;
				curTar = targets_[i];
			}
			addChainEnd(lrCount);

			float curTiling = curDis*2/tilingDis;
			foreach( Material mat in lr_.materials )
			{
				mat.mainTextureScale = new Vector2(curTiling, 1);
				mat.mainTextureOffset = mat.mainTextureOffset + uvSpeed*Time.deltaTime;
				if( mat.mainTextureOffset.x > 1 || mat.mainTextureOffset.x < -1 )
					mat.mainTextureOffset = new Vector2(0, mat.mainTextureOffset.y);
				if( mat.mainTextureOffset.y > 1  || mat.mainTextureOffset.y < -1 )
					mat.mainTextureOffset = new Vector2(mat.mainTextureOffset.x, 0);
			}
		}
	}

	void addChainStart()
	{
        Vector3 curPos = calc_pos(caster_, target_bind).position;
		lr_.SetPosition (0, curPos);
	}

    void addLinerChain(int count, GameObject cur, GameObject next, bool isEnd = false)
	{
        Vector3 curPos = calc_pos(cur, target_bind).position;
        Vector3 nextPos = calc_pos(next, target_bind).position;
		lr_.SetPosition (count, nextPos);
		lr_.SetPosition (count + 1, nextPos + (nextPos - curPos).normalized * returnDis);
	}
	
	void addChainEnd(int count)
	{
        Vector3 curPos = calc_pos(caster_, target_bind).position;
		lr_.SetPosition (count, curPos);
	}

    public void addCaster(GameObject caster)
	{
		caster_ = caster;
		targets_.Clear ();
		needCheck = false;
	}

    public void addTarget(GameObject target)
	{
		targets_.Add (target);
		if( isInit )
			spwanEffect(target);
		needCheck = true;
    }

    public void addTargets(GameObject caster, GameObject[] tars)
    {
        caster_ = caster;
        targets_.Clear();
        targets_.AddRange(tars);
        needCheck = true;
    }

    public void leave()
	{
		for(int i = 0; i < chainEffInst_.Count; i++)
		{
			Destroy (chainEffInst_[i]);
		}
		Destroy (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		if (caster_ == null || targets_.Count == 0 ) {
			if (needCheck)
			{
				for(int i = 0; i < chainEffInst_.Count; i++)
				{
					Destroy (chainEffInst_[i]);
				}
				Destroy (gameObject);
			}
		} 
		else 
		{
			updateChain();		
		}
	}

    Transform calc_pos(GameObject obj, BodyPos bind_pos)
    {
        CustomBindPos cbp = obj.GetComponent<CustomBindPos>();
        if (cbp != null)
            return cbp.calc_pos(bind_pos);
        else
            return obj.transform;
    }
}
