﻿using UnityEngine;
using System.Collections;
using System;

public class TableState
{
	public int level;
	public int stackMax;
	public string classType;
	public string endSkill;
	public string effect;
	public string pState;
	public string effectID;    
}