﻿using UnityEngine;
using System;
using System.Collections.Generic;

public enum IPropType
{
    IPT_None,
    IPT_Display,
    IPT_HP,
    IPT_CURXP,
    IPT_ProfessionType,
    IPT_AttributeType,
    IPT_Sex,
    IPT_Level,
    IPT_Exp,
    IPT_Star,
    IPT_Quality,
    IPT_Speed,
    IPT_AttackDistence,
    IPT_STR_ADD,
    IPT_DEX_ADD,
    IPT_INT_ADD,
    IPT_PSHEILD,
    IPT_MSHEILD,
    IPT_SHEILD,
    IPT_MAX,
}

public enum PassiveStateFlag
{
    PSF_None,

    //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓增益性状态Start↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    PSF_GainStart,
    PSF_DOT,//持续伤害
    PSF_HOT,//持续回血
    PSF_SuBlood,//吸血
    PSF_Absorb,//吸收能量
    PSF_AddStr,//增加力量
    PSF_AddInt,//增加智力
    PSF_AddDex,//增加敏捷
    PSF_AddPatk,//增加物理攻击
    PSF_AddMatk,//增加魔法攻击
    PSF_AddAtk,//增加攻击力
    PSF_AddDef,//增加防御力
    PSF_AddPdef,//增加物理防御
    PSF_AddRPdef,//增加物理防御穿透
    PSF_AddMdef,//增加法术防御
    PSF_AddIdr,
    PSF_AddCrr,//增加暴击率
    PSF_AddAvdr,//增加闪避率
    PSF_AddHitr,//增加命中率
    PSF_AddBlkr,//增加格挡率
    PSF_AddFAR,//增加攻击速度（普攻）
    PSF_AddFCR,//增加施法速度
    PSF_AddFHR,//快速打击恢复（被击动作）
    PSF_ReduceSPD,//减少攻击间隔
    PSF_ImmuneState,//免疫任何状态
    PSF_ImmunePhy,//免疫物理伤害
    PSF_ImmuneMag,//免疫魔法伤害
    PSF_ImmuneFatal,//免疫致命伤害
    PSF_ReflectPatk,//反弹物理伤害
    PSF_ReflectMatk,//反弹魔法伤害
    PSF_AtkAddM,//攻击增加怒气
    PSF_AddHP,//加血
    PSF_Sheild,//加盾
    PSF_PSheild,//加物理盾
    PSF_MSheild,//加魔法盾
    PSF_ImmuneDeath,//无敌
    PSF_ChangeTag,
    PSF_StealAttr,//盗取攻击
    PSF_AccumDam,
    PSF_Violent,
    PSF_BloodThisty,//嗜血
    PSF_Rebirth,//复活
    PSF_EnergyBall, //能量球
    PSF_GainEnd,
    //↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑增益性状态End↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓削弱性状态Start↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    PSF_WeakStart,
    PSF_ReduceStr,//减少力量
    PSF_ReducePatk,//减少物理攻击
    PSF_ReduceMatk,//减少魔法攻击
    PSF_ReduceInt,//减少智力
    PSF_ReduceDex,//减少敏捷
    PSF_ReduceHitr,//减少命中
    PSF_ReduceAvdr,//减少闪避
    PSF_ReduceCrr,//减少暴击
    PSF_ReduceBlkr,//减少格挡
    PSF_CanNotAttack,//不能攻击
    PSF_CanNotSkill,//不能使用技能
    PSF_CanNotMove,
    PSF_ForceMove,
    PSF_MoveAround,
    PSF_Confuse,
    PSF_Stay,
    PSF_Fear,
    PSF_Frozen,//冰冻
    PSF_Bind,//缠绕
    PSF_Silence,//沉默
    PSF_Rock,//石化
    PSF_Pushback,
    PSF_Stun,
    PSF_BlowFly,
    PSF_CanNotBlowFly,
    PSF_Taunt,//嘲讽
    PSF_MoveTo,
    PSF_Sleep,//睡眠
    PSF_Banish,
    PSF_ReduceDef,
    PSF_ReduceAtk,
    PSF_ReducePdef,
    PSF_ReduceMdef,
    PSF_ReduceFAR,
    PSF_ReduceFCR,
    PSF_ReduceFHR,
    PSF_AddSPD,
    PSF_TargetMiss,//不可选中为目标
    PSF_Ghost,//洛璃残影
    PSF_Trap,//洛璃残影
    PSF_CanNotBeHealed,//不能被治疗
    PSF_WeakEnd,
    PSF_DamageLimit1,  //受到伤害上限1
    PSF_Invincible,    //无敌,免疫所有伤害
                       //↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑削弱性状态End↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓其他状态Start↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    PSF_OtherStart,
    PSF_CasterAddStateOnDamage,//伤害时施法方增加State Zhaotan
    PSF_AbilityColor,//角色身上闪烁高光状态
    PSF_AuraMDEF,//魔法防御光环
    PSF_AuraAtk,//双攻加成光环
    PSF_AuraReduceMDEF,//魔法防御降低光环
    PSF_AuraSTR,//力量光环
    PSF_AuraWeakHITL,//降低命中光环
    PSF_AuraStrenghHITL,//升高命中光环
    PSF_AuraRPDEF,//穿透物理护甲光环
    PSF_AuraAddDEF,//提升双防光环
    PSF_AuraAddATK,//鹤妖被动双攻攻击
    PSF_AuraAddMCRL,//雨曦被动魔法暴击
    PSF_AuraAddRMDEF,//太苍被动增加忽视魔法抗性
    PSF_SoulLink,//灵魂连接
    PSF_SlowShield,//鹤妖减速护甲
    PSF_AuraSelfINT,//九幽被动智力加成
    PSF_AuraSelfSTR,//温清璇被动力量加成
    PSF_FightStage1,//战斗阶段1
    PSF_FightStage2,//战斗阶段2
    PSF_FightStage3,//战斗阶段3
    PSF_FightStage4,//战斗阶段4
    PSF_OtherEnd,
    //↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑其他状态End↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    PSF_Max,
}

public enum ActiveStateResult
{
    ASR_NONE,
    ASR_OK,
    ASR_IMMUME,
    ASR_RESIST,
    ASR_EXIST,
    ASR_HIGHEXIST,
    ASR_SoltFull,
    ASR_Max,
}

public enum DamageType
{
    DT_None,
    DT_Miss,
    DT_Critical,
    DT_Crush,
    DT_CriticalCrush,
    DT_Heal,
    DT_Xp,
    DT_DeathXp,
    DT_DOT,
    DT_HOT,
    DT_SOULLINK,
    DT_Normal,
}

public enum SkillRange
{
    TR_None,
    TR_Single,
    TR_Range,
    TR_All,
}

public enum SkillTargetType
{
    STT_None,
    STT_Enemay,
    STT_Self,
    STT_Myself,
    STT_ExceptMe,
}

public enum BattleNotice
{
    BN_Resist,
    BN_Miss,
    BN_Immume,
    BN_,
    BN_Blood,
    BN_Fury,
    BN_Angry,
}

public enum ItemEquipSlot
{
    IES_None,
    IES_ATTACK,
    IES_DEFENCE,
    IES_SPECIAL,
    IES_SKILL,
    IES_MAX,
}

public enum ProfessionType
{
    PT_NONE,
    PT_Attacker,
    PT_Defender,
    PT_Assistant,
    PT_MAX,
}

public enum ChapterType
{
    CHAPTERTP_Campaign,
    CHAPTERTP_MoneyInstance,
    CHAPTERTP_ExpInstance,
}

public enum StateType
{
    EST_CycAct,
    EST_DieAct,
    EST_Buff,
    EST_EventAct,
    EST_EventDieAct,
    EST_MAX,
}

public enum StateEventType
{
    ESET_None,
    ESET_Caster,
    ESET_Target,
    ESET_All
}

public enum TollgateType
{
    GK_Treasure,
    GK_Hunt,
    GK_Duplicate,
    GK_Shop,
    GK_DuelBattle,
    GK_Tower,
    GK_SoulRoad,
    GK_BattleArray,
    GK_DragonHillGardSelfBuilding,
    GK_DragonHillRobBuilding,
    GK_DragonHillChangeGardTroop,
    GK_GuideBattle,
    GK_Guild,
    GK_GuildStudy,
    GK_GuildBattle,
    GK_Rank,
    GK_DragonHillGardGuildMemberBuilding,
    GK_WorldBossChallengeBattle,
    GK_WorldBossExerciseBattle,
    None
}

public enum RankTabType
{
    RTT_None,
    RTT_Duel,
    RTT_Power,
    RTT_CardAll,
    RTT_CardBody,
    RTT_BattleArray,
    RTT_WorldBoss,
    //RTT_SoulYard,
    //RTT_WhiteDragon,
}

public enum RankTabSubType
{
    RT_None,
    RT_DuelLegend,
    RT_DuelStage,
    RT_AllHeroPower,
    RT_Top5Power,
    RT_HeroStar,
    RT_HeroNum,
    RT_ThreeAttribute,
    RT_FightFormationExp,
}

public enum RecordLogType
{
    StartLoad = 1,
    EndLoad,
    StoryLead,
    SkipStoryLead,
    GuideBattle,
    NewCommerGuide
}

public enum UIType
{
    None,
    Fuben,
    SelectPlayerToFight,
    Hero,
    HeroDetail,
    Backpack,
    Hunt,
    Treasure,
    SoulRoad,
    BattleArray,
    WorldBoss,
    DragonHill,
    Guild,
    GuildFuben,
    Task,
    DailyTask,
    Mail,
    DuelBattle,
    CallCircle,
    Shop,
    Ranking,
    ShowHeroInfo
}




