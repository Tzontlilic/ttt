﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum ShieldType
{
    None,
    PhyShield,
    MagShield,
    AllShield
}

public class StateMgr
{
    public static void enterState(TableState state, CPropData cpd, int stackCount = 1)
    {
        if(state.effect != null)
        {
            List<COM_CPropValue> effects_ = parseProp(state.effect);
            for (int i = 0; i < effects_.Count; i++)
                cpd.addCProp(effects_[i], stackCount);
        }
        if (state.pState != null)
        {
            List<PassiveStateFlag> pstates_ = parsePState(state.pState);
            for (int i = 0; i < pstates_.Count; i++)
                cpd.setPState(pstates_[i], stackCount);

        }
        //bind Effect Particle
        if( state.effectID != null && state.effectID.Length > 0)
        {
            string[] perEffID = state.effectID.Split(';');
            for (int i = 0; i < perEffID.Length; i++)
            {
                if (perEffID[i].Length > 0)
                {
                    int id = Convert.ToInt32(perEffID[i]);
                    cpd.addStateEffect(id);
                }
            }
        }
    }

    public static void exitState(TableState state, CPropData cpd, int stackCount = 1)
    {
        if (state.effect != null)
        {
            List<COM_CPropValue> effects_ = parseProp(state.effect);
            for (int i = 0; i < effects_.Count; i++)
                cpd.delCProp(effects_[i], stackCount);
        }
        if (state.pState != null)
        {
            List<PassiveStateFlag> pstates_ = parsePState(state.pState);
            for (int i = 0; i < pstates_.Count; i++)
            {
                cpd.unsetPState(pstates_[i], stackCount);
            }
        }
        if (state.endSkill != null && state.endSkill.Length > 0)
        {
            string[] perSkill = state.endSkill.Split(';');
            for (int i = 0; i < perSkill.Length; i++)
            {
                SkillMgr.performSkill(perSkill[i], cpd);
            }
        }
        //remove Effect Particle
        if (state.effectID != null && state.effectID.Length > 0)
        {
            string[] perEffID = state.effectID.Split(';');
            for (int i = 0; i < perEffID.Length; i++)
            {
                if (perEffID[i].Length > 0)
                {
                    int id = Convert.ToInt32(perEffID[i]);
                    cpd.removeEffect(id);
                }
            }
        }
    }

    public static List<COM_CPropValue> parseProp(string effctStr)
    {
        List<COM_CPropValue> result = new List<COM_CPropValue>();
        string[] perEff = effctStr.Split(';');
        for (int i = 0; i < perEff.Length; i++)
        {
            if (perEff[i].Length == 0)
                break;
            COM_CPropValue temVal = parseSProp(perEff[i]);
            result.Add(temVal);
        }
        return result;
    }

    public static COM_CPropValue parseSProp(string effctStr)
    {
        COM_CPropValue temVal = new COM_CPropValue();
        string[] propStr = effctStr.Split(' ');
        temVal.propType_ = (CPropType)Enum.Parse(typeof(CPropType), propStr[0]);
        temVal.propOpType_ = (int)(PropOPType)Enum.Parse(typeof(PropOPType), propStr[1]);
        temVal.value_ = Convert.ToSingle(propStr[2]);
        return temVal;
    }

    public static List<PassiveStateFlag> parsePState(string unit_states_str)
    {
        List<PassiveStateFlag> result = new List<PassiveStateFlag>();
        string[] perState = unit_states_str.Split(';');
        //parse pstate
        for (int i = 0; i < perState.Length; i++)
        {
            if (perState[i].Length == 0)
                break;
            PassiveStateFlag ps = (PassiveStateFlag)Enum.Parse(typeof(PassiveStateFlag), perState[i]);
            result.Add(ps);
        }
        return result;
    }

    //判断pstate是否在stateId对于的state表里
    public static bool compareStateIdAndPstate(string stateId, PassiveStateFlag pstate)
    {
        TableState state = TableMgr.ins.GetState(stateId);
        List<PassiveStateFlag> result = parsePState(state.pState);
        for (int i = 0; i < result.Count; i++)
        {
            if(result[i]== pstate)
            {
                return true;
            }
        }

        return false;
    }
}