﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum CPropType
{
    CPT_NONE,
    CPT_STR,
    CPT_DEX,
    CPT_INT,
    CPT_MAXHP,
    CPT_MAXXP,
    CPT_GENXP,
    CPT_PATK,
    CPT_MATK,
    CPT_PDEF,
    CPT_MDEF,
    CPT_PCRL,
    CPT_MCRL,
    CPT_HPHF,
    CPT_GENHF,
    CPT_AVDL,
    CPT_HITL,
    CPT_RPDEF,
    CPT_RMDEF,
    CPT_ATK2HPR,
    CPT_AHL,
    CPT_RSIL,
    CPT_RGEN,
    CPT_SPEED,
    CPT_MAXSPEED,
    CPT_RSPEED,
    CPT_FAR,
    CPT_RFAR,
    CPT_FCR,
    CPT_RFCR,
    CPT_FHR,
    CPT_RFHR,
    CPT_DBPR,
    CPT_DAM2HPR,
    CPT_DBMR,
    CPT_MAX,
}

public class PropInfo
{
    public CPropType type_;
    public float baseValue_;
}

public class UnitProp
{
    public List<CProp> m_cprops = new List<CProp>();

    public List<IProp> m_iprops = new List<IProp>();

    public UnitProp()
    {
        reset();
    }

    public void reset()
    {
        m_cprops.Clear();
        for (int i = 0; i < (int)CPropType.CPT_MAX; i++)
        {
            CProp cp = new CProp(i);
            m_cprops.Add(cp);
        }
        m_iprops.Clear();
        for (int i = 0; i < (int)IPropType.IPT_MAX; i++)
        {
            IProp cp = new IProp();
            m_iprops.Add(cp);
        }
    }

    public float getCurCProp(int propId)
    {
        return m_cprops[propId].curValue;
    }

    public string GetPropString(int propId)
    {
        return m_cprops[propId].ToString();
    }

    public float CurCPI(int propId, PropOPType op)
    {
        return m_cprops[propId].getOPValue(op);
    }

    public void setCurCPI(int propId, PropOPType op, float curValue)
    {
        m_cprops[propId].doPropOp(op, curValue);
    }

    public float getBaseCPI(int propId)
    {
        return m_cprops[propId].baseValue;
    }

    public void setBaseCurCPI(int propId, float b, float curValue)
    {
        m_cprops[propId].baseValue = b;
        m_cprops[propId].curValue = curValue;
    }

    public CProp getTypeProp(int propId)
    {
        return m_cprops[propId];
    }

    public float CurIPI(int propId)
    {
        return m_iprops[propId].curValue_;
    }

    public void setCurIPI(int propId, float curValue)
    {
        m_iprops[propId].curValue_ = (int)curValue;
    }
}

public class COM_CPropValue
{
    public CPropType propType_;
    public int propOpType_;
    public float value_;
};



public class IProp
{
    public IProp() { isDirty_ = false; }
    public int getCurValue() { return curValue_; }
    public void setCurValue(int value) { curValue_ = value; }
    public int curValue_;
    public bool isDirty_;	//<是否改变
};
