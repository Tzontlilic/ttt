﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
public class StateInst
{
    // 状态ID
    public string stateId;
    public float fullTime;
    public double lifeTime;
    public int stackCount;
}

public class CPropData
{
    public CPropData()
    {
        init();
    }

    //StateInst
    public List<StateInst> m_States;

    private Dictionary<int, GameObject> _allEffects = new Dictionary<int, GameObject>();

    //CProp
    public UnitProp m_props = new UnitProp();
    public int[] m_pStateRefcnts;       //<PState refcounts

    void init()
    {
        m_States = new List<StateInst>();
        m_pStateRefcnts = new int[(int)PassiveStateFlag.PSF_Max];
        for (int i = 0; i < m_pStateRefcnts.Length; i++)
        {
            m_pStateRefcnts[i] = 0;
        }
    }

    //更新状态
    public void updateState(float delta_sec = 0)
    {
        if (m_States.Count == 0)
        {
            return;
        }
        for (int i = m_States.Count - 1; i >= 0; i--)
        {
            StateInst item = null;
            if (m_States.Count > i)
                item = m_States[i];

            if (item == null)
                continue;

            if (item.lifeTime == -1)
                continue;

            if (item.lifeTime > 0)
            {
                item.lifeTime -= delta_sec;
            }

            if (item.lifeTime < 0.0001f)
            {
                ///LIFE TIME 到了 删除状态
                cancelState(item);
                i++;
            }
        }
    }

    public StateInst findStateInst(string stateID)
    {
        for (int i = 0; i < m_States.Count; i++)
        {
            if (m_States[i].stateId == stateID)
            {
                return m_States[i];
            }
        }
        return null;
    }

    public List<StateInst> findStateInstByPstate(PassiveStateFlag pstateId)
    {
        List<StateInst> result = new List<StateInst>();
        for (int i = 0; i < m_States.Count; i++)
        {
            string stateId = m_States[i].stateId;
            if(StateMgr.compareStateIdAndPstate(stateId, pstateId))
            {
                result.Add(m_States[i]);
            }
        }

        return result;
    }

    public float GetAllEffectValueByPstate(PassiveStateFlag pstateId)
    {
        float effect = 0;
        List<StateInst> result = findStateInstByPstate(pstateId);
        for (int i = 0; i < result.Count; i++)
        {
            TableState state = TableMgr.ins.GetState(result[i].stateId);
            COM_CPropValue cpv = StateMgr.parseSProp(state.effect);
            effect += cpv.value_;
        }

        return effect;
    }

    public double GetMaxLifeTimeByPstate(PassiveStateFlag pstateId)
    {
        double time = 0;
        List<StateInst> result = findStateInstByPstate(pstateId);
        for (int i = 0; i < result.Count; i++)
        {
            if(result[i].lifeTime> time)
            {
                time = result[i].lifeTime;
            }
        }

        return time;
    }

    public float GetMaxfullTimeByPstate(PassiveStateFlag pstateId)
    {
        float time = 0;
        List<StateInst> result = findStateInstByPstate(pstateId);
        for (int i = 0; i < result.Count; i++)
        {
            if (result[i].fullTime > time)
            {
                time = result[i].fullTime;
            }
        }

        return time;
    }

    //设置被动状态
    public void setPState(PassiveStateFlag pstateId, int stackCount = 1)
    {
        if (pstateId < PassiveStateFlag.PSF_Max)
        {
            int oldCnts = m_pStateRefcnts[(int)pstateId];
            m_pStateRefcnts[(int)pstateId]+=stackCount;
            if (oldCnts == 0 && stackCount > 0)
                enterPState(pstateId, true);
            else
                enterPState(pstateId, false);
        }
    }

    //删除被动状态
    public void unsetPState(PassiveStateFlag pstateId, int stackCount = 1)
    {
        if (pstateId < PassiveStateFlag.PSF_Max)
        {
            if (m_pStateRefcnts[(int)pstateId] == 0)
            {
                FpDebug.LogWarning("unsetPState value 0:" + Enum.GetName(typeof(PassiveStateFlag), pstateId));
                return;
            }
            m_pStateRefcnts[(int)pstateId] -= stackCount;
            if (m_pStateRefcnts[(int)pstateId] <= 0)
            {
                m_pStateRefcnts[(int)pstateId] = 0;
                leavePState(pstateId);
            }
        }
    }

    //检查被动状态
    public bool checkPState(PassiveStateFlag pstateId)
    {
        if (pstateId < PassiveStateFlag.PSF_Max)
        {
            return m_pStateRefcnts[(int)pstateId] > 0;
        }
        return false;
    }

    public void enterPState(PassiveStateFlag pstateId, bool isChange)
    {
    }
    public void leavePState(PassiveStateFlag pstateId)
    {
    }

    //属性接口
    public float getCurCProp(CPropType cpt) { return m_props.getCurCProp((int)cpt); }
    public float getCProp(CPropType cpt, PropOPType op) { return m_props.CurCPI((int)cpt, op); }
    public void setCProp(CPropType cpt, PropOPType op, float v) { m_props.setCurCPI((int)cpt, op, v); }

    public float getIProp(IPropType ipt) { return m_props.CurIPI((int)ipt); }
    public void setIProp(IPropType ipt, float v) { m_props.setCurIPI((int)ipt, v); }

    public CProp getTypeProp(CPropType cpt) { return m_props.getTypeProp((int)cpt); }

    public string GetCurPropString(CPropType cpt)
    {
        return m_props.GetPropString((int)cpt);
    }

    //使用在根据技能等级效果时的额外提升效果
    public void addCProp(COM_CPropValue p, int num)
    {
        float f = getCProp(p.propType_, (PropOPType)p.propOpType_);
        setCProp(p.propType_, (PropOPType)p.propOpType_, f + p.value_ * num);
    }
    public void addCProp(COM_CPropValue p)
    {
        float f = getCProp(p.propType_, (PropOPType)p.propOpType_);
        setCProp(p.propType_, (PropOPType)p.propOpType_, f + p.value_);
    }
    public void delCProp(COM_CPropValue p, int count = 1) { float f = getCProp(p.propType_, (PropOPType)p.propOpType_); setCProp(p.propType_, (PropOPType)p.propOpType_, f - p.value_ * count); }
    
    public void CalcProps()
    {
        m_props.reset();
        m_pStateRefcnts = new int[(int)PassiveStateFlag.PSF_Max];
    }

    //增加状态
    public bool addState(StateInst stateInst)
    {
        TableState state = TableMgr.ins.GetState(stateInst.stateId);
        if (state != null)
        {
            if (findAndReplaceInst(stateInst) == ActiveStateResult.ASR_OK)
            {
                return true;
            }
        }
        return false;
    }
    public ActiveStateResult findAndReplaceInst(StateInst stateInst)
    {
        TableState newState = TableMgr.ins.GetState(stateInst.stateId);
        for (int i = 0; i < m_States.Count; i++)
        {
            if (m_States[i].stateId.Length > 0)
            {
                TableState s = TableMgr.ins.GetState(m_States[i].stateId);

                if (m_States[i].stateId == stateInst.stateId)
                {
                    //叠加时间
                    m_States[i].lifeTime = stateInst.lifeTime;

                    //叠加效果
                    if (s.stackMax > 1)
                    {
                        if (m_States[i].stackCount < s.stackMax)
                        {
                            StateMgr.enterState(s, this, stateInst.stackCount);
                            m_States[i].stackCount += stateInst.stackCount;
                            if (m_States[i].stackCount > s.stackMax)
                                m_States[i].stackCount = s.stackMax;
                        }
                        else
                            return ActiveStateResult.ASR_EXIST;
                    }
                    return ActiveStateResult.ASR_OK;
                }
                else if (s.classType == newState.classType)
                {
                    //相同替代关系 判断等级
                    if (s.level <= newState.level)
                    {
                        ///替换状态 时删除旧状态
                        cancelState(m_States[i]);
                        m_States.Add(stateInst);
                        StateMgr.enterState(newState, this);
                        return ActiveStateResult.ASR_OK;
                    }
                    else {
                        return ActiveStateResult.ASR_HIGHEXIST;
                    }
                }
            }
        }
        //没找到,槽加状态
        m_States.Add(stateInst);
        StateMgr.enterState(newState, this, stateInst.stackCount);
        return ActiveStateResult.ASR_OK;
    }

    public void ReplaceState(StateInst stateInst)
    {
        TableState state = TableMgr.ins.GetState(stateInst.stateId);
        if (state != null)
        {
            for (int i = 0; i < m_States.Count; i++)
            {
                if(m_States[i].stateId == stateInst.stateId)
                {
                    cancelState(m_States[i]);
                    break;
                }
            }
            addState(stateInst);
        }
    }

    //减少状态叠加层数
    public void ReduceState(StateInst stateInst, int num)
    {
        TableState state = TableMgr.ins.GetState(stateInst.stateId);
        if (state == null)
            return;
        for (int i = 0; i < m_States.Count; i++)
        {
            if (m_States[i].stateId == stateInst.stateId)
            {
                if (m_States[i].stackCount <= num)
                    cancelState(stateInst);
                else 
                    StateMgr.exitState(state ,this, num);
            }
        }
    }

    //减少状态时间
    public void ReduceStateTime(StateInst stateInst, int time)
    {
        TableState state = TableMgr.ins.GetState(stateInst.stateId);
        if (state == null)
            return;
        for (int i = 0; i < m_States.Count; i++)
        {
            if (m_States[i].stateId == stateInst.stateId)
            {
                if (m_States[i].lifeTime <= time)
                {
                    cancelState(stateInst);
                    return;
                }                    
                else
                    m_States[i].lifeTime -= time;
            }
        }
    }

    //删除状态
    public bool cancelState(StateInst stateInst)
    {
        TableState state = TableMgr.ins.GetState(stateInst.stateId);
        for (int i = 0; i < m_States.Count; i++)
        {
            if (m_States[i].stateId == stateInst.stateId)
            {
                ///dispatch cancelState
                StateMgr.exitState(state, this, m_States[i].stackCount);
                m_States.RemoveAt(i);
                return true;
            }
        }
        return false;
    }
    
    public bool performSkill(TableSkill skill, CPropData target)
    {
        //对目标释放
        string[] targetActionStrs = skill.action.Split(';');
        for (int i = 0; i < targetActionStrs.Length; i++)
        {
            SkillMgr.performActionStr(targetActionStrs[i], this as UCharacterController, target);
        }
        return true;
    }

    public void addStateEffect(int effectID)
    {
        if (_allEffects.ContainsKey(effectID))
            return;        
    }

    public void removeAllEffect()
    {
        foreach (KeyValuePair<int, GameObject> effItem in _allEffects)
        {
            GameObject.Destroy(effItem.Value);
        }
        _allEffects.Clear();
    }

    public void removeEffect(int index)
    {
        //EffectsPool = PoolManager.Pools["Effects"];
        if (_allEffects.ContainsKey(index))
        {
            GameObject.Destroy(_allEffects[index]);
            //_allEffects[index].transform.parent = EffectsPool.transform;
            //EffectsPool.Despawn(_allEffects[index].transform);
            _allEffects.Remove(index);
        }
    }
       
    public void ReducePStateTime(string pstate, float time)
    {
        for (int i = 0; i < m_States.Count; i++)
        {
            StateInst newState = m_States[i];
            if (newState.lifeTime == -1)
                continue;

            TableState s = TableMgr.ins.GetState(newState.stateId);
            if (pstate == s.pState)
            {
                newState.lifeTime -= time;
                if (newState.lifeTime <= 0)
                {
                    cancelState(newState);
                    i--;
                }
            }
        }        
    }

    bool _effShow = true;
    public void ShowStateEffect(bool show)
    {
        _effShow = show;
        foreach (KeyValuePair<int, GameObject> effItem in _allEffects )
        {
            effItem.Value.SetActive(_effShow);
        }
    }

    public float maxHp
    {
        get
        {
            return this.getCurCProp(CPropType.CPT_MAXHP);
        }
    }

    public float hp
    {
        get
        {
            return getIProp(IPropType.IPT_HP);
        }
        set
        {
            setIProp(IPropType.IPT_HP, value);
        }
    }

    public bool IsDead()
    {
        //return isDead_;
        return getIProp(IPropType.IPT_HP) <= 0;
    }

}