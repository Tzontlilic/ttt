﻿using System;
using System.Collections;
using System.Collections.Generic;
using ComRpc;
using UnityEngine;

public class Damage
{
	public Damage(){}
	public Damage(UCharacterController caster)
	{
		caster_ = caster;
		exType_ = DamageType.DT_None;
	}
	public UCharacterController caster_ = null;
	public DamageType type_;
	public DamageType exType_;
	public int sum_;
	public float victimHp_;
	public SkillConfig skill_;
};

public class GameRules
{
	//  生命参数
	public static int hpParameter = 42;
	// 能量参数
	public static int xpParameter = 1000;
	// 物理攻击参数A
	public static float PATKParam_1 = 1.7f;
	// 物理攻击参数B
	public static float PATKParam_2 = 1.0f;
	// 魔法攻击参数
	public static float MATKParameter = 4.5f;
	// 物理护甲参数A
	public static float PDEFParam_1 = 0.8f;
	// 物理护甲参数B
	public static float PDEFParam_2 = 0.4f;
	// 魔法护甲参数
	public static float MDEFParameter = 1.3f;
	// 物理暴击参数
	//public static float PCRLParameter = 0.4f;
	// 攻击获得的固定能量值
	public const int kFixEnergy = 90;
	// 杀死目标的固定能量值
	public const int kFixKilledEnergy = 300;


	public static void calcCharacterProps(UCharacterController ch, int attributeType)
	{
		if( ch == null )
		{
			Debug.Log("GameRules.calcCharacterProps() is null");
			return;
		}

		ch.m_props.reset ();
		
		ch.m_pStateRefcnts = new int[(int)PassiveStateFlag.PSF_Max];
		for( int i = 0; i < ch.m_pStateRefcnts.Length; i++)
		{
			ch.m_pStateRefcnts[i] = 0;
		}
		
		ch.setCProp(CPropType.CPT_MAXXP, PropOPType.PT_B,  xpParameter);    //怒气上限
		ch.setIProp(IPropType.IPT_CURXP, 0);        //怒气初始


        //表基础属性
        for (int i = 0; i < ch.m_profession.props.Count; i++)
        {
            ch.addCProp(ch.m_profession.props[i]);
        }

        //ch.setCProp(CPropType.CPT_STR, PropOPType.PT_B, 10);
        //ch.setCProp(CPropType.CPT_DEX, PropOPType.PT_B, 10);
        //ch.setCProp(CPropType.CPT_INT, PropOPType.PT_B, 10);

        calcBattleProps(ch, attributeType, true);
    }

	public static void calcBattleProps(UCharacterController ch, int attributeType, bool isBattle)
	{
		float mainAttribute = 0;
		float strAttribute = ch.getCurCProp(CPropType.CPT_STR);
		float dexAttribute = ch.getCurCProp(CPropType.CPT_DEX);
		float intAttribute = ch.getCurCProp(CPropType.CPT_INT);
		float mainPATKParameter = 1.0f;		
		
		switch(attributeType){
			// 力量型
		case 1:
			mainAttribute = strAttribute; 
			break;
			// 敏捷型
		case 2:
			mainAttribute = dexAttribute;
			mainPATKParameter = 1.1f;
			break;
			// 智力型
		case 3:
			mainAttribute = intAttribute;
			mainPATKParameter = 0.9f;
			break;
        }
        
		float curMaxHP = ch.getCurCProp (CPropType.CPT_MAXHP);
		ch.setCProp(CPropType.CPT_MAXHP, PropOPType.PT_B,  ch.getCProp(CPropType.CPT_MAXHP, PropOPType.PT_B) + strAttribute * hpParameter);    //血量上限
		ch.setIProp(IPropType.IPT_HP, ch.getCurCProp(CPropType.CPT_MAXHP));	//设置当前血量
		
		ch.setCProp(CPropType.CPT_PATK, PropOPType.PT_B, ch.getCProp(CPropType.CPT_PATK, PropOPType.PT_B) + dexAttribute * PATKParam_1 + mainAttribute * mainPATKParameter); //物理攻击力

        ch.setCProp(CPropType.CPT_MATK, PropOPType.PT_B, ch.getCProp(CPropType.CPT_MATK, PropOPType.PT_B) + intAttribute * MATKParameter); //魔法攻击力

        ch.setCProp(CPropType.CPT_PDEF, PropOPType.PT_B, ch.getCProp(CPropType.CPT_PDEF, PropOPType.PT_B) + strAttribute * PDEFParam_1 + dexAttribute * PDEFParam_2);//物理防御力

        ch.setCProp(CPropType.CPT_MDEF, PropOPType.PT_B, ch.getCProp(CPropType.CPT_MDEF, PropOPType.PT_B) + intAttribute * MDEFParameter);	//魔法防御力

        ch.setCProp(CPropType.CPT_SPEED, PropOPType.PT_B, ch.getCProp(CPropType.CPT_SPEED, PropOPType.PT_B) * (1 - ch.getCurCProp(CPropType.CPT_RSPEED)));		//攻击间隔

        ch.setCProp(CPropType.CPT_FAR, PropOPType.PT_B, ch.getCProp(CPropType.CPT_FAR, PropOPType.PT_B) * (1 - ch.getCurCProp(CPropType.CPT_RFAR)));		//攻击速度

        ch.setCProp(CPropType.CPT_FCR, PropOPType.PT_B, ch.getCProp(CPropType.CPT_FCR, PropOPType.PT_B) * (1 - ch.getCurCProp(CPropType.CPT_RFCR)));		//施法速度

        ch.setCProp(CPropType.CPT_FHR, PropOPType.PT_B, ch.getCProp(CPropType.CPT_FHR, PropOPType.PT_B) * (1 - ch.getCurCProp(CPropType.CPT_RFHR)));		//被击速度
	}
    
	//物理伤害
	public static Damage phyAttack(UCharacterController attacker, UCharacterController victim, float diff = 0.0f, float per = 0.0f)
	{
        Damage dmg = calc_attack_dmg(attacker, victim, diff, per);        
		dmg.victimHp_ = victim.hp;
		dmg.sum_ = (int)deal_damage(attacker, victim, dmg.sum_);
		return dmg;
	}
	
	//魔法伤害
	public static Damage magicAttack(UCharacterController attacker, UCharacterController victim, SkillConfig skill, float diff = 0.0f, float per = 0.0f, float hpAdd = 0.0f, 
        bool needMiss = false, float exDamage = 0.0f, bool needDef = true)
	{
		//命中
		float hit = calc_attack_hit(attacker, victim);
		//暴击
		float critical = calc_attack_critical(attacker, victim, "magic");
		//伤害
        Damage dmg = calc_attack_dmg(attacker, victim, diff, per);//Zhaotan 魔法技能必定命中
		dmg.sum_ += (int)exDamage;
        
		dmg.victimHp_ = victim.hp;
        dmg.sum_ = (int)deal_damage(attacker, victim, dmg.sum_, ShieldType.MagShield);				

		// 普通攻击计算吸血
		if(skill.skillClass == SkillClass.NORMAL){
			Damage heal = new Damage ();
			heal.sum_ = GameRules.calc_normal_pleech(attacker, victim, dmg.sum_);
			heal.type_ = DamageType.DT_Heal;
			heal.skill_ = skill;
			if(heal.sum_ != 0)
			{
				GameRules.deal_heal(attacker, heal.sum_);
				attacker.OnDamage(heal);
			}
		}
		// 处理伤害反弹
		Damage casterdmg = GameRules.deal_reflect(attacker, victim, "magic", dmg.sum_);
		casterdmg.skill_ = skill;
		if(casterdmg.sum_ > 0){
			attacker.OnDamage(casterdmg);
		}
		//魔法攻击伤害事件
		//GloblaValue.trig_FE_Event(GloblaValue.FE_ONMATTACK, attacker, victim, dmg);
		return dmg;
	}

	//	无视防御伤害
	public static Damage RealDamage(UCharacterController attacker, UCharacterController victim, int dmg_sum)
	{
		//命中
		float hit = calc_attack_hit(attacker, victim);

		Damage dmg = new Damage(attacker);
		dmg.type_ = DamageType.DT_Normal;
		dmg.sum_ = 0;

		// MISS
		float rHit = (float)RandomManager.Range(0, 1000) * 0.001f;
		if (rHit > hit )
		{
			dmg.type_	= DamageType.DT_Miss;
			return dmg;
		}
			
		dmg.sum_ = dmg_sum;
		
		dmg.victimHp_ = victim.hp;
		dmg.sum_ = (int)deal_damage(attacker, victim, dmg.sum_, ShieldType.None);        
		
		return dmg;
	}
	
	//伤害计算
	public static Damage calc_attack_dmg(UCharacterController attacker, UCharacterController victim, float diff, float per)
	{

		Damage dmg = new Damage(attacker);
		dmg.type_ = DamageType.DT_Normal;
		dmg.sum_ = 0;		
		
		/// 原始攻击力
		float attack_sum = 0;
        /// 攻击者的物理攻击力
        float pAtk = attacker.getCurCProp(CPropType.CPT_PATK);

        attack_sum += (pAtk * per + diff) * 2;

		// 计算伤害
		dmg.sum_ = (int)attack_sum;

		return dmg;
	}
	
	//命中率
	public static float calc_attack_hit(UCharacterController attacker, UCharacterController victim)
	{
		float A = 1500.0f;
		float B = 1.2f;
		float C = 0.05f;
		float rhit = attacker.getCurCProp (CPropType.CPT_HITL);
		float vicAVDL = victim.getCurCProp (CPropType.CPT_AVDL);
		///取两者相差、和-100的最大值，避免算出来的命中率为负数
		float minus = Math.Max (vicAVDL - rhit, -100);
		float result = 1 - minus / (A + minus * B) - C;
		if( victim.checkPState(PassiveStateFlag.PSF_DamageLimit1) )
		   result = 1;
		return result;
	}
	
	//暴击率
	public static float calc_attack_critical(UCharacterController attacker, UCharacterController victim, string type)
	{
		float A = 1800.0f;
		float B = 1.2f;
		float C = 0.05f;
		//float D = 0.005f;
		///
		float pCrit = 0;
		//float pDef = 0;
		//float rpDef = 0;
		//float minus = 0;
		if(type == "phy"){
			pCrit = attacker.getCurCProp(CPropType.CPT_PCRL);
			//pDef = victim.getCurCProp(CPropType.CPT_PDEF);
			//rpDef = attacker.getCurCProp(CPropType.CPT_RPDEF);
			///取两者相差、和300的最小值，避免算出来的暴击率为负数
			//minus = Math.Min(rpDef - pDef,300);
			return pCrit/(A + pCrit*B) + C;
		}else if(type == "magic"){
			pCrit = attacker.getCurCProp(CPropType.CPT_MCRL);
			//pDef = victim.getCurCProp(CPropType.CPT_MDEF);
			//rpDef = attacker.getCurCProp(CPropType.CPT_RMDEF);
			///取两者相差、和300的最小值，避免算出来的暴击率为负数
			//minus = Math.Min(rpDef - pDef,300);
			return pCrit/(A + pCrit*B);
		}
		return 0;
	}    

	//伤害处理
	public static float deal_damage(UCharacterController attacker, UCharacterController victim, int dmg_sum, ShieldType sType = ShieldType.PhyShield)
	{
		if (victim == null)
			return 0.0f;
		int victim_hp = (int)victim.hp;
		if( victim_hp <= 0 )
			return 0.0f;

		if( victim.checkPState(PassiveStateFlag.PSF_DamageLimit1) )
			dmg_sum = 1;

		if( victim.checkPState(PassiveStateFlag.PSF_Invincible) )
		{
			victim.fightNotice(PassiveStateFlag.PSF_Invincible.ToString());
			dmg_sum = 0;
		}

		//deal sheild
		float real_dmg = dmg_sum;
	
		//deal hp dmg
		//死亡处理 死亡也显示过量伤害
		if( victim_hp <= real_dmg )
		{
			//免疫致命伤害
			if (victim.checkPState(PassiveStateFlag.PSF_ImmuneFatal)) {
				victim.fightNotice("PSF_ImmuneFatal");
				//GloblaValue.trig_FE_Event(GloblaValue.FE_UNITIMMUNEFATAL, victim, victim, null);
				return 0.0f;
			}

			//准备死亡
			//GloblaValue.trig_FE_Event(GloblaValue.FE_PREUNITDIE, victim, victim, null);
			// 免疫死亡  
			if(victim.checkPState(PassiveStateFlag.PSF_ImmuneDeath))
			{
				victim.hp = 1;
				//准备死亡
				//GloblaValue.trig_FE_Event(GloblaValue.FE_UNITIMMUNEDEATH, victim, victim, null);
			}
			else
			{
				victim.hp = 0;
                victim.OnDie();
			}
		}
		else
		{
			float new_HP = victim_hp - real_dmg;
			if(new_HP < 1)
				new_HP = 1;
			victim.hp = new_HP;
		}

		//BattleBehaviorManager.Instance.Add(attacker.UnitId, BattleBehaviorType.kDamage, RandomManager.Seed, dmg_sum);
		return dmg_sum;
	}
    
	public static Damage heal( UCharacterController src, UCharacterController dest, float diff, float power)
	{
		Damage dmg = new Damage(src);
		dmg.type_ = DamageType.DT_Heal;
		dmg.sum_ = 0;
		float cur_hp = dest.hp;
		if( cur_hp <= 0)
			return dmg;

		float heal_sum = (src.getCurCProp(CPropType.CPT_MATK) * power + diff) * (1 + src.getCurCProp(CPropType.CPT_AHL));
        
		//deal
		deal_heal (dest, heal_sum);
		
		dmg.sum_ = (int)heal_sum;
		
		return dmg;
	}
	
	public static void deal_heal(UCharacterController dest, float heal_sum)
	{
		float cur_hp = dest.hp;
		float cur_hp_max = dest.getCurCProp(CPropType.CPT_MAXHP);
		if (cur_hp + heal_sum >= cur_hp_max) {
			dest.hp = cur_hp_max;
		} else {
			dest.hp = cur_hp + heal_sum;
		}
	}

	public static int calc_normal_pleech(UCharacterController attacker, UCharacterController victim, int dmg)
	{
        return 0;
	}

	public static Damage deal_reflect(UCharacterController attacker, UCharacterController victim, string damagetype, float dmgvalue){
		Damage dmg = new Damage(victim);
		dmg.type_ = DamageType.DT_Normal;
		dmg.sum_ = 0;
		return dmg;
	}    
    

	//伤害处理
	public static float DealDamage(UCharacterController victim, int dmg_sum, ShieldType sType = ShieldType.PhyShield) {
		if (victim == null)
			return 0.0f;
		int victim_hp = (int)victim.hp;
		if (victim_hp <= 0)
			return 0.0f;

		if (victim.checkPState(PassiveStateFlag.PSF_DamageLimit1))
			dmg_sum = 1;
		
		if( victim.checkPState(PassiveStateFlag.PSF_Invincible) )
		{
			dmg_sum = 0;
		}

		//deal sheild
		float real_dmg = 0;

		//deal hp dmg
		//死亡处理 死亡也显示过量伤害
		if (victim_hp <= real_dmg) {
			victim.hp = 0;
		} else {
			float new_HP = victim_hp - real_dmg;
			if (new_HP < 1)
				new_HP = 1;
			victim.hp = new_HP;
		}
		return dmg_sum;
	}    
}