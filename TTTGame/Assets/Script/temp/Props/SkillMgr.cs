﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum PassiveSkillEvent
{
    PSE_None,
    PSE_CityGreen,
    PSE_CityYellow,
    PSE_CityRed,
    PSE_BuildingCollect,
    PSE_BuildingDemolish,
    PSE_ClickBuildingFire,
    PSE_ClickRoadAccident,
    PSE_MaintainceSkip,
    PSE_Max,
}

public class SkillMgr
{
    public static Dictionary<PassiveSkillEvent, List<TableSkill>> regPassiveSkill = new Dictionary<PassiveSkillEvent, List<TableSkill>>();
    public static void regPassiveSkillEvent(TableSkill skill)
    {
        if( skill.triggerEvent != null && skill.triggerEvent.Length > 0 )
        {
            PassiveSkillEvent evt = (PassiveSkillEvent)Enum.Parse(typeof(PassiveSkillEvent), skill.triggerEvent);
            List<TableSkill> passSkills = null;
            if( regPassiveSkill.ContainsKey(evt) )
            {
                passSkills = regPassiveSkill[evt];
                if (!passSkills.Contains(skill))
                    passSkills.Add(skill);
            }
            else
            {
                passSkills = new List<TableSkill>();
                passSkills.Add(skill);
                regPassiveSkill[evt] = passSkills;
            }
        }
    }

    public static void trigEventSkill(PassiveSkillEvent evt, CPropData caseter = null)
    {
        if (regPassiveSkill.ContainsKey(evt))
        {
            List<TableSkill> passSkills = regPassiveSkill[evt];
            for( int i = 0; i < passSkills.Count; i++)
            {
                performSkill(passSkills[i], caseter);
            }
        }
    }

    public static void performSkill(string skillID, CPropData caster = null)
    {
        TableSkill skill = TableMgr.ins.GetSkill(skillID);
        performSkill(skill, caster);
    }

    public static void performSkill(TableSkill skill, CPropData caster = null)
    {
        if ( skill.targetFilter != null )
        {
            string filterStr = skill.targetFilter;
            string name = skill.action;
            TargetFilter targetFilter = TargetFilter.findFilter(filterStr);
            string param = "";
            if (targetFilter == null)
            {
                name = filterStr.Substring(0, filterStr.IndexOf(' '));
                param = filterStr.Substring(filterStr.IndexOf(' ') + 1, filterStr.Length - filterStr.IndexOf(' ') - 1);
                targetFilter = TargetFilter.findFilter(name);
            }
            if (targetFilter != null)
            {
                targetFilter.parseParam(param);
                List<UCharacterController> result = targetFilter.selectTarget(caster as UCharacterController);
                for (int i = 0; i < result.Count; i++)
                {
                    caster.performSkill(skill, result[i]);
                }
            }
            else
            {
                FpDebug.LogError("(+" + name + ")targetFilter cann't find...");
            }
        }
        else
        {
            string[] targetActionStrs = skill.action.Split(';');
            for (int i = 0; i < targetActionStrs.Length; i++)
            {
                performActionStr(targetActionStrs[i], caster, null);
            }
        }
    }

    public static void performActionStr(string actionStr, CPropData caster, CPropData target)
    {
        if (actionStr.Length == 0)
            return;
        //Zhaotan，修复ActionString有可能没有参数的情况的Bug
        string[] actionSplitStrs = actionStr.Split(' ');
        string name = actionSplitStrs[0];
        SkillAction action = SkillAction.findAction(name);
        if (actionSplitStrs.Length > 1)
        {
            string param = actionStr.Substring(actionStr.IndexOf(' ') + 1, actionStr.Length - actionStr.IndexOf(' ') - 1);
            if (action.parseParam(param) == false)
            {
                FpDebug.LogWarning(action.name_ + " paramError: " + actionStr);
                return;
            }
        }
        action.action(null, caster as UCharacterController, target as UCharacterController);
    }

    static bool needCacheSkill = true;
    static List<object> cacheSkill = new List<object>();
    public static void performSvrSkill(object data)
    {
        if (needCacheSkill)
        {
            cacheSkill.Add(data);
            return;
        }
    }

    public static bool checkSkillAction(string actionStr)
    {
        if (actionStr.Length == 0)
            return true;
        string[] casterActionStrs = actionStr.Split(';');
        foreach (string aStr in casterActionStrs)
        {
            string[] actionSplitStrs = aStr.Split(' ');
            string name = actionSplitStrs[0];
            if (name.Length == 0)
                continue;
            SkillAction action = SkillAction.findAction(name);
            if (action == null)
            {
                FpDebug.LogWarning("checkAction skill action null: " + name);
                return false;
            }
            if (actionSplitStrs.Length > 1)
            {
                string param = aStr.Substring(aStr.IndexOf(' ') + 1, aStr.Length - aStr.IndexOf(' ') - 1);
                if (action.parseParam(param) == false)
                {
                    FpDebug.LogWarning("checkAction skill action paramError: " + aStr);
                    return false;
                }
            }
        }
        return true;
    }

    public static bool checkFilter(string filterStr)
    {
        if ( filterStr.Length == 0)
            return true;

        string name = filterStr;
        TargetFilter targetFilter = TargetFilter.findFilter(filterStr);
        string param = "";
        if (targetFilter == null)
        {
            if(filterStr.Contains(" "))
            {
                name = filterStr.Substring(0, filterStr.IndexOf(' '));
                param = filterStr.Substring(filterStr.IndexOf(' ') + 1, filterStr.Length - filterStr.IndexOf(' ') - 1);
                targetFilter = TargetFilter.findFilter(name);
            }
            else
            {
                FpDebug.LogWarning(filterStr + " doesn't Exist!");
                return true;
            }
        }
        if (targetFilter != null)
        {
            return targetFilter.parseParam(param);
        }
        else
            return false;
    }

    public static void Release()
    {
        cacheSkill.Clear();
        needCacheSkill = true;
        FpDebug.Log("=============================>SkillMgr Clear");
    }

    public static void Init()
    {
        if(needCacheSkill)
        {
            needCacheSkill = false;
            for( int i =0; i < cacheSkill.Count; i++)
            {
                performSvrSkill(cacheSkill[i]);
            }
            cacheSkill.Clear();
        }
    }
}