﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum PropOPType
{
    PT_B,
    PT_BP,
    PT_BD,
    PT_P,
    PT_D,
}

public class COM_CProp
{
    public float base_ = 0.0f;
    public float basePer_ = 0.0f;
    public float baseDelta_ = 0.0f;
    public float per_ = 0.0f;
    public float delta_ = 0.0f;
}

public class CProp
{
    public COM_CProp m_cprops = new COM_CProp();
    public int propType;
    public bool isDirty_;	                    //<是否改变
    public float m_curValue = 0;    

    public CProp() { }

    public CProp(int cpt)
    {
        isDirty_ = false;
        curValue = 0.0f;
        propType = cpt;
    }

    //public bool isShow()
    //{
    //    if (cProps_[propType].isShow_ == 0)
    //    {
    //        return false;
    //    }
    //    else {
    //        return true;
    //    }
    //}

    //public bool isPer()
    //{
    //    if (cProps_[propType].isPer_ == 0)
    //    {
    //        return false;
    //    }
    //    else {
    //        return true;
    //    }
    //}

    public float curValue
    {

        get
        {
            if (isDirty_)
            {
                m_curValue = (m_cprops.base_ * (1 + m_cprops.basePer_) + m_cprops.baseDelta_) * (1 + m_cprops.per_) + m_cprops.delta_;

                ////Clamp
                //if (m_curValue < cProps_[propType].min_)
                //    m_curValue = cProps_[propType].min_;
                //if (m_curValue > cProps_[propType].max_)
                //    m_curValue = cProps_[propType].max_;
            }
            return m_curValue;
        }
        set
        {
            m_curValue = value;
        }
    }

    public float baseValue
    {
        get
        {
            return m_cprops.base_;
        }
        set
        {
            m_cprops.base_ = value;
        }
    }

    public float getOPValue(PropOPType op)
    {
        switch (op)
        {
            case PropOPType.PT_B:
                return getBase();
            case PropOPType.PT_BP:
                return getBasePer();
            case PropOPType.PT_BD:
                return getBaseDelta();
            case PropOPType.PT_P:
                return getPer();
            case PropOPType.PT_D:
                return getDelta();
            default:
                {
                    return 0;
                }
        }
    }

    public void doPropOp(PropOPType op, float value)
    {
        switch (op)
        {
            case PropOPType.PT_B:
                setBase(value);
                break;
            case PropOPType.PT_BP:
                setBasePer(value);
                break;
            case PropOPType.PT_BD:
                setBaseDelta(value);
                break;
            case PropOPType.PT_P:
                setPer(value);
                break;
            case PropOPType.PT_D:
                setDelta(value);
                break;
            default:
                {
                    return;
                }
        }
    }


    public COM_CProp getCprop() { return m_cprops; }

    public float getBase() { isDirty_ = true; return m_cprops.base_; }
    public void setBase(float val) { isDirty_ = true; m_cprops.base_ = val; }
    public float getBasePer() { isDirty_ = true; return m_cprops.basePer_; }
    public void setBasePer(float val) { isDirty_ = true; m_cprops.basePer_ = val; }
    public float getBaseDelta() { isDirty_ = true; return m_cprops.baseDelta_; }
    public void setBaseDelta(float val) { isDirty_ = true; m_cprops.baseDelta_ = val; }
    public float getPer() { isDirty_ = true; return m_cprops.per_; }
    public void setPer(float val) { isDirty_ = true; m_cprops.per_ = val; }
    public float getDelta() { isDirty_ = true; return m_cprops.delta_; }
    public void setDelta(float val) { isDirty_ = true; m_cprops.delta_ = val; }

    public static CProp operator +(CProp lhs, CProp rhs)
    {
        CProp newPorp = new CProp();
        newPorp.m_cprops.base_ = lhs.m_cprops.base_ + rhs.m_cprops.base_;
        newPorp.m_cprops.baseDelta_ = lhs.m_cprops.baseDelta_ + rhs.m_cprops.baseDelta_;
        newPorp.m_cprops.basePer_ = lhs.m_cprops.basePer_ + rhs.m_cprops.basePer_;
        newPorp.m_cprops.delta_ = lhs.m_cprops.delta_ + rhs.m_cprops.delta_;
        newPorp.m_cprops.per_ = lhs.m_cprops.per_ + rhs.m_cprops.per_;
        newPorp.isDirty_ = true;
        return newPorp;
    }

    public override string ToString()
    {
        return m_curValue.ToString() + " = (" + m_cprops.base_.ToString() + " * (1 + " + m_cprops.basePer_.ToString() + ") + " + m_cprops.baseDelta_.ToString() + ") * (1 + " +
            m_cprops.per_.ToString() + ") + " + m_cprops.delta_.ToString();
    }
}
