﻿using UnityEngine;
using System.Collections;
using System;

public class TableSkill
{
	public int level;
	public string targetType;
	public string triggerEvent;
	public string classType;
	public string skillType;
	public string targetFilter;
	public string action;
}