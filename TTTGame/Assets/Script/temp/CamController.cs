﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ECamState
{
    ECS_None,
    ECS_Free,
    ECS_Bind,
    ECS_Lock,
    ECS_Max,
}

public class CamController : MonoBehaviour
{

    bool isLock = false;
    bool winCam = false;
    public float winCamYawRate = 1.0f;
    public float winCamYawMove = 0;
    //	bool isMoveToBack = false;
    ECamState camState = ECamState.ECS_None;

    public float _pitch;
    public float _yaw;
    public float _roll;
    public GameObject _curLookAt = null;
    public GameObject _chgLookAt = null;
    public GameObject _curTempLookAt = null;
    public GameObject _curTempPosition = null;
    public float _curLookAtYaw = 0;
    public float _curLookAtDeltaYaw = 0;
    Quaternion _curRot;

    public Vector2 _curMousePos = Vector2.zero;
    public Vector2 _curMouseDelta = Vector2.zero;

    bool rmDown = false;
    bool lmDown = false;

    float camMoveSpeed = 7.0f;
    float camZoomSpeed = 5.0f;
    public float camZoomAcc = 5.0f;
    public float camZoomScale = 50f;

    //和Lookat目标当前的绑定方向保持一致
    [Header("Syn BindObj's Yaw")]
    public bool _synBindYaw = false;
    public float _synBindOrginYaw = 0;
    public float _synBindDeltaYaw = 0;
    //========================================
    public float _bindPitch = 40f;
    public float _bindLerpPitch = 0.0f;
    public float _LerpFarClip = 0.0f;
    public float m_bindYaw = 0.0f;
    //	private float _dstBindYaw = 0.0f;
    public float _bindDefLen = 10.0f;
    public float _bindMaxZoom = 300.0f;
    public float _bindCurZoom = 50.0f;
    public float _bindMinZoom = 0.0f;
    float _bindZoomSpd = 20.0f;
    public float _bindMaxPitch = 90f;
    public float _bindMinPitch = 30f;
    float _camMaxClip = 5;
    public float _camDefFarClip = 500;
    bool _isInYawChange = false;
    //	float _yawChangeSpeed = 90.0f;
    bool _cacheYaw = false;
    //	float _cacheTargetBindYaw = 0.0f;
    bool _needFresh = false;

    //摄像机过度
    bool _isInChangeLookAt = false;
    //	float _moveDstYaw = 105f;
    //	Vector3 _moveToDst = Vector3.zero;
    //	Quaternion _rotToDst = Quaternion.Euler (0, 0, 0);

    //IOS
    // touch scale
    private Vector2 _curTouchPos1;
    private Vector2 _cacheTouchPos1;
    private Vector2 _deltaTouchPos1;
    private Vector2 _curTouchPos2;
    private Vector2 _cacheTouchPos2;
    private Vector2 _deltaTouchPos2;
    bool touchDown = false;
    bool doubleTouchDown = false;
    private RaycastHit[] camHitMeshObj = null;
    public Vector3 _lookAtDir = Vector3.up;
    public float _lookAtDirLen = 0;


    // Use this for initialization
    void Start()
    {
        _curRot = this.transform.rotation;
        _pitch = _curRot.eulerAngles.x;
        _yaw = _curRot.eulerAngles.y;
        _roll = _curRot.eulerAngles.z;
        if (_curLookAt != null)
            setCamLookAt(_curLookAt);
    }
    ECamState getCamState()
    {
        if (isLock)
            return ECamState.ECS_Lock;
        if (_curLookAt != null)
            return ECamState.ECS_Bind;
        if (_curLookAt == null)
            return ECamState.ECS_Free;

        return ECamState.ECS_None;
    }

    private static Shader _DefTransShader;
    private static Shader defTransShader
    {
        get
        {
            if (_DefShader == null)
            {
                _DefShader = Shader.Find("Transparent/Cutout/Diffiuse");
            }
            return _DefShader;
        }
    }

    private static Shader _DefShader;
    private static Shader defShader
    {
        get
        {
            if (_DefShader == null)
            {
                _DefShader = Shader.Find("Diffuse");
            }
            return _DefShader;
        }
    }

    private static Shader _TransDiffShader;
    private static Shader transShader
    {
        get
        {
            if (_TransDiffShader == null)
            {
                _TransDiffShader = Shader.Find("Transparent/Diffuse");
            }
            return _TransDiffShader;
        }
    }

    bool checkHitCollider(RaycastHit checkObj, RaycastHit[] hitObjs)
    {
        for (int i = 0; i < hitObjs.Length; i++)
        {
            if (hitObjs[i].collider == checkObj.collider)
                return true;
        }
        return false;
    }

    void setColliderTrans(Collider colObj, bool isTrans)
    {
    }

    void updateColliderTrans()
    {
        Vector3 camRayDir = (transform.position - _curLookAt.transform.position).normalized;
        Ray camRay = new Ray(_curLookAt.transform.position, camRayDir);

        Debug.DrawLine(_curLookAt.transform.position, _curLookAt.transform.position + camRayDir * 25);

        RaycastHit[] rayHits;
        int layerIndex = LayerMask.GetMask("SceneCollider");
        rayHits = Physics.RaycastAll(camRay, 25, layerIndex);
        if (rayHits.Length > 0)
        {
            if (camHitMeshObj != null)
            {
                for (int i = 0; i < camHitMeshObj.Length; i++)
                {
                    //检测上次的对象是否还在
                    if (!checkHitCollider(camHitMeshObj[i], rayHits))
                    {
                        //已经cast不到，需要不透明处理
                        setColliderTrans(camHitMeshObj[i].collider, false);
                    }
                }
            }
            camHitMeshObj = rayHits;
            for (int i = 0; i < camHitMeshObj.Length; i++)
            {
                setColliderTrans(camHitMeshObj[i].collider, true);
            }
        }
        else
        {
            if (camHitMeshObj != null)
            {
                for (int i = 0; i < camHitMeshObj.Length; i++)
                {
                    setColliderTrans(camHitMeshObj[i].collider, false);
                }
                camHitMeshObj = null;
            }
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {

        //updateTempLookAt(false);

        camState = getCamState();
        switch (camState)
        {
            case ECamState.ECS_Lock:
                return;
            case ECamState.ECS_Free:
                break;
            case ECamState.ECS_Bind:
                updateBindCamera();
                updateZoom();
                break;
            default:
                break;
        }
    }

    public void setLock(bool flag)
    {
        isLock = flag;
    }

    public void updateZoomValue(float v)
    {
        setCamZoom(v);
    }

    public void onZoomComplete()
    {
        iTween.ValueTo(this.gameObject, iTween.Hash(
            "from", 0,
            "to", 30,
            "time", 20,
            "delay", 0,
            "easetype", iTween.EaseType.linear,
            "onupdate", "updateYawMove",
            "oncomplete", "onYawMoveFEnd"
            ));
    }

    public void updateYawRate(float v)
    {
        winCamYawRate = v;
    }

    public void onYawRateEnd()
    {
        winCamYawRate = 0;
        iTween.ValueTo(this.gameObject, iTween.Hash(
            "from", 0,
            "to", 30,
            "time", 20,
            "delay", 0,
            "easetype", iTween.EaseType.linear,
            "onupdate", "updateYawMove",
            "oncomplete", "onYawMoveFEnd"
            ));
    }

    public void updateYawMove(float v)
    {
        winCamYawMove = v;
    }

    public void onYawMoveFEnd()
    {
        iTween.ValueTo(this.gameObject, iTween.Hash(
            "from", 30,
            "to", -30,
            "time", 40,
            "delay", 0,
            "easetype", iTween.EaseType.linear,
            "onupdate", "updateYawMove",
            "oncomplete", "onYawMoveSEnd"
            ));
    }

    public void onYawMoveSEnd()
    {
        iTween.ValueTo(this.gameObject, iTween.Hash(
            "from", -30,
            "to", 30,
            "time", 40,
            "delay", 0,
            "easetype", iTween.EaseType.linear,
            "onupdate", "updateYawMove",
            "oncomplete", "onYawMoveFEnd"
            ));
    }

    public void setBindSyn(bool flag)
    {
        _synBindYaw = flag;
        _curLookAtYaw = getTempLookAt().transform.rotation.eulerAngles.y;
        FollowTarget ft = getTempFollowTarget();
        if (ft != null)
        {
            ft._followRotation = _synBindYaw;
        }
    }

    public void angle360ToPi(ref float angle)
    {
        angle = angle % 360f;
    }

    public void mouseInputUpdate()
    {
        if (_isInChangeLookAt)
            return;

        if (_cacheYaw)
            return;

        //if (Input.GetMouseButtonDown (0) && !lmDown) {
        //	lmDown = true;
        //	_curMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        //}
        //if (Input.GetMouseButtonDown (1) && !rmDown) {
        //	rmDown = true;
        //	_curMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        //	return;
        //}
        //if( Input.GetMouseButtonUp(0) )
        //{
        //	lmDown = false;
        //}
        //if( Input.GetMouseButtonUp(1) )
        //{
        //	rmDown = false;
        //	updateTempLookAt (rmDown);
        //	return;
        //}

        //_curMouseDelta = (new Vector2(Input.mousePosition.x, Input.mousePosition.y)) - _curMousePos;
        //_curMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        //updateFreeCamera ();

        updateTempLookAt(rmDown);

        //if (rmDown && _curLookAt != null ) {
        //	setBindPitch(_bindPitch - _curMouseDelta.y*Time.deltaTime*camMoveSpeed*2);
        //	if( _needFresh )
        //		Debug.Log("Fresh _curMouseDelta.x = " + _curMouseDelta.x.ToString());
        //	setBindYaw(_bindYaw + _curMouseDelta.x*Time.deltaTime*camMoveSpeed*2);
        //}

        //if (rmDown && _curLookAt == null) {
        //	setPitch(_pitch - _curMouseDelta.y*Time.deltaTime*camMoveSpeed);
        //	setYaw(_yaw + _curMouseDelta.x*Time.deltaTime*camMoveSpeed);
        //}
    }

    public void updateFreeCamera()
    {
        if (_curLookAt != null)
            return;

        if (Input.GetKey("w"))
        {
            Vector3 dir = transform.forward;
            dir.y = 0;
            transform.position = transform.position + dir * camMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey("s"))
        {
            Vector3 dir = transform.forward;
            dir.y = 0;
            transform.position = transform.position - dir * camMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey("a"))
        {
            Vector3 dir = transform.right;
            dir.y = 0;
            transform.position = transform.position - dir * camMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey("d"))
        {
            Vector3 dir = transform.right;
            dir.y = 0;
            transform.position = transform.position + dir * camMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey("q"))
        {
            Vector3 dir = Vector3.up;
            transform.position = transform.position - dir * camMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey("e"))
        {
            Vector3 dir = Vector3.up;
            transform.position = transform.position + dir * camMoveSpeed * Time.deltaTime;
        }

        Quaternion rot = Quaternion.Euler(_pitch, _yaw, _roll);
        this.transform.rotation = rot;
    }

    public void setPitch(float angle)
    {
        _pitch = angle % 360;
    }


    public void setYaw(float angle)
    {
        _yaw = angle % 360;
    }

    public void setRoll(float angle)
    {

    }

    public void setZoom(float dis)
    {
        transform.position = transform.position + dis * transform.forward;
    }

    public void zoomSpeed(float dis, bool needLock = true)
    {
        camZoomSpeed += dis * camZoomScale;
        if (needLock && camZoomSpeed > 10)
            camZoomSpeed = 10;

        if (needLock && camZoomSpeed < -10)
            camZoomSpeed = -10;
    }

    public void updateZoom()
    {
        if (camZoomSpeed > 0)
        {
            camZoomSpeed -= camZoomAcc * camZoomScale * Time.deltaTime;
            if (camZoomSpeed < 0)
                camZoomSpeed = 0;
        }
        else if (camZoomSpeed < 0)
        {
            camZoomSpeed += camZoomAcc * camZoomScale * Time.deltaTime;
            if (camZoomSpeed > 0)
                camZoomSpeed = 0;
        }
        if (camZoomSpeed == 0)
            return;
        if (_curLookAt == null)
            setZoom(camZoomSpeed * Time.deltaTime);
        else
            setBindZoom(camZoomSpeed * Time.deltaTime);
    }

    GameObject getTempLookAt()
    {
        if (_curTempLookAt == null)
        {
            GameObject mygameobject = new GameObject();
            mygameobject.name = "CameraTempLookAtObject";
            _curTempLookAt = mygameobject;
            if (_curLookAt != null)
                _curTempLookAt.transform.position = _curLookAt.transform.position;
        }
        return _curTempLookAt;
    }

    FollowTarget getTempFollowTarget()
    {
        FollowTarget ft = getTempLookAt().GetComponent<FollowTarget>();
        if (ft == null)
        {
            ft = getTempLookAt().AddComponent<FollowTarget>();
        }
        return ft;
    }

    public void updateTempLookAt(bool moveDown)
    {
        //liner updateCam
        FollowTarget ft = getTempFollowTarget();
        if (ft.target != _curLookAt.transform)
            ft.target = _curLookAt.transform;
        if (ft != null && (ft._beginMove == false))
        {
            _curTempLookAt.transform.position = _curLookAt.transform.position;
            if (!_synBindYaw)
            {
                _curTempLookAt.transform.rotation = _curLookAt.transform.rotation;
            }
            ft._followRotation = _synBindYaw;
        }

        _needFresh = false;
        if (Mathf.Abs(_curLookAtYaw - _curTempLookAt.transform.eulerAngles.y) > 0.01f)
        {
            if (_synBindYaw)
            {
                if (moveDown)
                {
                    setBindSyn(false);
                }
                return;
            }
            else
            {
                _synBindOrginYaw = _curTempLookAt.transform.rotation.eulerAngles.y;
                _curLookAtDeltaYaw = _curTempLookAt.transform.rotation.eulerAngles.y - _curLookAtYaw;
                _curLookAtYaw = _curTempLookAt.transform.rotation.eulerAngles.y;
                _bindYaw -= _curLookAtDeltaYaw;
            }
            _curLookAtDeltaYaw = 0;
            _needFresh = true;
        }

    }

    public void setCamLookAt(GameObject lookObj, bool Immediately = true)
    {
        if (lookObj == null)
        {
            _curLookAt = null;
            _curRot = this.transform.rotation;
            _pitch = _curRot.eulerAngles.x;
            _yaw = _curRot.eulerAngles.y;
            _roll = _curRot.eulerAngles.z;
            _curLookAtYaw = _curRot.eulerAngles.y;
            return;
        }
        else {
            FollowTarget ft = getTempFollowTarget();
            if (_curLookAt == null)
            {
                getTempLookAt().transform.position = lookObj.transform.position;
                getTempLookAt().transform.rotation = lookObj.transform.rotation;
                ft._followPosition = false;
            }
            else
            {
                if (lookObj == _curLookAt)
                    return;
                ft._followPosition = true;
                ft._pspeed = 6;
                ft._beginMove = Immediately;
            }
            _curLookAt = lookObj;
            ft.target = _curLookAt.transform;
        }
    }

    public void setCamLookAt(Vector3 position)
    {
        _curLookAt.transform.position = position;
        calcBindcamPos();
    }

    public void setCamLookAt(Vector3 position, Quaternion rotation)
    {
        _curLookAt.transform.rotation = rotation;
        setCamLookAt(position);
    }

    public bool setCamZoom(float zoom)
    {
        if (_bindCurZoom == zoom)
            return false;

        _bindCurZoom = zoom;
        if (_bindCurZoom < _bindMinZoom)
            _bindCurZoom = _bindMinZoom;
        if (_bindCurZoom > _bindMaxZoom)
            _bindCurZoom = _bindMaxZoom;

        return true;
    }

    public GameObject getLookAt()
    {
        return _curLookAt;
    }

    public void updateBindCamera()
    {
        calcBindcamPos();
    }

    public void updateForceCamera()
    {
    }

    public float _bindYaw
    {
        get
        {
            return m_bindYaw;
        }
        set
        {
            m_bindYaw = value;
            m_bindYaw = _bindYaw % 360;
        }
    }

    public void setBindYaw(float angle)
    {
        if (_isInYawChange)
            return;
        _bindYaw = angle % 360;
    }

    public bool addBindYaw(float angle)
    {
        if (_curLookAt != null)
        {
            _curLookAt.transform.RotateAround(_curLookAt.transform.position, _curLookAt.transform.up, angle);
            //_curLookAt.transform.rotation = Quaternion.Euler(_curLookAt.transform.eulerAngles.x, _curLookAt.transform.eulerAngles.y + angle, _curLookAt.transform.eulerAngles.z);
        }
        return true;
    }

    public float getBindYaw()
    {
        if (_curLookAt != null)
        {
            return _curLookAt.transform.eulerAngles.y;
        }
        return 0;
    }

    public void setBindPitch(float angle)
    {
        //_bindLerpPitch = angle % 360;
        _bindPitch = angle % 360;
    }

    public bool addBindPitch(float angle, Range pRange)
    {
        if (_bindPitch == _bindPitch + angle)
            return false;
        _bindPitch += angle;
        if (_bindPitch > pRange.max)
            _bindPitch = pRange.max;
        if (_bindPitch < pRange.min)
            _bindPitch = pRange.min;
        return true;
    }

    public void updateBindLerpPitch()
    {
        if (_bindMaxPitch - _bindMinPitch == 0)
            return;

        _bindLerpPitch = (1 - _bindCurZoom / _bindMaxZoom) * (_bindMaxPitch - _bindMinPitch);

        //_LerpFarClip = (1 - _bindCurZoom / _bindMaxZoom) * (_camMaxClip);

        //GetComponent<Camera>().farClipPlane = _camDefFarClip - _LerpFarClip;
    }

    public void setBindZoom(float dis)
    {
        _bindCurZoom -= dis;
        if (_bindCurZoom < _bindMinZoom)
            _bindCurZoom = _bindMinZoom;
        if (_bindCurZoom > _bindMaxZoom)
            _bindCurZoom = _bindMaxZoom;
    }

    public void calcBindcamPos()
    {
        //updateBindLerpPitch();
        float curZoom = _bindCurZoom + _bindDefLen;
        Transform P = _curLookAt.transform;
        Vector3 forward = P.forward;
        forward.y = 0.0f;
        forward.Normalize();
        float finalPitch = _bindPitch - _bindLerpPitch;
        Vector3 A = (P.position + forward * curZoom * Mathf.Cos(finalPitch * Mathf.PI / 180f)) + (new Vector3(0, curZoom * Mathf.Sin(finalPitch * Mathf.PI / 180f), 0));
        this.transform.position = A + _lookAtDir * _lookAtDirLen;
        //_curTempLookAt.transform.position = P.position;
        //计算旋转位置
        Quaternion rot = Quaternion.Euler(0, _curLookAt.transform.eulerAngles.y, 0);
        this.transform.rotation = rot;
        float rotAngle = (_bindYaw * winCamYawRate + winCamYawMove) % 360;
        transform.RotateAround(_curLookAt.transform.position, Vector3.up, rotAngle);

        this.transform.LookAt(_curLookAt.transform.position + _lookAtDir * _lookAtDirLen);
        CameraShake cs = GetComponent<CameraShake>();
        if (cs != null)
        {
            cs.updateShakeCenter(this.transform.position + _lookAtDir * _lookAtDirLen);
            cs.updateShake();
        }
    }

    /// ==============================================================================
    /// IOS 控制
    public void IOSInputUpdate()
    {
        if (_curLookAt == null)
            return;

        if (_isInChangeLookAt)
            return;

        if (_cacheYaw)
            return;

        if (Input.touchCount == 0)
        {
            doubleTouchDown = false;
            touchDown = false;
            _deltaTouchPos1 = Vector2.zero;
            _deltaTouchPos2 = Vector2.zero;
            updateTempLookAt(touchDown);
            return;
        }

        if (Input.touchCount == 1)
        {
            if (touchDown == false)
            {
                _deltaTouchPos1 = Vector2.zero;
                _deltaTouchPos2 = Vector2.zero;
                touchDown = true;
                doubleTouchDown = false;
                _cacheTouchPos1 = Input.GetTouch(0).position;
                _curTouchPos1 = Input.GetTouch(0).position;
                updateTempLookAt(touchDown);
                return;
            }
        }

        if (Input.touchCount == 2)
        {
            if (doubleTouchDown == false)
            {
                _deltaTouchPos1 = Vector2.zero;
                _deltaTouchPos2 = Vector2.zero;
                doubleTouchDown = true;
                touchDown = false;
                _cacheTouchPos1 = Input.GetTouch(0).position;
                _curTouchPos1 = Input.GetTouch(0).position;
                _cacheTouchPos2 = Input.GetTouch(1).position;
                _curTouchPos2 = Input.GetTouch(1).position;
                updateTempLookAt(touchDown);
                return;
            }
        }

        updateTempLookAt(touchDown);

        if (touchDown)
        {
            _curTouchPos1 = Input.GetTouch(0).position;
            _deltaTouchPos1 = _curTouchPos1 - _cacheTouchPos1;
            float yawChange = _cacheTouchPos1.x - _curTouchPos1.x;
            if (yawChange != 0)
            {
                _needFresh = true;
                setBindYaw(_bindYaw - yawChange * Time.deltaTime * camMoveSpeed);
                _cacheTouchPos1 = Input.GetTouch(0).position;
            }
        }
        else if (doubleTouchDown)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                _curTouchPos1 = Input.GetTouch(0).position;
                _curTouchPos2 = Input.GetTouch(1).position;
                _deltaTouchPos1 = _curTouchPos1 - _cacheTouchPos1;
                _deltaTouchPos2 = _curTouchPos2 - _cacheTouchPos2;

                float zoomChange = IOSChangeZoom(Vector2.Distance(_cacheTouchPos1, _cacheTouchPos2), Vector2.Distance(_curTouchPos1, _curTouchPos2));

                //备份上一次触摸点的位置，用于对比
                _cacheTouchPos1 = Input.GetTouch(0).position;
                _cacheTouchPos2 = Input.GetTouch(1).position;
                //FpDebug.Log("camZoomSpeed" + camZoomSpeed.ToString() + " * Time.deltaTime" + Time.deltaTime.ToString() + " * zoomChange" + zoomChange.ToString() + " = " + (camZoomSpeed * Time.deltaTime * zoomChange).ToString());
                if (zoomChange != 0)
                {
                    setBindZoom(_bindZoomSpd * Time.deltaTime * zoomChange * 0.5f);
                }
            }
        }
    }

    // calc change zoom 
    float IOSChangeZoom(float oldDis, float newDis)
    {
        //函数传入上一次触摸两点的位置与本次触摸两点的位置计算出用户的手势 
        //FpDebug.Log("IOSChangeZoom = (" + _deltaTouchPos2.x.ToString() + " + " + _deltaTouchPos1.x.ToString() + ") = " + (_deltaTouchPos2.x - _deltaTouchPos1.x).ToString());
        return _deltaTouchPos2.x - _deltaTouchPos1.x;
    }

    // calc change yaw
    float IOSChangeYaw()
    {
        return _deltaTouchPos2.y - _deltaTouchPos1.y;
    }

    // calc change yaw
    float IOSChangePitch()
    {
        return Mathf.Abs(_deltaTouchPos1.y - _deltaTouchPos2.y);
    }

}

