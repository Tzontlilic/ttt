﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	private Camera		m_camera = null;

	private float		m_fTotalTime;
	private float		m_fSpaceTime;
	private float		m_fMinOffSet;
	private float		m_fMaxOffSet;

	private Vector3		m_centerPos;

	private float		m_fTotalPassTime;
	private float		m_fPassTime;
	private bool		m_bShaking = false;
	public  bool		m_testShake = false;
	public  float		m_testShakeTime = 0.5f;
	public  int			m_testShakeRate = 200;
	public  float		m_testShakeMin = 0.2f;
	public  float		m_testShakeMax = 0.8f;

	// Use this for initialization
	void Start () 
	{
		m_camera = gameObject.GetComponent<Camera>();	
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		if (m_testShake) {
			m_testShake = false;	
			shake(m_testShakeTime, m_testShakeRate, m_testShakeMin, m_testShakeMax);
		}
		//updateShake ();
	}	
	
	public void updateShake()
	{
		if(m_bShaking && m_camera != null)
		{
			m_fPassTime += Time.deltaTime;
			m_fTotalPassTime += Time.deltaTime;
			if(m_fTotalPassTime >= m_fTotalTime)
			{
				m_bShaking = false;
				m_camera.transform.localPosition = m_centerPos;
				return;
			}
			
			if(m_fPassTime >= m_fSpaceTime)
			{
				m_fPassTime -= m_fSpaceTime;
				//m_camera.transform.localPosition = m_centerPos + m_camera.transform.right * (Random.value - 0.5f) * (m_fMaxOffSet - m_fMinOffSet) + 
				m_camera.transform.localPosition = m_centerPos + m_camera.transform.right * UnityEngine.Random.Range(- 0.5f, 0.5f) * (m_fMaxOffSet - m_fMinOffSet) + 
					//new Vector3(0, 1, 0) * (Random.value - 0.5f) * (m_fMaxOffSet - m_fMinOffSet);
					new Vector3(0, 1, 0) * UnityEngine.Random.Range(-0.5f, 0.5f) * (m_fMaxOffSet - m_fMinOffSet);
			}			
		}
	}
	
	public void updateShakeCenter(Vector3 pos)
	{
		if( m_bShaking )
			m_centerPos = pos;
	}

	public void shake(float time, int rate, float minOffSet, float maxOffSet)
	{
		if(m_camera == null)
			return;
		if(!m_bShaking)
			m_centerPos = gameObject.transform.localPosition;
		m_fTotalTime = time;
		m_fSpaceTime = 1.0f / rate;
		m_fMinOffSet = minOffSet;
		m_fMaxOffSet = maxOffSet;

		m_fPassTime = 0;
		m_fTotalPassTime = 0;
		m_bShaking = true;
	}


}
