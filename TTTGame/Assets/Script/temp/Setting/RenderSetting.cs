﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class RenderSetting
{
    private static volatile RenderSetting _instance = null;
    public static RenderSetting instance
    {
        get
        {
            if (_instance == null)
                _instance = new RenderSetting();
            return _instance;
        }
    }

    UnityStandardAssets.CinematicEffects.TonemappingColorGrading CameraColorGrading = null;
    UnityStandardAssets.CinematicEffects.TonemappingColorGrading.ColorGradingSettings CameraColorGradingSetting;

    public void InitCameraColorGrading(GameObject _obj)
    {
        CameraColorGrading = _obj.GetComponent<UnityStandardAssets.CinematicEffects.TonemappingColorGrading>();
        if(CameraColorGrading != null)
            CameraColorGradingSetting = CameraColorGrading.colorGrading;
    }

    public void CameraDeathEffect()
    {
        CameraColorGradingSetting.basics.saturation = 0;
        CameraColorGrading.colorGrading = CameraColorGradingSetting;
        CameraColorGrading.SetDirty();
    }

    public void CameraLiveEffect()
    {
        CameraColorGradingSetting.basics.saturation = 1;
        CameraColorGrading.colorGrading = CameraColorGradingSetting;
        CameraColorGrading.SetDirty();
    }
}