﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

/// <summary>
/// MapCamera
/// Three transform orbit rig
/// Rig - base transform is orbit point the camera is looking at and rotates around
/// Orbit - second transform is the rotation joint which controls camera pitch / yaw (theta)
/// CameraTran - third transform controls distance from the orbit point
/// 
/// Two finger gestures are complicated, when you
/// pitch, yaw or zoom the camera it accumulates "energy" which is used to dampen the other two movements
/// </summary>
public class MapCamera : FpSingleton<MapCamera>
{
    public enum ECamState
    {
        GameView,
        TourView,
        DriveView,
        BuildSelect,
    }

    public Camera myCamera;

    /// <summary>
    /// Base transform is orbit point the camera is looking at and rotates around
    /// </summary>
    public Transform Rig;

    /// <summary>
    /// Second transform is the rotation joint which controls camera pitch / yaw
    /// </summary>
    public Transform Orbit;

    /// <summary>
    /// Third transform controls distance from the orbit point
    /// </summary>
    public Transform CameraTran;

    public GameObject DefLookAt;

    private CamController camControll = null;

    [Header("Drag")]

    [Tooltip("Multiplier coef on how fast finger dragging moves the camera")]
    public float DragSpeed = -3f;
    [Tooltip("Multiplier coef on how fast mouse mouse dragging moves the camera")]
    public float MouseDragSpeed = -3f;
    [Tooltip("Multiplier coef on how fast keyboard moves the camera")]
    public float KeyboardSpeed = 100f;
    [Tooltip("How long momentum is applied for (higher -> shorter)")]
    public float DragHardness = 2f;
    [Tooltip("Current velocity")]
    public Vector3 DragVel = Vector3.zero;
    bool _isInDrag = false;
    bool _isInZoomDrag = false;
    //Vector3 _CurDragMousePos = Vector3.zero;

    //float DragRate = 0;
    public Vector3 _lookatBeginPos = Vector3.zero;
    //Vector3 _beginDragPos = Vector3.zero;

    [Header("Yaw")]
    [Tooltip("Last Direction of The Two TouchPoint.")]
    Vector2 lastVecter = Vector2.zero;
    [Tooltip("cur delta angle between last touch vec and Cur touch vec.")]
    float deltaYaw = 0;
    [Tooltip("Multiplier coef on how fast two finger dragging rotates the camera")]
    public float YawSpeed = 1f;


    public UnityEngine.UI.Image yawImage = null;
    public bool showYawImage = false;

    public Range XRange = new Range(0, 0);
    public Range ZRange = new Range(1600, 1600);


    [Header("Zoom")]
    [Tooltip("Multiplier coef on how fast two finger zooming is applied")]
    public float ZoomSpeed = -0.0003f;
    [Header("_curZoomRatio is value form 0-1, lerp for _ZoomRange, begining of the Def Lerp.")]
    public float _curZoomRatio = 0f;
    [Tooltip("Min/Max values for zoom range")]
    public Range _ZoomRange = new Range(100f, 1000f);
    public Range _AimZoomRange = new Range(100f, 1000f);
    public float AimZoomRatio = 0.5f;
    public float DefZoomRatio = 0.5f;
    float _curTourZoomRatio = 0f;
    Range lerpZoomRange = new Range(0, 0);

    [Header("--------------------------------------------------")]
    [Tooltip("Ratio of dragging screen coverage to zoom applied")]
    public AnimationCurve ZoomRatioToDist = new AnimationCurve(new Keyframe(0, 0),
                                                                              new Keyframe(1, 1));
    public Range ZoomRange
    {
        get
        {
            return _ZoomRange;
        }
    }
    public float CurZoomRatio
    {
        set
        {
            _curZoomRatio = value;
        }
        get
        {
            return _curZoomRatio;
        }
    }
    public float ForceRatio = 0.05f;
    public float ForceHardness = 0.05f;

    [Tooltip("Current zoom")]
    internal float Zoom = 0.5f;
    [Tooltip("Zoom velocity")]
    internal float ZoomVel = 0;
    [Tooltip("Accumulated zoom damping energy")]
    internal float ZoomEnergy = 0;


    [Header("Pitch")]
    Vector2 lastPitchCenter = Vector2.zero;
    Vector2 lastPitchCacheY = Vector2.zero;
    [Tooltip("Multiplier coef on how fast finger gestures pitch the camera")]
    public float PitchSpeed = 1.0f;
    [Tooltip("Multiplier coef on how fast keyboard pitches the camera")]
    public float PitchSpeedKeyboard = 125f;
    [Tooltip("Min/Max pitch")]
    public Range PitchRange = new Range(0f, 90f);
    [Tooltip("How fast pitch damping is dissipated")]
    public float PitchEnergyDecay = 5.0f;

    float DefCacheZoomRatio = 0;
    float DefCachePitch = 40;
    public float TourDefPitch = 40;
    public float SelectViewPitch = 40;
    public float DrivePitch = 40;

    [Tooltip("Accumulated pitch damping")]
    internal float DDragEnergy = 0f;

    class CameraTransLerp
    {
        public Vector3 startLookAt = Vector3.zero;
        public Vector3 startEuler = Vector3.zero;
        public Vector3 lerpPos = Vector3.zero;
        public Vector3 lerpEuler = Vector3.zero;
        public Range startZoomRange;
        public float startZoom = 0;
        public float lerpZoom = 0;
        public float startPitch = 0;
        public float lerpPitch = 0;
    }
    bool isInLerpLockCam = false;
    bool isInLerpCam = false;
    bool isInTourCam = false;
    bool isInDriveCam = false;
    public bool IsInLerpCam
    {
        get
        {
            return isInLerpCam;
        }
    }
    CameraTransLerp CurTransDelta = null;
    [Header("Camera Lerp")]
    public float lerpTime = 2;
    public iTween.EaseType lerpType = iTween.EaseType.linear;
    public float unLocklerpTime = 2;
    public iTween.EaseType unLocklerpType = iTween.EaseType.linear;
    GameObject _TempLerpLookAt;
    GameObject TempLerpLookAt
    {
        get
        {
            if (_TempLerpLookAt == null)
            {
                GameObject mygameobject = new GameObject();
                mygameobject.name = "CameraLerpTempLookAt";
                mygameobject.transform.position = Vector3.zero;
                mygameobject.transform.rotation = Quaternion.Euler(0, 180, 0);
                _TempLerpLookAt = mygameobject;
            }
            return _TempLerpLookAt;
        }
    }

    /// <summary>
    /// On start sync our state values to the current transform settings
    /// </summary>
    void Start()
    {
        _instance = this;
        camControll = GetComponent<CamController>();
        if (camControll != null)
        {
            float zoomValue = ZoomRange.Evaluate(ZoomRatioToDist.Evaluate(CurZoomRatio));
            camControll.setCamZoom(zoomValue);
        }
        RenderSetting.instance.InitCameraColorGrading(myCamera.gameObject);
        SetCamEnable(false);
    }

    bool hasBindPlayer = false;
    public void BindPlayer(Vector3 pos, float dt)
    {
        if (!hasBindPlayer)
        {
            hasBindPlayer = true;
            DefLookAt.transform.position = pos;
            CurZoomRatio = DefZoomRatio;
            float zoomValue = ZoomRange.Evaluate(ZoomRatioToDist.Evaluate(CurZoomRatio));
            camControll.setCamLookAt(DefLookAt);
            camControll.setCamZoom(zoomValue);
        }
        else
            DefLookAt.transform.position = pos;// Vector3.Lerp(DefLookAt.transform.position, pos, dt);
    }

    void LateUpdate()
    {

        float dt = Mathf.Clamp(Time.deltaTime, 0, 0.1f);

        //		DragVel += keyDrag;
        if (camControll != null)
        {
            if (camControll._curLookAt != null)
            {
                // Apply Rig Movement
                if (DragVel.magnitude > 0.1f)
                {
                    float dragParam = _isInDrag ? DragSpeed : DragHardness * dt;
                    if (_isInDrag)
                        dragParam = Mathf.Clamp(dragParam * dt, 0, 0.5f);
                    if (camControll._curLookAt.transform.position != camControll._curLookAt.transform.position + DragVel)
                    {
                        //camControll._curLookAt.transform.position = Vector3.Lerp(camControll._curLookAt.transform.position, _lookatBeginPos + DragVel, lerpT);
                        Vector3 lookPos = Vector3.Lerp(camControll._curLookAt.transform.position, camControll._curLookAt.transform.position + DragVel, dragParam);
                        DragVel -= (lookPos - camControll._curLookAt.transform.position);
                        camControll.setCamLookAt(lookPos);
                        //camControll._curLookAt.transform.position = _lookatBeginPos + DragVel;
                        string curPS = camControll._curLookAt.transform.position.ToString();
                        string dragPS = (_lookatBeginPos + DragVel).ToString();
                        string lerpVal = dragParam.ToString();
                        //FpDebug.Log("DragVel"+ DragVel.ToString()+ ": CurPos" + curPS +" DestPos" + dragPS +" lerp by value = " + lerpVal);
                    }
                    else
                    {
                        if (!_isInDrag)
                        {
                            _lookatBeginPos = camControll._curLookAt.transform.position;
                            DragVel = Vector3.zero;
                        }
                    }
                    checkDragRange(camControll._curLookAt.transform);
                }
            }
        }

        if (yawImage != null && camControll != null && yawImage.gameObject.activeSelf != showYawImage)
        {
            yawImage.gameObject.SetActive(showYawImage);
            yawImage.rectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, camControll.getBindYaw()));
            float xscale = Mathf.Clamp(CurZoomRatio, 0.1f, 1f);
            yawImage.rectTransform.localScale = new Vector3(xscale, 1, 1);
            float yPos = (camControll._bindPitch / PitchRange.max) * Screen.height / 2 - Screen.height / 2;
            yawImage.rectTransform.localPosition = new Vector3(0, yPos, 0);
        }

        if (upAim)
        {
            if (CurZoomRatio < AimZoomRatio)
            {
                CurZoomRatio += 1.0f - 1.0f / Mathf.Exp(ForceHardness * dt);
                float zoomValue = ZoomRange.Evaluate(ZoomRatioToDist.Evaluate(CurZoomRatio));
                if (CurZoomRatio > AimZoomRatio)
                    CurZoomRatio = AimZoomRatio;
                camControll.setCamZoom(zoomValue);
            }
        }
        else
        {            
            if (CurZoomRatio > DefZoomRatio)
            {
                CurZoomRatio -= 1.0f - 1.0f / Mathf.Exp(ForceHardness * dt);
                float zoomValue = ZoomRange.Evaluate(ZoomRatioToDist.Evaluate(CurZoomRatio));
                if (CurZoomRatio < DefZoomRatio)
                {
                    CurZoomRatio = DefZoomRatio;
                }
                camControll.setCamZoom(zoomValue);
            }
        }
    }

    public void checkDragRange(Transform t)
    {
        Vector3 curPos = t.position;
        if (curPos.x < XRange.min)
            curPos = new Vector3(XRange.min, curPos.y, curPos.z);
        if (curPos.x > XRange.max)
            curPos = new Vector3(XRange.max, curPos.y, curPos.z);
        if (curPos.z < ZRange.min)
            curPos = new Vector3(curPos.x, curPos.y, ZRange.min);
        if (curPos.z > ZRange.max)
            curPos = new Vector3(curPos.x, curPos.y, ZRange.max);
        t.position = curPos;
    }


    public Vector3 WorldToScreenHalfWPoint(Vector3 pos)
    {
        Vector3 result = myCamera.WorldToScreenPoint(pos);
        result.x = result.x * GameApp.inst.hScale + GameApp.inst.halfWOffset;
        result.y = result.y * GameApp.inst.hScale;// + GameApp.inst.hOffset;
        return result;
    }
    public Vector3 WorldToScreenPoint(Vector3 pos)
    {
        Vector3 result = myCamera.WorldToScreenPoint(pos);
        result.x = result.x * GameApp.inst.hScale;
        result.y = result.y * GameApp.inst.hScale;// + GameApp.inst.hOffset;
        return result;
    }

    //all UI Evt Position Need Scale to new Position
    public Vector2 ScaleScreenEvnPoint(Vector2 position)
    {
        Vector2 evtScreenPos = new Vector2(position.x * GameApp.inst.wScale + GameApp.inst.halfWOffset, position.y * GameApp.inst.hScale);
        return evtScreenPos;
    }
    public Vector3 ScaleScreenEvnPoint(Vector3 position)
    {
        Vector3 evtScreenPos = new Vector3(position.x * GameApp.inst.wScale, position.y * GameApp.inst.hScale, position.z);
        return evtScreenPos;
    }

    public Vector3 ScreenPointToWorld(Vector2 pos)
    {
        return myCamera.ScreenToWorldPoint(pos);
    }

    public GameObject ScreenTracRayObject(Vector2 ScreenPos, LayerMask layer)
    {
        GameObject hitObj = null;
        RaycastHit hit;
        Ray ray = myCamera.ScreenPointToRay(ScreenPos);
        if (Physics.Raycast(ray, out hit, float.MaxValue, layer))
        {
            hitObj = hit.collider.gameObject;
        }
        return hitObj;
    }
    public bool ScreenTracRayGround(Vector2 ScreenPos, ref Vector3 outPos)
    {
        return false;
    }

    public void addYaw(float yawAngle)
    {
        camControll.addBindYaw(yawAngle);
    }

    public void addPitch(float pitchAngel)
    {
        camControll.addBindPitch(pitchAngel, PitchRange);
    }

    public void addZoom(float zoomVal)
    {
        CurZoomRatio += zoomVal * 2 / GameApp.width;
        if (CurZoomRatio > ZoomRatioToDist.keys[ZoomRatioToDist.length - 1].time)
        {
            CurZoomRatio = ZoomRatioToDist.keys[ZoomRatioToDist.length - 1].time;
        }

        if (CurZoomRatio < ZoomRatioToDist.keys[0].time)
            CurZoomRatio = ZoomRatioToDist.keys[0].time;

        float zoomValue = ZoomRange.Evaluate(ZoomRatioToDist.Evaluate(CurZoomRatio));
        camControll.setCamZoom(zoomValue);
    }


    public void OnLookAddComplete()
    {
        isInLerpCam = false;
    }

    public void Log(string s)
    {
        //if (GameApp.inst.showLog)
        //    FpDebug.Log("[MapCamera]: " + s);
    }
    
    void LerpUpdate(float value)
    {
        if (CurTransDelta != null)
        {
            camControll.setCamLookAt(TempLerpLookAt);
            TempLerpLookAt.transform.position = CurTransDelta.startLookAt + CurTransDelta.lerpPos * value;
            TempLerpLookAt.transform.rotation = Quaternion.Euler(CurTransDelta.startEuler + CurTransDelta.lerpEuler * value);
            CurZoomRatio = CurTransDelta.startZoom + CurTransDelta.lerpZoom * value;
            Range curlerpZoomRang = new Range(CurTransDelta.startZoomRange.min + lerpZoomRange.min * value, CurTransDelta.startZoomRange.max + lerpZoomRange.max * value);
            float zoomValue = curlerpZoomRang.Evaluate(ZoomRatioToDist.Evaluate(CurZoomRatio));
            camControll.setCamZoom(zoomValue);
            float curPitch = CurTransDelta.startPitch + CurTransDelta.lerpPitch * value;
            camControll.setBindPitch(curPitch);
        }
    }

    public bool upAim = false;
    public bool downAim = false;
    public void ShiftUpAim()
    {
        upAim = true;
    }
    public void ShiftDownAim()
    {
        upAim = false;
    }


    void SelectLerpUpdate(float value)
    {
    }

    void SelectLerpComplete()
    {
        isInLerpLockCam = false;
        isInLerpCam = false;
        CurTransDelta = null;
    }

    void InitDefCam()
    {
        if (camControll != null && DefLookAt != null)
        {
            camControll.setCamLookAt(DefLookAt);
            float zoomValue = ZoomRange.Evaluate(ZoomRatioToDist.Evaluate(CurZoomRatio));
            camControll.setCamZoom(zoomValue);
            isInTourCam = false;
            isInDriveCam = false;
        }
    }

    public float ValideDegree(float degree)
    {
        if (degree > 180)
        {
            degree = degree - 360;
        }
        else if (degree < -180)
        {
            degree = degree + 360;
        }
        return degree;
    }

    public bool checkPosInCamRange(Vector3 pos)
    {
        if (!XRange.checkInclusive(pos.x))
            return false;
        if (!ZRange.checkInclusive(pos.z))
            return false;
        return true;
    }

    public void OnDrawGizmosSelected()
    {
    }

    public void SetCamEnable(bool _enable)
    {
        myCamera.gameObject.SetActive(_enable);
    }
}
