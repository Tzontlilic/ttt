﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameApp : MonoBehaviour
{
    public static GameApp inst = null;
    private int lastScreenWidth = 0;
    private int lastScreenHeight = 0;
    private float _mrate = 1.77866f;
    public string loginAcc = "";
    public bool showLuaLog = true;
    public static int _width = 1334;
    public static bool useScreen = false;
    [HideInInspector]
    public bool screenFlag = false;
    public static int width
    {
        get
        {
            if(!useScreen)
                return _width;
            else
                return Screen.width;
        }
    }
    public static int _height = 750;
    public static int height
    {
        get
        {
            if (!useScreen)
                return _height;
            else
                return Screen.height;
        }
    }
    private float widthScale = 1;
    public float wScale
    {
        get
        {
            if (!useScreen)
                return widthScale;
            else
                return 1;
        }
    }
    private float heightScale = 1;
    public float hScale
    {
        get
        {
            if (!useScreen)
                return heightScale;
            else
                return 1;
        }
    }
	private float halfWidthOffset = 0;
	public float halfWOffset
	{
		get
		{
			//return 1;
			return halfWidthOffset;
		}
    }

    private float fullWidthOffset = 0;
    public float fullWOffset
    {
        get
        {
            //return 1;
            return fullWidthOffset;
        }
    }
    private float heightOffset = 0;
	public float hOffset
	{
		get
		{
			//return 1;
			return heightOffset;
		}
	}

    public string _AccountName = "";

    [HideInInspector]
    public bool needConfirmLogin = false;

    public event Action OnFocus = null;
    public event Action OnLoseFocus = null;
    private float leaveFocusTime = 0;
    public float quitTime = 3;
    public bool hasSendLogin = false;
    public bool isLogin = false;
    private bool isOnFocus = false;
    private string curLoginType = "";
    private bool hasSendQuitMsg = false;
    

    //Controll Screen Light
    public bool _hasEnterGame = false;
    public int _defaultScreenBrightness = 0;
    public int _curScreenBrightness = 0;

    void Start()
    {
        inst = this;
        refreshResolution();
    }

    public static void Init()
    {

    }

    void refreshResolution()
    {
        lastScreenWidth = Screen.width;
        lastScreenHeight = Screen.height;

		// 场景到屏幕的转化公式，完全以高度进行等比例放缩，宽度以中心向两边延展的方式，得到该公式（仅用于此种放缩方式）————wenyang.fang
		widthScale = Convert.ToSingle(_height) / Convert.ToSingle(Screen.height);
		heightScale = Convert.ToSingle(_height) / Convert.ToSingle(Screen.height);
        halfWidthOffset = -heightScale * Convert.ToSingle(Screen.width)/2 + Convert.ToSingle(_width)/2;
        fullWidthOffset = -halfWidthOffset * 2.0f;        
        heightOffset = 0;
    }    

    private void OnDestroy()
    {
        //SkillAction.clearAction();
        //TargetFilter.clearFilter();
    }    

    // Update is called once per frame
    void LateUpdate()
    {
        if (lastScreenWidth != Screen.width || lastScreenHeight != Screen.height)
        {
            refreshResolution();
        }
    }

    public void LoginGame(InputField input)
    {
    }

    void OnApplicationPause(bool isPause)
    {
    }

    void OnApplicationFocus(bool focusStatus)
    {
    }

    public void RestartGame()
    {
    }

    public void OnLogoutOK()
    {
    }


    public bool isInCityView = false;
    
}
