﻿using System.Collections.Generic;
using Tnet;
using UnityEngine;

public class LockStepActionMgr
{
    public LockStepActionInfo m_currStepActionInfo;
    public PendingActionInfo m_currPendingAcitonInfo;

    private List<LockStepActionInfo> m_allStepActions = new List<LockStepActionInfo>();
    private List<LockStepActionInfo> m_allPendingActions = new List<LockStepActionInfo>();

    public LockStepActionMgr()
    {
    }

    public void start()
    {
        m_currStepActionInfo = null;
        m_currPendingAcitonInfo = null;
        m_allStepActions.Clear();
        m_allPendingActions.Clear();
    }

    public void AddLockStepAction(LockStepActionInfo _lsai)
    {
        m_allStepActions.Add(_lsai);
    }

    public void AddPendingStepAction(LockStepActionInfo _lsai)
    {
        m_allPendingActions.Add(_lsai);
    }

    public void GameFrameTurn()
    {
        m_currStepActionInfo.ProcessActions();
        m_allStepActions.Remove(m_currStepActionInfo);
        m_currStepActionInfo = null;
    }

    public bool ReadyForNextStep()
    {
        if(m_currStepActionInfo == null)
        {
            if( m_allStepActions.Count > 0 )
            {
                m_currStepActionInfo = m_allStepActions[0];
                //FpDebug.Log(" m_currStepActionInfo.m_iStepID = " + m_currStepActionInfo.m_iStepID);
            }
        }
        if (m_currStepActionInfo != null)
            return m_currStepActionInfo.ReadyForNextStep();
        else
            return false;
    }

    public int getStepActionNum()
    {
        return m_allStepActions.Count;
    }
}

public class PendingActionInfo
{
    public int m_iStepID;
    public bool m_bHasPending = false;
    public List<string> m_pendingActions = new List<string>();
    PendingActionInfo m_next;

    public PendingActionInfo(int _stepID)
    {
        m_iStepID = _stepID;
        m_bHasPending = false;
        m_next = null;
    }

    public void addPendingAction(string _actionStr)
    {
        m_pendingActions.Add(_actionStr);
    }
    
    public bool ReadyForNextStep()
    {
        bool flag = m_iStepID == LockStepMgr.g_iStepCount;
        return m_bHasPending && flag;
    }

    public void SendPendingAction()
    {
        m_bHasPending = true;
    }
}

public class LockStepActionInfo
{
    public int m_iStepID;
    /// <summary>
    /// int used for playerID in network
    /// string used for action 
    /// </summary>
    public List<G_StepOpt> m_lockStepActions = new List<G_StepOpt>();
    /// <summary>
    /// if next != null can run stepActions for each
    /// </summary>
    LockStepActionInfo m_next;

    public bool runOver = false;

    public LockStepActionInfo(int _stepID)
    {
        m_iStepID = _stepID;
        m_next = null;
        runOver = false;
    }

    public bool ReadyForNextStep()
    {
        if (m_iStepID == LockStepMgr.g_iStepCount)
            return true;
        else
            Debug.LogWarning(string.Format("StepCount Error! C_Step Id = {0}, S_Step_Id={1}", LockStepMgr.g_iStepCount.ToString(), m_iStepID.ToString()));
        return false;
    }

    public void addStepAction(G_StepOpt _stepOpt)
    {
        m_lockStepActions.Add(_stepOpt);
    }

    public bool addStepActionInfo(int _stepID, LockStepActionInfo _newStepAcionts)
    {
        while (m_iStepID != _stepID)
        {
            if (m_next == null)
                return false;
            LockStepActionInfo nextInfo = m_next;
            if (nextInfo.m_iStepID == _stepID && nextInfo.m_next == null)
            {
                nextInfo.m_next = _newStepAcionts;
                return true;
            }
        }
        return false;
    }

    public void ProcessActions()
    {
        for (int i =0; i < m_lockStepActions.Count; i++)
        {
            IStepUnit player = LockStepMgr.findUnit( m_lockStepActions[i].playerId );
            if (player != null)
            {
                int actionId = m_lockStepActions[i].actionId;
                StepAction action = StepAction.findAction(actionId);
                if (action.parseParam(m_lockStepActions[i]) == false)
                {
                    return;
                }
                action.Action(player);
            }
        }
        m_lockStepActions.Clear();
        runOver = true;
    }

    public struct LockActionInfo
    {
        public int playerID;
        public string actionStr;

        public LockActionInfo(int _playerID, string _actionStr)
        {
            playerID = _playerID;
            actionStr = _actionStr;
        }
    }
}

