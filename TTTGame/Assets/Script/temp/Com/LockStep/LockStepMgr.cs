﻿using System.Collections.Generic;
using Tnet;
using UnityEngine;
using VGame;

//Use Edit -> project settings -> Time -> Fixed Timestep rate = 0.02
public class LockStepMgr : FpSingleton<LockStepMgr>
{
    public static int g_iStepCount = 0;
    public static bool g_bStart = false;

    private float m_fCurStepTime = 0;
    private float m_fFixedCacheTime = 0;


    private float m_fAccumilateTime = 0;

    private int m_fFrameLength = 20;

    public static Dictionary<int, IStepUnit> m_LockStepUnits = new Dictionary<int, IStepUnit>();

    [Range(10, 100)]
    public float m_fGameFramePerSecond = 20;

    public LockStepActionMgr lsam = new LockStepActionMgr();

    void Start()
    {
        _instance = this;
        m_fFixedCacheTime = Time.realtimeSinceStartup;
        g_bStart = false;
    }

    void FixedUpdate()
    {
        if (!ClientGame.isCurState(VGame.EGameState.VGS_GamePlay))
            return;

        if (!g_bStart)
        {
            foreach (IStepUnit stepUnit in m_LockStepUnits.Values)
            {
                stepUnit.StepUpdate(20);
            }
            return;
        }
        m_fAccumilateTime += Time.deltaTime * 1000;

        while(m_fAccumilateTime > m_fFrameLength)
        {
            GameFrameTurn();
            m_fAccumilateTime = m_fAccumilateTime - m_fFrameLength;
        }
    }

    //void FixedUpdate()
    //{
    //    float dt = Time.realtimeSinceStartup - m_fFixedCacheTime;
    //    //Debug.Log("LockStepMgr Fixed DeltaTime = " + dt.ToString());
    //    m_fFixedCacheTime = Time.realtimeSinceStartup;
    //    m_fAccumilateTime += dt;

    //}       
    
    void GameFrameTurn()
    {
        if (lsam.ReadyForNextStep())
        {
            lsam.GameFrameTurn();
            foreach (IStepUnit stepUnit in m_LockStepUnits.Values)
            {
                stepUnit.StepUpdate(m_fFrameLength);
            }
			App.Game.bulletMgr.Update (m_fFrameLength / 1000.0f);
            g_iStepCount++;
        }
    } 

    public void BeginLockStep()
    {
        m_fAccumilateTime = 0;
        g_iStepCount = 0;
        g_bStart = true;
        lsam.start();
        g_iStepCount++;
        FpDebug.Log("<<------------- BeginLockStep ----------------->>");
    }

    public void Reset()
    {
        m_fAccumilateTime = 0;
        g_iStepCount = 0;
        g_bStart = false;
        g_iStepCount = 0;
    }

    public static IStepUnit findUnit(int _playerId)
    {
        if (m_LockStepUnits.ContainsKey(_playerId))
        {
            return m_LockStepUnits[_playerId];
        }
        return null;
    }

    public static void AddStepUnit(IStepUnit isu)
    {
        if(m_LockStepUnits.ContainsKey(isu.getID()))
        {
            FpDebug.Log(isu.ToString() + " already In StepUnits.");
            return;
        }
        m_LockStepUnits.Add(isu.getID(), isu);
    }

    public static void RemoveStepUnit(IStepUnit isu)
    {
        if (m_LockStepUnits.ContainsKey(isu.getID()))
        {
            m_LockStepUnits.Remove(isu.getID());
            return;
        }
        FpDebug.Log(isu.ToString() + " Doesn't Exist In StepUnits.");
    }

#region Step Synchronize
    public void ReceiveAction(int _stepCount, G_StepOpt[] _lockActions)
    {
        LockStepActionInfo lsai = new LockStepActionInfo(_stepCount);
        lsai.m_iStepID = _stepCount;
        for ( int i =0; i < _lockActions.Length; i++)
        {
            lsai.addStepAction(_lockActions[i]);
        }
        lsam.AddLockStepAction(lsai);
    }

    public void AddPendingAction(int _stepCount, G_StepOpt[] _lockActions)
    {
        LockStepActionInfo lsai = new LockStepActionInfo(_stepCount);
        lsai.m_iStepID = g_iStepCount;
        for (int i = 0; i < _lockActions.Length; i++)
        {
            lsai.addStepAction(_lockActions[i]);
        }
        lsam.AddPendingStepAction(lsai);
    }
    #endregion
}