﻿
public interface IStepUnit
{
    void Init();

    int getID();

    bool ReadyForNextStep();

    void StepUpdate(int deltaTime);

    void Release();

    void AddPendingAction(string _act);

    string ToString();
}
