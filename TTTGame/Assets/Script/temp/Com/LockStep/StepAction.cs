﻿
using System;
using System.Collections.Generic;
using Tnet;
using UnityEngine;

public class StepAction
{
    static bool m_bInit = false;
    public string m_sName = string.Empty;
    public int m_Id = -1;
    public static List<StepAction> m_Actions = new List<StepAction>();

    public StepAction()
    {

    }
    public StepAction(string name) {
        m_sName = name;
        m_Id = m_Actions.Count;
    }

    public virtual bool parseParam(G_StepOpt _opt) { return true; }


    public static void AddStepAcion(StepAction sa)
    {
        if (findAction(sa.m_sName) == null)
        {
            m_Actions.Add(sa);
        }
    }

    public static StepAction findAction(string name)
    {
        for (int i = 0; i < m_Actions.Count; i++)
        {
            if (m_Actions[i].m_sName == name)
            {
                return m_Actions[i];
            }
        }
        return null;
    }

    public static StepAction findAction(int _id)
    {
        if (_id < m_Actions.Count)
            return m_Actions[_id];
        return null;
    }

    public virtual void Action(IStepUnit _stepUnit)
    {

    }

    public static void LoadActions()
    {
        new AxisMove("AxisMove");
		new NormalFire("NormalFire");
		new LockTarget("LockTarget");
        new WalkRun("WalkRun");
        new DirectShoot("DirectShoot");
    }
}

public class AxisMove : StepAction
{
    public AxisMove(string name)
    {
        m_sName = name;
        m_Id = m_Actions.Count;
        AddStepAcion(this);
    }

    public struct Param
    {
        public float moveX_;
        public float moveZ_;
    };

    public Param p;
    public override bool parseParam(G_StepOpt _opt)
    {
        p = new Param();
        p.moveX_ = _opt.fp[0];
        p.moveZ_ = _opt.fp[1];
        return true;
    }

    public override void Action(IStepUnit _stepUnit)
    {
        PlayerController player = _stepUnit as PlayerController;
        if (player != null)
        {
            player.SetMoveDirection(new Vector3(p.moveX_, 0, p.moveZ_));
        }
    }
}

public class DirectShoot : StepAction
{
    public DirectShoot(string name)
    {
        m_sName = name;
        m_Id = m_Actions.Count;
        AddStepAcion(this);
    }

    public struct Param
    {
        public float moveX_;
        public float moveZ_;
    };

    public Param p;
    public override bool parseParam(G_StepOpt _opt)
    {
        p = new Param();
        p.moveX_ = _opt.fp[0];
        p.moveZ_ = _opt.fp[1];
        return true;
    }

    public override void Action(IStepUnit _stepUnit)
    {
        PlayerController player = _stepUnit as PlayerController;
        if (player != null)
        {
            player.SetShootDir(new Vector3(p.moveX_, 0, p.moveZ_));
        }
    }
}

public class NormalFire : StepAction
{
    public NormalFire(string name)
    {
        m_sName = name;
        m_Id = m_Actions.Count;
        AddStepAcion(this);
    }    
    public override bool parseParam(G_StepOpt _opt)
    {
        return true;
    }

    public override void Action(IStepUnit _stepUnit)
    {
        PlayerController player = _stepUnit as PlayerController;
        if (player != null)
        {
            player.PlayAttack();
        }
    }
}

public class LockTarget : StepAction
{
	public LockTarget(string name)
	{
		m_sName = name;
		m_Id = m_Actions.Count;
		AddStepAcion(this);
	}    

	public struct Param
	{
		public bool InLock_;
	};

	public Param p;
	public override bool parseParam(G_StepOpt _opt)
	{
		p = new Param ();
		float loclVal = _opt.fp[0];
		if (loclVal < 1)
			p.InLock_ = false;
		else
			p.InLock_ = true;
		return true;
	}

	public override void Action(IStepUnit _stepUnit)
	{
		PlayerController player = _stepUnit as PlayerController;
		if (player != null)
		{
			player.LockAim(p.InLock_);
		}
	}
}

public class WalkRun : StepAction
{
    public WalkRun(string name)
    {
        m_sName = name;
        m_Id = m_Actions.Count;
        AddStepAcion(this);
    }

    public struct Param
    {
        public bool InRun_;
    };

    public Param p;
    public override bool parseParam(G_StepOpt _opt)
    {
        p = new Param();
        float loclVal = _opt.fp[0];
        if (loclVal < 1)
            p.InRun_ = false;
        else
            p.InRun_ = true;
        return true;
    }

    public override void Action(IStepUnit _stepUnit)
    {
        PlayerController player = _stepUnit as PlayerController;
        if (player != null)
        {
            player.SetRunState(p.InRun_);
        }
    }
}