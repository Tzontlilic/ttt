﻿using UnityEngine;
using System.Collections;
using System;
using VGame;

public class LocalPrefs
{
    public static string GetLastAccName()
    {
        if( PlayerPrefs.HasKey("AccName"))
        {
            return PlayerPrefs.GetString("AccName");
        }
        return "";
    }

    public static void SetLastAccName(string _name)
    {
        PlayerPrefs.SetString("AccName", _name);
    }
}
