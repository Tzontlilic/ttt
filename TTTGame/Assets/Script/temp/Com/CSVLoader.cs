﻿/*
    CSVLoader Is Used For *.csv File Parse
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UnityEngine;

namespace ComRpc
{
	public class CSVLoader
	{
		public string[] column_names_;
		public List<string[]> records_;
		public bool isValid_;
		private string curFileName="";
		public CSVLoader(string fileName, char sperator, bool useU3D = false)   
		{
			isValid_ = false;
			curFileName = fileName;
			if (useU3D)
				isValid_ = load_u3d_table_file(fileName);
			else
				isValid_ = load_table_file(fileName, sperator);
		}
		
		public bool load_table_file(string fileName, char sperator)
		{
			records_ = new List<string[]>();
			StreamReader sr = File.OpenText(fileName);			
			string firstLine = sr.ReadLine();
			if (firstLine != null)
				column_names_ = firstLine.Split(sperator);
			else
				return false;
			string lineStr = "";
			while ( (lineStr = sr.ReadLine()) != null )
			{
				string[] lineSplits = lineStr.Split(sperator);
				records_.Add(lineSplits);
			}
			return true;
		}
		
		public bool load_u3d_table_file(string fileName)
		{
			records_ = new List<string[]>();
            //修改为从bundle读取数据
            string fullName = "table/" + fileName;
            TextAsset binAsset = ResourcesMgr.Load(fullName) as TextAsset;
			if (binAsset == null)
				return false;
			string[] lineArray = binAsset.text.Split("\n"[0]);
			
			string firstLine = lineArray[0];
			
			column_names_ = firstLine.Split(',');
			
			string lineStr = "";
			for (int i = 1; i < lineArray.Length - 1; i++)
			{
				lineStr = lineArray[i];
				//去掉一个/n
                if( lineStr.IndexOf("\r") != -1)
				    lineStr = lineStr.Substring(0, lineStr.Length - 2);
				string[] rec = lineStr.Split(',');
				records_.Add(rec);
			}
            ResourcesMgr.UnloadAsset(fullName);
			return true;
		}
		
        public int ColunmCount
        {
            get
            {
                return column_names_.Length;
            }
            
        }
		public string getColunmName(int col)
		{
			if( col >= 0 && col < column_names_.Length )
				return column_names_[col];
			return "";
		}
		
		public int checkColunmKey( string columnname)
		{
			for (int i = 0; i < column_names_.Length; i++ )
			{
				if (columnname == column_names_[i])
					return i;
			}
			return -1;
		}
		
		public string get_item_data(int row, int col)
		{
			if (row > records_.Count) {
                FpDebug.LogWarning(curFileName+ " row("+row.ToString() +") out of records_("+records_.Count.ToString() +")!");
				return "";
			}

			if (col > column_names_.Length) {
                FpDebug.LogWarning(curFileName+ " col("+col.ToString() +") out of column_names_("+column_names_ +") Length!");
				return "";						
			}

			if (col > records_ [(int)row].Length) {
                FpDebug.LogWarning(curFileName+ " col("+col.ToString() +") out of records_[row]("+records_ [(int)row].Length.ToString() +")!");
				return "";
			}
			
			return (records_[(int)row])[col];
		}
		
		public int get_int(int row, string columnname)
		{
			int nameIndex = checkColunmKey(columnname);
			if (nameIndex == -1)
				return 0;
			string strData = get_item_data(row, nameIndex);
			if (strData == "") {	
				strData = "0";
			}
			return Convert.ToInt32(strData);
		}
		
		public float get_float(int row, string columnname)
		{
			int nameIndex = checkColunmKey(columnname);
			if (nameIndex == -1)
				return 0.0F;
			string strData = get_item_data(row, nameIndex);
			if (strData == "") {
				strData = "0";
			}
			return Convert.ToSingle(strData);
		}
		
		public bool get_bool(int row, string columnname)
		{
			int nameIndex = checkColunmKey(columnname);
			if (nameIndex == -1)
				return false;
			string strData = get_item_data(row, nameIndex);
			if (strData == "") {
				strData = "FALSE";
				//Debug.Log("strData is FALSE");
			}
			strData = strData.ToUpper ();
			if (strData != "TRUE" && strData != "FALSE") {
				Debug.Log("get_bool strData is ERROR");
			}
			return Convert.ToBoolean(strData);
		}
		
		public string get_string(int row, string columnname)
		{
			int nameIndex = checkColunmKey(columnname);
			if (nameIndex == -1)
				return "";
			return get_item_data(row, nameIndex);
		}
	}
}
