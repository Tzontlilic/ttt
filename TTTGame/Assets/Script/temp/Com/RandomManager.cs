using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RandomManager : MonoBehaviour {
	//private static RandomManager _instance;
	private static uint seed = 0;
	private static uint seedLast = 0;
	private const float kFloatFactor = 10000.0f;

	void Awake() {
		SetSeed();
	}

	//public static RandomManager Instance {
	//	get {
	//		if (_instance == null) {
	//			_instance = new RandomManager();
	//		}
	//		return _instance;
	//	}
	//}

	//System.Random random = new System.Random();
	//static Dictionary<string, System.Random> sysRandomDict = new Dictionary<string, System.Random>();

	//public static System.Random getNameRandom(string name)
	//{
	//	if (sysRandomDict.ContainsKey (name)) {
	//		return sysRandomDict[name];		
	//	}
	//	else{
	//		sysRandomDict[name] = new System.Random();
	//	}
	//	return sysRandomDict[name];
	//}

	//public static void setSeed(string name, int seedID)
	//{
	//	sysRandomDict[name] = new System.Random(seedID);
	//}

	public static void SetSeed(uint value = 0) {
		if (value == 0) {
			value = (uint)Utils.GetSeconds("1970-1-1");
		}
		seed = value;
	}

	public static uint Seed {
		get {
			return seed;
		}
		set {
			SetSeed(value);
		}
	}

	public static uint SeedLast {
		get {
			return seedLast;
		}
	}

	//public static int getRandom(string name, int min, int max)
	//{
	//	return getNameRandom(name).Next(min, max);
	//}

	public static uint Rand() {
		//Debug.Log("Rand(): " + seed);
		seedLast = seed; //仅用于方便使用后取计算前的种子
		seed = seed * 237917 + 256841;
		//Debug.Log("Rand(): after calc " + seed + ", " + seed % 65535);
		return seed % 65535;
	}

	public static int Range(int minValue, int maxValue) {
		if (maxValue <= minValue)
			return minValue;
		return minValue + (int)(Rand() % (maxValue - minValue));
	}

	public static float Range(float minValue, float maxValue) {
		if (maxValue < minValue || Utils.FloatEqual(minValue, maxValue))
			return minValue;
		if (minValue < 0.0f && maxValue < 0.0f)
			return RandNegative(minValue, maxValue);
		else
			return RandPositive(minValue, maxValue);
	}

	private static float RandPositive(float minValue, float maxValue) {
		int intMinValue = (int)minValue;
		int intMaxValue = Utils.FloatEqual(maxValue, 0.0f) ? 1 : (int)System.Math.Ceiling((double)maxValue);
		float fractionMin = minValue - intMinValue;
		float fractionMax = maxValue - (int)maxValue;
		float floatValue = 0.0f;
		int intRand = Range(intMinValue, intMaxValue);

		if (intRand == intMinValue && intRand == intMaxValue - 1) { //最大最小都是边界值
			floatValue = fractionMin + Rand(fractionMax - fractionMin);
		} else if (intRand == intMinValue) { //最小边界值
			floatValue = fractionMin + Rand(1.0f - fractionMin);
		} else if (intRand == intMaxValue - 1) { //最大边界值
			floatValue = Rand(fractionMax);
		} else { //中间值
			floatValue = Rand(1.0f);
		}

		return intRand + floatValue;
	}

	private static float RandNegative(float minValue, float maxValue) {
		int intMinValue = (int)minValue;
		int intMaxValue = (maxValue > -1.0f && maxValue < 0.0f) ? 1 : (int)(maxValue + 1.0f);
		float fractionMin = minValue - intMinValue;
		float fractionMax = maxValue - (int)maxValue;
		float floatValue = 0.0f;
		int intRand = Range(intMinValue, intMaxValue);

		if (intRand == intMinValue && intRand == intMaxValue - 1) { //最大最小都是边界值
			floatValue = fractionMin + Rand(fractionMin - fractionMax);
		} else if (intRand == intMinValue) { //最小边界值
			floatValue = -Rand(fractionMin);
		} else if (intRand == intMaxValue - 1) { //最大边界值
			floatValue = fractionMax - Rand(1.0f + fractionMax);
		} else { //中间值
			floatValue = -Rand(1.0f);
		}

		return intRand + floatValue;
	}

	private static float Rand(float maxValue) {
		int intValue = (int)(System.Math.Abs(maxValue) * kFloatFactor);
		intValue = Range(0, intValue);
		return (float)intValue / kFloatFactor;
	}
}