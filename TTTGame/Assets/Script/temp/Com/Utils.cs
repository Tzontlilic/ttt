﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ComRpc;

public class Utils {
	//private static Utils instance;
	public const float kFloatEpsinon = 0.000001f; //用于判断浮点或双精度相等
	public const double kDoubleEpsinon = 0.00000001f; //用于判断浮点或双精度相等

	//public static Utils Instance {
	//  get {
	//    if (instance == null)
	//      instance = new Utils();
	//    return instance;
	//  }
	//}

	//判断浮点相等
	public static bool FloatEqual(float left, float right) {
		float f = left - right;
		if (f > kFloatEpsinon || f < -kFloatEpsinon)
			return false;
		return true;
	}

	public static bool DoubleEqual(double left, double right) {
		double d = left - right;
		if (d > kDoubleEpsinon || d < -kDoubleEpsinon)
			return false;
		return true;
	}

	public static int[] SplitString2Int32s(string str, params char[] separator) {
		string[] args = str.Split(separator);
		int[] int32s = new int[args.Length];
		for (int i = 0; i < args.Length; i++) {
			int32s[i] = Convert.ToInt32(args[i]);
		}
		return int32s;
	}

	public static int Limit(int value, int valueMin, int valueMax) {
		if (value < valueMin)
			return valueMin;
		else if (value > valueMax)
			return valueMax;
		return value;
	}

	public static float Limit(float value, float valueMin, float valueMax) {
		if (value < valueMin)
			return valueMin;
		else if (value > valueMax)
			return valueMax;
		return value;
	}

	public static double GetSeconds(string dateTimeStr = "2015-1-1") {
		TimeSpan ts = DateTime.Now - DateTime.Parse(dateTimeStr);
		return ts.TotalSeconds;
	}

	public static double GetMilliseconds(string dateTimeStr = "2015-1-1") {
		TimeSpan ts = DateTime.Now - DateTime.Parse(dateTimeStr);
		return ts.TotalMilliseconds;
	}

	public static bool CheckOpen() {
		return true;
	}

	public static int CalcAllHeroFightCapacity()
    {
        int totalFightCapacity = 0;
        return totalFightCapacity;
    }
}