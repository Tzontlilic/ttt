using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DynamicMesh
{
	public List<Vector3> vertices = new List<Vector3>();
	public List<Vector2> uvs = new List<Vector2>();
	public List<int> triangles = new List<int>();

    public void AddLineRect(Vector3 a, Vector3 b, float width)
    {
        Vector3 a2b = (a - b).normalized;
        Vector3 behindA = a + a2b * width * 0.5f;
        Vector3 forwardB = b - a2b * width * 0.5f;
        AddLine(behindA, forwardB, width);
    }

    public void AddLine (Vector3 a, Vector3 b, float width)
	{
		Vector3 a2b = (a - b).normalized;
        Vector3 right = Mathfx.Rotate90_XZ(a2b) * width * 0.5f;
		int index0 = vertices.Count;
		vertices.Add(a - right);        //--A
        vertices.Add(a + right);        //--B
		vertices.Add(b + right);        //--C
		vertices.Add(b - right);        //--D

		uvs.Add(new Vector2(0f, 0f));
		uvs.Add(new Vector2(0f, 1f));
		uvs.Add(new Vector2(1f, 1f));
		uvs.Add(new Vector2(1f, 0f));

        //C-B-A
        triangles.Add(index0 + 2);      
		triangles.Add(index0 + 1);
		triangles.Add(index0);

        //A-D-C
		triangles.Add(index0);
		triangles.Add(index0 + 3);
		triangles.Add(index0 + 2);
	}

    public void TriangleBegin(Vector3 p0, Vector3 p1)
    {
        int index0 = vertices.Count;
        vertices.Add(p0);
        vertices.Add(p1);
        uvs.Add(new Vector2(0f, 0f));
        uvs.Add(new Vector2(0f, 1f));
        //C-B-A
        triangles.Add(index0 + 2);
        triangles.Add(index0 + 1);
        triangles.Add(index0);
    }

    public void TriangleEnd(Vector3 p2, Vector3 p3)
    {
        int index0 = vertices.Count - 2;
        vertices.Add(p2);
        vertices.Add(p3);
        uvs.Add(new Vector2(1f, 1f));
        uvs.Add(new Vector2(1f, 0f));
        //A-D-C
        triangles.Add(index0);
        triangles.Add(index0 + 3);
        triangles.Add(index0 + 2);
    }

    public void AddLineList(List<Vector3> points, float width)
    {
        if (points.Count < 2)
            return;

        //First Point
        Vector3 a = points[0];
        Vector3 b = points[1];
        Vector3 A2BDir = (a - b).normalized;
        a = a + A2BDir * width * 0.5f;
        Vector3 right = Mathfx.Rotate90_XZ(A2BDir) * width * 0.5f;
        //addFirstVectex
        TriangleBegin(a - right, a + right);
        if(points.Count == 2)
        {
            b = b - A2BDir * width * 0.5f;
            TriangleEnd(b + right, b - right);
            return;
        }



        for ( int i = 2; i < points.Count; i++)
        {
            Vector3 c = points[i];
            Vector3 B2CDir = (b - c).normalized;
            if ( i != points.Count - 1)
            {
                if (B2CDir.Equals(A2BDir))
                {
                    continue;
                }
                else
                {
                    b = b - A2BDir * width * 0.5f;
                    TriangleEnd(b + right, b - right);
                    //update right
                    right = Mathfx.Rotate90_XZ(B2CDir) * width * 0.5f;
                    c = c + B2CDir * width * 0.5f;
                    TriangleBegin(c - right, c + right);
                    A2BDir = B2CDir;
                }
            }
            else
            {
                if (B2CDir.Equals(A2BDir))
                {
                    c = c + B2CDir * width * 0.5f;
                    TriangleEnd(c + right, c - right);
                }
                else
                {
                    Vector3 endb = b - A2BDir * width * 0.5f;
                    TriangleEnd(endb + right, endb - right);
                    //update right
                    right = Mathfx.Rotate90_XZ(B2CDir) * width * 0.5f;
                    Vector3 startb = b - B2CDir * width * 0.5f;
                    TriangleBegin(startb - right, startb + right);
                    c = c - B2CDir * width * 0.5f;
                    TriangleEnd(c + right, c - right);
                }
            }
            b = c;
        }
    }

	const int segments = 8;
	public void AddLineCapped(Vector3 a, Vector3 b, float width)
	{
		Vector3 a2b = (a - b);
		Vector3 right = Mathfx.Rotate90_XZ(a2b.normalized) * width * 0.5f;

		int index0 = vertices.Count;
		vertices.Add(a - right);
		vertices.Add(a + right);
		vertices.Add(b + right);
		vertices.Add(b - right);
		
		uvs.Add(new Vector2(0f, 0f));
		uvs.Add(new Vector2(0f, 1f));
		float uvLength = a2b.magnitude / width;
		uvs.Add(new Vector2(uvLength, 1f));
		uvs.Add(new Vector2(uvLength, 0f));
		
		triangles.Add(index0 + 2);
		triangles.Add(index0 + 1);
		triangles.Add(index0);
		triangles.Add(index0);
		triangles.Add(index0 + 3);
		triangles.Add(index0 + 2);

		int indexA = vertices.Count;
		vertices.Add(a);
		uvs.Add(new Vector2(0f, 0.5f));

		for(int i = 0; i <= segments; ++i) {
			float theta = 180.0f * (float)i / (float)segments;
			Quaternion q = Quaternion.Euler(0, theta, 0);
			Vector3 offset = q * right;
			vertices.Add(a + offset);
			uvs.Add(new Vector2(0f, 1f));
			
			if(i > 0) {
				triangles.Add(indexA);
				triangles.Add(vertices.Count - 2);
				triangles.Add(vertices.Count - 1);
			}
		}

		int indexB = vertices.Count;
		vertices.Add(b);
		uvs.Add(new Vector2(0f, 0.5f));
		
		for(int i = 0; i <= segments; ++i) {
			float theta = 180.0f + 180.0f * (float)i / (float)segments;
			Quaternion q = Quaternion.Euler(0, theta, 0);
			Vector3 offset = q * right;
			vertices.Add(b + offset);
			uvs.Add(new Vector2(0f, 1f));
			
			if(i > 0) {
				triangles.Add(indexB);
				triangles.Add(vertices.Count - 2);
				triangles.Add(vertices.Count - 1);
			}
		}
	}

	public void AddAARect (Vector3 ul, Vector3 lr)
	{
		float t;
		if(lr.x < ul.x) {
			t = lr.x;
			lr.x = ul.x;
			ul.x = t;
		}
		if(lr.y < ul.y) {
			t = lr.y;
			lr.y = ul.y;
			ul.y = t;
		}

		int index0 = vertices.Count;

		vertices.Add(new Vector3(ul.x, 0, ul.z));
		vertices.Add(new Vector3(ul.x, 0, lr.z));
		vertices.Add(new Vector3(lr.x, 0, lr.z));
		vertices.Add(new Vector3(lr.x, 0, ul.z));

		uvs.Add(new Vector2(0f, 0f));
		uvs.Add(new Vector2(0f, 1f));
		uvs.Add(new Vector2(1f, 1f));
		uvs.Add(new Vector2(1f, 0f));

		triangles.Add(index0);
		triangles.Add(index0 + 1);
		triangles.Add(index0 + 2);

		triangles.Add(index0 + 2);
		triangles.Add(index0 + 3);
		triangles.Add(index0 + 0);
	}

	public Mesh CreateMesh()
	{
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
        //FpDebug.Log(mesh.vertices.ToString());
		mesh.uv = uvs.ToArray();
		mesh.triangles = triangles.ToArray();
        //FpDebug.Log(triangles.ToString());
        return mesh;
	}
}

public class FieldOfView
{
    private float radius;
    private float angleDegree;
    private int segments;

    private Mesh cacheMesh;

    private Transform refTrans = null;

    public LayerMask obstacleMask;

    Mesh viewMesh;

    Vector3 DirFromAngle(float angleInDegree, bool globalAngle = false)
    {
        if(globalAngle && refTrans != null)
        {
            angleInDegree += refTrans.eulerAngles.y * Mathf.Deg2Rad;
        }
        return new Vector3(Mathf.Sin(angleInDegree), 0, Mathf.Cos(angleInDegree));
        //return new Vector3(Mathf.Cos(angleInDegree), 0, Mathf.Sin(angleInDegree));
    }

    /// <summary>  
    /// 创建一个扇形Mesh  
    /// </summary>  
    /// <param name="radius">扇形半径</param>  
    /// <param name="angleDegree">扇形角度</param>  
    /// <param name="segments">扇形弧线分段数</param>  
    /// <pre>  
    /// 扇形半价精度（在满足半价精度范围内，被认为是同个半价）。  
    /// 比如：半价精度为1000，则：1.001和1.002不被认为是同个半径。因为放大1000倍之后不相等。  
    /// 如果半价精度设置为100，则1.001和1.002可认为是相等的。  
    /// </pre>  
    /// </param>  
    /// <returns></returns>  
    public Mesh CreateMesh(float radius, float angleDegree, int segments, Transform trans = null)
    {

        if (viewMesh == null)
        {
            viewMesh = new Mesh();
            viewMesh.name = "ViewMesh";
            viewMesh.MarkDynamic();
        }
        dirty = true;
        this.radius = radius;
        this.angleDegree = angleDegree;
        this.segments = segments;
        refTrans = trans;
        UpdateMesh();
        return viewMesh;
    }

    bool dirty = true;
    float refTransCacheY = 0;
    Vector3 refCachePos = Vector3.zero;
    bool NeedUpdate()
    {
        if (dirty)
            return true;

        if (refTransCacheY == refTrans.eulerAngles.y && refCachePos == refTrans.position)
            return false;

        return true;
    }

    float stepAngleSize = 0;
    public void UpdateMesh()
    {
        //if (!NeedUpdate())
        //    return;

        if (segments == 0)
        {
            segments = 1;
#if UNITY_EDITOR
            Debug.Log("segments must be larger than zero.");
#endif
        }

        float angle = Mathf.Deg2Rad * angleDegree;
        float halfAngle = angle / 2;
        float currAngle = -halfAngle;
        stepAngleSize = angle / segments;

        ViewCastInfo oldViewCast = new ViewCastInfo();
        List<VertexInfo> vertexPoints = new List<VertexInfo>();
        for (int i = 0; i < segments; i++)
        {
            ViewCastInfo newViewCast = ViewCast(currAngle);
            if( i > 0)
            {
                if (oldViewCast.hit != newViewCast.hit || newViewCast.hitObj != oldViewCast.hitObj)
                {
                    EdgeInfo edge = findEdge(oldViewCast, newViewCast);
                    if( edge.pointA != Vector3.zero)
                    {
                        VertexInfo edgeVertex = new VertexInfo(edge.pointA, edge.angleA);
                        vertexPoints.Add(edgeVertex);
                        Debug.DrawLine(refTrans.position,  edgeVertex.point, Color.blue);
                    }
                    if( edge.pointB != Vector3.zero)
                    {
                        VertexInfo edgeVertex = new VertexInfo(edge.pointB, edge.angleB);
                        vertexPoints.Add(edgeVertex);
                        Debug.DrawLine(refTrans.position, edgeVertex.point, Color.blue);
                    }
                }
            }
            VertexInfo newVertex = new VertexInfo(newViewCast.point, newViewCast.angle);
            vertexPoints.Add(newVertex);
            currAngle += stepAngleSize; 
            oldViewCast = newViewCast;
            Debug.DrawLine(refTrans.position, newVertex.point, Color.cyan);
        }

        //Vertices && UV Init && Indices
        int vertexCount = vertexPoints.Count + 1;

        List<Vector3> verticesList = new List<Vector3>(vertexCount);
        verticesList.Add(Vector3.zero);

        List<Vector2> uvList = new List<Vector2>(vertexCount);
        uvList.Add(Vector2.zero);

        List<int> triangleList = new List<int>((vertexCount - 2) * 3);
        //int[] triangles = new int[(vertexCount-2) * 3];

        for ( int i = 0; i < vertexCount - 1; i++)
        {
            verticesList.Add(refTrans.InverseTransformPoint(vertexPoints[i].point));

            float u = 0; float v = 0;
            if (vertexPoints[i].angle < 0)
            {
                u = Mathf.Abs(vertexPoints[i].angle / halfAngle);
                v = 1.0f;
            }
            else
            {
                u = 1.0f;
                v = Mathf.Abs(vertexPoints[i].angle / halfAngle);
            }
            uvList.Add(new Vector2(u, v));

            //if (i <= uvMid)
            //    uvs[i] = new Vector2((float)(i - 1) / (float)uvMid, 1.0f);
            //else
            //    uvs[i] = new Vector2(1.0f, (1.0f - ((float)(i - uvMid - 1) / (float)uvMid)));

            if (i < vertexCount - 2)
            {
                triangleList.Add( 0 );
                triangleList.Add(i + 1);
                triangleList.Add(i + 2);
            }
        }

        viewMesh.Clear();
        viewMesh.SetVertices(verticesList);
        viewMesh.SetUVs(0, uvList);
        viewMesh.SetTriangles(triangleList, 0);
        viewMesh.RecalculateNormals();

        dirty = false;
        refTransCacheY = refTrans.eulerAngles.y;
        refCachePos = refTrans.position;
    }

    EdgeInfo findEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;

        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for( int i =0; i < Main.inst.FOVMeshEdgeAdjust; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);
            if( newViewCast.hit == minViewCast.hit && newViewCast.hitObj == minViewCast.hitObj)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }
        return new EdgeInfo(minPoint, maxPoint, minAngle, maxAngle);
    }


    ViewCastInfo ViewCast(float angle)
    {
        Vector3 dir = DirFromAngle(angle, true).normalized;
        RaycastHit hit;

        if (Physics.Raycast(refTrans.position, dir, out hit, radius, Main.inst.FOV_CastLayer))
            return new ViewCastInfo(hit.transform.gameObject, true, hit.point, hit.distance, angle);
        else
            return new ViewCastInfo(null, false, refTrans.position + dir * radius, radius, angle);
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public GameObject hitObj;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(GameObject _hitObj, bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            hitObj = _hitObj;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;
        public float angleA;
        public float angleB;

        public EdgeInfo(Vector3 _pointA, Vector3 _pointB, float _angleA, float _angleB)
        {
            pointA = _pointA;
            pointB = _pointB;
            angleA = _angleA;
            angleB = _angleB;
        }
    }

    public struct VertexInfo
    {
        public Vector3 point;
        public float angle;

        public VertexInfo(Vector3 _point, float _angle)
        {
            point = _point;
            angle = _angle;
        }
    }
}