﻿using UnityEngine;

public class FpSingleton<T> : MonoBehaviour
{
    protected static T _instance;
    public static T instance
    {
        get { return _instance; }
    }
}

public class GNS
{
    /// <summary>
    /// Config default equipment informations.
    /// </summary>
	public const int DEFAULT_WEAPON = 0;
    public const int DEFAULT_HEAD = 2;
    public const int DEFAULT_CHEST = 0;
    public const int DEFAULT_HAND = 0;
    public const int DEFAULT_FEET = 1;

    public const int DUILayer = 8;
    public const int DUILayerMask = 1 << 8;
    public const int StaticBlockLayerMask = 1 << 9;
    public const int DestroyBlockLayerMask = 1 << 10;
    public const int EnemyBlockLayer = 11;
    public const int EnemyBlockLayerMask = 1 << 11;
    public const int PlayerLayer = 12;
    public const int PlayerLayerMask = 1 << 12;
    public const int EnemyFogLayer = 13;
    public const int EnemyFogLayerMask = 1 << 13;


    public const string WeaponPrefabPath = "Prefab/Guns/";
}