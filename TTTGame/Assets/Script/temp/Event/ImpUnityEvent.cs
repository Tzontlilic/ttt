﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class ImpUnityEvent : UnityEvent<int, Hashtable>
{

}

public class ExampleUnityEventClass : MonoBehaviour
{
    public ImpUnityEvent m_ImpEvent;

    void Start()
    {
        if (m_ImpEvent == null)
            m_ImpEvent = new ImpUnityEvent();

        m_ImpEvent.AddListener(Ping);
    }

    void Update()
    {
        if (Input.anyKeyDown && m_ImpEvent != null)
        {
            Hashtable hashArg = new Hashtable();
            hashArg.Add("arg0", 1);
            hashArg.Add("arg1", "1");
            hashArg.Add("arg2", gameObject);
            hashArg.Add("arg3", true);
            m_ImpEvent.Invoke(5, hashArg);
        }
    }

    void Ping(int i, Hashtable args)
    {
        Debug.Log("Type(" + i + " : " + args.ToString());
    }
}