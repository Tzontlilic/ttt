﻿using UnityEngine;
using System.Collections;

public class SelfCleaner : MonoBehaviour
{
	public float		fLife = 5;
	
	private const float	_DEF_MAX_LIFE = 3600f;
	
	private GameObject _obj;
	private Transform 	_trans;
	public Transform 	trans
	{
		get{ return _trans; }
	}
	
	void Awake()
	{
		_obj = gameObject;
		_trans = _obj.transform;
	}

    void Update()
    {
        fLife -= Time.deltaTime;
        if (fLife <= 0)
        {
            if (_obj != null)
            {
                    DestroyImmediate(_obj);
            }
        }
    }	
}
