﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SkillAction
{
    static bool m_bInit = false;
    public SkillAction()
    {
    }

    public SkillAction(string name) { name_ = name; }

    public virtual bool parseParam(string str) { return true; }

    //public virtual void action(CPropData target) { }
    public virtual void action(SkillConfig skill, UCharacterController caster, UCharacterController target, int assignedLevel = -1) { }

    public virtual string getStateID() { return "-1"; }
    public virtual void loadCheck(string param) { }

    public string name_;
    public static List<SkillAction> m_allActions_ = new List<SkillAction>();

    public static void addAction(SkillAction sa)
    {
        if (findAction(sa.name_) == null)
        {
            m_allActions_.Add(sa);
        }
    }

    public static SkillAction findAction(string name)
    {
        for (int i = 0; i < m_allActions_.Count; i++)
        {
            if (m_allActions_[i].name_.ToLower() == name.ToLower())
            {
                return m_allActions_[i];
            }
        }
        return null;
    }

    public static void loadActions()
    {
        if(m_bInit == false)
        {
            new NormalAttack("na");
            m_bInit = true;
        }
    }

    public static void clearAction()
    {
        m_allActions_.Clear();
    }

    public void Log(string s)
    {
    }
}