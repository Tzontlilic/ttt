﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class NormalAttack : SkillAction
{
    public NormalAttack(string name)
    {
        name_ = name;
        SkillAction.addAction(this);
    }

    public NormalAttack()
    {
    }

    public struct Param
    {
        public float diff_;     //< 技能伤害定值	
        public float per_;      //< 技能系数
    };

    public Param p;

    // 为这个action分配存放param的内存，并从一个string中分析出这个action所需要的param，将其返回
    public override bool parseParam(string str)
    {
        string[] perPar = str.Split(' ');
        p = new Param();
        p.diff_ = Convert.ToSingle(perPar[0]);
        p.per_ = Convert.ToSingle(perPar[1]);
        return true;
    }

    public override void action(SkillConfig skill, UCharacterController caster, UCharacterController target, int assignedLevel = -1)
    {
        if (target.IsDead())
            return;
        Damage dmg = GameRules.phyAttack(caster, target, p.diff_, p.per_);
        dmg.skill_ = skill;
        target.OnDamage(dmg);

        Tnet.Log.Debug(caster.Instance.name + " Attack Action On " + target.Instance.name + ".");

        //GloblaValue.trig_FE_Event(GloblaValue.FE_NORMALATTACK, caster, target);
    }
}