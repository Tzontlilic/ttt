﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TargetFilter
{
    static bool _init = false;
    public TargetFilter()
    {
    }

    public TargetFilter(string name) { name_ = name; }

    public virtual bool parseParam(string str) { return true; }

    public string name_;    // filter name.
    public static List<TargetFilter> m_allTargetFilters_ = new List<TargetFilter>();

    public static void addFilter(TargetFilter sa)
    {
        if (findFilter(sa.name_) == null)
        {
            m_allTargetFilters_.Add(sa);
        }
    }

    public static TargetFilter findFilter(string name)
    {
        for (int i = 0; i < m_allTargetFilters_.Count; i++)
        {
            if (m_allTargetFilters_[i].name_.ToLower() == name.ToLower())
            {
                return m_allTargetFilters_[i];
            }
        }
        return null;
    }
    public virtual List<UCharacterController> selectTarget(UCharacterController caster) { return null; }

    public static void loadFilter()
    {
        if (_init == false)
        {
            new SelVisionEnemy("selVisionEnemy");
            _init = true;
        }
    }

    public static void clearFilter()
    {
        m_allTargetFilters_.Clear();
    }

    public void Log(string s)
    {
    }
}

/// <summary>
/// 选择所有视野内的敌人
/// </summary>
public class SelVisionEnemy : TargetFilter
{
    public SelVisionEnemy(string name)
    {
        name_ = name;
        addFilter(this);
    }

    public SelVisionEnemy() { }

    public override bool parseParam(string str)
    {
        return true;
    }

    public override List<UCharacterController> selectTarget(UCharacterController caster)
    {
        List<UCharacterController> results = new List<UCharacterController>();

        if (caster.m_gunInfo == null)
            return results;

        PlayerController[] allTarget = GameRoom.instance.targetPlayers;
        if (allTarget == null)
            return results;
        float radius = caster.m_gunInfo.sectorAngle / 2;
        float range = caster.m_gunInfo.bulletRange;
        int count = allTarget.Length;
        for ( int i =0;i < count; i++)
        {
            PlayerController target = allTarget[i];
            
            if( caster == target )
                continue;

            if (target.IsDead())
                continue;

            if( caster.CanAimTarget(target, Main.inst.visionDistance))
                results.Add(target);
        }

        return results;
    }
}