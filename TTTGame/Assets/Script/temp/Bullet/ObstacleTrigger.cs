﻿
using UnityEngine;

public class ObstacleTrigger:MonoBehaviour
{
    public bool useRigidbody = false;
    void OnTriggerEnter(Collider collider)
    {
        FpDebug.Log("Bullet Hit : " + collider.name);
        BulletController bc = App.Game.bulletMgr.findBulletController(collider.gameObject);
        if( bc != null)
        {
            if (useRigidbody)
            {
                Rigidbody rb = this.transform.GetComponentInChildren<Rigidbody>();
                Vector3 dir = (this.transform.position - bc.orginPos).normalized;
                Vector3 expPos = (this.transform.position - dir * 2);
                rb.AddExplosionForce(bc.gunForce, expPos, bc.gunForceRange);
            }
            bc.OnHitBlock();
            bc.Release();
        }
    }
}

