﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EB_ShootType
{
    EBST_LockedTarget,
    EBST_ColliderTarget,
    EBST_None
}

public class BulletController
{
    public GameObject Instance = null;
    public float moveSpeed = 1f;
    public float shootRange = 10f;
    public Vector3 orginPos = Vector3.zero;
    Vector3 forwardVec = Vector3.zero;
    WeaponConfig m_gunInfo = null;
    BulletConfig m_bulletInfo = null;

    public Transform destTrans = null;
    public UCharacterController _target = null;

    Transform bulletTrans = null;

    UCharacterController caster = null;
    public List<UCharacterController> _other;

    EB_ShootType m_shootType = EB_ShootType.EBST_None;
    Vector3 colliderPos = Vector3.zero;
    GameObject colliderObj = null;
    int colliderLayer = -1;

    bool underDestroy = false;

    TrailRenderer BulletTrailRD = null;

    public BulletController(UCharacterController owner, int bulletId, Vector3 pos, Quaternion rot)
    {
        m_gunInfo = owner.m_gunInfo;
        caster = owner;
        m_bulletInfo = WeaponBullet.getBullet(bulletId);
        if(m_bulletInfo != null)
        {
            Object bulletObj = ResourcesMgr.Load(m_bulletInfo.res);
            if (bulletObj != null)
            {
                this.Instance = GameObject.Instantiate(bulletObj) as GameObject;
                bulletTrans = Instance.transform;
                bulletTrans.rotation = rot;
                bulletTrans.position = pos;
                orginPos = bulletTrans.position;
            }
            moveSpeed = m_bulletInfo.speed;
            shootRange = m_gunInfo.bulletRange;
        }
        //Instance.AddComponent<BulletTriger>().OnBulletHit += OnBulletHit;
        BulletTrailRD = Instance.GetComponentInChildren<TrailRenderer>();
    }

    public float gunForce
    {
        get
        {
            if (m_bulletInfo != null)
                return m_bulletInfo.phyForce;
            return 0;
        }
    }

    public float gunForceRange
    {
        get
        {
            if (m_bulletInfo != null)
                return m_bulletInfo.phyForceRange;
            return 0;
        }
    }

    public void Release()
    {
        if( Instance != null )
        {
            _target = null;
            destTrans = null;
            GameObject.Destroy(Instance);
        }
    }

    public void SetLockedTarget(UCharacterController target)
    {
        destTrans = target.getAimTrans();
        _target = target;
        m_shootType = EB_ShootType.EBST_LockedTarget;
        UpdateColliderPos();
    }

    public void InitTarget(UCharacterController[] targets)
    {
        _other = new List<UCharacterController>();
        for ( int i =0;i < targets.Length; i++)
        {
            _other.Add(targets[i]);
        }
        m_shootType = EB_ShootType.EBST_ColliderTarget;
        //无目标
        forwardVec = caster.forward;
        forwardVec.y = 0;
        UpdateColliderPos();
    }

    public void UpdateColliderPos()
    {
        //orginPos 
        Ray ray = new Ray(orginPos, forwardVec);
        RaycastHit hit;
        if (Physics.Raycast(orginPos, forwardVec, out hit, float.MaxValue, GNS.StaticBlockLayerMask))
        {
            // 如果射线与平面碰撞，打印碰撞物体信息  
            //Debug.Log("碰撞Static对象: " + hit.collider.name);
            // 在场景视图中绘制射线  
            colliderObj = hit.transform.gameObject;
            colliderPos = orginPos + hit.distance * forwardVec;
            colliderLayer = GNS.StaticBlockLayerMask;
        }
        RaycastHit hit2;
        if (Physics.Raycast(orginPos, forwardVec, out hit2, float.MaxValue, GNS.DestroyBlockLayerMask))
        {
            // 如果射线与平面碰撞，打印碰撞物体信息  
            //Debug.Log("碰撞Destroy对象: " + hit.collider.name);
            Debug.DrawLine(orginPos, orginPos + hit.distance * forwardVec, Color.green);
            if(hit2.distance < hit.distance)
            {
                colliderObj = hit2.transform.gameObject;
                colliderPos = orginPos + hit2.distance * forwardVec;
                colliderLayer = GNS.DestroyBlockLayerMask;
            }
        }
        
    }

	public virtual void Update(float _deltaTime)
    {
        if( Instance != null)
        {
            if (underDestroy)
            {
                Color color = BulletTrailRD.materials[0].GetColor("_TintColor");
				color.a -= _deltaTime;
                BulletTrailRD.materials[0].SetColor("_TintColor", color);
                return;
            }
            switch (m_shootType)
            {
                case EB_ShootType.EBST_ColliderTarget:
					bulletTrans.position += forwardVec * moveSpeed * _deltaTime;
                    if (Vector3.Distance(orginPos, bulletTrans.position) > shootRange)
                    {
                        App.Game.bulletMgr.RemoveBullet(this);
                        return;
                    }
                    else if( colliderObj != null )
                    {
                        if( Calc_Cross_Position(colliderPos) )
                        {
                            OnHitBlock();
                            App.Game.bulletMgr.RemoveBullet(this);
                            return;
                        }
                    }
                    Calc_Cross_UnitCollider();
                    break;
                case EB_ShootType.EBST_LockedTarget:
                    forwardVec = (destTrans.position - bulletTrans.position).normalized;
                    bulletTrans.forward = forwardVec;
                    Calc_Cross_UnitCollider();
                    break;
            }
        }
    }

    //adjust the bullet has crossed the position
    bool Calc_Cross_Position(Vector3 pos)
    {
        Vector3 bullet_resDir = bulletTrans.position - orginPos;
        bullet_resDir.y = 0;
        Vector3 bullet_u_dir = bulletTrans.position - pos;
        bullet_u_dir.y = 0;
        float result = Vector3.Dot(bullet_resDir.normalized, bullet_u_dir.normalized);
        /// 点乘大于0 夹角小于90 子弹飞行方向和 目标点到子弹方向同向，已穿过目标点
        if (result >= 0)
        {
            return true;
        }
        return false;
    }

    bool Calc_Cross_UnitCollider()
    {
        //orginPos 
        List<RaycastHit> allHits = new List<RaycastHit>();
        Ray ray = new Ray(orginPos, forwardVec);
        RaycastHit[] hit = Physics.RaycastAll(ray, float.MaxValue, GNS.PlayerLayerMask);
        RaycastHit[] hit2 = Physics.RaycastAll(ray, float.MaxValue, GNS.EnemyFogLayerMask);
        allHits.AddRange(hit);
        allHits.AddRange(hit2);
        int count = allHits.Count;
        int hitIndex = -1;
        float hitDist = 0;
        int hitPlayerId = -1;
        for (int i = 0; i < count; i++) 
        {
            hitPlayerId = UCharacterMgr.instance.getObjPlayerId(allHits[i].collider.gameObject);
            if (hitPlayerId == caster.playerId || hitPlayerId == -1)
                continue;
            Vector3 hitPos = allHits[i].point;
            if (Calc_Cross_Position(hitPos))
            {
                if (hitIndex == -1)
                {
                    hitIndex = i;
                    hitDist = Vector3.Distance(allHits[i].point, bulletTrans.position);
                }
                else
                {
                    float newDist = Vector3.Distance(allHits[i].point, bulletTrans.position);
                    if(hitDist > newDist)
                        hitIndex = i;
                }
            }
        }
        if( hitIndex != -1 )
        {
            PlayerController targetUnit = GameRoom.instance.getPlayer(hitPlayerId);
            if (targetUnit != null)
                SkillMgr.performActionStr(m_bulletInfo.dmgAction, caster, targetUnit);
            else
                Tnet.Log.Debug(caster.Instance.name + " bullet hit null Player at ");
            OnHitBlockPos(allHits[hitIndex].point);
            App.Game.bulletMgr.RemoveBullet(this);
            return true;
        }
        return false;
    }

    bool Calc_Cross_FightUnit(UCharacterController target)
    {
        Vector3 bullet_u_dir = bulletTrans.position - target.position;
        bullet_u_dir.y = 0;
        float result = Vector3.Dot(Instance.transform.forward, bullet_u_dir.normalized);
        /// 点乘小于零 夹角大于90 反之
        if (result >= 0)
        {
            if( PerformHit(target, Instance.transform.forward, bullet_u_dir, result) )
            {
                return true;
            }
        }
        return false;
    }

    bool PerformHit(UCharacterController _targetUnit, Vector3 bullet_t_vec, Vector3 bullet_u_vec, float dotResult)
    {
        if (m_shootType == EB_ShootType.EBST_LockedTarget)
        {
            SkillMgr.performActionStr(m_bulletInfo.dmgAction, caster, _targetUnit);
            //caster.performSkill(_skill, u.character, _bullet);
            OnBulletHit(_targetUnit);
            Main.inst.StartCoroutine(DestroyBulletDelay(BulletTrailRD.time));
            //App.Game.bulletMgr.RemoveBullet(this);
            return true;
        }
        Vector3 vec_to_target = _targetUnit.position - orginPos;
        vec_to_target.y = 0;
        float dis_b_u = Vector3.Distance(bullet_t_vec, vec_to_target);
        float corss_diss = dis_b_u * Mathf.Sqrt(1 - dotResult / (bullet_u_vec.magnitude * vec_to_target.magnitude));
        float geomWidth = 1;
        CapsuleCollider cc = _targetUnit.Instance.GetComponent<CapsuleCollider>();
        if (cc != null)
        {
            geomWidth = cc.radius;
        }
        if (corss_diss < geomWidth + m_bulletInfo.width)
        {
            SkillMgr.performActionStr(m_bulletInfo.dmgAction, caster, _targetUnit);
            //caster.performSkill(_skill, u.character, _bullet);
            OnBulletHit(_targetUnit);
            App.Game.bulletMgr.RemoveBullet(this);
            return true;
        }
        return false;
    }

    public void OnBulletHit(UCharacterController target)
    {
        if (m_bulletInfo.hitEff != string.Empty)
        {
            Object hitEffObj = ResourcesMgr.Load(m_bulletInfo.hitEff);
            if(hitEffObj != null)
            {
                GameObject hitEff = GameObject.Instantiate(hitEffObj) as GameObject;
                hitEff.transform.position = target.getAimTrans().position;
                Instance.transform.position = hitEff.transform.position;
            }
        }
    }

    public void OnHitBlock()
    {
        if (m_bulletInfo.hitEff != string.Empty)
        {
            Object hitEffObj = ResourcesMgr.Load(m_bulletInfo.hitEff);
            if (hitEffObj != null)
            {
                GameObject hitEff = GameObject.Instantiate(hitEffObj) as GameObject;
                hitEff.transform.position = (colliderObj == null) ? Instance.transform.position : colliderPos;
                if (colliderLayer == GNS.DestroyBlockLayerMask)
                {
                    Main.inst.AddExplosive(hitEff.transform.position, m_bulletInfo.phyForce, m_bulletInfo.phyForceRange);
                }
            }
        }
    }
    public void OnHitBlockPos(Vector3 hitPos, bool _needExplosive = false)
    {
        if (m_bulletInfo.hitEff != string.Empty)
        {
            Object hitEffObj = ResourcesMgr.Load(m_bulletInfo.hitEff);
            if (hitEffObj != null)
            {
                GameObject hitEff = GameObject.Instantiate(hitEffObj) as GameObject;
                hitEff.transform.position = hitPos;
                if (_needExplosive)
                {
                    Main.inst.AddExplosive(hitEff.transform.position, m_bulletInfo.phyForce, m_bulletInfo.phyForceRange);
                }
            }
        }
    }

    IEnumerator DestroyBulletDelay(float second)
    {
        Instance.GetComponentInChildren<MeshRenderer>().enabled = false;
        underDestroy = true;
        yield return new WaitForSeconds(second);
        App.Game.bulletMgr.RemoveBullet(this);
    }
}
