﻿using System.Collections.Generic;
using UnityEngine;

public class BulletMgr
{
    private List<BulletController> bulletList = new List<BulletController>();
    private Dictionary<GameObject, BulletController> bulletDict = new Dictionary<GameObject, BulletController>();

    public BulletController GenerateBullet(UCharacterController owner, int bulletId, Vector3 pos, Quaternion rot)
    {
        BulletController instance = new BulletController(owner, bulletId, pos, rot);
        bulletList.Add(instance);
        bulletDict.Add(instance.Instance, instance);

        Tnet.Log.Debug(owner.Instance.name + " Gen Bullet at " + pos.ToString() + " rot " + rot.eulerAngles.ToString());
        return instance;
    }

    public void RemoveBullet(BulletController bullet)
    {
        if( bulletList.Contains(bullet))
        {
            bulletList.Remove(bullet);
            bulletDict.Remove(bullet.Instance);
            bullet.Release();
        }
    }

    public BulletController findBulletController(GameObject bullet)
    {
        if( bulletDict.ContainsKey(bullet))
        {
            return bulletDict[bullet];
        }
        return null;
    }

    //Call At LockStepMgr.GameFrameTurn()
    public void Update(float _deltaTime = 0.02f)
    {
        for(int i =0; i < bulletList.Count; i++)
        {
			bulletList[i].Update(_deltaTime);
        }
    }
}