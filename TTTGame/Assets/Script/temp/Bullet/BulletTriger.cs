﻿
using System;
using UnityEngine;

public class BulletTriger : MonoBehaviour
{

    public delegate void OnBulletColliderHit(GameObject go);

    public OnBulletColliderHit OnBulletHit;

    void OnTriggerEnter(Collider collider)
    {
        if(OnBulletHit != null)
        {
            OnBulletHit(collider.gameObject);
        }
    }
}
