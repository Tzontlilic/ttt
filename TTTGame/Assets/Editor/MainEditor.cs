﻿using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Main))]
public class MainEditor : Editor {

    void OnSceneGUI()
    {
        Main main = (Main)target;
        if(GameRoom.instance.Player != null)
        {
            Handles.color = Color.white;
            Handles.DrawWireArc(GameRoom.instance.Player.position, Vector3.up, Vector3.forward, 360, GameRoom.instance.Player.m_gunInfo.bulletRange);
            //Vector3 viewAngleA = 
            Handles.color = Color.red;
            for (int i =0; i < GameRoom.instance.Player.visionTargets.Count; i++)
            {
                Handles.DrawLine(GameRoom.instance.Player.position, GameRoom.instance.Player.visionTargets[i].position);
            }
        }
    }
}
