﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemInfoUI : MonoBehaviour {

    [SerializeField]
    private Text StepCountTxt = null;

    [SerializeField]
    private Text ACNumberTxt = null;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (StepCountTxt != null)
            StepCountTxt.text = "StepCount: " + LockStepMgr.g_iStepCount.ToString();
        if (ACNumberTxt != null)
            ACNumberTxt.text = "ACNum: " + LockStepMgr.instance.lsam.getStepActionNum().ToString();
    }



}
